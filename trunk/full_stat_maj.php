<?php
session_start();
?>
<?php include 'misc_functions.php' ; ?>
<?php 
openconnexion();
update_full_stats_contents();
update_postit_stats_contents();

function update_full_stats_contents () {
	$rez = '';

	$nb = count_all_pending_games();
    $sub =   "<P>Il y a $nb parties en attente.";
    echo $sub;
    flush();
    $rez = $rez . $sub;
    
    $nb = count_all_played_games();
    $sub =  " $nb parties ont été créées depuis le début.";
    echo $sub;
    flush();
    $rez = $rez . $sub;
     
    $nb =  COUNT_term_comp_have_idea_relation_p();
    $sub =  "<P>$nb <img src=\"pics/JDM-bonus-dot.gif\"> ont été capturés.";
    echo $sub;
    flush();
    $rez = $rez . $sub;
     
    $nb = COUNT_term_comp_no_relation_p();
    $sub =  " Il reste encore $nb <img src=\"pics/JDM-bonus-dot.gif\"> à conquérir.";
    echo $sub;
    flush();
    $rez = $rez . $sub;
     
    $nb =  COUNT_term_comp_have_active_OUT_relation_p();
    $sub =  "<P>$nb termes ont au moins une relation utile sortante.";
    echo $sub;
    flush();
    $rez = $rez . $sub;
     
    $nb =  COUNT_term_comp_have_active_IN_relation_p();
    $sub =  " $nb termes ont au moins une relation utile entrante.";
    echo $sub;
    flush();
    $rez = $rez . $sub;
     
    $nb =  COUNT_term_comp_have_active_OUT_CAT_p();
    $sub = "<P> $nb termes sont renseignés avec un POS.";
    echo $sub;
    flush();
    $rez = $rez . $sub;
    
    $sub = "<P>Dernière mise à jour : " . date("F j, Y, g:i a");
    echo $sub;
    flush();
    $rez = $rez . $sub;
    
  	$file = fopen('jdm-full-stats.txt', "w+");
    fwrite($file,$rez);
    fclose($file);
}

function count_all_pending_games() {
   		$query = "SELECT count(id) FROM PendingGames";
    	$r =  @mysql_query($query) or die("pb count_pending_games : $query");
    	$nb = mysql_result($r , 0 , 0);
		return $nb;
	}
	
function count_all_played_games() {
   		$arr = mysql_fetch_array(mysql_query("SHOW TABLE STATUS LIKE 'PendingGames'" ));
   		$nb = $arr['Auto_increment'];
   		return $nb;
	}
	
function COUNT_term_comp_no_relation_p () {
	$query = "SELECT count(distinct(node1)) FROM Relations WHERE type=0";
	$r =  @mysql_query($query) or die("pb COUNT_term_comp_no_relation_p 1 : $query");
    $nb1 = mysql_result($r , 0 , 0);
   
    $query = "SELECT count(id) FROM Nodes WHERE type=1";
	$r =  @mysql_query($query) or die("pb COUNT_term_comp_no_relation_p 1 : $query");
    $nb2 = mysql_result($r , 0 , 0);

    return ($nb2 - $nb1);
}

function COUNT_term_comp_have_idea_relation_p () {
	$query = "SELECT count(distinct(node1)) FROM Relations WHERE type=0";
	$r =  @mysql_query($query) or die("pb COUNT_term_comp_no_relation_p 1 : $query");
    $nb1 = mysql_result($r , 0 , 0);
    return $nb1;
}

function COUNT_term_comp_have_active_OUT_relation_p () {
	$query = "SELECT count(distinct(node1)) FROM Relations
	 WHERE type in (0, 5, 8, 3, 6, 7, 9, 10, 15, 17, 11, 22, 20, 21, 34, 13, 14, 16, 32, 35);";
	$r =  @mysql_query($query) or die("pb COUNT_term_comp_have_active_relation_p 1 : $query");
    $nb1 = mysql_result($r , 0 , 0);
    return $nb1;
}

function COUNT_term_comp_have_active_IN_relation_p () {
	$query = "SELECT count(distinct(node2)) FROM Relations
	 WHERE type in (0, 5, 8, 3, 6, 7, 9, 10, 15, 17, 11, 22, 20, 21, 34, 13, 14, 16, 32, 35);";
	$r =  @mysql_query($query) or die("pb COUNT_term_comp_have_active_relation_p 1 : $query");
    $nb1 = mysql_result($r , 0 , 0);
    return $nb1;
}

function COUNT_term_comp_have_active_OUT_CAT_p () {
	$query = "SELECT count(distinct(node1)) FROM Relations
	 WHERE type = 4;";
	$r =  @mysql_query($query) or die("pb COUNT_term_comp_have_active_OUT_CAT_p 1 : $query");
    $nb1 = mysql_result($r , 0 , 0);
    return $nb1;
}

?>
