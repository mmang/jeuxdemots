		<H1>Carta de admisión al juego JeuxDeMots</H1>
		
		<p>
		JeuxDeMots es un juego php gratuito creado en el marco de un proyecto de investigación por Mathieu LAFOURCADE en el seno del LIRMM 
		y en cooperación con otras universidades. El objetivo de este proyecto es la construcción de recursos lexicales 
		cuya meta es la aplicación al Tratamiento Automático del Lenguaje Natural (TALN). Estos datos son el producto de la actividad 
		de los jugadores de JeuxDeMots.
		<P>
		Los datos lexicales son accesibles (<a href="http://www.lirmm.fr/~lafourcade/JDM-LEXICALNET-FR/">http://www.lirmm.fr/~lafourcade/JDM-LEXICALNET-FR/</a>) 
		bajo la licencia Creative Commons (<a href="http://creativecommons.org/">http://creativecommons.org/</a>).
		<P>
		Dicha carta tiene como objetivo de permitir a los jugadores jugar en el entorno más agradable posible,
		si respetan las algunas reglas establecidas a continuación.<br />
		Toda infracción o abuso será objeto de sanción que podrá alcanzar el destierro definitivo del jugador.<br />
		De manera general, se espera por parte de los jugadores que sean de buena fe con el fin de respetar la diversión de todos.<br />
		</p>

		<h3>(1) Inscripción</h3>
		<p>
		La participación en el juego es totalmente gratuita y se abre a todos los internautas que disponen de un acceso al internet regular y eventualmente de un correo electrónico personal y activo (pero este último punto no es una obligación).<br />
		Por la naturaleza del vocabulario eventualmente encontrado, JeuxDeMots puede no convenir a personas menores de 16 años.<br />
		</p>
		
		<h3>(2) Datos personales</h3>
		<p>
		Ciertas informaciones registradas en nuestra base de datos son personales (contraseña, correo electrónico, dirección IP,), otras serán eventualmente proporcionadas a otras personas (respuestas propuestas por el jugador durante una partida, la clasificación,) en el marco del juego.<br />
		
		No se procederá a ninguna difusión de los datos personales fuera de las modalidades previstas por el juego (informaciones mencionadas en la clasificación, visualización del resultado de una partida,).
		Sin embargo, todos los datos del juego son consultables por el equipo del juego sin restricción.<br />
		
		El equipo del juego se reserva el derecho de explotar con fines de investigación los datos lexicales cosechados durante una partida (una vez esta terminada).
		En ningún caso, dichos datos contienen informaciones personales que permiten identificar a sus autores.
		
		<br /><br />
		El jugador puede pedir en cualquier momento la supresión de su cuenta así que la totalidad de las informaciones relativas a su persona en la base de datos.
		<br />
		</p>
		
		<h3>(3) Entradas</h3>
		<p>
		Toda entrada considerada como una ofensa a la ética acarreará la anulación inmediata y sin previo aviso de la cuenta del jugador.<br />
		</p>
		<P>
		Ciertos términos o ciertas preguntas propuestos en JeuxDeMots pueden ser considerados como profanos, vulgares u ofensivos por algunos jugadores 
		Siendo el objetivo de JeuxDeMots constituir un diccionario que engloba el conjunto de los hechos y opiniones posibles, tales informaciones no serán a priori retiradas del juego.
		Sin embargo, se incita a los jugadores a señalar todo contenido inapropiado.
		
		<h3>(4) Venta de cuentas</h3>
		<p>
		Se prohíbe terminantemente la venta contra dinero o bienes materiales de una cuenta y/o elementos del juego.<br />
		</p>
		
		<h3>(5) Automatización</h3>
		<p>Una cuenta debe ser controlada por un jugador: 
		se prohíbe terminantemente automatizar las acciones por cualquier medio, sea cual sea (script, programa, página web,).
		</p>
		
		<h3>(6) Bug</h3>
		<p>
		JeuxDeMots puede padecer bug e incoherencias. 
		Cada jugador tiene que relatar (por correo electrónico o en el foro) lo más rápido posible, todo problema encontrado.  
		Toda utilización abusiva o voluntaria de un bug o de un fallo será sancionado y podrá resultar en la supresión de la cuenta del jugador concernido.<br />
		Compensaciones podrán intervenir por parte de los administradores, pero los administradores serán únicos jueces.<br />
		</p>
		
		<h3>(7) Informaciones en relación con los cookies de navegación</h3>
		Para el buen funcionamiento del juego, deseamos implantar un cookie en su ordenador. 
		Un cookie no nos permite identificarle; 
		sin embargo registra informaciones relativas a la navegación de su ordenador en nuestro sitio (las páginas consultadas, día y hora de consultación,) que podremos leer cuando sus visitas ulteriores.
		La duración de conservación de estas informaciones en su ordenador es de varios meses.
		<br />
		Se puede oponer el registro de cookies, sin embargo JeuxDeMots no podrá funcionar normalmente.
		 <br />
Le informamos que puede oponerse al registro de cookies. Por eso, siga las instrucciones que se detallan a continuación correspondientes a la versión de navegador que utilice.<br /><br />
<small>
Microsoft Internet Explorer 6.0 :
1. Abra el menú Herramientas y seleccione "Opciones de Internet".
2. Haga clic en la pestaña "Privacidad".
3. En "Configuración", seleccione el nivel deseado.
<br /><br />
Microsoft Internet Explorer 5 :
1. Abra el menú Herramientas y seleccione "Opciones de Internet". 
2. Haga clic en la pestaña " Seguridad".
3. Haga clic en el botón "Nivel personalizado".
4. Vaya a la sección "Cookies".
<br /><br />
Netscape 6.X et 7. X :
1. Seleccione "Preferences" (Preferencias) en el menú Edit (Editar).
2. Haga clic en la flecha que aparece junto a "Privacy & Security" (Privacidad y seguridad) en la ventana deslizante para ampliarla.
3. Cookies.
<br /><br />
Firefox :
1. Vaya al menú "Tools" (Herramientas).
2. Seleccione "Options" (Opciones).
3. Seleccione el icono "Privacy" (Privacidad) que aparece en el panel izquierdo. Aparece la rúbrica cookie.
<br />
Opéra 6.0 et au-delà :
1. Vaya al menú "Archivo">"Preferencias" ;
2. Vida Privada.
<br /><br />
</small>
		<h3>(8) Carta de admisión</h3>
		<p>
		Dicha carta tiene que ser leída y aceptada en su integralidad para poder jugar a JeuxDeMots.<br />
		Puede ser modificada por los gestionaros del juego. 
		Llegado el caso, los jugadores tendrán que aceptar explícitamente las modificaciones proporcionadas para seguir jugando.<br />

		<br />
		Mathieu Lafourcade - Administrador -
		<?php  echo "<a href=\"mailto:" . $_SESSION[ssig() . 'CONTACT-EMAIL']) . "?subject=JeuxDeMots feedback\" >" . get_msg('MISC-CONTACT') . "</a>"; ?>
		| Fuentes de la carta <a href="http://www.starshine-online.com">SSO</a> -
			<a href="http://www.mountyhall.com">MH</a> -
			<a href="http://www.cnil.fr">CNIL</a>
		<BR>
		Ultima puesta al día : diciembre 2011
		</p>
		</div>
		<br><br>
