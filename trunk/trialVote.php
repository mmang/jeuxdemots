<?php session_start();?>
<?php include_once 'misc_functions.php' ; ?>
<?php include_once 'trial_functions.php' ; ?>
<?php openconnexion(); ?>
<html>
 <head>
    <title>JeuxDeMots : <?php echo get_msg( "TRIALVOTE-TITLE" ); ?></title>
    <?php header_page_encoding(); ?>
  </head>
  <body>
<?php include 'HTML-body.html' ; ?>
<?php topblock(); ?>


<div class="jdm-level1-block">
	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
    <?php echo get_msg( "TRIALVOTE-INSTRUCTIONS" ); ?>
    </div>
	</div>

    <div class="jdm-login-block">
    <?php  loginblock(); ?>
    </div>
</div>


<div class="jdm-level2-block">
<TABLE	border="0"
	width="100%"
	cellspacing="0" cellpadding="0%"
	summary="JeuxDeMots">

<TR><TH bgcolor="#D3E5F8" align="right" width="200">
    <TH bgcolor="#EEF6F8" colspan="2" align="left">
	<div class="jdm-instruction">
	<?php if ($_POST['trial_vote'] != "") {
			echo get_msg("TRIALVOTE-INSTRWERE");
		}?>
	</div>
	
<TR><TH>
    <TH align="left">
    <?php 
    	if ($_POST['trial_vote'] != "") {
    		make_trial_vote_form($_POST['proc_id']);
    	}
    	
		if ($_POST['trial_vote_cr'] != "") {
    		make_trial_CR($_POST['proc_id']);
    	}
    	
		if ($_POST['trial_make_vote_A'] != "") {
			process_vote($_POST['proc_id'], "A");
    		echo "<h1>" . get_msg("TRIALVOTE-THANKS") . "</h1><hr>";
    	}
    	
		if ($_POST['trial_make_vote_B'] != "") {
			process_vote($_POST['proc_id'], "B");
    		echo "<h1>" . get_msg("TRIALVOTE-THANKS") . "</h1><hr>";
    	}
    	
		if ($_POST['trial_make_vote_C'] != "") {
			process_vote($_POST['proc_id'], "C");
    		echo "<h1>" . get_msg("TRIALVOTE-THANKSABS") . "</h1><hr>";
    	}
    	
    ?>		
    <TH>

<TR><TH>
    <TH align="left">
    <?php
    if ($_POST['trial_vote'] != "") {
    } else {	
		list_MY_TRIALS_form();   
    }

    ?>
    <TH>

</TABLE>
</div>

<div class="jdm-playas-block-1">
<?php produceplayasform(); ?>
</div>
<?php playerinfoblock($_SESSION[ssig() . 'playerid']) ?>
    
<?php
    bottomblock();
    closeconnexion();
?>

  </body>
</html>
