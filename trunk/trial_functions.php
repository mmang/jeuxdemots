<?php include_once 'misc_functions.php' ; ?>

<?php 
function make_trial($player1_answer, $player2_answer) {
	debugecho("IN MAKE TRIAL");
	//echo "IN MAKE TRIAL";
	// � commenter si plus debug
	//
	//if (test_playerp() !="Y") {return 0;}
	
	// a ne fait rien si guest (session tomb�e)
	//
	$playerid = $_SESSION[ssig() . 'playerid'];
	debugecho("fct make_trial -- player _id: $playerid") ;
	//echo "fct make_trial -- player _id: $playerid" ;
	if ($playerid == 0) {return;}
	 
	//debugecho("make_trial: $player1_answer");
	//print_r($player1_answer);
	//debugecho("make_trial: $player2_answer") ;
	//print_r($player2_answer);
	
	
	// game info
    $entryid = $_SESSION[ssig() . 'gameentryid'];
    $entry = $_SESSION[ssig() . 'gameentry'];	
    $gametype = $_SESSION[ssig() . 'gametype'];
    $gameid = $_SESSION[ssig() . 'gameid'];
    $gamerelationtype = $_SESSION[ssig() . 'gamerelationtype'];
    $gameentrylevel= $_SESSION[ssig() . 'gameentrylevel'];
    
    $instruction = strip_tags($_SESSION[ssig() . 'gameinstruction']);

    // creator info
    $gamecreatorid = $_SESSION[ssig() . 'gamecreatorid'];
    $gamecreatorname = get_player_name($gamecreatorid);
	$gamecreator_receive_mail_p = get_player_receive_mail_p($gamecreatorid);

    // player info
    //$playerid = $_SESSION[ssig() . 'playerid'];
    $playername = $_SESSION[ssig() . 'login'];
	
	$query = "INSERT INTO `Trials` (`instructions`, `term`, `player1_id`, `player2_id`)
			 VALUES (\"$instruction\", \"$entry\", '$gamecreatorid', '$playerid');";
	$r =  @mysql_query($query) or die("pb in make_trial : $query");
	$LASTID = mysql_insert_id();
	
	$nb = count($player1_answer);
	for ($i=0 ; $i<$nb ; $i++) {
	    $item = $player1_answer[$i];
	    $query = "INSERT INTO `TrialProposedWords` (`id`, `player_id`, `word`) 
	    		VALUES ('$LASTID', '$playerid', \"$item\");";
		$r =  @mysql_query($query) or die("make_trial : $query");
	}
	
	$nb = count($player2_answer);
	for ($i=0 ; $i<$nb ; $i++) {
	    $item = $player2_answer[$i];
	    $query = "INSERT INTO `TrialProposedWords` (`id`, `player_id`, `word`) 
	    		VALUES ('$LASTID', '$gamecreatorid', \"$item\");";
		$r =  @mysql_query($query) or die("make_trial : $query");
	}

	return $LASTID;
}

function finalize_trial($trial_id, $trial_reason="--aucun motif donn�--")  {
	$date = microtime_float();
	$query = "UPDATE `Trials` SET date = $date, reason=\"$trial_reason\" WHERE id= '$trial_id' ;";
	$r =  @mysql_query($query) or die("PB in finalize_trial: $query");
	
	$query = "SELECT player1_id, player2_id FROM Trials WHERE id=$trial_id";
	$r =  @mysql_query($query) or die("PB in finalize_trial 2: $query");
	
	$p1 = mysql_result($r , 0 , 0);
	$p2 = mysql_result($r , 0 , 1);
	
	// selection des jur�s
	$nb_jures_max = 140;
	$nb_jures = 7;
	
	$query = "SELECT id,name,email FROM Players 
				WHERE id != $p1 AND id != $p2 AND id != 0
				ORDER BY `lastlogin` DESC LIMIT $nb_jures_max";
	
	$r = randomselection($nb_jures, $query);
	$n = count($r);
	//print_r($r);
	
	// debug
	// a commenter
	//$nb_jures = 3;
	//$r[0]['id'] = 11; $r[0]['name'] = "kaput"; $r[0]['email'] = "kaput34@gmail.com";
	//$r[1]['id'] = 2; $r[1]['name'] = "mathieu"; $r[1]['email'] = "kaput34@gmail.com";
	//$r[2]['id'] = 108; $r[2]['name'] = "mat"; $r[2]['email'] = "kaput34@gmail.com";
	
	for ($i=0 ; $i<$n ; $i++) {
		$pid = $r[$i]['id'];
		$name = $r[$i]['name'];
		$email = $r[$i]['email'];
		//echo "id=$pid name=$name email=$email<br>";
			
		$query = "INSERT INTO `TrialVotes` (trial_id, player_id)
			VALUES ($trial_id, $pid);";
		$r2 =  @mysql_query($query) or die("pb in finalize_trial 3 : $query");
		// avertir les joueurs
		// par mail
		$GLOBALS[0] = $name;
		send_mail_to_player($pid, 
				get_msg('TRIALVOTE-MAIL-TITLE'), 
				get_msg('TRIALVOTE-MAIL-BODY') );
	}
}

function list_MY_TRIALS_form() {
    $playerid = $_SESSION[ssig() . 'playerid'];
    $query = "SELECT trial_id FROM TrialVotes WHERE vote='' and player_id = $playerid";
    $r =  @mysql_query($query) or die("pb list_MY_TRIALS_form : $query");

    $nb= mysql_num_rows($r);

    $now = round(microtime_float());
    //echo "Now : $now" ;
    // duration in sec
    $proc_duration = 7 * 24 * 3600;
    
    if ($nb>0) {
	echo "<P>" . get_msg("TRIALVOTE-PENDING-TRIAL");
	echo "<BR>";} else {
	echo get_msg("TRIALVOTE-NO-TRIAL");
	}
	
    echo "<ul>";
    for ($i=0 ; $i<mysql_num_rows($r) ; $i++) {
      $procid = mysql_result($r , $i , 0);
	
      $query = "SELECT date FROM Trials WHERE id = $procid";
	  $r2 =  @mysql_query($query) or die("PB 2 in list_MY_TRIALS_form: $query");
	  $proc_date = mysql_result($r2 , 0 , 0);
	  //echo "procdate : $proc_date" ;
	  
	  $time_left = round(($proc_duration - ($now - $proc_date))/3600, 2);
	  //echo " timeleft : " . $time_left . " "  . ($time_left / (3600)) ;
	    
	  $GLOBALS[0]=$i;
	  $GLOBALS[1]=$procid;
	  $GLOBALS[2]=$time_left;
	  echo get_msg('SOUK-LIST-TRIAL-TODO-FORM');
	  
     // echo "<li><form id=\"open_trial$i\" name=\"open_trial$i\" method=\"post\" action=\"trialVote.php\" >
	//	<input id=\"trial_vote\" type=\"submit\" name=\"trial_vote\" value=\"Voter\">
		//<input  id=\"proc_id\" type=\"hidden\" name=\"proc_id\" value=\"$procid\">
	//	 - �ch�ance dans $time_left heures.
	//	</form></li>";

      }
    echo "</ul>";
    }

    
function list_concluded_TRIALS_form() {
    $playerid = $_SESSION[ssig() . 'playerid'];
    if ($playerid == 0) {
    	$tag = "disabled";
    } else {
    	$tag = "";
    }
    
    $query = "SELECT id, date, player1_id,player2_id,term FROM Trials 
    WHERE p1_score > 0 OR p2_score > 0
    ORDER BY date_term DESC LIMIT 50";
    $r =  @mysql_query($query) or die("pb list_concluded_TRIALS_form : $query");

    $nb= mysql_num_rows($r);

    $now = round(microtime_float());
    //echo "Now : $now" ;
    // duration in sec
    $proc_duration = 7 * 24 * 3600;
    
    if ($nb>0) {
		echo "<P>" . get_msg( "TRIALVOTE-50-LAST");
    	if ($playerid == 0) {
    		echo get_msg("TRIALVOTE-NO-GUEST");
   		}
		echo "<BR>";
    } else {
		echo get_msg("TRIALVOTE-NO-TRIAL");
	}
	
    echo "<ul>";
    for ($i=0 ; $i<mysql_num_rows($r) ; $i++) {
      $procid = mysql_result($r , $i , 0);
	  $proc_date = mysql_result($r , $i , 1);
	  $p1 = mysql_result($r , $i , 2);
	  $p2 = mysql_result($r , $i , 3);
	  $terme = mysql_result($r , $i , 4);
	    
	  $p1name = get_player_name($p1);
	  $p2name = get_player_name($p2);
	  $p1name = "----";
	  
	  $time_left = round(($proc_duration - ($now - $proc_date))/3600, 2);
	  //echo " timeleft : " . $time_left . " "  . ($time_left / (3600)) ;
	  if ($proc_date > 0) {
	 	 if (($time_left < 0) OR trial_completed_p($procid) ) {
	 
	 	 	$GLOBALS[0]=$i;
			$GLOBALS[1]=$tag;
			$GLOBALS[2]=$procid;
			$GLOBALS[4]=$p1name;
			$GLOBALS[3]=$p2name;
			$GLOBALS[5]=format_entry($terme);
	 	 	
			echo get_msg('SOUK-LIST-TRIAL-COMPLETED-FORM');
			
      		//echo "<li><form id=\"open_cr_trial$i\" name=\"open_trial$i\" method=\"post\" action=\"trialVote.php\" >
			//	<input $tag id=\"trial_vote_cr\" type=\"submit\" name=\"trial_vote_cr\" value=\"Visualiser\">
			//	<input  id=\"proc_id\" type=\"hidden\" name=\"proc_id\" value=\"$procid\">
		 	//	le proc�s $procid opposant '$p1name' � '$p2name' pour le terme '$terme'.
			//	</form></li>";
    		}
      	}
    }
    echo "</ul>";
    }
    
    
function make_trial_CR($trial_id) {
	 $playerid = $_SESSION[ssig() . 'playerid'];
	 if ($playerid == 0) {echo "ERROR: playerid $playerid = 0" ; return;}
	 if ($trial_id == 0) {echo "ERROR: trial_id $trial_id = 0" ; return;}

	 $query = "SELECT * FROM Trials WHERE id='$trial_id'";
   	 $r =  @mysql_query($query) or die("pb 1 make_trial_vote_form : $query");
   	 $nb = mysql_num_rows($r);
   	 
   	 if ($nb <= 0) {echo "ERROR: no such trial '$trial_id' for player '$playerid' " ; return;}
   	 
   	 //$query = "UPDATE `TrialVotes` SET vote = -1 WHERE trial_id= '$trial_id' and player_id='$playerid' ;";
	 //$r2 =  @mysql_query($query) or die("PB 1bis in make_trial_vote_form: $query");
   	 
   	 $id = mysql_result($r , 0 , 0);
   	 $instr = mysql_result($r , 0 , 1);
	 $term = mysql_result($r , 0 , 2);
	 $p1 = mysql_result($r , 0 , 3);
	 $p2 = mysql_result($r , 0 , 4);
	 $date = mysql_result($r , 0 , 5);
	 $dateterm = mysql_result($r , 0 , 6);
	 $p1_score = mysql_result($r , 0 , 7);
	 $p2_score = mysql_result($r , 0 , 8);
	 $reason = mysql_result($r , 0 , 9);
	 
	 $query = "SELECT word FROM TrialProposedWords WHERE id='$trial_id' and player_id=$p1 ORDER BY RAND()";
	 $rp1 =  @mysql_query($query) or die("pb 2 list_MY_TRIALS_cr : $query");
	 
	 $query = "SELECT word FROM TrialProposedWords WHERE id='$trial_id' and player_id=$p2 ORDER BY RAND()";
	 $rp2 =  @mysql_query($query) or die("pb 3 list_MY_TRIALS_cr : $query");
	 
	 echo "<TABLE width=\"100%\">";
	 echo "<TR><TD width=\"50%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" 
	 		bgcolor=\"#FFFFCC\" 
	 		align=\"middle\">";
	 echo "<BR>" . get_msg('SOUK-TRIALS') . " $id - ";
	 $p1name = get_player_name($p1);
	 $p2name = get_player_name($p2);
	 $p1name = "----";
     $GLOBALS[0] = $p2name;
	 $GLOBALS[1] = $p1name;	 
	 echo get_msg("TRIALVOTE-INQUIRER") . "<P>";
     $GLOBALS[0] = $reason;	 
	 echo get_msg("TRIALVOTE-REASON-SHORT");
     $GLOBALS[0] = $instr;	 
	 echo get_msg("TRIALVOTE-INSTR");
	 
	 $term = format_entry($term);
     $GLOBALS[0] = $term;	 
	 echo get_msg("TRIALVOTE-TERM");
	 echo get_msg("TRIALVOTE-ANSWERS");
	 echo "</TABLE>";
	 
	 echo "<TABLE width=\"100%\">";
	 
	
	// echo "<TD width=\"50%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" 
	 //		bgcolor=\"#DCFFFF\" 
	 //		align=\"middle\">";
	// $p2name = get_player_name($p2);
	// echo "$p2name<P>";
	 
	 
	 echo "<TR><TD COLSPAN=2  border=\"0\" cellpadding=\"5\" cellspacing=\"0\" 
	 		bgcolor=\"#DCFFFF\" 
	 		align=\"middle\">";
	 for ($i=0 ; $i<mysql_num_rows($rp1) ; $i++) {
     	$w = mysql_result($rp1 , $i , 0);
     	echo "$w<BR>";
	 }
	// echo "<TD width=\"50%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" 
	 //		bgcolor=\"#DCFFFF\"
	 //		align=\"middle\">";
	// for ($i=0 ; $i<mysql_num_rows($rp2) ; $i++) {
    // 	$w = mysql_result($rp2 , $i , 0);
     //	echo "$w<BR>";
	// }
	 echo "<TR>";
	 
	 $bg1color = "#DCFFFF";
	 $bg2color = "#DCFFFF";
	 	
	 if ($p1_score >=  ($p2_score +2)) {
	 	$bg2color = "#DCFFFF";
	 	$bg1color = "green";
	 }
	 if ($p2_score >=  ($p1_score +2)) {
	 	$bg2color = "red";
	 	$bg1color = "#DCFFFF";
	 }
	 
	 echo "<TR><TD width=\"50%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" 
	 		bgcolor=$bg1color 
	 		align=\"middle\">";
	 $GLOBALS[0]=$p1_score;
	 echo get_msg("TRIALVOTE-INNOCENT") . "<br>" . get_msg("TRIALVOTE-N-VOTES") . "<P>";
	 echo "<TD width=\"50%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" 
	 		bgcolor=$bg2color
	 		align=\"middle\">";
	 $GLOBALS[0]=$p2_score;
	 echo get_msg("TRIALVOTE-GUILTY") . "<br>" . get_msg("TRIALVOTE-N-VOTES") . "<P>";
	 if (abs($p1_score -  $p2_score) < 2) {
	 	echo "<TR><TD COLSPAN=2  border=\"0\" cellpadding=\"5\" cellspacing=\"0\" 
	 		bgcolor=blue 
	 		align=\"middle\">";
		echo get_msg("TRIALVOTE-DOUBT");
	 }
	 
	 echo "</TABLE>";
}


function make_trial_vote_form($trial_id) {
	 $playerid = $_SESSION[ssig() . 'playerid'];
	 if ($playerid == 0) {echo "ERROR: playerid $playerid = 0" ; return;}
	 if ($trial_id == 0) {echo "ERROR: trial_id $trial_id = 0" ; return;}

	 $query = "SELECT * FROM Trials WHERE id='$trial_id'";
   	 $r =  @mysql_query($query) or die("pb 1 make_trial_vote_form : $query");
   	 $nb = mysql_num_rows($r);
   	 
   	 if ($nb <= 0) {echo "ERROR: no such trial '$trial_id' for player '$playerid' " ; return;}
   	 
   	 $query = "UPDATE `TrialVotes` SET vote = -1 WHERE trial_id= '$trial_id' and player_id='$playerid' ;";
	 //$r2 =  @mysql_query($query) or die("PB 1bis in make_trial_vote_form: $query");
   	 
   	 $id = mysql_result($r , 0 , 0);
   	 $instr = mysql_result($r , 0 , 1);
	 $term = mysql_result($r , 0 , 2);
	 $p1 = mysql_result($r , 0 , 3);
	 $p2 = mysql_result($r , 0 , 4);
	 $date = mysql_result($r , 0 , 5);
	 $dateterm = mysql_result($r , 0 , 6);
	 $p1_score = mysql_result($r , 0 , 7);
	 $p2_score = mysql_result($r , 0 , 8);
	 $reason = mysql_result($r , 0 , 9);
	  
	 $query = "SELECT word FROM TrialProposedWords WHERE id='$trial_id' and player_id=$p1 ORDER BY RAND()";
	 $rp1 =  @mysql_query($query) or die("pb 2 make_trial_vote_form : $query");
	 
	 $query = "SELECT word FROM TrialProposedWords WHERE id='$trial_id' and player_id=$p2 ORDER BY RAND()";
	 $rp2 =  @mysql_query($query) or die("pb 3 make_trial_vote_form : $query");
	 
	 echo "<TABLE width=\"100%\" border=\"0\">";
	 echo "<TR><TD width=\"50%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" 
	 		bgcolor=\"#FFFFCC\" 
	 		align=\"middle\">";
	 
	 $s = str_replace(" :", " :<br>", $instr);
	 echo "<BR>
	 		$s ";
	 		
	 $term = format_entry($term);
	 echo "<P><b>$term</b><P>";
	 echo "</TABLE>";
	 
	 echo "<TABLE width=\"100%\" border=\"0\">";
	 
	 
//	 echo "<TR><TD width=\"50%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" 
//	 		bgcolor=\"#DCFFFF\" 
//	 		align=\"middle\">";
//	 echo "Joueur A<P>";
//	 echo "<TD width=\"50%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" 
//	 		bgcolor=\"#DCFFFF\" 
//	 		align=\"middle\">";
//	  echo "Joueur B<P>";
	 
	 
	 echo "<TR><TD COLSPAN=2  border=\"0\" cellpadding=\"5\" cellspacing=\"0\" 
	 		bgcolor=\"#DCFFFF\"  border=\"0\" align=\"middle\">";
	 echo get_msg("TRIALVOTE-ANSWERS") . "<P>";
	 
	 for ($i=0 ; $i<mysql_num_rows($rp1) ; $i++) {
     	$w = mysql_result($rp1 , $i , 0);
     	echo "$w<BR>";
	 }
	// echo "<TD width=\"50%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" 
	// 		bgcolor=\"#DCFFFF\"
	// 		align=\"middle\">";
	// for ($i=0 ; $i<mysql_num_rows($rp2) ; $i++) {
    // 	$w = mysql_result($rp2 , $i , 0);
    // 	echo "$w<BR>";
	// }
	 echo "<TR><TD width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" 
	 		bgcolor=\"#FFFFCC\" border=\"0\" align=\"middle\">";
	 echo get_msg("TRIALVOTE-REASON") . "<BR>" . $reason;
	 echo "</TABLE>";
	 
	 echo "<TABLE width=\"100%\" border=\"0\">";
	 echo "<TR height=\"10mm\" width=\"100%\" align=\"middle\">";
	 echo "<TD border=\"0\"><BR><form id=\"vote_trial\" name=\"vote_trial\" method=\"post\" action=\"trialVote.php\" >
		<input id=\"trial_make_vote_A\" type=\"submit\" name=\"trial_make_vote_A\" value=\"" . get_msg("TRIALVOTE-INNOCENT") . "\">
		<input  id=\"proc_id\" type=\"hidden\" name=\"proc_id\" value=\"$trial_id\">
		</form></li>";
	 echo "<TD border=\"0\"><BR><form id=\"vote_trial\" name=\"vote_trial\" method=\"post\" action=\"trialVote.php\" >
		<input id=\"trial_make_vote_C\" type=\"submit\" name=\"trial_make_vote_C\" value=\"" . get_msg("TRIALVOTE-ABSTAIN") . "\">
		<input  id=\"proc_id\" type=\"hidden\" name=\"proc_id\" value=\"$trial_id\">
		</form></li>";
	 echo "<TD border=\"0\"><BR><form id=\"vote_trial\" name=\"vote_trial\" method=\"post\" action=\"trialVote.php\" >
		<input id=\"trial_make_vote_B\" type=\"submit\" name=\"trial_make_vote_B\" value=\"" . get_msg("TRIALVOTE-GUILTY") . "\">
		<input  id=\"proc_id\" type=\"hidden\" name=\"proc_id\" value=\"$trial_id\">
		</form></li>";
	 

	 
	 echo "</TABLE>";
}

function process_vote($trial_id, $vote)  {
	//echo "vote $vote<p>";
	$playerid = $_SESSION[ssig() . 'playerid'];
	if ($playerid == 0) {echo "ERROR: $trial_id playerid $playerid = 0" ; return;}
	if ($trial_id == 0) {echo "ERROR: trial_id  = 0" ; return;}
	//if ($vote == 0) {echo "ERROR: $trial_id vote = 0" ; return;}
	
	$query = "UPDATE `TrialVotes` SET vote = '$vote' WHERE trial_id= '$trial_id' and player_id='$playerid' ;";
	$r =  @mysql_query($query) or die("PB in process_vote: $query");
	
	inc_player_honnor($playerid, 1);
	
	if (trial_completed_p($trial_id)) {
		debugecho("Trial completed");
		conclude_trial($trial_id);
	}
}

function clean_trial_OVERDUE() {
	$query = "SELECT id FROM `Trials` WHERE date > '0' and date_term = 0";
	$r =  @mysql_query($query) or die("bug in clean_trials : $query");
	$nb = mysql_num_rows($r);
	if ($nb > 0) {
		for ($i=0 ; $i<$nb ; $i++) {
			$id = mysql_result($r , $i , 0);
			trial_overdue_p($id);
		}
	}
}

function trial_overdue_p($trial_id) {
	$query = "SELECT date FROM Trials WHERE id='$trial_id'";
   	$r =  @mysql_query($query) or die("pb 1 trial_overdue_p : $query");
   	$date = mysql_result($r , 0 , 0);
   	$now = round(microtime_float());
   	if (($now - $date) > (7 * 24 * 3600)) {
   		
   		$query = "UPDATE `TrialVotes` SET vote = -1 WHERE trial_id= '$trial_id' and vote='' ;";
		$r =  @mysql_query($query) or die("pb 2 trial_overdue_p : $query");
		conclude_trial($trial_id);
   		return true;
   	} else {
   		return false;
   	}
}

function trial_completed_p($trial_id) {
	$query = "SELECT count(*) FROM TrialVotes WHERE trial_id='$trial_id' and vote = ''";
   	$r =  @mysql_query($query) or die("pb 1 make_trial_vote_form : $query");
   	$nb = mysql_result($r , 0 , 0);
   	//debugecho("trial_completed_p ==> $nb votes remaining");
   	return ($nb == 0);
}

function conclude_trial($trial_id)  {
	$A_count = 0;
	$B_count = 0;
	
	$query = "SELECT player1_id, player2_id, term FROM Trials WHERE id='$trial_id'";
   	$r =  @mysql_query($query) or die("pb 0 conclude_trial : $query");
   	$p1 = mysql_result($r , 0 , 0);
   	$p2 = mysql_result($r , 0 , 1);
   	$term = mysql_result($r , 0 , 2);
   	
	$query = "SELECT * FROM TrialVotes WHERE trial_id='$trial_id'";
   	$r =  @mysql_query($query) or die("pb 1 conclude_trial : $query");
   	$nb = mysql_num_rows($r);
   	for ($i=0 ; $i<$nb ; $i++) {
      $id = mysql_result($r , $i , 0);
      $trial_id = mysql_result($r , $i , 1);
      $player_id = mysql_result($r , $i , 2);
      $vote = mysql_result($r , $i , 3);
      
    	$query = "SELECT honnor, nbplayed FROM Players WHERE id='$player_id'";
   		$r2 =  @mysql_query($query) or die("pb 2 conclude_trial : $query");
   		$honnor = mysql_result($r2 , 0 , 0);
   		$nbplayed = mysql_result($r2 , 0 , 1);
   		$eff = max(0.5,$honnor/($nbplayed+1));
   		
   		if ($vote == "A") {
   			$A_count = $A_count + $eff;
   			debugecho("voter $player_id has eff $eff and votes for A = $A_count");
   		} else {
   			if ($vote == "B") {
   				$B_count = $B_count + $eff;
   				debugecho("voter $player_id has eff $eff and votes for B = $B_count");
   			} else {
   				if ($vote == "C") {
   					// abstention
   					debugecho("voter $player_id has eff $eff and abst :  A = $A_count B = $B_count");
   				} else {
		   			// cas else pour le vote
   					// penaliser le non votant
   					debugecho("voter $player_id has eff $eff and HAS NOT VOTED");
   					inc_player_honnor($player_id, -100);
   				}
   			}
   		}
   	}
   	
   	$now = round(microtime_float());
   	
   	$query = "UPDATE `Trials` SET p1_score = '$A_count', p2_score = '$B_count', date_term='$now' WHERE id= '$trial_id';";
	$r =  @mysql_query($query) or die("PB in conclude_trial: $query");
	
	// �galit�
	//
	if (abs($A_count - $B_count) < 2) {
		debugecho("It's a draw");
		
		$winner_name = get_player_name($win);
 		$loser_name = get_player_name($loser);
   		
   		$winner_name = get_player_name($p1);
   		$loser_name = get_player_name($p2);
   	
   		//make_event("non-lieu pour le proc�s entre '$winner_name' et '$loser_name' pour le mot '$term'.");
		
    	return;
	}
	
   	// selectionner et recompenser le vainqueur
   	// punir le perdant <-- NON
   	//
   	if 	($A_count > ($B_count + 2)) {
   		$win = $p1 ;
   		$loser = $p2;
   	} else {
   		$win = $p2;
   		$loser = $p1;
   		
   	}
   	debugecho("The winner is $win and loser is $loser");
   	
   	$diff = min(25,abs($A_count - $B_count));
   	
   	// le perdant
   //	$cr = get_player_credit($loser);
   	$h = get_player_honnor($loser);
   	
   	
   //	$gain_cr = max(1,min(1000, round(0.001*$cr*$diff)));
   	$gain_h = max(1,min(5,round(0.001*$h*$diff))); 
   	
   	// le vainqueur
   	//inc_player_credit($win, 250 + $gain_cr);
   	inc_player_honnor($win, $gain_h);
   	
   	// le perdant
   	$cr = get_player_credit($loser);
   	$h = get_player_honnor($loser);
   	//inc_player_credit($loser, round(-1*$gain_cr));
   	//inc_player_honnor($loser, round(-1*$gain_h));
   	
   	// generer evenement
   	$winner_name = get_player_name($win);
   	$loser_name = get_player_name($loser);

   	$GLOBALS[0] = $winner_name;
   	$GLOBALS[1] = $loser_name;
   	$GLOBALS[2] = $term;
    make_event(get_msg("TRIALVOTE-EVENT"));
	
	$GLOBALS[0] = $winner_name;
	$GLOBALS[1] = $trial_id;
	$GLOBALS[2] = $loser_name;
	$GLOBALS[3] = $term;
	$msg = get_msg( "TRIALVOTE-WIN-BODY" );
   	send_mail_to_player($win, get_msg("TRIALVOTE-WIN-TITLE"), $msg);
   	   	
	$GLOBALS[0] = $loser_name;
	$GLOBALS[1] = $trial_id;
	$GLOBALS[2] = $winner_name;
	$GLOBALS[3] = $term;   	
	$msg = get_msg( "TRIALVOTE-LOSE-BODY" );	
   	send_mail_to_player($loser, get_msg("TRIALVOTE-LOSE-TITLE"), $msg);
   	   	
   	$rand = rand(0,1);
   	if ($rand <= 0) {
   		suggest_special_relation_game_GEN($win, 1, 13, 24, get_msg("TRIALVOTE-JUDGE"), get_msg("TRIALVOTE-JUDGE-GIFT"));
   	} else {
   		suggest_special_relation_game_GEN($win, 1, 17, 23, get_msg("TRIALVOTE-JUDGE"), get_msg("TRIALVOTE-JUDGE-GIFT"));
   	}
		
   	// detruire le proc�s
   	// plus tard - test d'abord
}


function delete_trial($trial_id) {

}

// a appeller de tremps en temps
// par exemple au login
//
function clean_trials() {
	//  on fait le menage
	// on vire tous les events d'un id < au 500e
	$query = "SELECT id FROM `Trials` WHERE date = '0' ORDER BY `id` ASC LIMIT 50,350";
	$r =  @mysql_query($query) or die("bug in clean_trials : $query");
	$nb = mysql_num_rows($r);
	if ($nb > 0) {
		$id = mysql_result($r , 0 , 0);
		$query = "DELETE FROM `Trials` WHERE id < $id AND date = '0' ;";
		//echo $query;
		$r =  @mysql_query($query) or die("bug in clean_trials : $query");
	}
	
	$query = "DELETE FROM `TrialProposedWords` WHERE id NOT IN
			(SELECT id FROM Trials)";
	$r =  @mysql_query($query) or die("bug dans clean_trials 2 :$query");
	
	$query = "DELETE FROM `TrialVotes` WHERE trial_id NOT IN
			(SELECT id FROM Trials)";
	$r =  @mysql_query($query) or die("bug dans clean_trials 3 :$query");
	
}

?>
