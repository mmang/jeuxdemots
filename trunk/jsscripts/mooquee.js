/*
 * Created By - Robert Inglin
 * Homepage   - http://robert.ingl.in/mooquee
 * License    - MIT License Agreement

Copyright (c) 2008 Robert Inglin

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 */

Mooquee = new Class({
	Implements: [Options],

	options: {
		element: 'mooquee',
		cssitem: 'mooquee_item',
		firstitem:0,
		direction:'up', //up down left or right
		pause: 1, //seconds (keep pause equal or higher to duration to allow time for items to reset)
		duration: 1, //number of seconds to move marquee items
		overflow:'hidden', //if your item flows over how do you want to handle it. Auto(scroll) or Hidden work best...
		startOnLoad:true 
	},
	initialize: function(options){
		this.setOptions(options);
		this.itemFXs = [];
		this.started = false;
		this.currentitem = this.options.firstitem;
		this.loop = true;
		
		window.addEvent('domready', function() {
			//get all mooqueeItems
			this.items = $$('#' + this.options.element + ' .' + this.options.cssitem);
			this.totalitems = this.items.length;
			if($(this.options.element).style.overflow != 'hidden')
				$(this.options.element).style.overflow = 'hidden';
			if($(this.options.element).style.position != 'relative')
				$(this.options.element).style.position = 'relative';

			this.setMooqueeFXs();
			this.setDirection(this.options.direction);// has setMooqueeItems in it

			if(this.options.startOnLoad)
				this.mooveAll.delay(this.options.pause*1000 ,this);
		}.bind(this));
		
		
	},
	setMooqueeItems: function(){
		this.resetting =true;
		var i=0;
		
		this.items.each(function (element){
			if($(element).style.position != 'absolute')
				$(element).style.position = 'absolute';
			$(element).style.width = $(this.options.element).clientWidth + 'px';
			$(element).style.overflow = this.options.overflow;
			
			if(i == this.currentitem)
				startingposition =0;
			else
				startingposition = this.pixels;
			
			this.itemFXs[i].set(this.style,startingposition);
			this.itemFXs[i].set(this.antistyle,0);
			i++;
		}.bind(this));
		this.resetting =false;
	},
	setMooqueeFXs: function(){
		var i=0;
		this.items.each(function (element){
			this.itemFXs[i] = new Fx.Tween(element,{duration:(this.options.duration*1000)});
			i++;
		}.bind(this));
	},
	mooveAll: function(){

		this.previousitem = this.currentitem;
		
		if((this.currentitem + 1) == this.totalitems)
			this.currentitem = 0;
		else
			this.currentitem = this.currentitem + 1;
			
		this.moove(this.previousitem)
		this.moove(this.currentitem);

	},
	moove: function(itemnumber){
		if(itemnumber == this.previousitem)
			this.itemFXs[itemnumber].start(this.style,this.antipixels).chain(function(){
				if(!this.resetting)this.itemFXs[itemnumber].set(this.style,this.pixels);
			}.bind(this));
		else
			this.itemFXs[itemnumber].start(this.style,0).chain(function(){
				if(this.loop == true)
					this.loopTimer = this.mooveAll.delay(this.options.pause*1000 ,this);
			}.bind(this));
		
	},
	setDirection: function(newDirection){
		switch(newDirection){
				case 'up':
					this.style = 'top';
					this.antistyle = 'left'
					this.pixels = $(this.options.element).clientHeight;
					this.antipixels = this.pixels * -1;
				break;
				case 'down':
					this.style = 'top';
					this.antistyle = 'left';
					this.antipixels = $(this.options.element).clientHeight;
					this.pixels = this.antipixels * -1;
				break;
				case 'left':
					this.style = 'left';
					this.antistyle = 'top';
					this.pixels = $(this.options.element).clientWidth;
					this.antipixels = this.pixels * -1;
				break;
				case 'right':
					this.style = 'left';
					this.antistyle = 'top';
					this.antipixels = $(this.options.element).clientWidth;
					this.pixels = this.antipixels * -1;
				break;						
		}
		this.setMooqueeItems();
	}
});