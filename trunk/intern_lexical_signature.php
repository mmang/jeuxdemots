<?php session_start();?>
<?php 
include_once 'misc_functions.php'; 
include_once 'intern_lexical_signature_functions.php';
?>
<?php
    openconnexion();

?>
<html>
 <head>
    <title><?php echo "Lexical signatures"; ?></title>
    <?php header_page_encoding(); ?>
  </head>
<?php include 'HTML-body.html' ; ?>
<?php //topblock(); ?>

<?php

function generate_compute_sig_form() {
	$term_1 = stripslashes(trim($_POST['term_1']));
	//$term_2 = stripslashes(trim($_POST['term_2']));
	//$rel = $_POST['reltype'];
	echo "<form id=\"linkit\" name=\"linkit\" method=\"post\" action=\"intern_lexical_signature.php\" >";	
	echo "<input id=\"compute_submit\" type=\"submit\" name=\"compute_submit\" value=\"Calculer\">
	la signature lexicale
	de <input id=\"term_1\" type=\"text\" name=\"term_1\" value=\"$term_1\">
	";
	echo "</form>";
	
}

function generate_sim_form() {
	$term_1 = stripslashes(trim($_POST['term_1']));
	$term_2 = stripslashes(trim($_POST['term_2']));
	echo "<form id=\"linkit\" name=\"linkit\" method=\"post\" action=\"intern_lexical_signature.php\" >";	
	echo "<input id=\"sim_submit\" type=\"submit\" name=\"sim_submit\" value=\"Calculer\">
	la similarit� entre
	de <input id=\"term_1\" type=\"text\" name=\"term_1\" value=\"$term_1\">	
	�
	<input id=\"term_2\" type=\"text\" name=\"term_2\" value=\"$term_2\">
	";
	echo "</form>";
	
}

function my_compute_sig($term) {
	//return compute_full_lexical_signature($term);
	return compute_full_lexical_signature_level($term, 1);
}


generate_compute_sig_form();

if ($_POST['compute_submit'] != '') {
	
	//$id1 = term_exist_in_BD_p($_POST['term_1']);
	//echo "<br>check = " . check_need_update($id1);
	$t1 = fetch_arg('term_1');
	$id1 = term_exist_in_BD_p($t1);
	$check1 = check_need_update($id1);
	if ($check1 == false) {
		echo "fetching..."; flush();
		$sig = unserialize_sig(fetch_signature($id1));
	} else {
		echo "computing..."; flush();
		$sig = my_compute_sig($t1);
	}
	//echo "<p>";
	//echo $sig;
	if (($sig=='-1') || (count($sig) == 0)) {echo " pas d'information sur ce terme";}
	echo "<p>";
	decode_sig($sig);
}

generate_sim_form();
if ($_POST['sim_submit'] != '') {
	
	$t1 = stripslashes(fetch_arg('term_1'));
	$t2 = stripslashes(fetch_arg('term_2'));
	$id1 = term_exist_in_BD_p($t1);
	$id2 = term_exist_in_BD_p($t2);
	
	$check1 = check_need_update($id1);
	$check2 = check_need_update($id2);
	
	//echo "<br>check1 $check1";
	//echo "<br>check2 $check2";
	
	$sig1 = unserialize_sig(fetch_signature($id1));
	if (($sig1 == -1) || $check1) {
		$sig1 = my_compute_sig($t1);
	}
	$sig2 = unserialize_sig(fetch_signature($id2));
	if (($sig2 == -1) || $check2) {
		$sig2 = my_compute_sig($t2);
	}
/*	echo "<P>sig1=" . fetch_signature($id1);
	print_r($sig1);
	
	echo "<p>sig2=" . fetch_signature($id2);
	print_r($sig2);
	*/
	/*
	echo "<p>$t1 == ";
	decode_sig($sig1);
	echo "<p>$t2 == ";
	decode_sig($sig2);
	*/
	
	//$sim = signature_sim($sig1,$sig2);
	start_time_record('simbiz');
	$sim = signature_sim_biz($id1, $id2, $sig1, $sig2);
	$duree = end_time_record('simbiz');
	echo "<p>sim entre '$t1' et '$t2' == <DATA>$sim</DATA><P>dur�e == <DUREE>$duree</DUREE>";
}

if ($_SESSION[ssig() . 'playerid'] == 11) {
	
	echo "<form id=\"computesig\" name=\"computesig\" method=\"post\" action=\"intern_lexical_signature.php\" >";	
	echo "<input id=\"computesig\" type=\"submit\" name=\"computesig\" value=\"Calculer 1000 signatures\">";
	echo "</form>";
	
	if ($_POST['computesig'] != '') {
		
	//$query = " SELECT id FROM `Nodes` WHERE type=1 ORDER BY `Nodes`.`w` DESC LIMIT 2000 ";
	$query = " SELECT termid FROM `LexSig` WHERE sig = '' ";
	$r =  @mysql_query($query) or die("pb1 in fetch_signature : $query");
	$nb =  mysql_num_rows($r);
	for ($j=0 ; $j<$nb ; $j++) {
    	$id = mysql_result($r , $j , 0);
    	//$check1 = check_need_update($id);
    	$check1=true;
    	if ($check1 == true) {
    		$term = stripslashes(get_term_from_id($id));
    		echo "<br>$j) Calcul sig pour '$term' ($id)";
    		flush();
    		my_compute_sig($term);
    	} else {
    		$term = get_term_from_id($id);
    		echo "<br>$j) sig pour '$term' ($id) d�ja calcul�e";
    		flush();
    	}
	}
	}
}
?>




<?php 
   // bottomblock();
    closeconnexion();
?>

  </body>
</html>
