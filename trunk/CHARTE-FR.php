		<H1>Charte d'Admission au jeu JeuxDeMots</H1>
		
		<p>
		JeuxDeMots est un jeu php gratuit créé dans le cadre d'un projet de recherche par Mathieu Lafourcade au sein du LIRMM 
		et en coopération avec d'autres universités. L'objet de ce projet est la construction de ressources lexicales dans le
		but d'application au Traitement Automatique du Langage Naturel(TALN). Ces données sont le produit de l'activité 
		des joueurs de JeuxDeMots.
		<P>
		Les données lexicales sont accessibles (<a href="http://www.lirmm.fr/~lafourcade/JDM-LEXICALNET-FR/">http://www.lirmm.fr/~lafourcade/JDM-LEXICALNET-FR/</a>) 
		sous la licence Creative Commons (<a href="http://creativecommons.org/">http://creativecommons.org/</a>).
		<P>
		La présente charte a pour but de permettre aux joueurs de jouer dans l'environnement le plus agréable possible,
		s'ils respectent les quelques règles établies ci-dessous.<br />
		Tout manquement ou tout abus fera l'objet de sanctions pouvant aller jusqu'au banissement définitif du joueur.<br />
		De manière générale, il est attendu de la part des joueurs un comportement fair-play visant à respecter l'amusement de tous.<br />
		</p>

		<h3>(1) Inscription</h3>
		<p>
		La participation au jeu est entièrement gratuite et l'inscription est ouverte à tous les internautes 
		disposant d'un accès à Internet régulier et éventuellement d'une adresse Email personnelle et active 
		(ce dernier point n'étant pas obligatoire)<br />
		De part la nature du vocabulaire potentiellement rencontré, JeuxDeMots peut ne pas convenir 
		à des personnes de moins de 16 ans.<br />
		</p>
		
		<h3>(2) Données Personnelles</h3>
		<p>
		Certaines informations enregistrées dans notre base de donnée sont personnelles (mot de passe, adresse email, adresse ip, etc), 
		d'autres sont amenées à être éventuellement fournies à d'autres personnes 
		(réponses proposées par le joueur lors d'une partie, classement, etc)
		dans le cadre du jeu.<br />
		
		Aucune diffusion des Données Personnelles ne sera fait à aucun titre que ce soit en dehors des modalités prévues par le jeu 
		(informations affichées dans le Classement, affichage du résultat d'une partie, etc).
		Toutefois, toutes les données du jeu sont consultables par l'Equipe du jeu, 
		sans restriction.<br />
		
		L'Equipe du jeu se réserve le droit d'exploiter à des fins de recherches les données lexicales récoltées lors d'une partie 
		(une fois celle-ci achevée).
		En aucun cas, ces données ne contiennent d'informations personnelles permettant de retrouver leur(s) auteur(s).
		
		<br /><br />
		Le joueur peut demander à tout moment 
		la suppression de son compte ainsi que la totalité des informations le concernant dans la base de données.
		<br />
		</p>
		
		<h3>(3) Saisies</h3>
		<p>
		Toute saisie qui sera jugée comme une atteinte à la morale entraînera l'élimination immédiate et sans préavis du compte du joueur.<br />
		</p>
		<P>
		Certains termes ou certaines questions proposés dans JeuxDeMots peuvent être sont considérés comme profanes, vulgaires ou offensants par certains joueurs. 
		L'objet de JeuxDeMots étant de constituer un dictionnaire couvrant l'ensemble des faits ou opinions possibles,
		de telles informations ne seront a priori pas retirées du jeu. Toutefois, les joueurs sont encouragés à signaler tout contenu
		inaproprié.
		
		<h3>(4) Vente de comptes</h3>
		<p>
		La vente pour de l'argent ou des biens matériels d'un compte et/ou d'éléments du jeu est formellement interdite.<br />
		</p>
		
		<h3>(5) Automatisation</h3>
		<p>Un compte doit être contrôlé par un joueur : 
		il est formellement interdit d'automatiser les actions par quelque moyen que ce soit (script, programme, site web, etc.).
		</p>
		
		<h3>(6) Bogues</h3>
		<p>
		JeuxDeMots n'est pas à l'abri de bugs et incohérences. 
		Chaque joueur est tenu de rapporter (par mail ou sur le forum) au plus vite tout problème décelé, 
		et toute utilisation abusive ou volontaire d'un bug ou d'une faille se verra sanctionnée et pourra se solder par la suppression du compte du joueur concerné.<br />
		Des compensations pourront intervenir de la part des administateurs, mais les administateurs seront seuls juges.<br />
		</p>
		
		<h3>(7) Information relative aux "cookies" de navigation</h3>
		Pour le bon fonctionnement du jeu, nous souhaitons implanter un cookie dans votre ordinateur. 
		Un cookie ne nous permet pas de vous identifier; 
		en revanche, il enregistre des informations relatives à la navigation de votre ordinateur sur notre site (les pages que vous avez consultées,
		la date et l'heure de la consultation, etc.) que nous pourrons lire lors de vos visites ultérieures. 
		La durée de conservation de ces informations dans votre ordinateur est de plusieurs mois.
		<br />
		Vous pouvez vous opposer à l'enregistrement de cookies, toutefois JeuxDeMots ne pourra pas fonctionner normalement.
		 <br />
Nous vous informons que vous pouvez vous opposer à l'enregistrement de cookies en configurant votre navigateur de la manière suivante :
<br /><br />
<small>
Pour Microsoft Internet Explorer 6.0 :
1. choisissez le menu "Outils" (ou "Tools"), puis "Options Internet" (ou "Internet Options").
2. cliquez sur l'onglet "Confidentialité" (ou "Confidentiality")
3. sélectionnez le niveau souhaité à l'aide du curseur.
<br /><br />
Pour Microsoft Internet Explorer 5 :
1. choisissez le menu "Outils" (ou "Tools"), puis "Options Internet" (ou "Internet Options") ;
2. cliquez sur l'onglet "Sécurité" (ou "Security") ;
3. sélectionnez "Internet" puis "Personnaliser le niveau" (ou "CustomLevel") ;
4. repérez la rubrique "cookies" et choisissez l'option qui vous convient.
<br /><br />
Pour Netscape 6.X et 7. X :
1. choisissez le menu "Edition">"Préférences" ;
2. Confidentialité et Sécurité ;
3. Cookies.
<br /><br />
Pour Firefox :
1. choisissez le menu "Outils">"Options" ;
2. cliquez sur l'option "Vie privée";
3. rubrique "Cookies".
<br />
Pour Opéra 6.0 et au-delà :
1. choisissez le menu "Fichier">"Préférences" ;
2. Vie Privée.
<br /><br />
</small>
		<h3>(8) Charte d'admission</h3>
		<p>
		La présente charte doit être lue et acceptée dans son intégralité pour pouvoir jouer à JeuxDeMots.<br />
		Elle est susceptible d'être modifiée par les gestionnaires du jeu. 
		Le cas échéant, il sera demandé aux joueurs d'accepter explicitement les modifications apportées pour pouvoir continuer à jouer.<br />

		<br />
		Mathieu Lafourcade - Administrateur -
		<?php  echo "<a href=\"mailto:" . $_SESSION[ssig() . 'CONTACT-EMAIL']) . "?subject=JeuxDeMots feedback\" >" . get_msg('MISC-CONTACT') . "</a>"; ?>
		| Sources de la charte <a href="http://www.starshine-online.com">SSO</a> -
			<a href="http://www.mountyhall.com">MH</a> -
			<a href="http://www.cnil.fr">CNIL</a>
		<BR>
		Dernière mise à jour : décembre 2011
		</p>
		</div>
		<br><br>
