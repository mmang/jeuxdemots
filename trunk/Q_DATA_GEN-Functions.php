
<?php
//-----------
function qinfo_relation_exists($termid, $label) {
	insert_potential_data_node($label);
	$label_id = term_exist_in_BD_p($label);
		
	$query = "SELECT *  FROM `Relations` WHERE node1 = $termid AND node2 = $label_id AND type = 36";
	//echo "<br>$query";
	$r =  @mysql_query($query) or die("bug in qtest_has_pos : $query");
    $nb = mysql_num_rows($r);
    if ($nb == 0) {
    	//echo "<br>rel to $label NOT exists"; 
    	return false;
    } else {
    	//echo "<br>rel to $label exists"; 
    	return true;
    }
}

function qinfo_relation_saturated($termid, $label, $th=1000) {
	insert_potential_data_node($label);
	$label_id = term_exist_in_BD_p($label);
		
	$query = "SELECT w FROM `Relations` WHERE node1 = $termid AND node2 = $label_id AND type = 36";
	//echo "<br>$query";
	$r =  @mysql_query($query) or die("bug in qtest_has_pos : $query");
    $nb = mysql_num_rows($r);
    if ($nb == 0) {
    	//echo "<br>rel to $label NOT exists"; 
    	return false;
    } else {
    	//echo "<br>rel to $label exists"; 
    	$w = mysql_result($r , 0 , 0);
    	return ($w >= $th);
    }
}



function qtest_has_pos($termid) {
	$query = "SELECT *  FROM `Relations` WHERE `node1` = $termid AND `type` = 4";
	$r =  @mysql_query($query) or die("bug in qtest_has_pos : $query");
    $nb = mysql_num_rows($r);
    if ($nb == 0) {return false;} else {return true;}
}

function qtest_composed($termid) {
	$term = get_term_from_id($termid);
	$pos = strrpos($term, " ");
	//echo "<br>termid=$termid term=$term pos===$pos";
	if ($pos === false) { // note : trois signes �gal
    	//echo "<br>test qtest_composed : 0";
   		return false;
	} else {
		//echo "<br>test qtest_composed : 1";
		return true;
	}
}

function qtest_special_question($termid) {
	$term = get_term_from_id($termid);
	$pos = strrpos($term, "::>");
	//echo "<br>termid=$termid term=$term pos===$pos";
	if ($pos === 0) { // note : trois signes �gal
    	//echo "<br>test qtest_composed : 0";
   		return true;
	} else {
		//echo "<br>test qtest_composed : 1";
		return false;
	}
}

function qtest_nom_propre($termid) {
	$term = get_term_from_id($termid);
	$test = (strtolower($term) != $term);
	//echo "<br>test nom propre : $test";
	return test;
}

function qtest_ask_question($label){
	$key = array_search($label, $_SESSION[ssig() . 'DATA-SYMB']);
	if ($key == 0) {
		return;
	}
	return $_SESSION[ssig() . 'DATA-Q'][$key];
	
}

function prepare_questions($termid) {
	
	$qtest_has_pos = qtest_has_pos($termid);
	$qtest_nom_propre = qtest_nom_propre($termid);
	$qtest_composed = qtest_composed($termid);
	$qtest_special_question = qtest_special_question($termid);
	
	$i=1;
	$_SESSION[ssig() . 'DATA-ASSERT-LIST'] = array();
	
	$_SESSION[ssig() . 'DATA-ASSERT'] = array();
	$_SESSION[ssig() . 'DATA-Q'] = array();
	$_SESSION[ssig() . 'DATA-SELECT'] = array();
	$_SESSION[ssig() . 'DATA-SYMB'] = array();
	
	if ($termid==0) {return;}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][0] = "Rien ne convient";
	$_SESSION[ssig() . 'DATA-Q'][0] = "Rien";
	$_SESSION[ssig() . 'DATA-SYMB'][0] = "_INFO-NO-MORE-QUESTION";
	$_SESSION[ssig() . 'DATA-SELECT'][0] = 0;
	qinfo_relation_exists($termid, $_SESSION[ssig() . 'DATA-SYMB'][0]);
	
	if (qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][0]))  {return;}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "mal orthographi�";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il mal orthographi� ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-BAD-ORTHO";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "inapropri� dans JeuxDeMots";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il inapropri� ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-INAP";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un nom commun";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il un nom commun ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-POS-NC";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !$qtest_has_pos && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un nom propre";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il un nom propre ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-POS-NP";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && $qtest_nom_propre && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un terme au pluriel";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il au pluriel";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-POS-PLUR";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !$qtest_has_pos && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un terme au singulier";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il au singulier ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-POS-SING";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !$qtest_has_pos && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un terme masculin";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il masculin ";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-POS-MASC";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !$qtest_has_pos && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un terme f�minin";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il f�minin ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-POS-FEM";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !$qtest_has_pos && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "une expression verbale";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "S'agit-il d'une expression verbale ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-POS-VER";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && $qtest_composed && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un participe pass�";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "S'agit-il d'un participe pass� ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-POS-PP";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !$qtest_has_pos && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un verbe";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "S'agit-il d'un verbe ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-POS-VERB";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !$qtest_has_pos && !$qtest_composed && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "une expression adverbiale";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "S'agit-il d'une expression adverbiale ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-POS-ADV-EXPR";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && $qtest_composed && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un adjectif";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "S'agit-il d'un verbe ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-POS-ADJ";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !$qtest_has_pos && !$qtest_composed && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "une expression adjectivale";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "S'agit-il d'une expression adjectivale ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-POS-ADJ-EXPR";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && $qtest_composed && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un lieu";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il un nom de lieu ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-SEM-PLACE";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "une substance/mati�re";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "S'agit-il d'un nom de substance/mati�re ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-SEM-SUBST";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un �v�nement";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il un nom d'�v�nement ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-SEM-EVENT";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "une personne";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il un nom de personne ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-SEM-PERS";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "une organisation";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il un nom de organisation ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-SEM-ORGA";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !(qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0))) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "une chose concr�te";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il une chose concr�te ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-SEM-THING";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "une chose abstraite";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il une chose abstraite?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-SEM-THING-ABSTR";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "du vocabulaire populaire";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il populaire";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-LPOP";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "du vocabulaire soutenu";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Ce terme est-il soutenu";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-LSOUT";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "du vocabulaire technique";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Est-ce du vocabulaire technique ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-LTECH";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un artefact";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Est-ce un artefact ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-SEM-THING-ARTEFACT";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un objet naturel";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Est-ce un objet naturel ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-SEM-THING-NATURAL";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un �tre vivant";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Est-ce un �tre vivant ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-SEM-LIVING-BEING";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un �tre imaginaire";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Est-ce un �tre imaginaire ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-SEM-IMAGINARY";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "une caract�ristique";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Est-ce une caract�ristique ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-SEM-CARAC";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "une action";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Est-ce une action ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-SEM-ACTION";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un moment/temps";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Est-ce une moment/temps ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-SEM-TIME";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	$_SESSION[ssig() . 'DATA-ASSERT'][$i] = "un ensemble de choses";
	$_SESSION[ssig() . 'DATA-Q'][$i] = "Est-ce un ensemble de choses ?";
	$_SESSION[ssig() . 'DATA-SYMB'][$i] = "_INFO-SEM-SET";
	$_SESSION[ssig() . 'DATA-SELECT'][$i] = 0;
	if (!$qtest_special_question && !qinfo_relation_saturated($termid, $_SESSION[ssig() . 'DATA-SYMB'][$i], 0)) {
		$_SESSION[ssig() . 'DATA-SELECT'][$i] = 1;
		array_push($_SESSION[ssig() . 'DATA-ASSERT-LIST'], $i);
		$i++;
	}
	
	shuffle($_SESSION[ssig() . 'DATA-ASSERT-LIST']);
	return(count($_SESSION[ssig() . 'DATA-ASSERT-LIST']));
	
}


//-----------

function insert_potential_data_node($label) {
	$query = "INSERT INTO Nodes (name, type, w, creationdate, level) 
						VALUES(\"$label\" , 36 , 50, CURRENT_TIMESTAMP, 0)";
	$r =  mysql_query($query);
}

function insert_potential_data_relation($termid, $label) {
	//echo "<br>insert_potential_data_relation($termid, $label)";
	if ($termid == 0) {return;}	
	$label_id = term_exist_in_BD_p($label);
	if ($label_id == 0) {return;}
	update_network_relation ($label, $termid, 36, 1);
}

function generate_random_special_question_data($nbcol=5,$size=20){
    	
    
  	//$nbcol = 5;
	//$size = 20;
	$j=-1;
    echo "<CENTER><TABLE cellpadding=\"1px\">";
    for ($i=0 ; $i<$size ; $i++) {
    	$val=$_SESSION[ssig() . 'DATA-ASSERT-LIST'][$i];
    	$q = $_SESSION[ssig() . 'DATA-ASSERT'][$val];
    	$k=$i+1;
    	if ($q != "") {
    		$j++;
    		if ($j%$nbcol == 0) {echo "<TR>";}
    		echo "<TH width=\"180px\"  >";
    		echo "\n<div class=\"jdm-special-block\">";
			echo "\n<img BORDER=0 width=\"170px\" height=\"80px\" src=\"http://www.lirmm.fr/jeuxdemots/pics/postits/postitdeg.png\">";
			echo "\n<div class=\"jdm-special-question\">";
			echo "\n<a href=\"Q_DATA_GEN.php?q=$k\"><font size=\"+1\">" .  $q . "</font></a>";
			echo "\n</div>";
			echo "\n</div>";
    	}
    }
    		$i++;
    		$q = $_SESSION[ssig() . 'DATA-ASSERT'][0];
   			echo "<TR><TH width=\"180px\"  >";
    		echo "\n<div class=\"jdm-special-block\">";
			echo "\n<img BORDER=0 width=\"170px\" height=\"80px\" src=\"http://www.lirmm.fr/jeuxdemots/pics/postits/postitdeg2.png\">";
			echo "\n<div class=\"jdm-special-question\">";
			echo "\n<a href=\"Q_DATA_GEN.php?q=0\"><font size=\"+1\">" .  $q . "</font></a>";
			echo "\n</div>";
			echo "\n</div>";
				
    echo "</TABLE></CENTER>";
}


function process_QDATA_choice () {
	//if 	($_GET['q'] == "") {return;}
	//echo "<P>choix=" . $_GET['q'];
	$choix = $_GET['q'];
	if (($choix != '') && ($choix == 0)) {
		$val = 0;
	} else {
		$val=$_SESSION[ssig() . 'DATA-ASSERT-LIST'][$choix-1];
	}
	$label = $_SESSION[ssig() . 'DATA-SYMB'][$val];
	$assert = $_SESSION[ssig() . 'DATA-ASSERT'][$val];
	//echo "<P>PROCESS: label=$label" ;
	$termid = term_exist_in_BD_p($_SESSION[ssig() . 'gameentry']);
	//echo "<P>PROCESS: termid=$termid" ;
	//echo "<P>PROCESS: question=" . qtest_ask_question($label) ;
	
	
	if ($label == "") {return false;}
	if ($termid == 0) {return false;}

	insert_potential_data_relation($termid, $label);
	
	$target = 'generateResult.php';
	echo "<script language=\"JavaScript1.2\"> 
	    window.setTimeout(\"location=('$target');\",1000) 
	    </script>  ";
	echo "<div id=\"jdm-consigne-warn-block\">
	<div id=\"jdm-consigne-warn\"><font size=\"4\">
	Vous avez choisi:<br>$assert<p>Merci !
	</font>
	</div></div>";
    	
	return true;
}



function simple_select_word() {
	
	$trueid = '';
	do {
		$id = rand(1, 200000);
		$query = "SELECT id FROM `Nodes` WHERE id=$id and type=1; " ;
		$r =  @mysql_query($query) or die("pb in simple_select_word : $query");
		$nb = mysql_num_rows($r);
		if ($nb > 0) {
			$trueid= mysql_result($r , 0 , 0);
		}
	} while ($trueid <= 0);
	//echo "TRUEID=$trueid";
	return $trueid;
}


?>

