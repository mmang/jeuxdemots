<?php session_start();?>
<?php include 'misc_functions.php' ; ?>
<?php openconnexion();?>

<html>
 <head>
    <title><?php echo get_msg('RANKING-TITLE'); ?></title>
    <?php header_page_encoding(); ?>
    <meta http-equiv="expires" content="0">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache, must-revalidate"> 
  </head>
<?php include 'HTML-body.html' ; ?>
<?php topblock(); ?>

<?php

if ($_SESSION[ssig() . 'playerid'] == "") {$_SESSION[ssig() . 'playerid'] = 0;}

$_SESSION[ssig() . 'time_span'] = round($_GET['time_span']);
if ($_SESSION[ssig() . 'time_span'] < 1) {$_SESSION[ssig() . 'time_span'] = 30;}

if ($_GET['rank_display'] != 0) {
	$_SESSION[ssig() . 'rank_display'] = round($_GET['rank_display']);
}
if ($_SESSION[ssig() . 'rank_display'] < 1) {$_SESSION[ssig() . 'rank_display'] = 10;}

if ($_SESSION[ssig() . 'rank_display'] >= 10000) {
	$_SESSION[ssig() . 'rank_display_tag'] = "Tous les joueurs";
} else {
	$_SESSION[ssig() . 'rank_display_tag'] = "Que les voisins";
}

// debug
//echo "<br>_SESSION['rank_display']=".$_SESSION[ssig() . 'rank_display'];
//echo "<br>_SESSION['time_span']=".$_SESSION[ssig() . 'time_span'];
//echo "<br>_SESSION['playerid']=".$_SESSION[ssig() . 'playerid'];

function generateRanking_3() {
    $_SESSION[ssig() . 'state']=0;

    $last_player = get_last_player();
    if ($last_player == 0) {$last_player = -1;}
    
    $query =  "SELECT id FROM `Players`";
    $r =  @mysql_query($query) or die("Bug in generateRanking 1: $query");
    // echo $query;
    for ($i=0 ; $i<mysql_num_rows($r) ; $i++) {
		$id = mysql_result($r , $i , 0);
	// 	consolidate_stats($id);
    }
    
    //echo "<br>crit posted" . $_GET['crit'];
    $crit = "nb_tres";
    if ($_GET['crit'] != "") {
	$crit = $_GET['crit'];
    }
    
    if ($crit == "name") {$order = "ASC" ;} else  {$order = "DESC";}
    if (($crit != "name") AND
    	($crit != "honnor") AND
    	($crit != "nb_tres2") AND
    	($crit != "nb_tres")
    	) 
    	{return;}
    	
    //$query =  "SELECT name , honnor , credit , maxhonnor , nbplayed, level, id FROM `Players`  ORDER BY `$crit` " . $order;

    $timetag = " WHERE TO_DAYS(NOW()) - TO_DAYS(lastlogin) <= " . $_SESSION[ssig() . 'time_span'];
    $query =  "SELECT id, name, nb_tres, nb_tres2, lastlogin, perso_url, css_mus_url
		FROM `Players`  
		$timetag OR id=0
		ORDER BY `$crit` $order ";
		
		// ORDER BY `$crit` " . $order . ", eff DESC
    $r =  @mysql_query($query) or die("Bug in generateRanking 2: $query");
    
    // on cherche la pos du joueur courant
    for ($i=0 ; $i<mysql_num_rows($r) ; $i++) {
		$id = mysql_result($r , $i , 0);
		if ($_SESSION[ssig() . 'playerid']==$id) {
			$playerpos = $i;
			//echo "<br>find player =".$_SESSION[ssig() . 'playerid']." at pos=$playerpos";
			break;
		}
    }
    
    for ($i=0 ; $i<mysql_num_rows($r) ; $i++) {
		$id = mysql_result($r , $i , 0);
		$name = mysql_result($r , $i , 1);
		$nb_tres = mysql_result($r , $i , 2);
		$nb_tres2 = mysql_result($r , $i , 3);
		$last_login = mysql_result($r , $i , 4);
		$perso_url = mysql_result($r , $i , 5);
		$css_mus_url = mysql_result($r , $i , 6);
		$j = $i+1;

		if (abs($playerpos - $i) < $_SESSION[ssig() . 'rank_display']) {
			
 	  	if ($perso_url != "") {
			$fname = "<a  target='_BLANK' href=\"$perso_url\">" . $name . "</a>" ;
		} else {
			$fname = $name;
		}
	
	
		if ($css_mus_url != "-") {
			$tres_str = "<a href=\"jdm-mylittlemuseum.php?owner=$id\">" . $nb_tres . "</a>" ;
		} else {
			$tres_str = $nb_tres;
		}
	
		if ($last_player == $id) {
			$mail_tag = "<A href=\"jdm-mail.php\"><img src=\"pics/email-1.jpg\"  BORDER=0 alt=\"email\" ALIGN=ABSMIDDLE></A>";
		} else {
			$mail_tag = "";
		}
		if ($id == $_SESSION[ssig() . 'playerid']) {echo "<TR bgcolor=\"lightgreen\"  VALIGN=MIDDLE>";} else {echo "<TR  VALIGN=MIDDLE>";}
		if (test_any_playerp($id)=="Y") {
	  	  echo "<TH>$j<TH><s>$heart$fname$mail_tag</s><TH>$tres_str<TH>$nb_tres2\n";
	   	} 
		else {
	   		echo "<TH>$j<TH>$heart$fname$mail_tag<TH>$tres_str<TH>$nb_tres2\n";
	    }
    }
    }
}

?>

<div class="jdm-level1-block">
	
	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
    <?php echo get_msg('RANKING-PROMPT') . ": " .  get_msg('RANKING-DISPLAY-TREASURES') . " 
     <font size=-1>&gt; 
     <a href=\"generateRanking.php\">" .  get_msg('RANKING-DISPLAY-SCORES') . " </a></font>
     
     <font size=-1>&gt; 
     <a href=\"generateRanking-3.php\">" .   get_msg('RANKING-DISPLAY-PLAYERS') . " </a></font>
     "; 
     
     ?>
     </div>
	</div>

    <div class="jdm-login-block">
    <?php  loginblock(); ?>
    </div>
</div>

<div class="jdm-level2-block">

	<div class="jdm-ranking-block-shadowxxx">
    <div class="jdm-ranking-block">
   
      <?php 
   $GLOBALS[0] = $_SESSION[ssig() . 'rank_display_tag'];
   $GLOBALS[1] = $_SESSION[ssig() . 'time_span'];
   echo get_msg('RANKING-DISPLAY-TITLE'),' <br>';
   	echo get_msg('RANKING-DISPLAY-MENU'); ?>
   	<a href="generateRanking-3.php?time_span=365&rank_display=10000">1 <?php echo get_msg('RANKING-DISPLAY-YEAR');?></a> -
   	<a href="generateRanking-3.php?time_span=182&rank_display=10000">6 <?php echo get_msg('RANKING-DISPLAY-MONTHS');?></a> -
   	<a href="generateRanking-3.php?time_span=30&rank_display=10000">1 <?php echo get_msg('RANKING-DISPLAY-MONTH');?></a> - 
    <a href="generateRanking-3.php?time_span=15&rank_display=10000">15 <?php echo get_msg('RANKING-DISPLAY-DAYS');?></a> - 
    <a href="generateRanking-3.php?time_span=7&rank_display=10000">1 <?php echo get_msg('RANKING-DISPLAY-WEEK');?></a> -
    <a href="generateRanking-3.php?time_span=1&rank_display=10000">1 <?php echo get_msg('RANKING-DISPLAY-DAY');?></a>
    / <a href="generateRanking-3.php?time_span=<?php echo $_SESSION[ssig() . 'time_span']; ?>&rank_display=10"><?php echo get_msg('RANKING-DISPLAY-ONLY-NEIGHBOURS');?></a>
	<TABLE	border="1px"
		    width="100%"
		    cellspacing="0" cellpadding="0%"
		    summary="Ranking">
	<TR bgcolor="#EEF6F8"><TH>
	<TH <?php if ($_GET['crit']== "name") echo "bgcolor=\"lightgrey\"" ?>><a  href="generateRanking-3.php?crit=name"><?php echo get_msg('RANKING-LABEL-NAME'); ?></a>
	<TH <?php if ($_GET['crit']== "nb_tres") echo "bgcolor=\"lightgrey\"" ?>><a  href="generateRanking-3.php?crit=nb_tres"><?php echo get_msg('RANKING-LABEL-NBTREASURE'); ?></a>
	<TH <?php if ($_GET['crit']== "nb_tres2") echo "bgcolor=\"lightgrey\"" ?>><a  href="generateRanking-3.php?crit=nb_tres2"><?php echo "<img border=0 src=\"pics/JDM-bonus-dot.gif\">"; ?></a>
	
   <?php generateRanking_3() ?>
	</TABLE>	
    </div>
    </div>
    
   </div>

<div class="jdm-playas-block-1">
<?php produceplayasform(); ?>
</div>

<?php playerinfoblock($_SESSION[ssig() . 'playerid']) ?>
<?php 
    bottomblock();
    closeconnexion();
?>

  </body>
</html>
