<?php 
// debug
//

function help_consigne($relid) {
	if ($_SESSION[ssig() . 'PARAM-HELP-INSTRUCTIONS'] != 'Y') {
		return;
	}
	
    switch (true){
	    case ($relid == 3):
		// rdomaine
		help_consigne_domaine();
		break;
    }
}

function help_consigne_domaine() {
    
    $l = $GLOBALS[$_SESSION['LANG'] . '-'. 'GENGAME-FUNCTIONS-ARRAY-3'];
     

    echo "<br><font color=\"gray\" size= \"3\">";
    echo get_msg('GENGAME-FUNCTIONS-ARRAY-3-INSTR');
    echo ' </font>';
	    
	   
    format_list_to_table($l, 5);
}


function format_list_to_table($l, $nbcolumn) {
    
    echo "<P><table WIDTH=80% aLIGN=\"left\">";
    $nb = count($l);
    for ($i=0 ; $i<$nb ; $i++) {
	if (fmod($i,$nbcolumn)==0) {
	    echo "<TR><TH><font color=\"gray\" size= \"2\">";}
	else {
	    echo "<TH><font color=\"gray\" size= \"2\">";}
	    echo $l[$i];
	    echo "</font>";
    }
    echo "</table>";
}

?>
