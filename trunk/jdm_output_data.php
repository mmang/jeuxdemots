<?php session_start();?>
<?php include_once 'misc_functions.php'; ?>
<?php
    openconnexion();

?>
<html>
 <head>
    <title><?php echo "Outputing data"; ?></title>
    <?php header_page_encoding(); ?>
  </head>
<?php include 'HTML-body.html' ; ?>
<?php topblock(); ?>

<?php

function generate_output_stats() {
	
	$string = "";
	
	
	$string =  $string . "\n\n// ---- Relation stats\n";
	$query = "SELECT id, name, gpname FROM RelationTypes;";
	$r =  @mysql_query($query) or die("pb in generate_bd_stats 1: $query");
	$nb = mysql_num_rows($r);
	for ($i=0 ; $i<$nb ; $i++) {
	    $reltype = mysql_result($r , $i , 0);
	    $relname = mysql_result($r , $i , 1);
	    $relgpname = mysql_result($r , $i , 2);
	    $query = "SELECT COUNT(*) FROM Relations WHERE type = $reltype;";
	    $r2 =  @mysql_query($query) or die("pb in generate_bd_stats 2 : $query");
	    $count = mysql_result($r2 , 0 , 0);
	    $string =  $string .  "\n// $count occurrences of relations $relname (t=$reltype)";
	}

	$string =  $string .  "\n\n// ---- Node stats\n";
	$query = "SELECT id, name FROM NodeTypes;";
	$r =  @mysql_query($query) or die("pb in generate_bd_stats 3 : $query");
	$nb = mysql_num_rows($r);
	for ($i=0 ; $i<$nb ; $i++) {
	    $nodetype = mysql_result($r , $i , 0);
	    $nodename = mysql_result($r , $i , 1);
	    $query = "SELECT COUNT(*) FROM Nodes WHERE type = $nodetype;";
	    $r2 =  @mysql_query($query) or die("pb in generate_bd_stats 4 : $query");
	    $count = mysql_result($r2 , 0 , 0);
	    $string =  $string .  "\n// $count occurrences of nodes $nodename (t=$nodetype)";
	}
	
	$string =  $string .  "\n\n// ---- 50 most frequents terms\n";
	$query= "SELECT name, w FROM `Nodes`  ORDER BY `Nodes`.`w`  DESC limit 50";
	$r =  @mysql_query($query) or die("pb in display_wordrelation_list : $query");
	$nb = mysql_num_rows($r);
	for ($i=0 ; $i<$nb ; $i++) {
		$name = htmlentities(mysql_result($r , $i , 0));
		$w = mysql_result($r , $i , 1);
	    $string =  $string .  "\n// n=\"" . $name . "\":w=" . $w;
	}

	return($string);
}

function jdm_outputing_data() {

	$realdate = date('l dS \of F Y h:i:s A');
	
	$date  = date("mdY"); 
	$path = '/auto/lafourca/www-docs/JDM-LEXICALNET-FR/';
	$name = $date . '-LEXICALNET-JEUXDEMOTS-FR.txt';
	$filename = $path. $name;
	//$filename = '012007-LEXICALNET-JEUXDEMOTS-FR.txt';

	$entete = "//
// JEUXDEMOT LEXICAL NET
//
// Donn�es de JeuxDeMots. (c) Mathieu Lafourcade et LIRMM, 2008
// Data of JeuxDeMots. (c) Mathieu Lafourcade AND LIRMM, 2008
// 
//
// Filename: $name
// File avalable at: http://www.lirmm.fr/~lafourcade/JDM-LEXICALNET-FR/
// jeuxdeMots : http://www.lirmm.fr/jeuxdemots
//
// Licence CreativeCommons Paternit�-Partage des Conditions Initiales � l'Identique 2.0 France
// http://creativecommons.org/licenses/by-sa/2.0/fr/
// 
// Attribution-Share Alike 2.0 France
// http://creativecommons.org/licenses/by-sa/2.0/fr/deed.en
//
// 
// Aknowledgments
// Thank to all people who particpated to the design and development of JeuxDemots, namely:
// Gilles S�rasset, St�phane Riou, Alain Joubert, Mehdi Yousfi-Monod, Mathieu Mangeot and
// other people I might have forget.
// Many many thanks to all players of JeuxDeMots
//
// The POS (part of speech) definitions are partially from ABU (http://abu.cnam.fr). 
// Concepts (unused) - terms stating with c0,c1,c2,c3,c4 are inspired from thesaurus Larousse
//
//   Mathieu Lafourcade
//   $realdate
//
// Ma�tre de Conf�rence � L'Universit� Montpellier 2 - LIRMM - Equipe TAL
// 161, rue ADA  -  F-34392 Montpellier Cedex 5
// 33 (0)4 67 41 85 71 -- 33 (0)6 09 57 25 91
// mathieu.lafourcade@lirmm.fr
// http://www.lirmm.fr/jeuxdemots
// http://www.lirmm.fr/jeuxdemots/world-of-jeuxdemots.php
//
// STRUCTURE
//
// A node, for example eid=336:n=\"Astrakhan\":t=1:w=50
// has
// a unique identifier eid (entry id)
// a name n
// a type t (see  STATS for the list of node type)
// a weight w
//
// A relation, for example rid=133275:n1=141480:n2=18280:t=10:w=50
// has
// a unique identifier rid (relation id)
// a starting node id n1
// an ending node id n2
// a type t (see  STATS for the list of relation type)
// a weight w
//

";

//echo "rez = " . file_put_contents($filename,$entete);

//echo "CWD=" . getcwd() . "\n";
//exit;

    // Dans notre exemple, nous ouvrons le fichier $filename en mode d'ajout
    // Le pointeur de fichier est plac� � la fin du fichier
    // c'est l� que $somecontent sera plac�
    unlink($filename);
    if (!$handle = fopen($filename, 'a')) {
        echo "Impossible d'ouvrir le fichier ($filename)";
        exit;
    }
 	if (fwrite($handle, $entete) === FALSE) {
        echo "Impossible d'�crire dans le fichier ($filename)";
		exit;
   	}

    $entete_stats = "\n\n// -- STATS" 	;
 	fwrite($handle, $entete_stats);
   	$s = generate_output_stats();
   	echo "STATS= " . $s;
   	fwrite($handle, $s);
   	
   	
	$entete_nodes = "\n\n// -- NODES\n\n" 	;
 	fwrite($handle, $entete_nodes);
 	 
 	$start_id = 0;
	$end_id = 200000;
	$block_size = 1000;
	$nbblock = round(($end_id - $start_id) / $block_size) + 1;
	$k = 0;
	echo "(nodes ";
	for ($i=0 ; $i<$nbblock ; $i++) {
		$start = $i * $block_size;
		$end = $start + $block_size;
			//$query= "SELECT id,name,w FROM `Nodes`  WHERE type = 1 ORDER BY name ASC";
		$query = "SELECT id,name,w,type FROM Nodes WHERE id >= $start AND id < $end
			ORDER BY name ASC;
			";
    	$r =  @mysql_query($query) or die("pb jdm_outputing_data 1 : $query");
		//echo $query;
		//echo " --> "  . mysql_num_rows($r) . "\n";
		if (mysql_num_rows($r) >0) {
	 		for ($j=0 ; $j<mysql_num_rows($r) ; $j++) {
	 			//echo "$id ";
	 			$id = mysql_result($r , $j , 0);
				$name = htmlentities(mysql_result($r , $j , 1));
				$w = mysql_result($r , $j , 2);
				$type = mysql_result($r , $j , 3);
	   			// Ecrivons quelque chose dans notre fichier.
				$data = "eid=$id:n=\"$name\":t=$type:w=$w\n";
	 			if (fwrite($handle, $data) === FALSE) {
       				echo "Impossible d'�crire dans le fichier ($filename)";
	 			}		
	 		$k++;
	 		}
		 echo "block $i<br>";
		}
	}
	echo ")";
	
	 $entete_relations = "\n\n// -- RELATIONS\n\n" ;	
 	 fwrite($handle, $entete_relations);
	
 	$start_id = 0;
	$end_id = 1000000;
	$block_size = 1000;
	$nbblock = round(($end_id - $start_id) / $block_size) + 1;
	$k = 0;
	echo "(nodes ";
	for ($i=0 ; $i<$nbblock ; $i++) {
		$start = $i * $block_size;
		$end = $start + $block_size;
			//$query= "SELECT id,name,w FROM `Nodes`  WHERE type = 1 ORDER BY name ASC";
		$query = "SELECT id,node1,node2,type,w FROM Relations  WHERE id >= $start AND id < $end
			ORDER BY id ASC;
			";
    	$r =  @mysql_query($query) or die("pb jdm_outputing_data 2 : $query");
		//echo $query;
		//echo " --> "  . mysql_num_rows($r) . "\n";
		if (mysql_num_rows($r) >0) {
	 		for ($j=0 ; $j<mysql_num_rows($r) ; $j++) {
	 			//echo "$id ";
	 			$id = mysql_result($r , $j , 0);
				$node1 = mysql_result($r , $j , 1);
				$node2 = mysql_result($r , $j , 2);
				$type = mysql_result($r , $j , 3);
				$w = mysql_result($r , $j , 4);
	   			// Ecrivons quelque chose dans notre fichier.
				$data = "rid=$id:n1=$node1:n2=$node2:t=$type:w=$w\n";
	 			if (fwrite($handle, $data) === FALSE) {
       				echo "Impossible d'�crire dans le fichier ($filename)";
	 			}		
	 		$k++;
	 		}
		 echo "block $i<br>";
		}
	}
	echo ")";
 	 
 	 
 	 
 	 
 	 $epilogue = "\n\n//EOF" ;	
 	 fwrite($handle, $epilogue);
 	 
 	 echo "L'�criture de ($somecontent) dans le fichier ($filename) a r�ussi";
	 fclose($handle);
}


jdm_outputing_data()


?>




<?php 
    bottomblock();
    closeconnexion();
?>

  </body>
</html>
