<?php session_start();?>
<?php include 'misc_functions.php' ; ?>
<?php openconnexion(); ?>
<html>
 <head>
    <title>JeuxDeMots : achat de jetons</title>
    <?php header_page_encoding(); ?>
  </head>
  <body>
<?php include 'HTML-body.html' ; ?>
<?php topblock(); ?>
<?php check_referer(); ?>

<?php

function addtoken() {
	
	if ($_SESSION[ssig() . 'LASTACTION'] == "addtoken")
		{return;}
	$_SESSION[ssig() . 'LASTACTION'] = "addtoken";
	
	
    $login = $_SESSION[ssig() . 'login'];
    if ($login == "") {$login = "guest";}
    $playerid = $_SESSION[ssig() . 'playerid'];
    $gameid = $_SESSION[ssig() . 'gameid'];
    $entry = $_SESSION[ssig() . 'gameentry'];

    
    if ($playerid == 0) {
    	//echo "<h1>D�sol� le joueur invit� ne peut pas acheter des jetons. Si vous lisez ceci, c'est que la session a du expirer</h1>";
    	echo "<div class=\"jdm-notice1-block\"><div class=\"jdm-notice1\">";
    	echo get_msg('BUYTOKEN-CHEAT');
    	echo "\n</div>\n</div>";
    	return;
    }
    
    $nbtoken =0;
    if ($_POST["submit5"] != "") 
	$nbtoken =5;
    if ($_POST["submit10"] != "") 
	$nbtoken =10;
    if ($_POST["submit20"] != "") 
	$nbtoken =20;

   // echo "<br> nb token " . $nbtoken;
    $cost = $nbtoken * $_SESSION[ssig() . 'player_mgain'];

    if (get_player_credit($playerid) >= $cost) {
    	$query = "SELECT token From `PendingGames` WHERE id= '$gameid';" ;
    	$r =  @mysql_query($query) or die("pb in addtoken: $query");
    	$token = mysql_result($r , 0 , 0);
    	
    	$token = min(10, $token+$nbtoken);
    	
		$query = "UPDATE `PendingGames` SET token = $token WHERE id= '$gameid';" ;
       // echo $query;
		$r =  @mysql_query($query) or die("pb in addtoken");
		inc_player_credit($playerid, -$cost);
   		//echo "<h1>Bravo pour cet investissement dans $nbtoken jetons pour un montant de $cost cr�dits.</h1>";
		$GLOBALS[0]=$nbtoken;
		$GLOBALS[1]=$cost;
		echo "<div class=\"jdm-notice1-block\"><div class=\"jdm-notice1\">";
		echo get_msg('BUYTOKEN-CONGRAT-INVEST');
		echo "\n</div>\n</div>";
		
		$fentry = format2_entry($entry);
		//make_event("le joueur '$login' a investi dans le mot <b>'$fentry'</b>.");
		$GLOBALS[0]=$login;
		$GLOBALS[1]=$fentry;
    	make_event(get_msg('BUYTOKEN-EVENT'));
    	
    } else {
		//echo "<h1>D�sol�, vous n'avez pas cette somme de $cost cr�dits, il fallait v�rifier avant :)</h1>";
		$GLOBALS[0]=$cost;
		echo "<div class=\"jdm-notice1-block\"><div class=\"jdm-notice1\">";
		echo get_msg('BUYTOKEN-SORRY');
		echo "\n</div>\n</div>";
    }
}

?>

<div class="jdm-level1-block">
	
	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
    </div>
    <?php if ($_SESSION[ssig() . 'FRAUD'] == true) {
    	echo " :: acc�s frauduleux...";
    }
    ?>
	</div>
    <div class="jdm-login-block">
    <?php  loginblock(); ?>
    </div>
</div>

<div class="jdm-level2-block">
<TABLE	border="0" width="100%"
		cellspacing="0" cellpadding="0%"
		summary="JeuxDeMots">
    
<TR><TH bgcolor="#EEF6F8" align="rigth" width="200">
    <TH bgcolor="#EEF6F8" colspan="2" align="left">
	<div class="jdm-instruction">
	</div>
	
<TR><TH>
    <TH align="left">
    <div class="jdm-prompt-2">
    <?php
	addtoken()
    ?>	
    </div>

</TABLE>
<P>
</div>
<div class="jdm-playas-block-3">
<?php produceplayasform(); ?>
</div>

<?php playerinfoblock($_SESSION[ssig() . 'playerid']) ?>
<?php 
    bottomblock();
    closeconnexion();
?>

  </body>
</html>
