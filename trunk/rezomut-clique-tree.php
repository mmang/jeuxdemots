<?php session_start();?>
<?php include_once 'misc_functions.php'; ?>
<?php
    openconnexion();
	$_SESSION[ssig() . 'redirect_if_session_finished'] = 'N';
?>
<html>
 <head>
    <title><?php echo "Arbre de  cliques"; ?></title>
    <?php header_page_encoding(); ?>
  </head>
<?php include 'HTML-body.html' ; ?>
<?php topblock(); ?>

<?php



function rezomut_make_wordrelation_form() {

	//echo "'". $_POST['gotermrel'] . "'";
	//echo "'". $_GET['gotermrel'] . "'";
	
	$term = trim($_POST['gotermrel']);
	if ($term == "") {
		$term = trim($_GET['gotermrel']);	
	}
	if ($_POST['mat_diplay']) {$mat_tag = 'checked';} else {$mat_tag = '';}
	if ($_POST['cliq_diplay']) {$cliq_tag = 'checked';} else {$cliq_tag = '';}

	$_SESSION[ssig() . 'clique_target'] = $term;
	echo "<form id=\"gotermrel\" name=\"gotermrel\" method=\"post\" action=\"rezomut3.php\" >
	    <input id=\"gotermsubmit\" type=\"submit\" name=\"gotermsubmit\" value=\"Chercher\"> le mot
	    <input  id=\"gotermrel\" type=\"text\" name=\"gotermrel\" value=\"$term\" size=70>
	    <INPUT $mat_tag type=checkbox name=\"mat_diplay\" value=\"mat_diplay\"> matrice poids
	    <INPUT $cliq_tag type=checkbox name=\"cliq_diplay\" value=\"cliq_diplay\"> matrice clique
	    </form>";
}

function rezomut_make_fusion_form() {
	echo "<form id=\"fusion_form\" name=\"gotermrel\" method=\"post\" action=\"rezomut3.php\" >
	    <input id=\"clique_fusion\" type=\"submit\" name=\"clique_fusion\" value=\"Fusionner\"> la clique
	    <input  id=\"clique1\" type=\"text\" name=\"clique1\" value=\"\" size=5> et
	    <input  id=\"clique2\" type=\"text\" name=\"clique2\" value=\"\" size=5>
	    <input  id=\"cliq_diplay\" type=\"hidden\" name=\"cliq_diplay\" value=\"1\" size=5>
	    </form>";
}

function rezomut_make_clique_tree_form() {
	echo "<form target=\"blank\" id=\"tree_form\" name=\"tree_form\" method=\"post\" action=\"rezomut3.php\" >
	    <input id=\"clique_tree\" type=\"submit\" name=\"clique_tree\" value=\"G�n�rer l'arbre\"> 
	    </form><P><br>";
}


function process_forms() {
	if (($_POST['gotermsubmit']!= "") || ($_GET['gotermrel']!= "")){
		$term = trim($_POST['gotermrel']);
		if ($term == "") {
			$term = trim($_GET['gotermrel']);	
		}
		rezomut_display_wordrelation_list($term);
		flush();
		display_cliques_pages();
		flush();
	}
	
	if ($_POST['clique_fusion']!= ""){
		$i = trim($_POST['clique1']);
		$j = trim($_POST['clique2']);
		//echo "<br>i=$i j=$j";
		$t = array_keys($_SESSION[ssig() . 'mat_clique']);
		$c_1 = $t[$i];
		$c_2 = $t[$j];
		fusion_cliques($c_1, $c_2);
		display_cliques_pages();
		flush();
	}
}


function compute_term_list_id($node1_id) {
	$tab = array();
	array_push($tab,$node1_id);
	
	 $query= "SELECT distinct(node2) from Relations WHERE
			node1=$node1_id
		    AND Relations.type in (0, 5, 6, 3, 8)";
	 $r =  @mysql_query($query) or die("pb in compute_term_list($node1_id)  : $query");
	 $nb = mysql_num_rows($r);
	 for ($i=0 ; $i<$nb ; $i++) {
		$node2 = mysql_result($r , $i , 0);
		//echo "<br>node2 = $node2";
		$query2= "SELECT count(id) from Relations WHERE
			node1=$node2 and node2=$node1_id
		    AND Relations.type in (0, 5, 6, 3, 8)";	
		$r2 =  @mysql_query($query2) or die("pb in compute_term_list($node1_id)  : $query");
	 	$count = mysql_result($r2 , 0 , 0);
		if (($count > 0) && ($node2 > 0) && ($node2 != '')) {
			//echo "<br>node2 = $node2 KEEP = " . get_term_from_id($node2);
			if ((array_search($node2, $tab) == false) && ($node2 != '')) {
		 		array_push($tab,$node2);
			}
		 }
	}
	
	$tab = array_merge(array_unique($tab));
//	print_r($tab);
//	flush();
	return($tab);
}



function rezomut_display_wordrelation_list($term) {
    //echo "<P>in display_wordrelation_list value :" . $_POST['gotermsubmit'] . "--" . $_POST['gotermrel'];
  	
	$rel=$_POST['relation'];
	if ($rel == '') {$rel = 0;}
	$type = $rel; 
  		
  	$constraint=$_POST['constraint'];
  	$strictp = (($constraint == "strict")|| ($constraint == ''));
	
  	if (($_POST['gotermsubmit']!= "") || ($_GET['gotermrel']!= "")){
	$id = term_exist_in_BD_p($term);
	if ($id == 0) {display_warning("<br>Le terme $term n'existe pas !");}
	    else {
	    $_SESSION[ssig() . 'mat_term'] = '';
	    $_SESSION[ssig() . 'mat_id'] = '';	
	    $_SESSION[ssig() . 'mat_weight'] = '';
	    $_SESSION[ssig() . 'mat_size'] = 0;
	    
	  	$term_list_id = compute_term_list_id($id);
	   
	    
	    for ($i=0 ; $i<count($term_list_id) ; $i++) {
		//	echo "<br>term = " . get_term_from_id($term_list_id[$i]) . " " . $term_list_id[$i];
	   	//	flush();
	    	$_SESSION[ssig() . 'mat_term'][$_SESSION[ssig() . 'mat_size']] = get_term_from_id($term_list_id[$i]);
	     	$_SESSION[ssig() . 'mat_id'][$_SESSION[ssig() . 'mat_size']] = $term_list_id[$i];
	     	$_SESSION[ssig() . 'mat_size'] = $_SESSION[ssig() . 'mat_size']+1;	     		
		}
	  
	    $_SESSION[ssig() . 'mat_total_weight'] = 0;
	    $_SESSION[ssig() . 'mat_missing_links'] = 0;
	    
	    // on remplie la matrice de poids
	   	compute_weight_matrix($type);
	     
	    $_SESSION[ssig() . 'mat_clique'] = '';
	    $_SESSION[ssig() . 'mat_col'] = '';
	    $_SESSION[ssig() . 'mat_ligne'] = '';
	  
	    compute_cliques2();	   
    }
  }
}


function consolidate_clique_ligne($line) {
	//echo "<br>validate_clique($line)";
	$clique1 = $_SESSION[ssig() . 'mat_ligne'][$line];
	$clique2 = $_SESSION[ssig() . 'mat_col'][$line];
	$newclique = intersect_cliques($clique1, $clique2);
	return $newclique;
}


function compute_cliques2() {
	start_time_record('compute_cliques3');
	
	$_SESSION[ssig() . 'mat_cand_clique'] = array();
	$_SESSION[ssig() . 'mat_result_clique'] = array();
	$_SESSION[ssig() . 'mat_done_clique'] = array();
	
	$_SESSION[ssig() . 'clique_status'] = array();
	
	for ($i=1 ; $i< $_SESSION[ssig() . 'mat_size'] ; $i++) {
	      	 make_ligne_potclique($i);
	      	 make_col_potclique($i);	    	
	}
	
 	for ($i=1 ; $i< $_SESSION[ssig() . 'mat_size'] ; $i++) {
		$newclique = consolidate_clique_ligne($i);
		//echo "<br> consolidated $newclique";
		if (is_clique_in_cand_p($newclique) === false) {
	    	insert_candidate_clique($newclique);
		}
	}
	
	//print_r($_SESSION[ssig() . 'mat_cand_clique']);
	
	do {
		$clique = array_shift($_SESSION[ssig() . 'mat_cand_clique']);
		//echo "<br>exploring $clique";
		explore_cand_clique($clique);
		
		//echo "<br>  SIZCAND=" . count($_SESSION[ssig() . 'mat_cand_clique']);
	} while (count($_SESSION[ssig() . 'mat_cand_clique']) > 0);
    
    $duree=end_time_record('compute_cliques3');
    echo "<br>computing cliques with compute_cliques 3() : $duree ";
    flush();
   // print_r($_SESSION[ssig() . 'mat_result_clique']);
}


function skip_clique($clique) {
	if (is_clique_in_result_p($clique) === false) {} else {//echo "<BR>$clique already in result" ; 
		return true;}
	if (is_clique_in_done_p($clique) === false) {} else {//echo "<BR>$clique already processed" ;
		 return true;}		
	if (is_clique_subclique_result_p($clique) === true) {//echo "<BR>$clique contained in result" ; 
		insert_done_clique($clique);
		return true;}	
	return false;
}

function explore_cand_clique($clique) {
	// deja dans les resultats
	if (skip_clique($clique) === true) {
		return;}
	
	// c'est une clique ?
	if (check_true_clique_p($clique)) {
		insert_result_clique($clique);
		//echo "<BR>$clique new clique found!" ;
		return;
	}
	// il faut chercher
	//echo "<br>LOOKING in  clique : $clique";
	for ($i=1 ; $i< strlen($clique) ; $i++) {
		$val = $clique[$i];
		if ($val == 1) {
			$newclique = clique_set_0_nth_bit($clique, $i);
			if ((is_clique_in_cand_p($newclique) === false) && (skip_clique($newclique) === false)) {
				//echo "<br>inserting CANDIDATE clique : $newclique";
				insert_candidate_clique($newclique);
			}
		}
	}
	insert_done_clique($clique);
}

function make_ligne_potclique ($line) {
	//echo "<br>make_ligne_potclique ($line)";
	$clique = '';
	 for ($j=0 ; $j< $_SESSION[ssig() . 'mat_size'] ; $j++) {
	 	$w1 = $_SESSION[ssig() . 'mat_weight'][$line][$j];
	 	$w2 = $_SESSION[ssig() . 'mat_weight'][$j][$line];
	 	//echo "$w ";
	 	if (($w1 > 0) && ($w2 > 0)) {
	 		$clique = $clique . '1'	;
	 	} else {
	 		$clique = $clique . '0'	;
	 	}	 	
	 }
	 //echo "== $clique";
	 $_SESSION[ssig() . 'mat_ligne'][$line]=$clique;
}

function make_col_potclique ($line) {
	//echo "<br>make_col_potclique ($line)";
	$clique = '';
	 for ($j=0 ; $j< $_SESSION[ssig() . 'mat_size'] ; $j++) {
	 	$w1 = $_SESSION[ssig() . 'mat_weight'][$j][$line];
	 	$w2 = $_SESSION[ssig() . 'mat_weight'][$line][$j];
	 	//echo "$w ";
	 	if (($w1 > 0) && ($w2 >0)) {
	 		$clique = $clique . '1'	;
	 	} else {
	 		$clique = $clique . '0'	;
	 	}	 	
	 }
	 //echo "== $clique";
	 $_SESSION[ssig() . 'mat_col'][$line]=$clique;
}


function intersect_cliques($c_1, $c_2) {
	//echo "<br>fusion_cliques($c_1, $c_2)" ;
	$l1 = strlen($c_1);
	$l2 = strlen($c_2);
	$lmax = max($l1, $l2); /// en fait c'est pareil
	
	$clique_inter = "";
	for ($i=0 ; $i<$lmax ; $i++) {
		$clique_inter = $clique_inter . $c_1[$i]*$c_2[$i];
	}
	return $clique_inter;
}

function sub_clique($cli_a, $cli_b) {
	//echo "<br>====> testing of sub_clique: is $cli_a in $cli_b ?";
	$nb = strlen($cli_a);
	for ($i=0 ; $i< $nb ; $i++) {
		//$val = substr($cli_a, $i, 1);
		$val1 = $cli_a[$i];
		$val2 = $cli_b[$i];
		//echo "val1=$val1/val2=$val2";
		if (($val1 == 1) && ($val2 != 1)) {
			return false;
		}
	}
	//echo " ==> true";
	return true;
}



function display_cliques_pages() {
	
	echo"<P>TERMES";
	display_matrix_terms();
	flush();	
	if ($_POST['mat_diplay']) {
	echo"<P>MATRICE DE POIDS";
		display_weight_table();
		flush();
	}
	echo"<P>CLIQUES";
	display_clique_table();
	flush();
	if ($_POST['cliq_diplay']) {
	echo"<P>SIMILARITE";
		display_sim_table($_SESSION[ssig() . 'mat_clique']);
		flush();
	}
}

function compute_weight_matrix ($type) {
	start_time_record('compute_weight_matrix');
	//$chunk = "AND type = $type";
	$chunk = "AND type in (0, 5, 6, 3, 8)";
  	if ($type == -1) {
		$chunk = "";
	}
	//print_r($_SESSION[ssig() . 'mat_id']);
	//print_r($_SESSION[ssig() . 'mat_term']);
	//flush();
	
	// on remplie la matrice de poids
	    for ($i=0 ; $i< $_SESSION[ssig() . 'mat_size'] ; $i++) {
	     	 for ($j=0 ; $j< $_SESSION[ssig() . 'mat_size'] ; $j++) {
	     	 	if ($i != $j) {
	     	 	$n1id = $_SESSION[ssig() . 'mat_id'][$i];
	     	 	$n2id = $_SESSION[ssig() . 'mat_id'][$j];
	     	 	$query= "SELECT sum(w) FROM Relations Where 
					node1 = $n1id  AND node2 = $n2id $chunk";
				$r =  @mysql_query($query) or die("pb in compute_weight_matrix : $query");
				$val = mysql_result($r , 0 , 0);
				if ($val > 0){
					//$t1 = $_SESSION[ssig() . 'mat_term'][$i];
					//$t2 = $_SESSION[ssig() . 'mat_term'][$j];
					//echo "<br>lien entre $t1 $val $t2",
					$_SESSION[ssig() . 'mat_total_weight'] = $_SESSION[ssig() . 'mat_total_weight'] + $val;
				} else {
					$_SESSION[ssig() . 'mat_missing_links'] = $_SESSION[ssig() . 'mat_missing_links'] + 1;
				}
				$_SESSION[ssig() . 'mat_weight'][$i][$j] = $val;
			}
	     		if ($i == $j) {
					$_SESSION[ssig() . 'mat_weight'][$i][$j] = 1;
				}
	     	 }
	     }
	$duree = end_time_record('compute_weight_matrix');     
	echo "<br>compute_weight_matrix : $duree ";
    flush();
}

function get_term_weight($term) {
	$query = "SELECT w FROM `Nodes` WHERE `name` = \"$term\"";
	$r =  @mysql_query($query) or die("bug in get_term_weight : $query");
	return mysql_result($r , 0 , 0);
}


function display_clique_table() {
	$t = $_SESSION[ssig() . 'mat_result_clique'];
	$nb = count($t);
	for ($i=0 ; $i< $nb ; $i++) {
		
		$elt = $t[$i];
		if (is_clique_in_result_p($elt)){
		
			compute_clique_weight($elt);
		
			echo "<br> $i: ";
			display_clique($elt);
		
		//$tw = $_SESSION[ssig() . 'mat_clique'][$elt]['totw'];
		//$nbl = $_SESSION[ssig() . 'mat_clique'][$elt]['nblinks'];
		//$moy = round($_SESSION[ssig() . 'mat_clique'][$elt]['moy']);
		//$conf = round($_SESSION[ssig() . 'mat_clique'][$elt]['conf']);
			echo " <font size=1>(P = $tw / nl = $nbl / moy = $moy / REL = $conf)</font> ";
		}
	}
}

function display_matrix_terms() {
	$nb = $_SESSION[ssig() . 'mat_size'];
	for ($i=0 ; $i< $nb ; $i++) {
		//echo "<TD><center>" . $_SESSION[ssig() . 'mat_term'][$i] . "</center>";
		echo " <SMALL>$i:" .  $_SESSION[ssig() . 'mat_term'][$i]. " - </SMALL>";
	}
}

function display_weight_table() {
	//$table = $_SESSION[ssig() . 'mat_weight'];
	//print_r($_SESSION[ssig() . 'mat_weight']);
	$nb = $_SESSION[ssig() . 'mat_size'];
	if ($nb <= 1) {
		echo "<P>Aucune clique";
		return;
	}
	
	echo "<span style=\"font-size:10\"><TABLE border=1><TR><TD>";
	for ($i=0 ; $i< $nb ; $i++) {
		//echo "<TD><center>" . $_SESSION[ssig() . 'mat_term'][$i] . "</center>";
		echo "<TD><SMALL><center>" . $i . "</center></SMALL>";
	
	}
	for ($i=0 ; $i<$nb ; $i++) {
		$poids = get_term_weight($_SESSION[ssig() . 'mat_term'][$i]);
		echo "<TR><TD><SMALL>" . $i . ": " . $_SESSION[ssig() . 'mat_term'][$i] . " ($poids)</SMALL>";
		for ($j=0 ; $j< $nb ; $j++) {
			
			$w = $_SESSION[ssig() . 'mat_weight'][$i][$j];
			//$w = round($sim,2);
			if ($w > 0) {
				if ($_SESSION[ssig() . 'mat_weight'][$j][$i]> 0) {
					echo "<TD BGCOLOR=\"99FF99\"><SMALL><center>$w</center></SMALL>";
				} else {
					echo "<TD BGCOLOR=\"FFFF66\"><SMALL><center>$w</center></SMALL>";
				}
			} else {
				if ($i==$j) {
					echo "<TD BGCOLOR=\"99FF99\"><SMALL><center>1</center>";
				} else {
					echo "<TD>";
				}
			}
		}
	}
	echo "</TABLE></span>";
	flush();
}


function display_sim_table($table) {
	$t = array_keys($table);
	$nb = count($t);
	echo "<P><TABLE border=1><TR><TD>";
	for ($i=0 ; $i< $nb ; $i++) {
		echo "<TD><center>$i</center>";
	}
	for ($i=0 ; $i< $nb ; $i++) {
		echo "<TR><TD>$i";
		for ($j=0 ; $j< $nb ; $j++) {
		
		if ($i==$j) 
		{$sim = 1;} else {
			$c_1 = $t[$i];
			$c_2 = $t[$j];
			$sim = compare_cliques_2($c_1, $c_2);
			$sim = round($sim,2);
		}
		echo "<TD><tt><center>$sim</center></tt>";
		}
	}
	echo "</TABLE>";
	
	//echo'<P>T==';
	//print_r($t);
	$tbis = array_keys($_SESSION[ssig() . 'mat_clique']);
	$nb_bis = count($tbis);
	
	//echo'<P> TBIS ==';
	//print_r($tbis);
	
	
	for ($i=0 ; $i < $nb_bis ; $i++) {
		$c = $tbis[$i];
		//echo "<br>unseting $c";
		//$val = array_search($c,$t);
		if ($i >= $nb) {
			//echo "<br>$i: $c  ==> deleted";
			unset($_SESSION[ssig() . 'mat_clique'][$c]);
		} else {
			//echo "<br>$i: $c ==> kept";
		}
	}
}

function display_clique($clique) {
	$nb = strlen($clique);
	//$code = bindec($clique);
	//echo " $code ";
	for ($i=0 ; $i< $nb ; $i++) {
		$val = substr($clique, $i, 1);
		if ($val == "1"){
			$term = $_SESSION[ssig() . 'mat_term'][$i];
			echo "'$term' ";
		}
	}
	
}


function compare_cliques_2($c1, $c2) {
	$c_1 = "$c1";
	$c_2 = "$c2";
	$l1 = strlen($c_1);
	$l2 = strlen($c_2);
	$nb = max($l1, $l2); /// en fait c'est pareil
	
	//if ($c_1 == $c_2) {return 1;}
	//echo "<br>$c_1<br>$c_2 toto";
	//echo "test=" . '' . clique_return_nth_bit($c_1,3). ' ';
	
	$som_tot = 0;
	$sub1 = 0;
	$sub2 = 0;
	
	$target_horiz = 0;
	$target_vert = 0;
	$target_horiz_inter = 0;
	$target_vert_inter = 0;
	
	$inter = 0;
	
	for ($i=0 ; $i<$nb ; $i++) {
		for ($j=0 ; $j<$nb ; $j++) {
			$term_clic1_i = $c_1[$i]; $term_clic1_j = $c_1[$j];
			$term_clic2_i = $c_2[$i]; $term_clic2_j = $c_2[$j];
			
			//echo "<br>c$c_1 A=$term_clic1_i B=$term_clic1_j c=$term_clic2_i d=$term_clic2_j";
			
			//$_SESSION[ssig() . 'mat_weight'][$i][$j]
			$poids = $_SESSION[ssig() . 'mat_weight'][$i][$j];
			if ($i == $j) {$poids = 0;}
			//echo "<br>poids=$poids";
			
			if ((($term_clic1_i == 1) || ($term_clic2_i == 1)) && (($term_clic1_j == 1) || ($term_clic2_j == 1))){
				$som_tot = $som_tot	+ $poids;
			//	echo "<br>poids=$poids";
			}
			
			if ((($term_clic1_i == 1) && ($term_clic2_i == 0)) && (($term_clic1_j == 1) && ($term_clic2_j == 0))) {
				$sub1 = $sub1 + $poids;
			}

			if ((($term_clic2_i == 1) && ($term_clic1_i == 0)) && (($term_clic2_j == 1) && ($term_clic1_j == 0))) {
				$sub2 = $sub2 + $poids;
			}
			
			if (($i == 0) && (($term_clic1_j == 1) || ($term_clic2_j == 1))) {
				$target_horiz = $target_horiz + $poids;
			}
			if (($j == 0) && (($term_clic1_i == 1) || ($term_clic2_i == 1))) {
				$target_vert = $target_vert + $poids;
			}
			
			if (($i == 0) && (($term_clic1_j == 1) && ($term_clic2_j == 1))) {
				$target_horiz_inter = $target_horiz_inter + $poids;
			}
			if (($j == 0) && (($term_clic1_i == 1) && ($term_clic2_i == 1))) {
				$target_vert_inter = $target_vert_inter + $poids;
			}
			
		}
	}
	
	
	$num = $som_tot - $sub1 - $sub2 - $target_horiz - $target_vert + $target_horiz_inter + $target_vert_inter;
	//if ($num >= $som_tot) {return 0;}

	$denum = $som_tot;
	
	//echo "<br>denum= $denum num=$num";
	$sim = $num / $denum;
	
	return $sim;
}


function compare_cliques($c_1, $c_2) {
	$l1 = strlen($c_1);
	$l2 = strlen($c_2);
	$lmax = max($l1, $l2); /// en fait c'est pareil
	
	$sum_inter = 0;
	$sum_c1 = 0;
	$sum_c2 = 0;

	//echo "lmax $lmax";
	$clique_inter = "";
	for ($i=0 ; $i< $lmax ; $i++) {
		$val1 = substr($c_1, $i, 1);
		$val2 = substr($c_2, $i, 1);
		if (($val1 == "1") && ($val2 == "1")) {
			$clique_inter = $clique_inter . "1";
		} else {
			$clique_inter = $clique_inter . "0";
		}
	}
	//echo $clique_inter . "<br>";
	compute_clique_weight($clique_inter);
	$inter_w = $_SESSION[ssig() . 'mat_clique'][$clique_inter]['totw'];
	$w1 = $_SESSION[ssig() . 'mat_clique'][$c_1]['totw'];
	$w2 = $_SESSION[ssig() . 'mat_clique'][$c_2]['totw'];
	
	$sim = $inter_w / sqrt($w1 * $w2);
	//echo "unseting:" . $_SESSION[ssig() . 'mat_clique'][$clique_inter];
	//unset($_SESSION[ssig() . 'mat_clique'][$clique_inter]);
	return $sim;
}

function fusion_cliques($c_1, $c_2) {
	//echo "<br>fusion_cliques($c_1, $c_2)" ;
	$l1 = strlen($c_1);
	$l2 = strlen($c_2);
	$lmax = max($l1, $l2); /// en fait c'est pareil
	
	$sum_inter = 0;
	$sum_c1 = 0;
	$sum_c2 = 0;

	//echo "lmax $lmax";
	$clique_union = "";
	for ($i=0 ; $i< $lmax ; $i++) {
		$val1 = substr($c_1, $i, 1);
		$val2 = substr($c_2, $i, 1);
		if (($val1 == 1) || ($val2 == 1)) {
			$clique_union = $clique_union . '1';
		} else {
			$clique_union = $clique_union . '0';
		}
	}
	//echo "fusion cliques  ==> " . $clique_union . "<br>";
	unset($_SESSION[ssig() . 'mat_clique'][$c_1]);
	unset($_SESSION[ssig() . 'mat_clique'][$c_2]);
	compute_clique_weight($clique_union);
	
	insert_result_clique($clique_union);
	
	insert_done_clique($c_1);
	insert_done_clique($c_2);
}

function check_true_clique_p($clique) {
	$nb = strlen($clique);
	// echo "<br>strlen($clique)=" . strlen($clique);
	for ($i=1 ; $i<$nb ; $i++) {
		if ($clique[$i] == 1) {
			for ($j=1 ; $j<$nb ; $j++) {
				if ($clique[$j] == 1) {
					//echo "<br> $i-$j == " . $_SESSION[ssig() . 'mat_weight'][$i][$j];
					//echo "<br> $j-$i == " . $_SESSION[ssig() . 'mat_weight'][$j][$i];
				
					if ((($_SESSION[ssig() . 'mat_weight'][$i][$j] == 0) || ($_SESSION[ssig() . 'mat_weight'][$j][$i] == 0))) {
					//	echo " FALSE";
						return false;
					}
				}
		}
	}
	}
	return true;
}

// return true if a = b
function same_clique($cli_a, $cli_b) {
	$nb = strlen($cli_a);
	for ($i=0 ; $i<$nb ; $i++) {
		//$val1 = $cli_a[$i];
		//$val2 = $cli_b[$i];
		//echo "val1=$val1/val2=$val2";
		if ($cli_a[$i] == $cli_b[$i]) {
			
		} else {
			return false;
		}
	}
	//echo " ==> true";
	return true;
}

function my_array_search ($cand, $tab) {
	$nb = count($tab);
	for ($i=0 ; $i<$nb ; $i++) {
		$elt = $tab[$i];
		if (same_clique($cand, $elt) === true) {
		/*	echo "<BR> FOUND : $i ==> ";
			echo "<BR> cand : $cand ";
			echo "<BR> elt : $elt ";*/
			return $i;
		}
	}
	return false;
}
function is_clique_in_cand_p($clique) {
	return ($_SESSION[ssig() . 'clique_status'][$clique] == 1);
	/*$val = my_array_search($clique, $_SESSION[ssig() . 'mat_cand_clique']);
	if (same_clique($clique, '100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001')
		=== true)
	{
		if ($val != false) {
			$len = strlen($clique);
			echo "<BR> TEST CLIQUE TESTED l=$len val=$val : $clique";
			echo "<BR> ELEMENT FOUND = " . $_SESSION[ssig() . 'mat_cand_clique'][$val];
		} else {
			echo "<BR> TEST CLIQUE TESTED NOT FOUND l=$len val=$val : $clique";
		}
	}
	//echo "<br>is_clique_in_result_p($clique) =>" . array_search($clique, $_SESSION[ssig() . 'mat_result_clique']);
	return $val; */
}

function insert_candidate_clique($clique) {
	$_SESSION[ssig() . 'clique_status'][$clique] = 1;
	array_push($_SESSION[ssig() . 'mat_cand_clique'], $clique);
}

function is_clique_in_result_p($clique) {
	//echo "<br> TEST IN RESULT : $clique"; print_r($_SESSION[ssig() . 'mat_result_clique']);
	//echo "<br>is_clique_in_result_p($clique) =>" . array_search($clique, $_SESSION[ssig() . 'mat_result_clique']);
	//return (my_array_search($clique, $_SESSION[ssig() . 'mat_result_clique'])); 
	
	return ($_SESSION[ssig() . 'clique_status'][$clique] == 3);
}

function is_clique_subclique_result_p($clique) {
	$nb = count($_SESSION[ssig() . 'mat_result_clique']);
	for ($i=0 ; $i<$nb ; $i++) {
		$elt = $_SESSION[ssig() . 'mat_result_clique'][$i];
		$test = sub_clique($clique, $elt);
		if ($test == true) {return true;}
	}
	return false;
}

function insert_result_clique($clique) {
	$_SESSION[ssig() . 'clique_status'][$clique] = 3;
	//echo "<br>INSERTING RESULT : $clique";
	$nb = array_push($_SESSION[ssig() . 'mat_result_clique'], $clique);
	//echo "   --- $nb cliques trouv�es";
}

function is_clique_in_done_p($clique) {
	return ($_SESSION[ssig() . 'clique_status'][$clique] >= 2);
	//return (my_array_search($clique, $_SESSION[ssig() . 'mat_done_clique'])); 
}

function insert_done_clique($clique) {
	$_SESSION[ssig() . 'clique_status'][$clique] = 2;
	//echo "<br> INSERTING IN DONE : $clique";
	//array_push($_SESSION[ssig() . 'mat_done_clique'], $clique);
}

function clique_make($size) {
	return str_repeat ('1', $size);
}

function clique_return_nth_bit ($clique, $n) {
	//return substr($clique, $n, 1);
	return $clique[$n];
}

function clique_true_p_bit ($clique, $n) {
	//return (substr($clique, $n, 1) == '1');
	return ($clique[$n] == 1);
}

// substr_replace  ( mixed $string  , string $replacement  , int $start  [, int $length  ] )
function clique_set_1_nth_bit ($clique, $n) {
	return substr_replace(''.$clique, "1", $n, 1);
	//$clique[$n] = 1;
	//return $clique;
}

function clique_set_0_nth_bit ($clique, $n) {
	return substr_replace(''.$clique, "0", $n, 1);
	//$clique[$n] = 0;
	//return $clique;
}

function compute_clique_weight($clique){
	$w = 0;
	    for ($i=0 ; $i< $_SESSION[ssig() . 'mat_size'] ; $i++) {
	     	 for ($j=0 ; $j< $_SESSION[ssig() . 'mat_size'] ; $j++) {
	     	 	if ($i != $j) {
	     	 	$n1id = $_SESSION[ssig() . 'mat_id'][$i];
	     	 	$n2id = $_SESSION[ssig() . 'mat_id'][$j];
				$val = $_SESSION[ssig() . 'mat_weight'][$i][$j];	
				
				if (clique_true_p_bit($clique, $i)
					&&
					clique_true_p_bit($clique, $j)
					) {
						$w = $w+ $val;
					}
	     	 }
	     	 }
	     }
	$_SESSION[ssig() . 'mat_clique'][$clique]['totw'] = $w;
	$size = substr_count($clique, '1');
	$_SESSION[ssig() . 'mat_clique'][$clique]['nblinks'] = ($size * ($size -1));
	$_SESSION[ssig() . 'mat_clique'][$clique]['moy'] = $w / $_SESSION[ssig() . 'mat_clique'][$clique]['nblinks'];  
	$_SESSION[ssig() . 'mat_clique'][$clique]['conf'] = $_SESSION[ssig() . 'mat_clique'][$clique]['moy'] * log($size);    
	return $w; 
}



function process_tree () {
	
	$_SESSION[ssig() . 'tree_dot_nodes'] = "";
	$_SESSION[ssig() . 'tree_dot_links'] = "";
	
	//echo "coucou";
	init_clique_tree ();
	//print_r($_SESSION[ssig() . 'tree_clique']);
	//echo "<br>";
	//print_r($_SESSION[ssig() . 'tree_clique_sim']);
	
	construct_tree();

}

function clique_tree_find_best() {
	$seuil = 0.1;
	
	$nb = count($_SESSION[ssig() . 'tree_clique']);
	$max = 0;
	$maxi = -1;
	$maxj = -1;
	for ($i=0 ; $i< $nb ; $i++) {
		if ($_SESSION[ssig() . 'tree_clique_done'][$i] != 1) {
			for ($j=0 ; $j< $nb ; $j++) {
				if (($_SESSION[ssig() . 'tree_clique_done'][$j] != 1) && ($i != $j)) {
					$sim = $_SESSION[ssig() . 'tree_clique_sim'][$j] [$i];
					if ($sim > $max) {
						$max = $sim;
						$maxi = $i;
						$maxj = $j;
					}
				
				}
			}
		}
	}
	$_SESSION[ssig() . 'tree_clique_maxi'] = $maxi;
	$_SESSION[ssig() . 'tree_clique_maxj'] = $maxj;
	$_SESSION[ssig() . 'tree_clique_maxsim'] = $max;
	
	if (($maxi != -1) && ($maxj != -1) && ($max >= $seuil)) {return true;} else {return false;}
}

function construct_tree() {
	$found = clique_tree_find_best();
	while($found) 
	{
		$nb = count($_SESSION[ssig() . 'tree_clique']);
		
		$maxi= $_SESSION[ssig() . 'tree_clique_maxi'];
		$maxj = $_SESSION[ssig() . 'tree_clique_maxj'];
		
		$_SESSION[ssig() . 'tree_clique_done'][$maxi] = 1;
		$_SESSION[ssig() . 'tree_clique_done'][$maxj] = 1;
			
		$c1 = $_SESSION[ssig() . 'tree_clique'][$maxi];
		$c2 = $_SESSION[ssig() . 'tree_clique'][$maxj];
		
		$fusion = tree_clique_fusion($c1, $c2);
		
	//	echo "<br>max i = " . $_SESSION[ssig() . 'tree_clique_maxi'];
	//	echo "<br>max j = " . $_SESSION[ssig() . 'tree_clique_maxj'];
		//echo "<br>fusions de $c1 et $c2 => $fusion";
		//echo "<br>fusions de $maxi et $maxj => " . $_SESSION[ssig() . 'tree_clique_maxsim'];
		echo "<br>$nb : "; display_clique($fusion);
		echo "<br><font color=\"grey\"> $maxi et $maxj / " . $_SESSION[ssig() . 'tree_clique_maxsim'] . "</font>";
		echo "";
		flush();
		
		$_SESSION[ssig() . 'tree_dot_nodes'][$nb] = $fusion;
		$_SESSION[ssig() . 'tree_dot_links'][$nb][$maxi] = 1;
		$_SESSION[ssig() . 'tree_dot_links'][$nb][$maxj] = 1;
		
		$_SESSION[ssig() . 'tree_clique'][$nb] = $fusion;
		
		// on complete le tablo de sim
		//
		for ($i=0 ; $i<$nb+1 ; $i++) {
			$j = $nb;
			if ($i == $j) {
				$_SESSION[ssig() . 'tree_clique_sim'][$j][$i] = 1;
			} else {
				$c1 = $_SESSION[ssig() . 'tree_clique'][$i];
				$c2 = $_SESSION[ssig() . 'tree_clique'][$j];
				$sim = compare_cliques_2($c1, $c2);
				$_SESSION[ssig() . 'tree_clique_sim'][$i] [$j] = $sim;
				$_SESSION[ssig() . 'tree_clique_sim'][$j] [$i] = $sim;
			}			
		}									
		$found = clique_tree_find_best();
	}
	
	
	// finalisation
	
	//searching
	$found=false;
	for ($i=0 ; $i< $nb ; $i++) {
		if ($_SESSION[ssig() . 'tree_clique_done'][$i] != 1) {
			$found=true;
		}
	}
	
	if ($found) {
		$term = $_SESSION[ssig() . 'mat_term'][0];
		$nb = count($_SESSION[ssig() . 'tree_clique']);
		$_SESSION[ssig() . 'tree_dot_nodes'][$nb] = $term;
		for ($i=0 ; $i< $nb ; $i++) {
			if ($_SESSION[ssig() . 'tree_clique_done'][$i] != 1) {
				$_SESSION[ssig() . 'tree_dot_links'][$nb][$i] = 1;
			}
		}
	} else {
		
	}
	name_tree('');
}

function tree_clique_fusion($c1, $c2) {
	$c_1 = "$c1";
	$c_2 = "$c2";
	//echo "<br>fusion_cliques($c_1, $c_2)" ;
	$l1 = strlen($c_1);
	$l2 = strlen($c_2);
	$lmax = max($l1, $l2); /// en fait c'est pareil

	//echo "lmax $lmax";
	$clique_union = "";
	for ($i=0 ; $i<$lmax ; $i++) {
		$val1 = substr($c_1, $i, 1);
		$val2 = substr($c_2, $i, 1);
		if (($val1 == 1) || ($val2 == 1)) {
			$clique_union = $clique_union . '1';
		} else {
			$clique_union = $clique_union . '0';
		}
	}
	return $clique_union;
}

function init_clique_tree () {
	$_SESSION[ssig() . 'tree_clique'] = array();
	$_SESSION[ssig() . 'tree_clique_sons'] = array();
	$_SESSION[ssig() . 'tree_clique_done'] = array();
	$_SESSION[ssig() . 'tree_clique_sim'] = array();
	
	$_SESSION[ssig() . 'tree_dot_nodes'] = array();
	$_SESSION[ssig() . 'tree_dot_links'] = array();
	$_SESSION[ssig() . 'tree_dot_name'] = array();
	
	//$t = $_SESSION[ssig() . 'mat_clique'];
	$t = array_keys($_SESSION[ssig() . 'mat_clique']);
	$nb = count($t);
	//echo "<br>remplir tablo"; flush();
	$j = 0;
	for ($i=0 ; $i< $nb ; $i++) {
		$elt = $t[$i];
		//if (is_clique_in_result_p($elt)){
			$_SESSION[ssig() . 'tree_clique'][$j] = $elt;
			echo "<br>$j : ";
			display_clique($elt);
			
			$_SESSION[ssig() . 'tree_dot_nodes'][$j] = $elt;
			
			$j++;
		//}
	}
//	echo "<br>calculer sim"; flush();
	$nb = count($_SESSION[ssig() . 'tree_clique']);
	for ($i=0 ; $i< $nb ; $i++) {
		for ($j=0 ; $j< $nb ; $j++) {
			
			if ($i ==$j) {
				$_SESSION[ssig() . 'tree_clique_sim'][$j] [$i] = 1;
			} else {
				$c1 = $_SESSION[ssig() . 'tree_clique'][$i];
				$c2 = $_SESSION[ssig() . 'tree_clique'][$j];
				$sim = compare_cliques_2($c1, $c2);
				$_SESSION[ssig() . 'tree_clique_sim'][$i] [$j] = $sim;
				$_SESSION[ssig() . 'tree_clique_sim'][$j] [$i] = $sim;
			}
		}
	}
}


function name_tree($node) {
	if ($node=='') {
		$nb = count($_SESSION[ssig() . 'tree_dot_nodes']);
		$_SESSION[ssig() . 'tree_dot_name'][$nb-1] = $term = $_SESSION[ssig() . 'mat_term'][0];
	}
}


function dot_generate () {
	//print_r($_SESSION[ssig() . 'tree_dot_name']);
	echo "<P><PRE>\n\r digraph G {";
	foreach ($_SESSION[ssig() . 'tree_dot_nodes'] as $nodeid => $name) {
		//$utf8_name = utf8_encode($name);
		//$name = utf8_encode(addslashes($name));
		$truename = $_SESSION[ssig() . 'tree_dot_name'][$nodeid];
		if ($truename == '') {
			$nom = $nodeid;
		} else {
			$nom = $truename;
		}
		$nom = addslashes($nom);
		echo "\r\n$nodeid [shape=ellipse,label=\"$nom\"];";
	}
	$nb = count($_SESSION[ssig() . 'tree_dot_nodes']);
	for ($i=0 ; $i< $nb ; $i++) {
		for ($j=0 ; $j< $nb ; $j++) {
			$val = $_SESSION[ssig() . 'tree_dot_links'][$i][$j];
			if ($val == 1) {
				echo "\r\n$i -> $j [];";
			}
		}
	}
	echo "\n\r}</PRE>";

}

?>

<div class="jdm-level1-block">
	
	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
    <?php echo "Le R�zo mutuel"; 
     //	rezomut_make_wordrelation_form();
     ?>
    </div>
	</div>

</div>

<div class="jdm-level2-block">
<TABLE border="0" width="100%" cellspacing="0" cellpadding="0%"
	summary="jeuxdemots">

<TR><TH  valign="top" width="200pts">
    <TH  align="left">
    <?php
    if ($_POST['clique_tree']!= ""){
    	process_tree ();
    	dot_generate ();
    	
    } else {
	}
    ?>
    <TH>
    
</TABLE>
</div>

<?php //playerinfoblock($_SESSION[ssig() . 'playerid']) ?>
<?php 
    bottomblock();
    closeconnexion();  
?>

  </body>
</html>
