<?php session_start();?>
<?php include_once 'misc_functions.php'; ?>
<?php
    openconnexion();

?>
<html>
 <head>
    <title><?php echo get_msg('SOUK-TITLE'); ?></title>
    <?php header_page_encoding(); ?>
  </head>
<?php include 'HTML-body.html' ; ?>
<?php topblock(); ?>

<div class="jdm-level1-block">
	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
    <?php echo get_msg('SOUK-PROMPT') . get_msg('SOUK-SUBTITLE-TRIALS'); ?>
    </div>
	</div>

    <div class="jdm-login-block">
    <?php  loginblock(); ?>
    </div>
</div>

<div class="jdm-level2-block">
<TABLE border="0" width="100%" cellspacing="0" cellpadding="0%"
	summary="jeuxdemots" bgcolor="white">
	
<TR><TH bgcolor="#FFFFCC" valign="top" width="200pts">
    <TH bgcolor="#FFFFCC" align="left">
    <BR> 
    <?php 
    list_MY_TRIALS_form(); 
    ?>
    <BR>
    <TH bgcolor="#FFFFCC">
    

<TR><TH bgcolor="#EEF6F8" valign="top" align="center" width="200pts">
    <TH bgcolor="#EEF6F8" align="left">
     <BR>
    <?php
	list_concluded_TRIALS_form();
    ?>
     <BR>
    <TH bgcolor="#EEF6F8">

</TABLE>
</div>

<div class="jdm-playas-block-1">
<?php produceplayasform(); ?>
</div>

<?php playerinfoblock($_SESSION[ssig() . 'playerid']) ?>
<?php 
    bottomblock();
    closeconnexion();
?>

  </body>
</html>
