<? 
$GLOBALS['JP-'] = '[ことばを見つかりませんでした]';

//------
$GLOBALS['JP-ENCODING-GENERAL'] = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />";
$GLOBALS['JP-ENCODING-MAIL'] = "UTF-8";
$GLOBALS['JP-ENCODING-DB'] = "utf8";
$GLOBALS['JP-ENCODING-MUSEUM'] = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />";


//------

$GLOBALS['JP-GENGAME-TITLE'] = "ことば遊び： 連想ゲーム";
$GLOBALS['JP-GENGAME-GAMETYPE'] = "連想ゲーム";
$GLOBALS['JP-GENGAME-WARN-NEWINSTR'] = "<blink>注意<br/>新しい ルール</blink>";


$GLOBALS['JP-GENGAME-PASS'] = "パス";
$GLOBALS['JP-GENGAME-BUYTIME'] = "30秒を買う";
$GLOBALS['JP-GENGAME-SEND'] = "送る";
$GLOBALS['JP-GENGAME-TABOO'] = "タブーワード: ";
$GLOBALS['JP-GENGAME-NOPROPTERM'] = "単語入力なし";
$GLOBALS['JP-GENGAME-LASTPROPTERM'] = "最後の単語:";

$GLOBALS['JP-GENGAME-PROMPT-GENERATION'] = "ゲームを作っています";
$GLOBALS['JP-GENGAME-PROMPT-INSTRUCTION'] = "ルールを読んでください";
$GLOBALS['JP-GENGAME-PROMPT-INGAME'] = "ゲーム中";
$GLOBALS['JP-GENGAME-PROMPT-RES'] = "結果";

$GLOBALS['JP-GENGAME-REMAININGTIME'] = "残り時間";
$GLOBALS['JP-GENGAME-PROMPT-WORDLEVEL'] = "レベル: ";

$GLOBALS['JP-GENGAME-COUNTDOWN-SECONDS'] = "秒";
$GLOBALS['JP-GENGAME-COUNTDOWN-END'] = "終わり";

//------
$GLOBALS['JP-SIGNIN-CONNECT-SUBMIT'] = "ログイン";
$GLOBALS['JP-SIGNIN-CONNECT-PROMPT'] = "ログイン";
$GLOBALS['JP-SIGNIN-YOURNAME'] = "お名前:";
$GLOBALS['JP-SIGNIN-YOURPWSD'] = "パスワード (最大10文字):";
$GLOBALS['JP-SIGNIN-YOUREMAIL'] = "メール:";

$GLOBALS['JP-SIGNIN-REGISTER-SUBMIT'] = "登録";
$GLOBALS['JP-SIGNIN-REGISTER-PROMPT'] = "新規登録";
$GLOBALS['JP-SIGNIN-REGISTER-WARNING'] = "<br />注意、メールアドレスが正しくなかったら、連絡できません";

$GLOBALS['JP-SIGNIN-PWDFORGOT-SUBMIT'] = "送信";
$GLOBALS['JP-SIGNIN-PWDFORGOT-PROMPT'] = "パスワードを忘れましたか？";

$GLOBALS['JP-SIGNIN-WELCOME'] = "'<0>'さんが登録しましたようこそ。";
$GLOBALS['JP-SIGNIN-THX-REGISTERING']  = "&#39;<0>&#39;さんが登録しましたありがとう、ホームに行きます。";

$GLOBALS['JP-SIGNIN-INVALID-LOGIN'] = "ログインが出来ません。 - <a href=\"jdm-signin.php\">バック</a>";
$GLOBALS['JP-SIGNIN-OK-LOGIN'] = "出来ました、ホームに行きます。 <a href=\"jdm-accueil.php\">ホーム</a>";


//------
// "Le $date, $what";
$GLOBALS['JP-EVENT-TMPL-DATE'] = "<0>, <1>";

//------

$GLOBALS['JP-MAIL-FROM'] = "kotobaasobi";

$GLOBALS['JP-MAIL-LEVEL'] = "\n今、<0>クレジット、<1>ポイントがあります。レベルは<2>番です。";

$GLOBALS['JP-MAIL-BOTTOM'] = "
    
    またね、
    宜しく、
    ことば遊びより
    http://jibiki.univ-savoie.fr/kotobaasobi
    
    チップ : <0>
    ";

$GLOBALS['JP-MAIL-RESULT-TITLE'] = "”ことば遊び”得点獲得";
$GLOBALS['JP-MAIL-RESULT'] = "こんにちは <0>さん、
   '<2>'単語と、<3>さんと、<1>クレジットを得ました。
    
    あなた<0>さんの答えは： <4> でした。
   　<3>さんの答えは： <5> でした。
    同じ単語は <6> でした。
    
    ルールは <7> <2>　でした。
    ";


$GLOBALS['JP-MAIL-GIFT-TITLE'] = "”ことば遊び”からのサプライズが待っていますよ";
$GLOBALS['JP-MAIL-GIFT'] = "こんにちは <0>さん、
    プレゼントを貰いましたよ！
    開けためにmあれこれに行って下さい。
    ";

$GLOBALS['JP-MAIL-GIFT-DALTON'] = $GLOBALS['JP-MAIL-GIFT'];

$GLOBALS['JP-MAIL-PARRAINAGE-TITLE'] = "<0>さんから”ことば遊び”への招待状";
$GLOBALS['JP-MAIL-PARRAINAGE'] = "こんにちは、\n\n

インターネット上で、複数の人と競う”ことば遊び”に参加しませんか。

私たちは今、興味深いプロジェクトについて研究しています：

二つの言葉で、連関を作ります。
例えば、左に示した言葉を読んで、どんな言葉が連想されますか？
お風呂　→　温泉、お湯、シャワ、石けん、シャンプー、タオル、 
ジャクジー、露天風呂、裸、

例えば、左に示した言葉を読んで、どんな類義語が思いつきますか？
きれい　→　美しい、麗しい、秀麗、清い、清らか、清潔、清 
浄、 かわいい、ハンサム、かっこい、美人

このデータと、データベース中の全部の言葉とで、自動的に、連絡網を作ることが出来ます。
その連絡網は言語学の研究のため、大変役にたちます。

同僚のラフカード・マチューさんはことば遊びのゲームをフランス 
語で初めて作りました：
http://www.lirmm.fr/jeuxdemots/
その後で、日本語のバージョンを作り始めました：
http://jibiki.univ-savoie.fr/kotobaasobi
クリックして頂くと、今すぐ遊べます。

このゲームに興味がある日本語を話す人を探しています。
サイト上、ゲストとして体験できますが、保存はできません。
連関を保存するために、ご自分のログイン名でログインして下さい。
皆さん、是非参加して下さい。

翻訳の間違いに気づかれましたら、お知らせ下さい。

お友達の方にも是非紹介して下さい。

研究へのご協力ありがとうございます。

\"<0>\"さんは”ことば遊び”へ招待します。是非一緒に遊びましょう。\n";

//------

$GLOBALS['JP-MISC-CONTACT'] = "コンタクト";
$GLOBALS['JP-MISC-CONTACT-EMAIL'] = "mailto:mathieu@mangeot.org?subject=kotobaasobi";
$GLOBALS['JP-MISC-HOME'] = "ホーム";
$GLOBALS['JP-MISC-FORUM'] = "フォーラム";
$GLOBALS['JP-MISC-RANKING'] = "ランキング";
$GLOBALS['JP-MISC-EVENTS'] = "イベント";
$GLOBALS['JP-MISC-SOUK'] = "あれこれ";
$GLOBALS['JP-MISC-OPTIONS'] = "オプション";
$GLOBALS['JP-MISC-HELP'] = "ヘルプ";

$GLOBALS['JP-MISC-CREDIT'] = "クレジット: ";
$GLOBALS['JP-MISC-HONNOR'] = "ポイント: ";
$GLOBALS['JP-MISC-LEVEL'] = "レベル: ";

$GLOBALS['JP-MISC-LOGIN'] = "ログアウット";
$GLOBALS['JP-MISC-LOGOUT'] = "ログイン";

$GLOBALS['JP-MISC-PLAYAS-PLAY'] = "遊ぶ";
$GLOBALS['JP-MISC-PLAYAS-GUEST'] = " ゲスト";
$GLOBALS['JP-MISC-PLAYAS-GUESTWARNING'] = "ゲストとしてゲームできます、その際ポイントは加算されません.<br/>
どうぞログインしてください.";
//------

$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-0'] = "連想することばを入力して下さい。";

$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-3'] = "Donner des THEMES/DOMAINES (par exemple: 'Sports', 'Médecine', 'Cinéma', 'Cuisine,' etc.) pour le terme suivant :";
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-5'] = "同義語を入力して下さい。";
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-6'] = "Donner des GENERIQUES (par exemple: 'véhicule' pour 'voiture', 'félin', 'animal' pour 'chat') du terme suivant :";
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-7'] = "Donner des CONTRAIRES (par exemple: 'froid' pour 'chaud', 'haut' pour 'bas') du terme suivant :";
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-8'] = "Donner des SPECIFIQUES (par exemple : 'chat', 'chien', 'animal de compagnie',  etc. pour 'animal' - ou encore 'voiture', 'train', 'véhicule spatial', etc. pour 'véhicule') du terme suivant :";
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-9'] = "Donner des PARTIES (par exemple : 'moteur', 'roue', etc. pour 'voiture' - ou encore 'couverture', 'pages', 'chapitre' etc. pour 'livre') du terme suivant :";
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-10'] = "Donner des TOUT (par exemple : 'corps', 'bras', etc. pour 'coude' - ou encore 'banque' pour 'guichet') du terme suivant :";
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-11'] = "Donner des LOCUTIONS (par exemple : 'langue au chat', 'chat de gouttière', 'chat à neuf queues', ... pour 'chat') pour le terme suivant :";

$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-13'] = "Donner des SUJETS typiques (par exemple : 'chat', 'animal', 'personne', ... pour 'manger') pour le VERBE suivant :";
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-14'] = "Donner des OBJETS typiques pour le VERBE suivant (l'objet est ce qui subit l'action, par exemple : 'viande', 'fruit', 'bonbon', ... pour 'manger' ou encore 'personne', 'homme politique', 'otage', ... pour 'assassiner') :";
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-15'] = "Donner des LIEUX typiques (par exemple : 'pré', 'écurie', 'champs de courses', ... pour 'cheval') pour le terme suivant :";

$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-16'] = "Donner des INSTRUMENTS typiques (par exemple : 'pelle', 'pioche', 'main', ... pour 'creuser') pour le VERBE suivant :";
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-17'] = "Donner des CARACTERISTIQUES typiques (par exemple : 'liquide', 'blanc', 'buvable', ... pour 'lait') pour le terme suivant :";

$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-20'] = "Qu'est ce qui est PLUS INTENSE (par exemple : 'forte fièvre', 'fièvre de cheval', ... pour 'fièvre' - ou encore 'vivre intensément' pour 'vivre') que le terme suivant :";
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-21'] = "Qu'est ce qui est MOINS INTENSE (par exemple : 'maisonette', ... pour 'maison' - ou encore 'marcher lentement', 'trainer' pour 'marcher') que le terme suivant :";

$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-22'] = "Donner des mots de la MEME FAMILLE (par exemple : 'chanter', 'chanteur'... pour 'chant' - ou encore 'vente', 'vendeur', 'vendu' pour 'vendre') pour le terme suivant :";

$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-23'] = "Qu'est-ce qui possède la CARACTERISTIQUE suivante (par exemple, 'eau', 'vin', 'lait' pour 'liquide') :";
		
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-24'] = "Que peut faire le SUJET suivant (par exemple, 'manger', 'dormir', 'chasser' pour 'lion') :";
		
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-25'] = "Que peut-on faire avec l'INSTRUMENT suivant (par exemple, 'écrire', 'dessiner', 'gribouiller' pour 'stylo') :";
		
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-26'] = "Que peut subir l'OBJET suivant (par exemple, ...) :";
		
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-27'] = "Donner des termes du DOMAINE suivant (par exemple, 'touche', 'penalty', 'but' pour 'Football') :";

$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-28'] =  "Que trouve-t-on dans le LIEU suivant (par exemple, 'poisson', 'coquillage', 'algue' pour 'mer') :";
		
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-30'] = "Que peut-on faire dans le LIEU suivant (par exemple, 'manger', 'boire', 'commander' pour 'restaurant' -- des verbes sont demandés) :";
		
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-31'] = "Dans quels LIEUX peut-on faire l'action suivante (par exemple, 'restaurant', 'cuisine', 'fast-food' pour 'manger' -- des lieux sont demandés) :";
		
$GLOBALS['JP-GENGAME-FUNCTIONS-INSTR-32'] = "Quels SENTIMENTS/EMOTIONS évoquent pour vous le terme suivant :";


// unused?
$GLOBALS['JP-TERMSELECT-FUNCTIONS-NAME-TEMPLATE'] = 'Nom%';
$GLOBALS['JP-TERMSELECT-FUNCTIONS-ADJ-TEMPLATE'] = 'Adj%';
$GLOBALS['JP-TERMSELECT-FUNCTIONS-ADV-TEMPLATE'] = 'Adv%';
$GLOBALS['JP-TERMSELECT-FUNCTIONS-ADV-TEMPLATE'] = 'Ver:Inf';

$GLOBALS['JP-READ-INSTRUCTIONS'] = 'ルールを読んでください!';

//------

$GLOBALS['JP-OPTIONS-TITLE'] = "ことば遊び: オプション";
$GLOBALS['JP-OPTIONS-PROMPT'] = "オプション";
$GLOBALS['JP-OPTIONS-CSS-FORM'] = 	"CSS :<BR>
	URLを<input <0> id=\"go_css_submit\" type=\"submit\" name=\"go_css_submit\" value=\"変わる\"> 
	<input  id=\"go_css_url\" type=\"text\" size = \"120\" name=\"go_css_url\" value=\"<1>\"><br />
	(ディフォールトはこのURL\"<2>\"を入らなければなりません)";
$GLOBALS['JP-OPTIONS-CSS-EXAMPLE'] = "CSSの例：";
$GLOBALS['JP-OPTIONS-CSS-MUSEUM-FORM'] = 	"あなたの博物館のCSS：<BR>
	URLを<input <0> id=\"go_css_museum_submit\" type=\"submit\" name=\"go_css_museum_submit\" value=\"変わる\">
	<inputid=\"go_css_museum_url\" type=\"text\" size = \"120\" name=\"go_css_museum_url\" value=\"<1>\">";


//------

$GLOBALS['JP-HOME-TITLE'] = "ことば遊び: ホーム";
$GLOBALS['JP-HOME-PROMPT'] = "ホーム";

$GLOBALS['JP-HOME-NEWS'] = "<p><blockquote><0>のニュース";

$GLOBALS['JP-HOME-INSTRUCTIONS-TITLE'] = "遊び方";


$GLOBALS['JP-HOME-INSTRUCTIONS'] = "ある語と指示がでます。一分間で、指示にしたがって、できるだけ多くの語を入力してください。 だいたいその語から自由に連想できる語です。
単語を入力し、送信ボタンか'enter'キイを推して下さい。 他の人があなたと同じ語を答えたら、あなたがポイントを獲得します。
単語が的確であればあるほど、高得点を獲得しますが。。。しかし、はたして他の人もあなたと同じ語を連想するでしょうか。<br/><br/>
<a href=\"jdm-signin.php\">ことば遊びのルール</a>を読んでください。<p></p><p></p>";


$GLOBALS['JP-HOME-MOSTWANTED'] = '流行ってることば';
$GLOBALS['JP-HOME-COLLECTIONS'] = 'コレクション';
$GLOBALS['JP-HOME-GIFT-WAITING'] = "プレゼント獲得、<あ href=\"souk.php#giftlist\">あれこれ</a>に行ってください";
$GLOBALS['JP-HOME-RELATION-COUNT'] = "これまでに<0>語の連想語組み合わせが誕生しています。そのうち、<1>はタブ—語ですぞ！やった？<br />
獲得ポイントは合計<2>ポイントで、";

$GLOBALS['JP-HOME-TERM-COUNT'] = "語は<0>語です。";

//------

$GLOBALS['JP-SOUK-TITLE'] = "ことば遊び: あれこれ";
$GLOBALS['JP-SOUK-PROMPT'] = "あれこれ";

$GLOBALS['JP-SOUK-SUBTITLE-COMP'] = "技量の買い物";
$GLOBALS['JP-SOUK-SUBTITLE-TREAS'] = "宝の取締り";

$GLOBALS['JP-SOUK-COMP-ITEM'] = "<nobr>技量</nobr>";
$GLOBALS['JP-SOUK-COMP-HONNOR-MIN'] = "<nobr>最高</nobr> <nobr>ポイント</nobr>";
$GLOBALS['JP-SOUK-COMP-CREDIT'] = "<nobr>クレジット</nobr>";
$GLOBALS['JP-SOUK-COMP-STATUS'] = "<nobr>ステータス</nobr>";
$GLOBALS['JP-SOUK-COMP-QUOT'] = "<nobr>獲得</nobr> <nobr>ポイント</nobr>";
$GLOBALS['JP-SOUK-COMP-SELECPC'] = "<nobr>出題</nobr> %";
$GLOBALS['JP-SOUK-COMP-ADJUSTPC'] = "<nobr>調整</nobr>";

$GLOBALS['JP-SOUK-DEFIS-NOTERM'] = "'<0>'の単語は存在しません !";
$GLOBALS['JP-SOUK-DEFIS-NOTFREE'] = "<0>の単語は使えます !";
$GLOBALS['JP-SOUK-DEFIS-NOGUEST'] = "ゲストのかたは単語を捕らえられません。";
$GLOBALS['JP-SOUK-DEFIS-NOTENOUGHMONEY'] = "クレジットは(<1>)の単語の値段の五倍が必要ですよ。";
$GLOBALS['JP-SOUK-DEFIS-NOMONEY'] = "クレジットは足りません (<1>)";
$GLOBALS['JP-SOUK-DEFIS-NOTNOW'] = "今すぐ、この単語を捕らえられません、後で、もう一度、やってみて下さい。";

$GLOBALS['JP-SOUK-LIST-TRIAL-COMPLETED-FORM'] = "<li><form id=\"open_cr_trial<0>\" name=\"open_trial<0>\" method=\"post\" action=\"trialVote.php\" >
				<input <1> id=\"trial_vote_cr\" type=\"submit\" name=\"trial_vote_cr\" value=\"Visualiser\">
				<input  id=\"proc_id\" type=\"hidden\" name=\"proc_id\" value=\"<2>\">
		 		le procès <2> opposant '<3>' à '<4>' pour le terme '<5>'.
				</form></li>";

$GLOBALS['JP-SOUK-LIST-TRIAL-TODO-FORM'] ="<li><form id=\"open_trial<0>\" name=\"open_trial<0>\" method=\"post\" action=\"trialVote.php\" >
		<input id=\"trial_vote\" type=\"submit\" name=\"trial_vote\" value=\"Voter\">
		<input  id=\"proc_id\" type=\"hidden\" name=\"proc_id\" value=\"<1>\">
		 - échéance dans <2> heures.
		</form></li>";

$GLOBALS['JP-MAKE-TRIAL-FORM'] = "<form id=\"form-trial\" name=\"form-trial\" method=\"post\" action=\"buyTrial.php\" >
			Faire un <input <0> id=\"submit-buy-trial\" type=\"submit\" name=\"submit-buy-trial\" value=\"procès\"> 
				 (500 Cr) à ce joueur
			 <input id=\"trial_id\" type=\"hidden\" name=\"trial_id\" value=\"<1>\"></form>";


//------ Le souk　par MM

// global warnings
$GLOBALS['JP-SOUK-WARNING-BADPLAYER'] = "この参加者はいません";
$GLOBALS['JP-SOUK-WARNING-BADTERM'] = "この単語はありません";
$GLOBALS['JP-SOUK-WARNING-NOCASH'] = "クレジットは足りません";
$GLOBALS['JP-SOUK-WARNING-NOHONNOR'] = "ポイントは足りません";
$GLOBALS['JP-SOUK-WARNING-NOGUEST'] = "ゲストの参加者はこのことが出来ません、登録して下さい。";
$GLOBALS['JP-SOUK-WARNING-NOCREDIT'] = "ことば遊びでローンができません！。";

//"le joueur '$player' a offert un cadeau au joueur '$dest'."
$GLOBALS['JP-SOUK-GIFT-EVENT'] = "&#39;<0>&#39;さんの方は&#39;<1>&#39;にプレゼントを上げました.";
$GLOBALS['JP-SOUK-BUYCOMP-EVENT'] = "&#39;<0>&#39;さんの方は技量を買いました。";
$GLOBALS['JP-SOUK-HYPEWORDS'] = "流行ってる単語";
$GLOBALS['JP-SOUK-INWORDS'] = "イン単語";
$GLOBALS['JP-SOUK-OUTWORDS'] = "アウッツ単語";

$GLOBALS['JP-SOUK-HYPEWORDS-VIEW-FORM'] = "<input id=\"formhypewordsumit\" 
type=\"submit\" name=\"formhypewordsumit\" value=\"する\">(10 クレジット)流行ってる言葉を見る。";

$GLOBALS['JP-SOUK-CAGNOTTE-VIEW-FORM'] = "	<input id=\"cagnotesubmit\" type=\"submit\" name=\"cagnotesubmit\" value=\"する\"> (5 クレジット)
宝くじを見る。";

$GLOBALS['JP-SOUK-CAGNOTTE-DISPLAY-WARNING'] = "宝くじは<0>クレジットです。 
		    <1>さんと<2>さんは<3>クレジットで一番です。
		    <4>ジャックポットの後で、宝くじが出ます。";
		    
$GLOBALS['JP-SOUK-GAME-GIFT-FORM'] = "<input id=\"formmakegift\" type=\"submit\" name=\"formmakegift\" value=\"する\">
	(100 クレジット)
		<input id=\"giftplayername\" type=\"text\" name=\"giftplayername\" size='13' value=\"<1>\">さんに、
		<input id=\"giftterm\" type=\"text\" name=\"giftterm\"  size='13' value=\"<0>\">の単語と、ゲーム
		を贈る。
		<input id=\"relation_type_gift\" type=\"hidden\" name=\"relation_type_gift\" value=\"0\">"; 

$GLOBALS['JP-SOUK-GAME-GIFT-WARNING-NOINPUT'] = "単語と参加者の名前を入れて下さい";
$GLOBALS['JP-SOUK-GAME-GIFT-WARNING-SELFGIFT'] = "いいんですけれども自分にプレゼントを贈れません！";
$GLOBALS['JP-SOUK-GAME-GIFT-WARNING-NOERROR'] = "プレゼントは買って送りましたよ";

$GLOBALS['JP-SOUK-GAME-GIFT-LIST'] = "呉れましたプレゼント：";
$GLOBALS['JP-SOUK-GAME-GIFT-LIST-EMPTY'] = "誰もプレゼントを呉れませんでした。";

$GLOBALS['JP-SOUK-GAME-GIFT-OPEN-FORM'] = "<input id=\"opengiftbut\" type=\"submit\" name=\"opengiftbut\" value=\"する\">
		 <0>さんが<1>、贈りましたプレゼントをあけます。";

$GLOBALS['JP-SOUK-GRAPH-VIEW-FORM'] = " <input id=\"opengiftbut\" type=\"submit\" name=\"opengiftbut\" value=\"する\">
		 (0 クレジット) 図表を見る。";

$GLOBALS['JP-SOUK-WORD-LOOKUP-FORM'] = "  <input id=\"gotermlist\" type=\"submit\" name=\"gotermlist\" value=\"する\">
       (0 クレジット) この文字<input  id=\"goterm\" type=\"text\" name=\"goterm\" value=\"<0>\">が入ってる単語を検索する。";

$GLOBALS['JP-SOUK-WORDLIST-DISPLAY-WARNING-NOINPUT'] = "単語を入れて下さい";

$GLOBALS['JP-SOUK-PARRAINAGE-FORM'] = "	  <input id=\"parainage_submit\" type=\"submit\" name=\"parainage_submit\" value=\"する\">
　　　(0 クレジット) このメールアドレス
	  <input  size=\"50\" id=\"parainage_email\" type=\"text\" name=\"parainage_email\" value=\"yamada@tokoro.jp\">
	  持ってる参加者を招待する。
	  ";
	  
$GLOBALS['JP-SOUK-PARRAINAGE-WARNING-NOERROR'] = "招待状を送りましたよ（メールアドレスを確認しませんでした）";
$GLOBALS['JP-SOUK-PARRAINAGE-WARNING-WRONGEMAIL'] = "他のメールアドレスを入れて下さい。";

$GLOBALS['JP-SOUK-GENERATE-DEFIS-FORM'] = "<input id=\"parainage_submit\" type=\"submit\" name=\"defis_submit\" value=\"する\">
	<input  size=\"20\" id=\"parainage_email\" type=\"text\" name=\"defis_amount\" value=\"1000\">クレジットで
	  <input  size=\"20\" id=\"parainage_email\" type=\"text\" name=\"defis_word\" value=\"単語\">の単語を捕らえる。";

$GLOBALS['JP-SOUK-WORD-CAPTURE-FORM'] = "<font color=\"red\">捕らえる準備が出来ましたよ</font> 
<input id=\"chosengamesubmit\" type=\"submit\" name=\"chosengamesubmit\" value=\"行く !\">
		";

$GLOBALS['JP-SOUK-WORD-CAPTURE-EVENT'] = "&#39;<0>&#39;さんは&#39;<1>&#39;の単語を捕らえてい増す。";

$GLOBALS['JP-SOUK-CHECK-TREASURE-FORM'] = 	"<input id=\"gochecktreasuressubmit\" type=\"submit\" name=\"gochecktreasuressubmit\" value=\"する\"> 
	<input  id=\"gochecktreasures\" type=\"text\" name=\"gochecktreasures\" value=\"\">の参加者の宝を見る。";

$GLOBALS['JP-SOUK-BUYCOMP-SUBMIT'] = "買う";
$GLOBALS['JP-SOUK-BUYCOMP-DONE'] = "貰った";

$GLOBALS['JP-SOUK-WORD-RELATION-FORM'] = " <input id=\"gotermsubmit\" type=\"submit\" name=\"gotermsubmit\" value=\"する\"> (DEBUG) 
	    <input  id=\"gotermrel\" type=\"text\" name=\"gotermrel\" value=\"\">の単語を検索する";

$GLOBALS['JP-SOUK-GAMELIST-FORM'] = "<input id=\"gamelist_submit\" type=\"submit\" name=\"gamelist_submit\" value=\"やってるゲーム\"> (DEBUG)";

$GLOBALS['JP-SOUK-CHOSENGAME-FORM'] = "<input id=\"chosengamesubmit\" type=\"submit\" name=\"chosengamesubmit\" value=\"遊ぶ\">";

$GLOBALS['JP-SOUK-STATISTICS-FORM'] = "<input id=\"bd_stats_submit\" type=\"submit\" name=\"bd_stats_submit\" value=\"統計\"> (DEBUG)";

$GLOBALS['JP-SOUK-CHECKOWNER-FORM'] = "	<input id=\"gotermownersubmit\" type=\"submit\" name=\"gotermownersubmit\" value=\"する\"> 
この単語<input  id=\"gotermowner\" type=\"text\" name=\"gotermowner\" value=\"\">のステータスを確認する。";


$GLOBALS['JP-SOUK-PROB-WARNING-NOERROR'] = "確率が変わりました";
$GLOBALS['JP-SOUK-COMP-WARNING-NOERROR'] = "連想語組みを買いましたよ";


//------

$GLOBALS['JP-EVENT-TITLE'] = "ことば遊び: イベント";
$GLOBALS['JP-EVENT-PROMPT'] = "イベント";

// "les joueurs '$gamecreatorname' et '$playername' ont gagné <b>$points</b> crédits avec <b>'$fentry'</b>  pour la compétence '$relgpname'. Jackpot !"
$GLOBALS['JP-EVENT-WIN-JACKPOT'] = "'<0>'と'<1>'の参加者は、'<4>'の技量で、<b>'<3>'の単語と、<b><2></b>クレジットを獲得しました。</b> 
ジャックポット !";

// "les joueurs '$gamecreatorname' et '$playername' ont gagné <b>$points</b> crédits avec <b>'$fentry'</b>."
$GLOBALS['JP-EVENT-WIN'] = "'<0>'と'<1>'の参加者は<b>'<3>'の単語と、<b><2></b>クレジットを獲得しました。</b>";

$GLOBALS['JP-EVENT-JACKPOT-TMPL'] = "ジャックポット !";
$GLOBALS['JP-EVENT-TRIALWON-TMPL'] = "公判を獲得しました";
$GLOBALS['JP-EVENT-TRIALNO-TMPL'] = "non-lieu pour le";

//------

$GLOBALS['JP-RANKING-TITLE'] = "ことば遊び: ランキング";
$GLOBALS['JP-RANKING-PROMPT'] = "ランキング";

$GLOBALS['JP-RANKING-LABEL-NAME'] = "名前";
$GLOBALS['JP-RANKING-LABEL-HONNOR'] = "<nobr>ポイント</nobr>";
$GLOBALS['JP-RANKING-LABEL-HONNORMAX'] = "<nobr>最大</nobr> <nobr>ポイント</nobr>";
$GLOBALS['JP-RANKING-LABEL-CREDITS'] = "<nobr>クレジット</nobr>";
$GLOBALS['JP-RANKING-LABEL-NBPLAYED'] = "<nobr>ゲーム</nobr>";
$GLOBALS['JP-RANKING-LABEL-LEVEL'] = "<nobr>レベル";
$GLOBALS['JP-RANKING-LABEL-EFF'] = "<nobr>&nbsp;&nbsp;性能&nbsp;&nbsp;</nobr>";
$GLOBALS['JP-RANKING-LABEL-NBTREASURE'] = "宝";

$GLOBALS['JP-RANKING-BEST-SCORES'] = "最高のポイント";
$GLOBALS['JP-RANKING-BEST-COUPLES'] = "最高のカップル";
$GLOBALS['JP-RANKING-BEST-FRIENDS'] = "最高の友達";

$GLOBALS['JP-RANKING-AND'] = "と";
$GLOBALS['JP-RANKING-MINUTE'] = "分";
$GLOBALS['JP-RANKING-15MINUTE'] = "15分";
$GLOBALS['JP-RANKING-30MINUTE'] = "30分";
$GLOBALS['JP-RANKING-HOUR'] = "時";
$GLOBALS['JP-RANKING-DAY'] = "日";
$GLOBALS['JP-RANKING-MONTH'] = "月";
$GLOBALS['JP-RANKING-YEAR'] = "年";
$GLOBALS['JP-RANKING-CREDITS'] = "クレジット";

//------

$GLOBALS['JP-MUSEUM-TITLE'] = "ことば遊び: 小さな直物";
$GLOBALS['JP-MUSEUM-PROMPT'] = "<0>の小さな直物";
$GLOBALS['JP-MUSEUM-WARNING-NOTREASURE'] = "この参加者は宝を持ってません。";



//------
$GLOBALS['JP-RESULT-TITLE'] = "ことば遊び: 結果";
$GLOBALS['JP-RESULT-PG-PROMPT'] = "<p>このゲーム(<1>)で、あなた(<0>)の答え：";
$GLOBALS['JP-RESULT-NOTHING'] =  " --なし-- ";
$GLOBALS['JP-RESULT-PROMPT'] = "結果";

$GLOBALS['JP-RESULT-NOPROP'] = "<h2>入力なし。。。またチャレンジをして下さい :)</h2>";

$GLOBALS['JP-RESULT-NOGUEST'] = "ゲストはできません :)";

$GLOBALS['JP-RESULT-PROMPT-BUYPLAY-HEADER'] = "あなたの答えが他の人より優れていると思いますか";
$GLOBALS['JP-RESULT-PROMPT-BUYPLAY-FORM'] = "<br/>他の人にあなたの答えを提案する賢慮を買うことが出来ます。";
$GLOBALS['JP-RESULT-TOKEN'] = "回";
$GLOBALS['JP-RESULT-OR'] = "か";

$GLOBALS['JP-RESULT-PROMP-INVOKEWORD-FORM'] ="<P><font color=\"red\"> ジャックポット !</font>
		<br>偉いです、 Pour vous récompenser, on vous invite à souhaiter qu'un terme de votre choix soit plus souvent proposé.";
$GLOBALS['JP-RESULT-INVOKE'] = "Invoquer";
$GLOBALS['JP-RESULT-THETERM'] = "le terme";


$GLOBALS['JP-RESULT-PROMPT-YOURANSWERS'] = "このゲーム(<1>)に、あなた(<0>)の答えです：　";
$GLOBALS['JP-RESULT-PROMPT-HISANSWERS'] = "このゲーム(<1>)の作り手(<0>)の答えです：　";
$GLOBALS['JP-RESULT-PROMPT-INTERSECTION'] = "論理積:";

$GLOBALS['JP-RESULT-PROMPT-CREATED-PLAY'] = "<h2>この言葉をほかの参加者に提案します。その時には、メールでを知らせします。。。</h2>";

$GLOBALS['JP-RESULT-BUYTOKEN-FORM'] = "もっと、ほかの参加者に送りたい場合はその権利を買うことが出来ます。<p> 
		<input id=\"submit5\" type=\"submit\" name=\"submit5\" value=\"５回\"> (750 クレジット) か
		<input id=\"submit10\" type=\"submit\" name=\"submit10\" value=\"１０回\"> (1500 クレジット)";

$GLOBALS['JP-RESULT-PROMPT-GAIN'] = "<h2><0>クレジットと<1>ポイントを獲得します。</h2>";

$GLOBALS['JP-RESULT-PROMPT-LEVEL-GAIN'] = "<br>あなたと相手が<0>ポイントを獲得します。";
$GLOBALS['JP-RESULT-PROMPT-LEVEL-LOOSE'] = "<br>あなたと相手が<0>ポイントを失います。";

$GLOBALS['JP-RESULT-PROMPT-TERMLEVEL-GAIN'] = "<br><0>の単語が<1>ポイントを獲得します。";
$GLOBALS['JP-RESULT-PROMPT-TERMLEVEL-LOOSE'] = "<br><0>の単語が<1>ポイントを失います。";

$GLOBALS['JP-RESULT-NOTHING'] =  " --なし-- ";
//------
$GLOBALS['JP-BUYPLAY-CONGRAT-INVEST'] = "<h1><1>クレジットで、<0>クレジットの買い物がありがとうございます。</h1>";
$GLOBALS['JP-BUYPLAY-CHEAT'] = "<h1>すみませんがゲストの参加者がゲームを買えません。 
    	これを読んだら、セッションが終わりましたかもしれません</h1>";
$GLOBALS['JP-BUYPLAY-EVENT'] = "'<0>'の参加者が<b>'<1>'</b>野単語のゲームを買いました。";

//------
$GLOBALS['JP-BUYTOKEN-CONGRAT-INVEST'] = "<h1><1>クレジットで、<0>クレジットの買い物がありがとうございます。</h1>";
$GLOBALS['JP-BUYTOKEN-CHEAT'] = "<h1>すみませんがゲストの参加者がクレジットを買えません。 
    	これを読んだら、セッションが終わりましたかもしれません</h1>";
$GLOBALS['JP-BUYTOKEN-SORRY'] = "<h1>すみませんがその<0>クレジットが足りません、前を確認した方が良かった :)</h1>";

// "le joueur '$login' a investi dans le mot <b>'$fentry'</b>."
$GLOBALS['JP-BUYTOKEN-EVENT'] = "'<0>'の参加者が<b>'<1>'</b>の単語に注ぎ込みました。";

//------
$GLOBALS['JP-BUYWISH-CONGRAT-INVEST'] = "<h1><1>クレジットで、'<0>'の単語の呼ぶことありがとうございます。 いつか出ます、きっと</h1>";
$GLOBALS['JP-BUYWISH-CHEAT'] = "<h1>すみませんがゲストの参加者が単語を聞けません。 
    	これを読んだら、セッションが終わりましたかもしれません</h1>";
$GLOBALS['JP-BUYWISH-SORRY'] = "<h1>すみませんがその<0>クレジットが足りません、前を確認した方が良かった :)</h1>";

// "le joueur '$login' a investi dans le mot <b>'$fentry'</b>."
$GLOBALS['JP-BUYWISH-EVENT'] = "'<0>'の参加者が単語を呼びました。。。";
$GLOBALS['JP-BUYWISH-EMPTY-WORD'] = "残念、空しい単語 !";

//------
$GLOBALS['JP-TRIAL-CONGRAT-INVEST'] = "<h1><h1><1>クレジットで、'<0>'徴証の呼ぶことありがとうございます。</h1>";
$GLOBALS['JP-TRIAL-CHEAT'] = "<h1>すみませんがゲストの参加者が公判ができません。 
    	これを読んだら、セッションが終わりましたかもしれません</h1>";
$GLOBALS['JP-BUYTRIAL-SORRY'] = "<h1>すみませんがその<0>クレジットが足りません、前を確認した方が良かった :)</h1>";

//------

$GLOBALS['JP-PLAYER-PERM-CAGNOTTE-BINGO'] = "おめでとう！ <0> クレジットの宝くじが出ました。";
$GLOBALS['JP-PLAYER-PERM-CAGNOTTE-EVENT'] = "'<0>'さんと'<1>'さんの参加者は<2>クレジットの宝くじを貰いました。。。と 他の参加者もクレジットを貰います";
$GLOBALS['JP-PLAYER-PERM-CAGNOTTE-MAIL'] = "こんにちは、\n宝くじが出ましたとクレジットを貰います。。。\n";
$GLOBALS['JP-PLAYER-PERM-CAGNOTTE-MAIL-TITLE'] = "クレジット。。。";


//------

$GLOBALS['JP-REQUEST-VERB-TRANSTIVE'] = "SELECT id FROM `Nodes` WHERE w > 50 
	AND id IN (SELECT node1 FROM Relations WHERE node2 
	IN (SELECT id FROM Nodes WHERE type=18 and name LIKE 'Ver:Transitif')
	) order by w DESC LIMIT <0>";

$GLOBALS['JP-REQUEST-VERB-EASY'] = "SELECT id FROM `Nodes` WHERE w > 50 
	AND id IN (SELECT node1 FROM Relations WHERE node2 
	IN (SELECT id FROM Nodes WHERE type=4 and name LIKE 'Ver:Inf')
	) order by w DESC LIMIT <0>";

$GLOBALS['JP-REQUEST-NOUN-EASY'] = "SELECT id FROM `Nodes` WHERE w > 50 
	AND id IN (SELECT node1 FROM Relations WHERE node2 
	IN (SELECT id FROM Nodes WHERE type=4 and name LIKE 'N%')
	) order by w DESC LIMIT <0>";

$GLOBALS['JP-REQUEST-NOUN-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'N%'); ";

$GLOBALS['JP-REQUEST-NOUN-ADJ-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'Adj%' OR name LIKE 'Adv%');";

$GLOBALS['JP-REQUEST-NOUN-ADJ-VER-ADV-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'Nom%' OR name LIKE 'Ver:Inf' OR name LIKE 'Adj%' OR name LIKE 'Adv%');";

//------
// "le joueur '$player_name' fait de la magie noire avec le mot '$fentry'."
$GLOBALS['JP-BLACK-MAGIC-EVENT'] = "'<0>'の参加者は'<1>'の単語と、魔術をします。";
//"le joueur '$player_name' fait de la magie blanche avec le mot '$fentry'."
$GLOBALS['JP-WHITE-MAGIC-EVENT'] = "'<0>'の参加者は'<1>'の単語と、奇術をします。";

?>
