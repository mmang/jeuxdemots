<?  
$GLOBALS['ES-'] = '[Cha�ne non trouv�e]';

//------
$GLOBALS['ES-ENCODING-GENERAL'] = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\"/>";
$GLOBALS['ES-ENCODING-MAIL'] = "ISO-8859-1";
$GLOBALS['ES-ENCODING-DB'] = "latin1";
$GLOBALS['ES-ENCODING-MUSEUM'] = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\"/>";

//------

$GLOBALS['ES-GENGAME-TITLE'] = "JeuxDeMots : jeu d'associations";
$GLOBALS['ES-GENGAME-GAMETYPE'] = "Jeu d'associations";
$GLOBALS['ES-GENGAME-WARN-NEWINSTR'] = "<blink>Attention<br>nouvelle consigne</blink>";

$GLOBALS['ES-GENGAME-PASS'] = "Passer";
$GLOBALS['ES-GENGAME-BUYTIME'] = "Acheter 30 s";
$GLOBALS['ES-GENGAME-SEND'] = "Envoyer";
$GLOBALS['ES-GENGAME-TABOO'] = "Mots tabous";
$GLOBALS['ES-GENGAME-NOPROPTERM'] = "Aucun terme propos�";
$GLOBALS['ES-GENGAME-LASTPROPTERM'] = "Dernier terme propos� :";

$GLOBALS['ES-GENGAME-PROMPT-GENERATION'] = "Cr�ation";
$GLOBALS['ES-GENGAME-PROMPT-INSTRUCTION'] = "Lecture";
$GLOBALS['ES-GENGAME-PROMPT-INGAME'] = "Temps";
$GLOBALS['ES-GENGAME-PROMPT-RES'] = "R�sultats";

$GLOBALS['ES-GENGAME-REMAININGTIME'] = "Temps restant";
$GLOBALS['ES-GENGAME-PROMPT-WORDLEVEL'] = "Niveau : ";

$GLOBALS['ES-GENGAME-COUNTDOWN-SECONDS'] = " s";
$GLOBALS['ES-GENGAME-COUNTDOWN-END'] = "fini";

//------
$GLOBALS['ES-SIGNIN-CONNECT-SUBMIT'] = "connexion";
$GLOBALS['ES-SIGNIN-CONNECT-PROMPT'] = "Connectez vous";
$GLOBALS['ES-SIGNIN-YOURNAME'] = "Votre nom :";
$GLOBALS['ES-SIGNIN-YOURPWSD'] = "Votre mot de passe (10 car max):";
$GLOBALS['ES-SIGNIN-YOUREMAIL'] = "Votre e-mail :";

$GLOBALS['ES-SIGNIN-REGISTER-SUBMIT'] = "Inscrivez-moi";
$GLOBALS['ES-SIGNIN-REGISTER-PROMPT'] = "Pas encore inscrit ?";
$GLOBALS['ES-SIGNIN-REGISTER-WARNING'] = "<BR>Attention, si votre adresse e-mail n'est pas valide, 
      nous ne serons pas en mesure de prendre contact avec vous.";

$GLOBALS['ES-SIGNIN-PWDFORGOT-SUBMIT'] = "Envoyez-le moi";
$GLOBALS['ES-SIGNIN-PWDFORGOT-PROMPT'] = "Mot de passe oubli� ?";

// "bienvenue � '$Nom' qui vient de s'inscrire."
$GLOBALS['ES-SIGNIN-WELCOME'] = "bienvenue � '<0>' qui vient de s'inscrire.";
$GLOBALS['ES-SIGNIN-THX-REGISTERING']  = "Merci de vous �tre inscrit, <0> . - vous allez �tre redirig� vers l'accueil.";

$GLOBALS['ES-SIGNIN-INVALID-LOGIN'] = "login invalide - <a href=\"jdm-signin.php\">Back</a>";
$GLOBALS['ES-SIGNIN-OK-LOGIN'] = "ok - vous allez �tre redirig� vers l'accueil. <a href=\"jdm-accueil.php\">Accueil</a>";

//------
// "Le $date, $what";
$GLOBALS['ES-EVENT-TMPL-DATE'] = "Le <0>, <1>";

//------

$GLOBALS['ES-MAIL-FROM'] = "JeuxDeMots";

$GLOBALS['ES-MAIL-LEVEL'] = "\nVous avez maintenant <0> Cr�dits, <1> points d'honneur et vous �tes niveau <2>.";

$GLOBALS['ES-MAIL-BOTTOM'] = "    
    � bient�t.
    Cordialement,
    JeuxDeMots
    http://www.lirmm.fr/jeuxdemots
    
    Astuce : <0>
    ";

$GLOBALS['ES-MAIL-RESULT-TITLE'] = "Bonnes nouvelles de JeuxDeMots";
$GLOBALS['ES-MAIL-RESULT'] = "Bonjour <0>,
    J'ai le plaisir de vous annoncer que vous venez de gagner <1> cr�dits avec '<2>' 
    avec l'aide de <3>.
    
    Les r�ponses donn�es par vous, <0>, pour cette partie ont �t� : <4> .
    Celles de <3> pour cette partie ont �t� : <5> .
    Les mots que vous avez en commun sont donc : <6> .
    
    La consigne �tait : <7> <2>
    
    De plus, vous gagnez quelques points d'honneur et votre niveau augmente l�g�rement.
    ";

$GLOBALS['ES-MAIL-GIFT-TITLE'] = "Une surprise de <0> vous attend � JeuxDeMots...";
$GLOBALS['ES-MAIL-GIFT'] = "Bonjour <0>,
    J'ai le plaisir de vous annoncer que vous venez de recevoir un cadeau de la part de <1>. 
    Rendez-vous dans le souk pour l'ouvrir. 
    ";

$GLOBALS['ES-MAIL-GIFT-DALTON'] = "Hey <0>, !
    Un des fr�res Dalton t'a fait un cadeau. Rendez-vous dans le souk pour l'ouvrir. 
    ";


$GLOBALS['ES-MAIL-PARRAINAGE-TITLE'] = "Invitation pour JeuxDeMots de la part de <0>";
$GLOBALS['ES-MAIL-PARRAINAGE'] = "Bonjour, \n\n

Si vous savez que Jean-Pierre Foucault, Thierry Beccaro, Julien Lepers et Laurence Boccolini ne sont 
pas respectivement philosophe, sportif, chanteur et plante potag�re du sud de l'Italie, vous venez 
de franchir la premi�re �tape pour participer � JeuxDeMots !

Vous r�viez de pouvoir encha�ner les � neuf points gagnants �, le � quatre � la  suite � et le 
� face-�-face � tout en pouvant choisir un � 50 / 50 � ou � l'appel � l'ami Google � sans devoir 
assimiler les r�gles de Motus et sans vous faire sortir parce que � franchement, pas conna�tre le 
titre de la derni�re chanson de Patrick S�bastien, ben, c'est carr�ment �tre trop nul � !!!
Et bien, JeuxDeMots est fait pour VOUS !

Vous voulez faire avancer la recherche sans devoir souscrire au 3637 
[mais ce n'est pas incompatible], devenir le Ma�tre Capello du wikipedia ou le Richard Virenque 
de l'encyclopaedia universalis ou tout simplement aider le CNRS (sans les OGM) : 
JeuxdeMots est aussi fait pour VOUS !

World of Warcraft, Second Life ou Disneyland� Resort Paris vous font encore peur ou vous n'avez pas
pris le train du Web2.0 � destination des MySpace, Skyblog, FaceBook et autres r�seaux sociaux : 
JeuxDeMot est tout de m�me fait pour VOUS !

Rejoignez nous et montrez ainsi � toute votre famille, vos amis et m�me � Madame Biglon
(votre ancienne ma�tresse d'�cole) votre valeur ! Et si vous ne voulez rien montrer,
vous pouvez toujours prendre un pseudo !

Bref, si vous voulez faire partie d'une aventure avant que Google ne la rach�te : venez participer 
� JeuxDeMots et inscrivez vous : http://www.lirmm.fr/jeuxdemots\n\n

\"<0>\" pense que vous �tes digne de nous rejoindre !\n";


//------

$GLOBALS['ES-MISC-CONTACT'] = "Contact";
$GLOBALS['ES-MISC-HOME'] = "Accueil";
$GLOBALS['ES-MISC-FORUM'] = "Forum";
$GLOBALS['ES-MISC-RANKING'] = "Classement";
$GLOBALS['ES-MISC-EVENTS'] = "Ev�nements";
$GLOBALS['ES-MISC-SOUK'] = "Souk";
$GLOBALS['ES-MISC-OPTIONS'] = "Options";
$GLOBALS['ES-MISC-HELP'] = "Aide";

$GLOBALS['ES-MISC-CREDIT'] = "Cr�dits : ";
$GLOBALS['ES-MISC-HONNOR'] = "Honneur : ";
$GLOBALS['ES-MISC-LEVEL'] = "Niveau : ";

$GLOBALS['ES-MISC-LOGIN'] = "D�connexion";
$GLOBALS['ES-MISC-LOGOUT'] = "Connexion";

$GLOBALS['ES-MISC-PLAYAS-PLAY'] = "Jouer";
$GLOBALS['ES-MISC-PLAYAS-GUEST'] = " invit�";
$GLOBALS['ES-MISC-PLAYAS-GUESTWARNING'] = "<font color=black>Vous allez jouer comme invit�.</font> <a href=\"http://www.lirmm.fr/jeuxdemots/jdm-signin.php\">Connectez-vous.</a>";

//------

$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-0'] = "Donner des IDEES ASSOCIEES au terme suivant :";
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-3'] = "Donner des THEMES/DOMAINES pour le terme suivant : <BR><font size=\"-1\">Par exemple, 'sports', 'm�decine', 'cin�ma', 'cuisine,' etc.</font>";
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-5'] = "Donner des SYNONYMES du terme suivant :";
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-6'] = "Donner des GENERIQUES pour le terme suivant : <BR><font size=\"-1\">Par exemple: 'v�hicule' pour 'voiture', 'f�lin', 'animal' pour 'chat'.</font>";
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-7'] = "Donner des CONTRAIRES pour le terme suivant : <BR><font size=\"-1\">Par exemple: 'froid' pour 'chaud', 'haut' pour 'bas'.</font>";
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-8'] = "Donner des SPECIFIQUES pour le terme suivant : <BR><font size=\"-1\">Par exemple : 'chat', 'chien', 'animal de compagnie',  etc. pour 'animal' - ou encore 'voiture', 'train', 'v�hicule spatial', etc. pour 'v�hicule'.</font>";
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-9'] = "Donner des PARTIES du terme suivant :<BR><font size=\"-1\">Une partie est une composante de l'objet, par exemple : 'moteur', 'roue', etc. pour 'voiture' - ou encore 'couverture', 'pages', 'chapitre' etc. pour 'livre'.</font>";
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-10'] = "Donner des TOUT du terme suivant :<BR><font size=\"-1\">Le tout est ce qui englobe/contient/poss�de la partie, par exemple : 'corps', 'bras', etc. pour 'coude' - ou encore 'banque' pour 'guichet'.</font>";
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-11'] = "Donner des LOCUTIONS pour le terme suivant :<BR><font size=\"-1\">Par exemple : 'langue au chat', 'chat de goutti�re', 'chat � neuf queues', ... pour 'chat'</font>";

$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-13'] = "Donner des SUJETS typiques pour le VERBE suivant :<BR><font size=\"-1\">Le sujet est celui qui effectue l'action, par exemple 'chat', 'animal', 'personne', ... pour 'manger'</font>";
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-14'] = "Donner des OBJETS typiques pour le VERBE suivant :<BR><font size=\"-1\">L'objet est ce qui <b>subit l'action</b>, par exemple : 'viande', 'fruit', 'bonbon', ... pour 'manger' ou encore 'personne', 'homme politique', 'otage', ... pour 'assassiner'</font>";
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-15'] = "Donner des LIEUX typiques pour le terme suivant :<BR><font size=\"-1\">Par exemple : 'pr�', '�curie', 'champs de courses', ... pour 'cheval'</font>";

$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-16'] = "Donner des INSTRUMENTS typiques pour le VERBE suivant :<BR><font size=\"-1\">L'instrument est quelque'chose avec lequel on peut effectuer l'action, par exemple : 'pelle', 'pioche', 'main', ... pour 'creuser'</font>";
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-17'] = "Donner des CARACTERISTIQUES typiques pour le terme suivant :<BR><font size=\"-1\">Par exemple : 'liquide', 'blanc', 'buvable', ... pour 'lait', ou 'coupant', 'tranchant', ... pour 'lame'</font>";

$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-20'] = "Qu'est ce qui est PLUS INTENSE que le terme suivant : <BR><font size=\"-1\">Par exemple : 'forte fi�vre', 'fi�vre de cheval', ... pour 'fi�vre' - ou encore 'vivre intens�ment' pour 'vivre'</font>";
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-21'] = "Qu'est ce qui est MOINS INTENSE que le terme suivant : <BR><font size=\"-1\">Par exemple : 'maisonette', ... pour 'maison' - ou encore 'marcher lentement', 'trainer' pour 'marcher'</font>";

$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-22'] = "Donner des mots de la MEME FAMILLE (par exemple : 'chanter', 'chanteur'... pour 'chant' - ou encore 'vente', 'vendeur', 'vendu' pour 'vendre') pour le terme suivant :";

$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-23'] = "Qu'est-ce qui poss�de la CARACTERISTIQUE suivante (par exemple, 'eau', 'vin', 'lait' pour 'liquide') :";
		
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-24'] = "Que peut faire le SUJET suivant (par exemple, 'manger', 'dormir', 'chasser' pour 'lion') :";
		
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-25'] = "Que peut-on faire avec l'INSTRUMENT suivant (par exemple, '�crire', 'dessiner', 'gribouiller' pour 'stylo') :";
		
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-26'] = "Que peut subir l'OBJET suivant (par exemple, ...) :";
		
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-27'] = "Donner des termes du DOMAINE suivant (par exemple, 'touche', 'penalty', 'but' pour 'Football') :";

$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-28'] = "Que trouve-t-on dans le LIEU suivant (par exemple, 'poisson', 'coquillage', 'algue' pour 'mer') :";
		
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-30'] = "Que peut-on faire dans le LIEU suivant (par exemple, 'manger', 'boire', 'commander' pour 'restaurant' -- des verbes sont demand�s) :";
		
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-31'] = "Dans quels LIEUX peut-on faire l'action suivante (par exemple, 'restaurant', 'cuisine', 'fast-food' pour 'manger' -- des lieux sont demand�s) :";
		
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-32'] = "A quels SENTIMENTS/EMOTIONS peut �tre associ� le terme suivant :";
				
$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-34'] = "De quelles MANIERES peut �tre effectu�e l'action suivante :<BR><font size=\"-1\">Il s'agira d'un adverbe ou d'un �quivalent, par exemple : 'rapidement', 'sur le pouce', 'goul�ment', 'salement' ... pour 'manger'</font>";

$GLOBALS['ES-GENGAME-FUNCTIONS-INSTR-35'] = "Quels SENS/SIGNIFICATIONS pouvez vous donner au terme suivant :<BR><font size=\"-1\">Il s'agira de termes �voquant chacun des sens possibles, par exemple : 'forces de l'ordre', 'contrat d'assurance', 'police typographique', ... pour 'police'</font>";

$GLOBALS['ES-TERMSELECT-FUNCTIONS-NAME-TEMPLATE'] = 'Nom%';
$GLOBALS['ES-TERMSELECT-FUNCTIONS-ADJ-TEMPLATE'] = 'Adj%';
$GLOBALS['ES-TERMSELECT-FUNCTIONS-ADV-TEMPLATE'] = 'Adv%';
$GLOBALS['ES-TERMSELECT-FUNCTIONS-ADV-TEMPLATE'] = 'Ver:Inf';

$GLOBALS['ES-READ-INSTRUCTIONS'] = 'Lisez la consigne !';

//------

$GLOBALS['ES-OPTIONS-TITLE'] = "JeuxDeMots : options";
$GLOBALS['ES-OPTIONS-PROMPT'] = "Options";
$GLOBALS['ES-OPTIONS-CSS-FORM'] = 	"CSS :<BR>
	<input <0> id=\"go_css_submit\" type=\"submit\" name=\"go_css_submit\" value=\"Changer\"> l'url
	<input  id=\"go_css_url\" type=\"text\" size = \"120\" name=\"go_css_url\" value=\"<1>\">
	(par d�faut mettre \"<2>\")";
$GLOBALS['ES-OPTIONS-CSS-EXAMPLE'] = "Exemple de css :";
$GLOBALS['ES-OPTIONS-CSS-MUSEUM-FORM'] = 	"CSS de votre Petit Mus�e :<BR>
	<input <0> id=\"go_css_museum_submit\" type=\"submit\" name=\"go_css_museum_submit\" value=\"Changer\"> l'url
	<input id=\"go_css_museum_url\" type=\"text\" size = \"120\" name=\"go_css_museum_url\" value=\"<1>\">";

$GLOBALS['ES-OPTIONS-PERSO-FORM'] = 	"Url personnelle :<BR>
	<input <0> id=\"go_perso_submit\" type=\"submit\" name=\"go_perso_submit\" value=\"Changer\"> l'url
	<input  id=\"go_perso_url\" type=\"text\" size = \"120\" name=\"go_perso_url\" value=\"<0>\">
	(par exemple mettre \"http://mapageperso.com\")";


//------

$GLOBALS['ES-HOME-TITLE'] = "JeuxDeMots : accueil";
$GLOBALS['ES-HOME-PROMPT'] = "Accueil";

$GLOBALS['ES-HOME-NEWS'] = "<P><blockquote>ANNONCE du <0>";

$GLOBALS['ES-HOME-INSTRUCTIONS-TITLE'] = "Comment �a marche ? Qu'est-ce qu'on doit faire ?<BR><BR>";

$GLOBALS['ES-HOME-INSTRUCTIONS'] = "Un terme va vous �tre pr�sent� ainsi qu'une consigne relative � ce terme. 
Pendant une p�riode d'une minute vous devez entrer autant de propositions que possible conform�ment � la consigne. 
Il s'agit en g�n�ral de fournir des termes que vous associez librement au terme pr�sent�.
Validez chaque proposition avec le bouton \"Envoyer\" ou avec la touche \"Entr�e\".
D'autres joueurs vont �tre confront�s au m�me terme.
Vous gagnerez des cr�dits lorsque les termes que vous avez donn�s correspondent � ceux des autres joueurs.<BR>
Plus un terme est pr�cis plus vous gagnerez de points, mais l'autre joueur y aura-t-il pens� ?<BR><BR>
Pensez � lire la <a href=\"jdm-signin.php\">Charte de JeuxDeMots</a>.<P><P>";


$GLOBALS['ES-HOME-MOSTWANTED'] = 'Most Wanted';
$GLOBALS['ES-HOME-COLLECTIONS'] = 'Collections';
$GLOBALS['ES-HOME-GIFT-WAITING'] = "Des cadeaux vous attendent dans le <A href=\"/jeuxdemots/jdm-list-gift.php\">souk</a>";
$GLOBALS['ES-HOME-RELATION-COUNT'] = "<0> relations ont �t� produites jusqu'� pr�sent, dont <1> tabous. Bravo !<P>";
$GLOBALS['ES-HOME-TERM-COUNT'] = "... et <0> termes, dont <1> en reliant <2>.";
$GLOBALS['ES-HOME-ACTIVE-PLAYER-COUNT'] = "<P><0> joueurs connect�s.";

//------
$GLOBALS['ES-SOUK-TITLE'] = "JeuxDeMots : le Souk";
$GLOBALS['ES-SOUK-PROMPT'] = "Le souk aux mille merveilles";

$GLOBALS['ES-SOUK-SUBTITLE-COMP'] = "Achat de comp�tences";
$GLOBALS['ES-SOUK-SUBTITLE-TREAS'] = "Gestion des tr�sors";

$GLOBALS['ES-SOUK-DEFIS-NOTERM'] = "Le terme '<0>' n'existe pas !";
$GLOBALS['ES-SOUK-DEFIS-NOTFREE'] = "Le terme <0> est libre !";
$GLOBALS['ES-SOUK-DEFIS-NOGUEST'] = "Un invit� ne peut pas tenter de capturer un mot";
$GLOBALS['ES-SOUK-DEFIS-NOTENOUGHMONEY'] = "L'investissement doit �tre sup�rieure � 5 fois la valeur du mot (<1>)";
$GLOBALS['ES-SOUK-DEFIS-NOMONEY'] = "Vous n'avez pas tant de cr�dit (<1>)";
$GLOBALS['ES-SOUK-DEFIS-NOTNOW'] = "Vous ne pouvez pas tenter de capturer ce mot maintenant, reessayez plus tard.";

$GLOBALS['ES-SOUK-COMP-ITEM'] = "Comp�tence";
$GLOBALS['ES-SOUK-COMP-HONNOR-MIN'] = "Honneur min";
$GLOBALS['ES-SOUK-COMP-CREDIT'] = "Cr�dits";
$GLOBALS['ES-SOUK-COMP-STATUS'] = "Statut";
$GLOBALS['ES-SOUK-COMP-QUOT'] = "Cotation";
$GLOBALS['ES-SOUK-COMP-SELECPC'] = "% de s�lection";
$GLOBALS['ES-SOUK-COMP-ADJUSTPC'] = "Ajustement %";


$GLOBALS['ES-SOUK-LIST-TRIAL-COMPLETED-FORM'] = "<li><form id=\"open_cr_trial<0>\" name=\"open_trial<0>\" method=\"post\" action=\"trialVote.php\" >
				<input <1> id=\"trial_vote_cr\" type=\"submit\" name=\"trial_vote_cr\" value=\"Visualiser\">
				<input  id=\"proc_id\" type=\"hidden\" name=\"proc_id\" value=\"<2>\">
		 		le proc�s <2> opposant '<3>' � '<4>' pour le terme '<5>'.
				</form></li>";

$GLOBALS['ES-SOUK-LIST-TRIAL-TODO-FORM'] ="<li><form id=\"open_trial<0>\" name=\"open_trial<0>\" method=\"post\" action=\"trialVote.php\" >
		<input id=\"trial_vote\" type=\"submit\" name=\"trial_vote\" value=\"Voter\">
		<input  id=\"proc_id\" type=\"hidden\" name=\"proc_id\" value=\"<1>\">
		 - �ch�ance dans <2> heures.
		</form></li>";

$GLOBALS['ES-MAKE-TRIAL-FORM'] = "<form id=\"form-trial\" name=\"form-trial\" method=\"post\" action=\"buyTrial.php\" >
			Faire un <input <0> id=\"submit-buy-trial\" type=\"submit\" name=\"submit-buy-trial\" value=\"proc�s\"> 
				 (500 Cr) � ce joueur
			 <input id=\"trial_id\" type=\"hidden\" name=\"trial_id\" value=\"<1>\">
			 - indiquez le motif :
			 <TEXTAREA NAME=\"trial_reason\" COLS=40 ROWS=6></TEXTAREA>
			 </form>";

//------ Le souk by MM
// global warnings
$GLOBALS['ES-SOUK-WARNING-BADPLAYER'] = "Ce joueur n'existe pas";
$GLOBALS['ES-SOUK-WARNING-BADTERM'] = "Ce terme n'existe pas";
$GLOBALS['ES-SOUK-WARNING-NOCASH'] = "Vous n'avez pas assez de cr�dits";
$GLOBALS['ES-SOUK-WARNING-NOHONNOR'] = "Vous n'avez pas assez d'honneur";
$GLOBALS['ES-SOUK-WARNING-NOGUEST'] = "Le joueur invit� ne peut pas faire cela - inscrivez-vous.";
$GLOBALS['ES-SOUK-WARNING-NOCREDIT'] = "La maison ne fait pas cr�dit !";

//"le joueur '$player' a offert un cadeau au joueur '$dest'."
$GLOBALS['ES-SOUK-GIFT-EVENT'] = "le joueur &#39;<0>&#39; a offert un cadeau au joueur &#39;<1>&#39;.";
$GLOBALS['ES-SOUK-BUYCOMP-EVENT'] = "le joueur &#39;<0>&#39; a achet� une comp�tence.";
$GLOBALS['ES-SOUK-HYPEWORDS'] = "Mots � la mode";
$GLOBALS['ES-SOUK-INWORDS'] = "Mots tendance";
$GLOBALS['ES-SOUK-OUTWORDS'] = "Mots has been";

$GLOBALS['ES-SOUK-HYPEWORDS-VIEW-FORM'] = "<input id=\"formhypewordsumit\" type=\"submit\" name=\"formhypewordsumit\" value=\"Visualiser\"> (10 Cr)
		les mots � la mode.";

$GLOBALS['ES-SOUK-CAGNOTTE-VIEW-FORM'] = "	<input id=\"cagnotesubmit\" type=\"submit\" name=\"cagnotesubmit\" value=\"Visualiser\"> (5 Cr)
		le montant de la cagnotte.";

$GLOBALS['ES-SOUK-CAGNOTTE-DISPLAY-WARNING'] = "La cagnote contient <0> Cr�dits. 
		    Les joueurs <1> et <2> sont en t�te avec un score de <3> Cr�dits. 
		    La cagnote tombera dans <4> jackpot(s)"; 

$GLOBALS['ES-SOUK-GAME-GIFT-FORM'] = "<input id=\"formmakegift\" type=\"submit\" name=\"formmakegift\" value=\"Offrir\"> (<2> Cr)
		une partie avec le terme 
		<input id=\"giftterm\" type=\"text\" name=\"giftterm\" value=\"<0>\">
		au joueur
		<input id=\"giftplayername\" type=\"text\" name=\"giftplayername\" value=\"<1>\"> 
		<input id=\"relation_type_gift\" type=\"hidden\" name=\"relation_type_gift\" value=\"0\">";


$GLOBALS['ES-SOUK-GAME-GIFT-WARNING-NOINPUT'] = "Entrez un terme et un nom de joueur";
$GLOBALS['ES-SOUK-GAME-GIFT-WARNING-SELFGIFT'] = "Ce serait sympa, mais on ne s'offre pas des cadeaux a soi-m�me";
$GLOBALS['ES-SOUK-GAME-GIFT-WARNING-NOERROR'] = "Cadeau achet� et envoy�";

$GLOBALS['ES-SOUK-GAME-GIFT-LIST'] = "Les cadeaux qui m'attendent :";
$GLOBALS['ES-SOUK-GAME-GIFT-LIST-EMPTY'] = "Personne ne vous a offert de cadeau.";

$GLOBALS['ES-SOUK-GAME-GIFT-OPEN-FORM'] = "<input id=\"opengiftbut\" type=\"submit\" name=\"opengiftbut\" value=\"Ouvrir\">
		 un cadeau offert par <0> le <1>";

$GLOBALS['ES-SOUK-GRAPH-VIEW-FORM'] = " <input id=\"viewgraph\" type=\"submit\" name=\"viewgraph\" value=\"Visualiser\"> (0 Cr) le graphe.";
$GLOBALS['ES-SOUK-TRIAL-VIEW-FORM'] = " <input id=\"viewtrial\" type=\"submit\" name=\"viewtrial\" value=\"Visualiser\"> (0 Cr) les proc�s.";
$GLOBALS['ES-SOUK-GIFT-VIEW-FORM'] = " <input id=\"viewgift\" type=\"submit\" name=\"viewgift\" value=\"Visualiser\"> (0 Cr) mes cadeaux.";

$GLOBALS['ES-SOUK-WORD-LOOKUP-FORM'] = "  <input id=\"gotermlist\" type=\"submit\" name=\"gotermlist\" value=\"Chercher\">
       (0 Cr) les mots contenant la cha�ne 
	    <input  id=\"goterm\" type=\"text\" name=\"goterm\" value=\"<0>\">";
	    
$GLOBALS['ES-SOUK-WORDLIST-DISPLAY-WARNING-NOINPUT'] = "Entrez un mot non vide";

$GLOBALS['ES-SOUK-PARRAINAGE-FORM'] = "	  <input id=\"parainage_submit\" type=\"submit\" name=\"parainage_submit\" value=\"Parrainer\">
      (0 Cr) un joueur ayant l'adresse email suivante
	  <input  size=\"50\" id=\"parainage_email\" type=\"text\" name=\"parainage_email\" value=\"machin@bidule.truc\">
	  ";

$GLOBALS['ES-SOUK-PARRAINAGE-WARNING-NOERROR'] = "Demande de parainage envoy�e (adresse email non v�rifi�e)";
$GLOBALS['ES-SOUK-PARRAINAGE-WARNING-WRONGEMAIL'] = "Essayez avec une autre adresse...";

$GLOBALS['ES-SOUK-GENERATE-DEFIS-FORM'] = "<input id=\"parainage_submit\" type=\"submit\" name=\"defis_submit\" value=\"Capturer\"> le mot
	  <input  size=\"20\" id=\"parainage_email\" type=\"text\" name=\"defis_word\" value=\"le mot\">
	pour un montant de 
	<input  size=\"20\" id=\"parainage_email\" type=\"text\" name=\"defis_amount\" value=\"1000\">
	Cr�dits.";

$GLOBALS['ES-SOUK-WORD-CAPTURE-FORM'] = "<font color=\"red\">Tentative de capture pr�te</font> 
<input id=\"chosengamesubmit\" type=\"submit\" name=\"chosengamesubmit\" value=\"Go !\">
		";

$GLOBALS['ES-SOUK-WORD-CAPTURE-EVENT'] = "le joueur &#39;<0>&#39; tente de capturer &#39;<1>&#39;.";

$GLOBALS['ES-SOUK-CHECK-TREASURE-FORM'] = 	"<input id=\"gochecktreasuressubmit\" type=\"submit\" name=\"gochecktreasuressubmit\" value=\"Afficher\"> les tr�sors du joueur
	<input  id=\"gochecktreasures\" type=\"text\" name=\"gochecktreasures\" value=\"\">";

$GLOBALS['ES-SOUK-BUYCOMP-SUBMIT'] = "Acheter";
$GLOBALS['ES-SOUK-BUYCOMP-DONE'] = "poss�d�";

$GLOBALS['ES-SOUK-WORD-RELATION-FORM'] = " <input id=\"gotermsubmit\" type=\"submit\" name=\"gotermsubmit\" value=\"Chercher\"> (DEBUG) le mot
	    <input  id=\"gotermrel\" type=\"text\" name=\"gotermrel\" value=\"\">";

$GLOBALS['ES-SOUK-GAMELIST-FORM'] = "<input id=\"gamelist_submit\" type=\"submit\" name=\"gamelist_submit\" value=\"parties en cours\"> (DEBUG)";

$GLOBALS['ES-SOUK-CHOSENGAME-FORM'] = "<input id=\"chosengamesubmit\" type=\"submit\" name=\"chosengamesubmit\" value=\"Jouer\">";

$GLOBALS['ES-SOUK-STATISTICS-FORM'] = "<input id=\"bd_stats_submit\" type=\"submit\" name=\"bd_stats_submit\" value=\"Statistiques\"> (DEBUG)";

$GLOBALS['ES-SOUK-CHECKOWNER-FORM'] = "	<input id=\"gotermownersubmit\" type=\"submit\" name=\"gotermownersubmit\" value=\"V�rifier\"> le statut du mot
	<input  id=\"gotermowner\" type=\"text\" name=\"gotermowner\" value=\"\">";


$GLOBALS['ES-SOUK-PROB-WARNING-NOERROR'] = "Probabilit� ajust�e";
$GLOBALS['ES-SOUK-COMP-WARNING-NOERROR'] = "Relation achet�e";

//------

$GLOBALS['ES-EVENT-TITLE'] = "JeuxDeMots : �v�nements";
$GLOBALS['ES-EVENT-PROMPT'] = "Ev�nements";

// "les joueurs '$gamecreatorname' et '$playername' ont gagn� <b>$points</b> cr�dits avec <b>'$fentry'</b>  pour la comp�tence '$relgpname'. Jackpot !"
$GLOBALS['ES-EVENT-WIN-JACKPOT'] = "les joueurs &#39;<0>&#39; et &#39;<1>&#39; ont gagn� <b><2></b> cr�dits avec <b>'<3>'</b> 
pour la comp�tence '<4>'. Jackpot !";

// "les joueurs '$gamecreatorname' et '$playername' ont gagn� <b>$points</b> cr�dits avec <b>'$fentry'</b>."
$GLOBALS['ES-EVENT-WIN'] = "les joueurs &#39;<0>&#39; et &#39;<1>&#39; ont gagn� <b><2></b> cr�dits avec <b>&#39;<3>&#39;</b>.";

$GLOBALS['ES-EVENT-JACKPOT-TMPL'] = "Jackpot !";
$GLOBALS['ES-EVENT-TRIALWON-TMPL'] = "a gagn� un proc�s";
$GLOBALS['ES-EVENT-TRIALNO-TMPL'] = "non-lieu pour le";
$GLOBALS['ES-EVENT-NEWPLAYER-TMPL'] = "qui vient de s'inscrire.";

//------

$GLOBALS['ES-RANKING-TITLE'] = "JeuxDeMots : classement";
$GLOBALS['ES-RANKING-PROMPT'] = "Classement";

$GLOBALS['ES-RANKING-LABEL-NAME'] = "Nom";
$GLOBALS['ES-RANKING-LABEL-HONNOR'] = "Honneur";
$GLOBALS['ES-RANKING-LABEL-HONNORMAX'] = "Honneur<br>max";
$GLOBALS['ES-RANKING-LABEL-CREDITS'] = "Cr�dits";
$GLOBALS['ES-RANKING-LABEL-NBPLAYED'] = "Parties<br>jou�es";
$GLOBALS['ES-RANKING-LABEL-LEVEL'] = "Niveau";
$GLOBALS['ES-RANKING-LABEL-EFF'] = "Efficacit�";
$GLOBALS['ES-RANKING-LABEL-NBTREASURE'] = "Tr�sors";

$GLOBALS['ES-RANKING-BEST-SCORES'] = "Meilleurs scores";
$GLOBALS['ES-RANKING-BEST-COUPLES'] = "Meilleurs couples";
$GLOBALS['ES-RANKING-BEST-FRIENDS'] = "Vos meilleurs amis";

$GLOBALS['ES-RANKING-AND'] = " et ";
$GLOBALS['ES-RANKING-MINUTE'] = "mn";
$GLOBALS['ES-RANKING-15MINUTE'] = "1/4h";
$GLOBALS['ES-RANKING-30MINUTE'] = "1/2h";
$GLOBALS['ES-RANKING-HOUR'] = "h";
$GLOBALS['ES-RANKING-DAY'] = "j";
$GLOBALS['ES-RANKING-MONTH'] = "m";
$GLOBALS['ES-RANKING-YEAR'] = " an";
$GLOBALS['ES-RANKING-CREDITS'] = "Cr";

//------

$GLOBALS['ES-MUSEUM-TITLE'] = "JeuxDeMots : petit mus�e";
$GLOBALS['ES-MUSEUM-PROMPT'] = "Le petit mus�e de <0>";
$GLOBALS['ES-MUSEUM-WARNING-NOTREASURE'] = "Ce joueur n'a pas de tr�sor";



//------
$GLOBALS['ES-RESULT-TITLE'] = "JeuxDeMots : r�sultats";
$GLOBALS['ES-RESULT-PG-PROMPT'] = "<P>R�ponses donn�es par vous (<0>) pour cette partie (<1>) : ";
$GLOBALS['ES-RESULT-NOTHING'] =  " --rien-- ";
$GLOBALS['ES-RESULT-PROMPT'] = "R�sultat";

$GLOBALS['ES-RESULT-NOPROP'] = "<h2>Vous n'avez rien propos�... je suis s�r que vous allez y arriver :)</h2>";

$GLOBALS['ES-RESULT-NOGUEST'] = "Possibilit� non offerte aux invit�s :)";

$GLOBALS['ES-RESULT-PROMPT-BUYPLAY-HEADER'] = "";
$GLOBALS['ES-RESULT-PROMPT-BUYPLAY-FORM'] = "<P>Vous pouvez racheter cette partie et la proposer � d'autres joueurs";
$GLOBALS['ES-RESULT-TOKEN'] = "jetons";
$GLOBALS['ES-RESULT-OR'] = "ou";

$GLOBALS['ES-RESULT-PROMP-INVOKEWORD-FORM'] ="<P><font color=\"red\"> Jackpot !</font>
		<BR>Vous �tes dou�. Pour vous r�compenser, on vous invite � souhaiter qu'un terme de votre choix soit plus souvent propos�.";
$GLOBALS['ES-RESULT-INVOKE'] = "Invoquer";
$GLOBALS['ES-RESULT-THETERM'] = "le terme";


$GLOBALS['ES-RESULT-PROMPT-YOURANSWERS'] = "R�ponses donn�es par vous (<0>) pour cette partie (<1>) : ";
$GLOBALS['ES-RESULT-PROMPT-HISANSWERS'] = "R�ponses donn�es par le cr�ateur (<0>) de cette partie (<1>) : ";
$GLOBALS['ES-RESULT-PROMPT-INTERSECTION'] = "Intersection : ";


$GLOBALS['ES-RESULT-PROMPT-CREATED-PLAY'] = "Votre partie va �tre propos�e � deux autres joueurs. On vous �crira...";

$GLOBALS['ES-RESULT-BUYTOKEN-FORM'] = "Si vous �tes content de vous, vous pouvez la proposer � davantage de joueurs.<P>Acheter 
		<input id=\"submit5\" type=\"submit\" name=\"submit5\" value=\"5 jetons\"> (<0> Cr) ou
		<input id=\"submit10\" type=\"submit\" name=\"submit10\" value=\"10 jetons\"> (<1> Cr)";

$GLOBALS['ES-RESULT-BUY-TRIAL-TOKEN-FORM'] = "<P>Vous pouvez investir dans un jeton de proc�s.<br>Acheter 
		<input id=\"submit1\" type=\"submit\" name=\"submit1\" value=\"1 jeton\"> (200 Cr)";

$GLOBALS['ES-RESULT-PROMPT-GAIN'] = "<h2>Vous gagnez <0> cr�dits et <1> point(s) d'honneur </h2>";

$GLOBALS['ES-RESULT-PROMPT-LEVEL-GAIN'] = "Vous gagnez <br><0><br> niveau.";
$GLOBALS['ES-RESULT-PROMPT-LEVEL-LOOSE'] = "Vous perdez <br><0><br> niveau.";

$GLOBALS['ES-RESULT-PROMPT-TERMLEVEL-GAIN'] = "<br>Le terme <0> gagne <1> point de niveau.";
$GLOBALS['ES-RESULT-PROMPT-TERMLEVEL-LOOSE'] = "<br>Le terme <0> perd <1> point de niveau.";

//------
$GLOBALS['ES-BUYPLAY-CONGRAT-INVEST'] = "Bravo pour cet investissement dans <0> jetons pour un montant de <1> cr�dits.";
$GLOBALS['ES-BUYPLAY-CHEAT'] = "<h1>D�sol� le joueur invit� ne peut pas racheter une partie. 
    	Si vous lisez ceci, c'est que la session a du expirer</h1>";
$GLOBALS['ES-BUYPLAY-EVENT'] = "le joueur &#39;<0>&#39; a rachet� une partie pour <b>&#39;<1>&#39;</b>.";

//------
$GLOBALS['ES-BUYTOKEN-CONGRAT-INVEST'] = "Bravo pour cet investissement dans <0> jetons pour un montant de <1> cr�dits.";
$GLOBALS['ES-BUYTOKEN-CHEAT'] = "D�sol� le joueur invit� ne peut pas acheter des jetons. 
    	Si vous lisez ceci, c'est que la session a du expirer";
$GLOBALS['ES-BUYTOKEN-SORRY'] = "D�sol�, vous n'avez pas cette somme de <0> cr�dits, il fallait v�rifier avant :)";

// "le joueur '$login' a investi dans le mot <b>'$fentry'</b>."
$GLOBALS['ES-BUYTOKEN-EVENT'] = "le joueur &#39;<0>&#39; a investi dans <b>&#39;<1>&#39;</b>.";

//------
$GLOBALS['ES-BUY-TRIAL-TOKEN-CONGRAT-INVEST'] = "Bravo pour cet investissement dans <0> jetons de proc�s pour <1> cr�dits.";
$GLOBALS['ES-BUY-TRIAL-TOKEN-CHEAT'] = "D�sol� le joueur invit� ne peut pas acheter des jetons. 
    	Si vous lisez ceci, c'est que la session a du expirer";
$GLOBALS['ES-BUY-TRIAL-TOKEN-SORRY'] = "D�sol�, vous n'avez pas cette somme de <0> cr�dits, il fallait v�rifier avant :)";
$GLOBALS['ES-BUY-TRIAL-TOKEN-EVENT'] = "le joueur &#39;<0>&#39; a achet� un jeton de proc�s.";


//------
$GLOBALS['ES-BUYWISH-CONGRAT-INVEST'] = "Bravo pour cette invocation du terme '<0>' pour un montant de <1> cr�dits. Il appara�tra un jour... peut-�tre.";
$GLOBALS['ES-BUYWISH-CHEAT'] = "<h1>D�sol� le joueur invit� ne peut pas invoquer de mots. 
    	Si vous lisez ceci, c'est que la session a du expirer</h1>";
$GLOBALS['ES-BUYWISH-SORRY'] = "<h1>D�sol�, vous n'avez pas cette somme de <0> cr�dits, il fallait v�rifier avant :)</h1>";

// "le joueur '$login' a investi dans le mot <b>'$fentry'</b>."
$GLOBALS['ES-BUYWISH-EVENT'] = "le joueur &#39;<0>&#39; a invoqu� un mot...";
$GLOBALS['ES-BUYWISH-EMPTY-WORD'] = "mot vide, tant pis !";

//------
$GLOBALS['ES-TRIAL-CONGRAT-INVEST'] = "Bravo pour cet investissement dans un proc�s.";
$GLOBALS['ES-TRIAL-CHEAT'] = "<h1>D�sol� le joueur invit� ne peut pas faire de proc�s. Si vous lisez ceci, c'est que la session a du expirer</h1>";
$GLOBALS['ES-BUYTRIAL-SORRY'] = "<h1>D�sol�, vous n'avez pas cette somme de <0> cr�dits, il fallait v�rifier avant :)</h1>";


//------
$GLOBALS['ES-PLAYER-PERM-CAGNOTTE-BINGO'] = "Bingo ! Vous faites tomber la cagnote de <0> cr�dits.";
$GLOBALS['ES-PLAYER-PERM-CAGNOTTE-EVENT'] = "les joueurs '<0>' et '<1>' ont gagn� la cagnote d'un montant de <2> et se la partagent... et en font profiter d'autres";
$GLOBALS['ES-PLAYER-PERM-CAGNOTTE-MAIL'] = "Bonjour,\nla cagnote est tomb�e et vous gagnez des cr�dits...\n";
$GLOBALS['ES-PLAYER-PERM-CAGNOTTE-MAIL-TITLE'] = "Des Cr�dits...";

//------

$GLOBALS['ES-REQUEST-VERB-TRANSTIVE'] = "SELECT id FROM `Nodes` WHERE w > 50 
	AND id IN (SELECT node1 FROM Relations WHERE node2 
	IN (SELECT id FROM Nodes WHERE type=18 and name LIKE 'Ver:Transitif')
	) order by w DESC LIMIT <0>";

$GLOBALS['ES-REQUEST-VERB-EASY'] = "SELECT id FROM `Nodes` WHERE w > 50 
	AND id IN (SELECT node1 FROM Relations WHERE node2 
	IN (SELECT id FROM Nodes WHERE type=4 and name LIKE 'Ver:Inf')
	) order by w DESC LIMIT <0>";

$GLOBALS['ES-REQUEST-NOUN-EASY'] = "SELECT id FROM `Nodes` WHERE w > 50 
	AND id IN (SELECT node1 FROM Relations WHERE node2 
	IN (SELECT id FROM Nodes WHERE type=4 and name LIKE 'Nom:%')
	) order by w DESC LIMIT <0>";

$GLOBALS['ES-REQUEST-NOUN-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'Nom:%' and type=4); ";

$GLOBALS['ES-REQUEST-VERB-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'Ver:Inf%' and type=4); ";

$GLOBALS['ES-REQUEST-NOUN-ADJ-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'Adj%' OR name LIKE 'Adv%' and type=4);";

$GLOBALS['ES-REQUEST-NOUN-ADJ-VER-ADV-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE  type=4 and name LIKE 'Nom%' OR name LIKE 'Ver:Inf' OR name LIKE 'Adj%' OR name LIKE 'Adv%');";

//------
// "le joueur '$player_name' fait de la magie noire avec le mot '$fentry'."
$GLOBALS['ES-BLACK-MAGIC-EVENT'] = "le joueur '<0>' fait de la magie noire avec '<1>'.";
//"le joueur '$player_name' fait de la magie blanche avec le mot '$fentry'."
$GLOBALS['ES-WHITE-MAGIC-EVENT'] = "le joueur '<0>' fait de la magie blanche avec '<1>'."

?>
