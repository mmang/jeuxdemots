<?php session_start();?>
<?php 
	include_once 'misc_functions.php';
	include_once 'intern_lexical_signature_functions.php';

?>
<?php
    openconnexion();
	$_SESSION[ssig() . 'redirect_if_session_finished'] = 'N';
?>
<html>
 <head>
    <title><?php echo "Devine AKI"; ?></title>
    <?php if (fetch_arg('SERVICE') == "") {header_page_encoding();} ?>
    
  </head>
  <body>
<?php if (fetch_arg('SERVICE') == "") {
		topblock();
		echo "\n<img STYLE=\"opacity:0.35;height:600px;z-index:-1;position:absolute;top:-200px;right:-300px;\"  
					src=\"pics/circle.png\">";
	} 
?>

<?php
if (!isset($_SESSION[ssig() . 'nbtries'])) {
	anr_reinit();
	$_SESSION['aki_fini'] = false;
}



function handle_AKI_service () {
	$service = fetch_arg('SERVICE');
	if ($service == 'ASK') {
		$nb = fetch_arg('NB');
		
		// decoder les indices
		$ar_sol = array();
		for ($i=0 ; $i<$nb ; $i++) {
			$tag = "CLUE$i";
			$clue = fetch_arg("$tag");
			
			$clue = UTF8_decode($clue);
			//echo "<P>$clue";
			$clue_id = term_exist_in_BD_p($clue);
			if ($clue_id <= 0) {
				$clue_noid = UTF8_encode($clue);
				echo "<RESULT><INCONNU>$clue_noid</INCONNU></RESULT>";
				return;
			}
		
		
			$assocs = make_term_assoc($clue);
			//print_r($assocs);
			if ($i > 0) {
				$maxitem = my_array_find_max($ar_sol);
				unset($ar_sol[$maxitem]);
				//echo "<br>max=$maxitem";
				
				$ar_sol = my_array_intersect_faster($ar_sol,$assocs);
				//print_r($ar_sol);
				
			} else {
				$ar_sol = $assocs;	
			}
		}
		//arsort($ar_sol);
		//print_r($ar_sol);
		//$tab = array_keys($ar_sol);
		$proposition = $maxitem = my_array_find_max($ar_sol);	
		if ($proposition == "") {
			echo "<RESULT><NORESULT></NORESULT></RESULT>";
		} else {
			$proposition = UTF8_encode($proposition);
			echo "<RESULT>$proposition</RESULT>";
		}
		return;
	}
	if ($service == 'TELL') {
		$ar = array();
		$nb = fetch_arg('NB');
		$cible = fetch_arg('CIBLE');
		$cible = UTF8_decode($cible);
		if ($cible == '') {
			//echo "NO TARGET";
			echo "<RESULT><VALIDATION>NO TARGET</VALIDATION></RESULT>";				
			return;
		}
		for ($i=0 ; $i<$nb ; $i++) {
			$tag = "CLUE$i";
			$clue = fetch_arg("$tag");
			
			$clue = UTF8_decode($clue);
			//echo "<P>$clue";
			$clue_id = term_exist_in_BD_p($clue);
			if ($clue_id <= 0) {
				//echo "INVALID term $clue";
				echo "<RESULT><VALIDATION>INVALID $clue</VALIDATION></RESULT>";				
			
			} else {
				$ar[$i] = $clue;
			}
		}
		//print_r($ar);
		echo "<RESULT><VALIDATION>$cible</VALIDATION></RESULT>";	
		validate_AKI_relation($cible, $ar);
		return;
	}
	
}



function validate_AKI_relation($cible, $ar) {
	//echo "<br>validate_AKI_relation()";
	$rel = 666;
	//print_r($ar);
	//echo "<br>$cible";
	if (count($ar) == 0) {return;}
	if ($cible == '') {return;}
	
	$node2 = term_exist_in_BD_p($cible);
	if ($node2 == 0) {
			//echo "$cible est un terme inconnu";
			$targ = addslashes($cible);
			$query = "INSERT INTO Nodes (name, type, creationdate) VALUES('$targ', 666, CURRENT_TIMESTAMP)";
			//echo $query;
			$r =  @mysql_query($query) or die("pb in validate_AKI_relation : $query");
			$node2 = term_exist_in_BD_p($cible);
	}
	if ($node2 == 0) {echo "<br>ca chie"; return;}
	
	for ($i=0 ; $i<count($ar) ; $i++) {
		$term_1 = trim($ar[$i]);
		//echo "<br>$term_1";
		if ($term_1 != "") {
			$node1 = term_exist_in_BD_p($term_1);
			if ($node1 > 0) {
				//echo "<br><br>$term_1 > $domaine : jdm_update_network_relation ($node1, $node2, $rel);";
				AKI_update_network_relation($node2, $node1, $rel);
			}
		}
	}
	anr_reinit($term);
}


function AKI_update_network_relation ($node1, $node2, $relationtype) {
	if ($node1 == $node2) {return;}
		//echo "<br>AKI_update_network_relation ($node1, $node2, $relationtype)";
		$query = "SELECT id, w FROM `Relations` WHERE node1 = '$node1' AND node2 = '$node2' 
			AND type='$relationtype';"  ;
		$r3 =  @mysql_query($query) or die("pb in update_network_relation 3 : $query");
		$nb3 = mysql_num_rows($r3);
		if ($nb3 > 0) {
				// la relation existe deja
				//echo "<BR>updating relation between $node1 and $node2";
				$poids = mysql_result($r3 , 0 , 1);
				$query = "UPDATE `Relations` SET w = w+1 WHERE node1= '$node1' AND node2= '$node2' 
							AND type='$relationtype';" ;
				//echo $query;
				$r =  @mysql_query($query) or die("pb in AKI_update_network_relation 6 : $query");
				//echo "<br> la relation existe d�ja !!";
				//$r =  @mysql_query($query) or die("pb update_network_relation 4 : $query");
			} else {
				// la relation n existe pas -> on insere
				//echo "<BR>inserting relation between $node1 and $node2";
				$query = "INSERT INTO Relations (node1, node2, type, w, creationdate) 
							VALUES('$node1', '$node2', '$relationtype', 1, CURRENT_TIMESTAMP)";
				//echo $query;
				$r =  @mysql_query($query) or die("pb in AKI_update_network_relation 6 : $query");
				//echo "<br> la relation est valid�e !!";
			}
}


function make_anr_form() {

	//echo "'". $_POST['gotermrel'] . "'";
	//echo "'". $_GET['gotermrel'] . "'";
	//echo "arg 1= " . $_POST['go_anr_answer_submit'];
	//echo "arg 2= " . $_POST['go_anr_submit'];
	
	if (fetch_arg('SERVICE') != ""){
		handle_AKI_service();
		return;
	}
	
	$term = trim($_POST['go_anr']);
	if ($term == "") {
		$term = trim($_GET['go_anr']);	
	}
	$term_id = term_exist_in_BD_p($term);
	$term = stripslashes($term);
	
	if ($term_id > 0) {
		$term_to_display = '';
	} else {
		$term_to_display = $term;
	}
	if ($_SESSION['aki_fini'] == true) {$tag = 'disabled';} 
	echo "<form id=\"go_anr_form\" name=\"go_anr_form\" method=\"post\" action=\"AKI.php\" >
	    <input $tag id=\"go_anr_submit\" type=\"submit\" name=\"go_anr_submit\" value=\"Soumettre\"> l'indice
	    <input  id=\"go_anr\" type=\"text\" name=\"go_anr\" value=\"$term_to_display\" size=100>
	    </form>";
	
	echo "<script TYPE=\"text/javascript\">
	$('go_anr').focus();
	</script>";
    
	if (($_POST['go_anr_submit']!= "") || ($_GET['go_anr_submit']!= "")){
		make_anr_guess($term);
	}
	if (($_POST['go_anr_reinit']!= "") || ($_GET['go_anr_reinit']!= "")){
		anr_reinit($term);
		$_SESSION['aki_fini'] = false;
	}
	
	if ($_POST['go_anr_answer_submit'] != '') {
		echo "Merci je vais retenir cela !";
		//echo $_POST['go_anr_answer'];
		$cible = stripslashes(trim($_POST['go_anr_answer'], ' ,:;.'));
		$ar = array_unique($_SESSION[ssig() . 'clues']);
		validate_AKI_relation($cible, $ar);
	}
	
	if ($_POST['go_anr_found_submit'] != '') {
		echo "Je suis trop fort !";
		//echo $_POST['go_anr_answer'];
		$cible = $_SESSION['aki_last_prop'];
		$ar = array_unique($_SESSION[ssig() . 'clues']);
		validate_AKI_relation($cible, $ar);
	}

}

function make_answer_form () {
	//echo "coucou";
	if ($_POST['go_anr_answer_submit'] != '') {
	} else {
		echo "<form id=\"go_anwser_form\" name=\"go_anwser_form\" method=\"post\" action=\"AKI.php\" >
	     Il s'agissait de 
	    <input id=\"go_anr_answer\" type=\"text\" name=\"go_anr_answer\" value=\"\" size=50>
	    <input id=\"go_anr_answer_submit\" type=\"submit\" name=\"go_anr_answer_submit\" value=\"Ok\">
	    <br>(faites attention aux majuscules/minuscules surtout si c'est un nom propre)
	    </form>";
		echo "<script TYPE=\"text/javascript\">
			$('go_anr_answer').focus();
			</script>";
	}
}

function make_found_form() {
	echo "<P><form id=\"go_found_form\" name=\"go_found_form\" method=\"post\" action=\"AKI.php\" >
	     <input id=\"go_anr_found_submit\" type=\"submit\" name=\"go_anr_found_submit\" value=\"C'est la bonne r�ponse !\">
	     </form>";
}

function anr_reinit() {
	$_SESSION[ssig() . 'assoc'] = array();
	$_SESSION[ssig() . 'clues'] = array();
	$_SESSION[ssig() . 'guesses'] = array();
	$_SESSION[ssig() . 'propositions'] = array();
	$_SESSION[ssig() . 'nbtries'] = 0;
	
	$_SESSION[ssig() . 'last_chance_done'] = false;
}


function make_term_assoc_faster ($term) {
		
		$curtime = time();
	
		$tag = 'make_term_assoc' . rand();
		start_time_record($tag);
		$ar = array();
		
		$term_id = term_exist_in_BD_p($term);
		if ($term_id <= 0) {
			return $ar;
		}
		
	    $query= "SELECT node1, w, touchdate FROM Relations WHERE
	    node2 = \"$term_id\"
	    and type not in (1, 2, 4, 12, 18, 29, 36)";
	    //echo "<br>$query<BR>";
	   	$r =  @mysql_query($query) or die("pb in make_term_assoc : $query");
	    $nb = mysql_num_rows($r);
	    for ($i=0 ; $i<$nb ; $i++) {
			$assoc_id = mysql_result($r , $i , 0);
			$assoc_w = mysql_result($r , $i , 1);
			$date = mysql_result($r , $i , 2);
			
			//echo "diff = $curtime - $date";
			
			if (($curtime - $date) < 3600) {$assoc_w = $assoc_w+100; echo " bonus" ;}
			$assoc = get_term_from_id($assoc_id);
			if (($assoc != '***') && (stripos($assoc, '::') === false)) {
				$ar[$assoc] = $ar[$assoc]+$assoc_w;
			}
		}
			
		$query= "SELECT node2, w, touchdate FROM Relations WHERE
	    node1 = \"$term_id\"
	    and type not in (1, 2, 4, 12, 18, 29, 36)";
		$r =  @mysql_query($query) or die("pb in make_term_assoc : $query");
	    $nb = mysql_num_rows($r);
	    for ($i=0 ; $i<$nb ; $i++) {
			$assoc_id = mysql_result($r , $i , 0);
			$assoc_w = mysql_result($r , $i , 1);
			$assoc = get_term_from_id($assoc_id);
	    	$date = mysql_result($r , $i , 2);
			if (($curtime - $date) < 3600) {$assoc_w = $assoc_w+100; echo " bonus" ;}
	   		if (($assoc != '***') && (stripos($assoc, '::') === false)) {
				$ar[$assoc] = $ar[$assoc]+$assoc_w;
			}
		}
		unset($ar[$term]);
		
		$duree = end_time_record($tag);
		echo ("<p>2 duree pour '$tag' = $duree");
		echo "<br>ar a" . count($ar) . "items";
		return($ar);	
}


function make_term_assoc($term) {
		//$tag = 'make_term_assoc_faster' . rand();
		//start_time_record($tag);
		$curtime = time();
		
		$ar_id = array();
		$ar = array();
		
		$term_id = term_exist_in_BD_p($term);
		if ($term_id <= 0) {
			return $ar;
		}
		
	    $query= "SELECT node1, w, touchdate FROM Relations WHERE
	    node2 = \"$term_id\"
	    and type not in (1, 2, 4, 12, 18, 29, 36)";
	    //echo "<br>$query<BR>";
	   	$r =  @mysql_query($query) or die("pb in make_term_assoc : $query");
	    $nb = mysql_num_rows($r);
	    for ($i=0 ; $i<$nb ; $i++) {
			$assoc_id = mysql_result($r , $i , 0);
			$assoc_w = mysql_result($r , $i , 1);
			$date = strtotime(mysql_result($r , $i , 2));
	    	if (($curtime - $date) < 3600) {$assoc_w = $assoc_w + 100; 
	    		//echo "<br>diff = $curtime - $date == " . ($curtime - $date);
	    		//echo "<br>$assoc_w";
	    		}	    	
	    	
			$ar_id[$assoc_id] = $ar_id[$assoc_id]+$assoc_w;
		}
			
		$query= "SELECT node2, w, touchdate FROM Relations WHERE
	    node1 = \"$term_id\"
	    and type not in (1, 2, 4, 12, 18, 29, 36)";
		$r =  @mysql_query($query) or die("pb in make_term_assoc : $query");
	    $nb = mysql_num_rows($r);
	    for ($i=0 ; $i<$nb ; $i++) {
			$assoc_id = mysql_result($r , $i , 0);
			$assoc_w = mysql_result($r , $i , 1);
			$date = strtotime(mysql_result($r , $i , 2));
	    	if (($curtime - $date) < 3600) {$assoc_w = $assoc_w + 100; 
	    		//echo "<br>diff = $curtime - $date == " . ($curtime - $date); ;
	    		//echo "<br>$assoc_w";
	    		}	    	
	    	
			$ar_id[$assoc_id] = $ar_id[$assoc_id]+$assoc_w;
		}
		
		foreach ($ar_id as $key => $value) {
			$assoc = get_term_from_id($key);
	   		if (($assoc != '***') && (stripos($assoc, '::') === false)) {
				$ar[$assoc] = $value;
			}
		}
		unset($ar[$term]);
		//$duree = end_time_record($tag);
		//echo ("<p>2 duree pour '$tag' = $duree");
		//echo "<br>ar a" . count($ar) . "items";
		return($ar);	
}



function find_possible($term) {
	$possible = ucwords($term);
	$newid = term_exist_in_BD_p($possible);
	if ($newid > 0) {
		echo("<br>Peut-�tre pensez-vous � '$possible' ?");
		return;
	}
	$possible = ucfirst($term);
	$newid = term_exist_in_BD_p($possible);
	if ($newid > 0) {
		echo("<br>Peut-�tre pensez-vous � '$possible' ?");
		return;
	}
	$possible = strtoupper($term);
	$newid = term_exist_in_BD_p($possible);
	if ($newid > 0) {
		echo("<br>Peut-�tre pensez-vous � '$possible' ?");
		return;
	}
	$possible = strtolower($term);
	$newid = term_exist_in_BD_p($possible);
	if ($newid > 0) {
		echo("<br>Peut-�tre pensez-vous � '$possible' ?");
		return;
	}
}


function make_anr_guess($term) {
    //echo "<P>in display_wordrelation_list value :" . $_POST['gotermsubmit'] . "--" . $_POST['gotermrel'];
    if (($_POST['go_anr_submit']!= "") || ($_GET['go_anr_submit']!= "")){
    	
    if 	($term == "") {
    	display_warning("<br>entrez un terme !");
    	return;
    }
	$id = term_exist_in_BD_p($term);
	
	if ($id == 0) {
		display_warning("<br>Le terme $term n'existe pas !");
		find_possible($term);
		echo "<p>";
		}
	else {
		$_SESSION[ssig() . 'clues'][$_SESSION[ssig() . 'nbtries']] = $term;
			
	 

	    $newar3 = make_term_assoc($term);
	  // arsort($newar3);
	    
		$nbt = $_SESSION[ssig() . 'nbtries'];
		$_SESSION[ssig() . 'cand'][$nbt] = $newar3;
		
		//echo "<p>candidats == ";
		//print_r($newar3);
		
		
		if ($nbt > 0) {
			$_SESSION[ssig() . 'guesses'][$nbt] = my_array_intersect_faster($_SESSION[ssig() . 'guesses'][$nbt-1],
												            $_SESSION[ssig() . 'cand'][$nbt]);
		} else {
			$_SESSION[ssig() . 'guesses'][0] = $_SESSION[ssig() . 'cand'][0];
			
		}
		//arsort($_SESSION[ssig() . 'guesses'][$nbt]);
		
		//echo "<p>essai num $nbt";
		//echo "<p>suggestions == ";
		//print_r($_SESSION[ssig() . 'guesses'][$nbt]);
		
		
		//$tab = array_keys($_SESSION[ssig() . 'guesses'][$nbt]);
		//$proposition = $tab[0];
		$proposition = my_array_find_max($_SESSION[ssig() . 'guesses'][$nbt]);
		$nbprop = count($_SESSION[ssig() . 'guesses'][$nbt]);
		
		$nbtbis = $nbt+1;
		if (($nbtbis > 2) && ($_SESSION[ssig() . 'last_chance_done'] == false) && ($proposition == "")) {
			$_SESSION[ssig() . 'last_chance_done'] = true;
			$proposition = find_last_chance();
		}
		
		if ($proposition == "") {
			
			$_SESSION['aki_last_prop'] = '';
			echo "<P>Apr�s $nbtbis indice(s), je suis perdu, d�sol� je donne ma langue au chat !";
			echo "<RESULT></RESULT>";
			$_SESSION['aki_fini'] = false;
			make_answer_form ();
		} else {
			$formated_proposition = format_entry($proposition);
			if ($_SESSION[ssig() . 'last_chance_done'] == true) {
				echo "<P>Apr�s $nbtbis indice(s), il s'agit peut-�tre de : ";
			} else {
				if ($nbprop == 1) {
					echo "<P>Apr�s $nbtbis indice(s), il s'agit s�rement  de : ";
				} else {
					echo "<P>Apr�s $nbtbis indice(s), je pense qu'il s'agit de : ";
				}
			}
			echo "<center><font size=\"6\" color=\"red\"><RESULT>$formated_proposition</RESULT></font></font></center>";
			/*echo "\n<img STYLE=\"opacity:0.35;height:250px;width:2500px;z-index:-1;position:absolute;top:-20px;right:200px;\"  
					src=\"pics/circle.png\">";
			*/
		$_SESSION['aki_last_prop'] = $proposition;
		echo "<center>";
		make_found_form();
		echo "</center>";
		
		$_SESSION[ssig() . 'propositions'][$nbt] = $proposition;
		$_SESSION[ssig() . 'guesses'][$nbt][$proposition] = 0;
		}
		
		$_SESSION[ssig() . 'nbtries'] = $_SESSION[ssig() . 'nbtries'] +1 ;
	    }
    }
}

function my_array_intersect($ar1, $ar2) {
	//$tag = 'my_array_intersect' . rand();
	//start_time_record($tag);
	$newar = array();
	$tab = array_keys($ar1);
	$nb = count($tab);
	for ($i=0 ; $i<$nb ; $i++) {
		if ($ar1[$tab[$i]]>0 && $ar2[$tab[$i]]>0) {
			$newar[$tab[$i]] = sqrt($ar1[$tab[$i]] * $ar2[$tab[$i]]);
		}
	}
	//	print_r($newar);
	//$duree = end_time_record($tag);
	//echo ("<p>2 duree pour '$tag' = $duree");
	return $newar;
}


function my_array_intersect_faster($ar1, $ar2) {
	//$tag = 'my_array_intersect_faster' . rand();
	//start_time_record($tag);
	$newar = array();
	foreach ($ar1 as $key => $value) {
		if ($ar1[$key]>0 && $ar2[$key]>0) {
			$newar[$key] = sqrt($ar1[$key] * $ar2[$key]);
		}
	}
	//$duree = end_time_record($tag);
	//echo ("<p>2 duree pour '$tag' = $duree");
	return $newar;
}

function my_array_find_max($ar1) {
	$max = 0;
	$maxitem = '';
	foreach ($ar1 as $key => $value) {
		if ($value > $max) {
			$max = $value;
			$maxitem = $key;
		}
	}
	return $maxitem;
}

function display_table() {
	if (fetch_arg('SERVICE') == ""){
	$nb = count($_SESSION[ssig() . 'propositions']);
	if ($nb > 0) {
	echo "<TABLE border=\"1\" width=\"70%\" cellspacing=\"0\" cellpadding=\"10%\"
	summary=\"AKI\" bgcolor=\"white\" style=\"opacity:0.8;\">
	<TR ><TH  align=\"right\" width=\"40%\" color=\"#006699\">
		<font color=\"#006699\">Vos indices</font>
		<TH  align=\"left\" width=\"40%\">
		<font color=\"#006699\">Mes propositions</font>
	<TR ><TH border=\"1\" bgcolor=\"\" align=\"right\" width=\"40%\">
	";
	
	$nb = count($_SESSION[ssig() . 'clues']);
	if ($nb >= 0) {
	}
	for ($i=0 ; $i<$nb ; $i++) {
		$clue = $_SESSION[ssig() . 'clues'][$i];
		echo "<font size=\"-1\" color=\"#006699\">$clue</font><br>";
	}
	
	echo "<TH align=\"left\" width=\"40%\">";
	
	for ($i=0 ; $i<$nb ; $i++) {
		$prop = $_SESSION[ssig() . 'propositions'][$i];
		$formated_proposition = format_entry($prop);
		echo "<font size=\"-1\" color=\"#006699\">$formated_proposition</font><br>";
	}
	echo "</TABLE>";
		
	}
	}
}

function find_last_chance() {
	//return "";
	$skip = array();
	$nb = count($_SESSION[ssig() . 'clues']);
	for ($i=0 ; $i<$nb ; $i++) {
		$clue = $_SESSION[ssig() . 'clues'][$i];
		$skip[$clue] = 1;
	}
	$nb = count($_SESSION[ssig() . 'propositions']);
	for ($i=0 ; $i<$nb ; $i++) {
		$prop = $_SESSION[ssig() . 'propositions'][$i];
		$skip[$prop] = 1;
	}
	//print_r($skip);
	
	$newsig = array();
	$nb = count($_SESSION[ssig() . 'clues']);
	if ($nb >= 0) {
	}
	for ($i=0 ; $i<$nb ; $i++) {
		$clue = $_SESSION[ssig() . 'clues'][$i];
		//echo "indices : $clue ";
		$sig = AKI_get_sig ($clue);
		if ($i==$nb-1) {
			$newsig = add_sig_count($newsig,$sig, 1.5);
		} else {
			$newsig = add_sig_count($newsig,$sig);	
		}
	}
	arsort($newsig);
	//decode_sig_nosort($newsig, 100);
	//print_r($newsig);
	foreach ($newsig as $key => $value) {
		$term = get_term_from_id($key);
		//echo "terme : $term";
		if (($skip[$term] == 1) || ($term == '***') || (stripos($assoc, '::') === 0)) {
		} else {return $term;}
	}
}

function AKI_get_sig ($term) {
	$sig = compute_full_lexical_signature($term, 2000);
	//echo "<br>term: "; decode_sig_nosort($sig, 30);
	return($sig);
	
	/*
	$id1 = term_exist_in_BD_p($term);
	$check1 = check_need_update($id1);
	if ($check1 == false) {
		//echo "fetching..."; flush();
		$sig = unserialize_sig(fetch_signature($id1));
	} else {
		//echo "computing..."; flush();
		$sig = compute_full_lexical_signature_level ($term, 1);
	}
	return($sig);*/
}


	
function make_reinit_form() {
	if (fetch_arg('SERVICE') == ""){
		echo "<form id=\"go_anr_form\" name=\"go_anr_form\" method=\"post\" action=\"AKI.php\" >
			<input id=\"go_anr_reinit\" type=\"submit\" name=\"go_anr_reinit\" value=\"Recommencer\">
	    	</form><P>";
	}
}

?>

<div class="jdm-level1-block">
	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
    <?php if (fetch_arg('SERVICE') == ""){
    	echo "Pensez � quelque chose et je vais tenter de le deviner..."; }
    ?>
    </div>
	</div>
</div>

<div class="jdm-level2-block">
<TABLE border="0" width="100%" cellspacing="0" cellpadding="0%"
	summary="jeuxdemots" style="opacity=0.8;">

<TR><TH valign="top" width="200pts">
	<?php make_reinit_form();?>
	
	<TH align="center"  valign="top">
    <?php  make_anr_form();?>
    
    <TH valign="top" width="200pts" align="right">
    <?php if (fetch_arg('SERVICE') == ""){
	 echo "<font size=\"1\" face=\"Verdana\" color=\"#006699\">";
	 echo "<P>Entrez un indice et cliquez sur \"Soumettre\". Attention aux majuscules et aux accents. 
    Vous pouvez proposer plusieurs fois le m�me mot. Un mot peut �tre un terme compos� 
    (comme pomme de terre).";
	 echo "</font>";
    }
    ?>
   
<TR><TH valign="top" width="200pts"> 
	<TH align="center" valign="top">
	<?php display_table(); ?>
	<TH valign="top" width="200pts">
</TABLE>
</div>

<?php 
if (fetch_arg('SERVICE') == ""){
    bottomblock();
    closeconnexion();
}
?>

  </body>
</html>
