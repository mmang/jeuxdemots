<?php  
$GLOBALS['FR-'] = '[Chaîne non trouvée]';

//------
$GLOBALS['FR-ENCODING-GENERAL'] = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>";
$GLOBALS['FR-ENCODING-MAIL'] = "UTF-8";
$GLOBALS['FR-ENCODING-DB'] = "utf8";
$GLOBALS['FR-ENCODING-MUSEUM'] = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>";

//------ relations
$GLOBALS['FR-r_associated'] = 'idée';
$GLOBALS['FR-r_associated-HELP'] = 'Tout terme lié d\'une façon ou d\'une autre au mot cible... Ce mot vous fait penser à quoi ?';
$GLOBALS['FR-r_acception'] = 'acception';
$GLOBALS['FR-r_acception-HELP'] = 'acception';
$GLOBALS['FR-r_definition'] = 'définition';
$GLOBALS['FR-r_definition-HELP'] = 'définition';
$GLOBALS['FR-r_domain'] = 'domaine';
$GLOBALS['FR-r_domain-HELP'] = 'Il est demandé de fournir des domaines relatifs au mot cible. Par exemple, pour \'corner\', on pourra donner les domaines \'football\' ou \'sport\'.';
$GLOBALS['FR-r_pos'] = 'r_pos';
$GLOBALS['FR-r_pos-HELP'] = 'r_pos';
$GLOBALS['FR-r_syn'] = 'synonyme';
$GLOBALS['FR-r_syn-HELP'] = 'À partir d\'un terme, il est demandé d\'énumérer les synonymes ou quasi-synonymes de ce terme.';
$GLOBALS['FR-r_isa'] = 'générique';
$GLOBALS['FR-r_isa-HELP'] = '\'animal\' est un générique de \'chat\', \'mammifère\', \'être vivant\' etc. en sont d\'autres...';
$GLOBALS['FR-r_anto'] = 'contraire';
$GLOBALS['FR-r_anto-HELP'] = '\'chaud\' est le contraire de \'froid\', vous vous rappelez ? :)';
$GLOBALS['FR-r_hypo'] = 'spécifique';
$GLOBALS['FR-r_hypo-HELP'] = '\'mouche\', \'abeille\', \'guêpe\' sont des spécifiques de \'insecte\'...';
$GLOBALS['FR-r_has_part'] = 'partie';
$GLOBALS['FR-r_has_part-HELP'] = 'Il faut donner des parties/constituants/éléments du mot cible. Par exemple, \'voiture\' pourrait avoir comme parties : \'porte\', \'roue\', \'moteur\', ...';
$GLOBALS['FR-r_holo'] = 'tout';
$GLOBALS['FR-r_holo-HELP'] = 'Le tout est ce qui contient l\'objet en question. Pour \'main\', on aura \'bras\', \'corps\', \'personne\', etc... On peut aussi voir le tout comme l\'ensemble auquel appartient un élément, comme \'classe\' pour \'élève\'.';
$GLOBALS['FR-r_locution'] = 'locution';
$GLOBALS['FR-r_locution-HELP'] = 'A partir d\'un terme, il est demandé d\'énumérer les locutions, expression ou mots composés en rapport avec ce terme. Par exemple, pour \'moulin\', ou pourra avoir \'moulin à vent\', \'moulin à eau\', \'moulin à café\'. Pour \'vendre\', on pourra avoir \'vendre la peau de l\'ours avant de l\'avoir tué\', \'vendre à perte\', etc...';
$GLOBALS['FR-r_agent'] = 'agent typique';
$GLOBALS['FR-r_agent-HELP'] = 'L\'agent (qu\'on appelle aussi le sujet) est l\'entité qui effectue l\'action. Par exemple dans - Le chat mange la souris -, l\'agent est le chat.';
$GLOBALS['FR-r_patient'] = 'patient typique';
$GLOBALS['FR-r_patient-HELP'] = 'Le patient (qu\'on appelle aussi l\'objet) est l\'entité qui subit l\'action. Par exemple dans - Le chat mange la souris -, le patient est la souris.';
$GLOBALS['FR-r_flpot'] = 'potentiel de FL';
$GLOBALS['FR-r_flpot-HELP'] = 'potentiel de FL';
$GLOBALS['FR-r_lieu'] = 'lieu';
$GLOBALS['FR-r_lieu-HELP'] = 'À partir d\'un nom d\'objet (ou autre), il est demandé d\'énumérer les lieux typiques ou peut se trouver l\'objet en question.';
$GLOBALS['FR-r_instr'] = 'instrument typique';
$GLOBALS['FR-r_instr-HELP'] = 'L\'instrument est l\'objet avec lequel on fait l\'action. Dans - Il mange sa salade avec une fourchette -, fourchette est l\'instrument.';
$GLOBALS['FR-r_carac'] = 'caractéristique';
$GLOBALS['FR-r_carac-HELP'] = 'Pour une terme donné, en général un objet, il est demandé d\'énumérer les caractéristiques possibles et/ou typiques de cet objet. Par exemple, pour \'eau\' on pourra avoir \'liquide\', \'froide\', \'chaude\', etc.';
$GLOBALS['FR-r_data'] = 'r_data';
$GLOBALS['FR-r_data-HELP'] = 'r_data';
$GLOBALS['FR-r_lemma'] = 'r_lemma';
$GLOBALS['FR-r_lemma-HELP'] = 'r_lemma';
$GLOBALS['FR-r_magn'] = 'magn';
$GLOBALS['FR-r_magn-HELP'] = 'La magnification ou amplification, par exemple - forte fièvre - ou - fièvre de cheval - pour fièvre. Ou encore - amour fou - pour amour, - peur bleue - pour peur.';
$GLOBALS['FR-r_antimagn'] = 'antimagn';
$GLOBALS['FR-r_antimagn-HELP'] = 'L\'inverse de la magnification, par exemple - bruine - pour pluie.';
$GLOBALS['FR-r_familly'] = 'famille';
$GLOBALS['FR-r_familly-HELP'] = 'Des mots de la même famille sont demandés. Par exemple, pour \'lait\' on pourrait mettre \'laitier\', \'laitage\', \'laiterie\', etc.';
$GLOBALS['FR-r_carac-1'] = 'caractéristique-1';
$GLOBALS['FR-r_carac-1-HELP'] = 'caractéristique-1';
$GLOBALS['FR-r_agent-1'] = 'agent typique-1';
$GLOBALS['FR-r_agent-1-HELP'] = 'agent typique-1';
$GLOBALS['FR-r_instr-1'] = 'instrument-1';
$GLOBALS['FR-r_instr-1-HELP'] = 'instrument-1';
$GLOBALS['FR-r-patient-1'] = 'patient-1';
$GLOBALS['FR-r-patient-1-HELP'] = 'patient-1';
$GLOBALS['FR-r_domain-1'] = 'domaine-1';
$GLOBALS['FR-r_domain-1-HELP'] = 'domaine-1';
$GLOBALS['FR-r_lieu-1'] = 'lieu-1';
$GLOBALS['FR-r_lieu-1-HELP'] = 'lieu-1';
$GLOBALS['FR-r_lieu-1-HELP'] = 'A partir d\'un lieu, il est demandé d\'énumérer ce qu\'il peut typiquement s\'y trouver.';
$GLOBALS['FR-r_pred'] = 'prédicat';
$GLOBALS['FR-r_pred-HELP'] = 'prédicat';
$GLOBALS['FR-r_lieu_action'] = 'lieu_action';
$GLOBALS['FR-r_lieu_action-HELP'] = 'énumérer les action typiques possibles dans ce lieu.';
$GLOBALS['FR-r_action_lieu'] = 'action_lieu';
$GLOBALS['FR-r_action_lieu-HELP'] = 'A partir d\'une action (un verbe), énumérer les lieux typiques possibles où peut etre réalisée cette action.';
$GLOBALS['FR-r_maniere'] = 'manière';
$GLOBALS['FR-r_maniere-HELP'] = 'De quelles MANIÈRES peut être effectuée l\'action (le verbe) proposée. Il s\'agira d\'un adverbe ou d\'un équivalent comme une locution adverbiale, par exemple : \'rapidement\', \'sur le pouce\', \'goulûment\', \'salement\' ... pour \'manger\'.';
$GLOBALS['FR-r_sentiment'] = 'sentiment';
$GLOBALS['FR-r_sentiment-HELP'] = 'Pour un terme donné, évoquer des SENTIMENTS ou EMOTIONS que vous pourriez associer à ce terme. Par exemple, la joie, le plaisir, le dégoût, la peur, la haine, l\'amour, l\'indifférence, l\'envie, etc.';
$GLOBALS['FR-r_sens'] = 'sens';
$GLOBALS['FR-r_sens-HELP'] = 'Quels SENS/SIGNIFICATIONS pouvez vous donner au terme proposé. Il s\'agira de termes évoquant chacun des sens possibles, par exemple : \'forces de l\'ordre\', \'contrat d\'assurance\', \'police typographique\', ... pour \'police\'.';
$GLOBALS['FR-r_cause'] = 'cause';
$GLOBALS['FR-r_cause-HELP'] = 'B (que vous devez donner) est une cause possible de A. A et B sont des verbes ou des noms.  Exemples : se blesser -> tomber ; vol -> pauvreté ; incendie -> négligence ; mort --> maladie ; etc.';
$GLOBALS['FR-r_consequence'] = 'conséquence';
$GLOBALS['FR-r_consequence-HELP'] = 'B (que vous devez donner) est une conséquence possible de A. A et B sont des verbes ou des noms.  Exemples : tomber -> se blesser ; faim -> voler ; allumer -> incendie ; négligence --> accident ; etc.';
$GLOBALS['FR-r_telique'] = 'rôle télique';
$GLOBALS['FR-r_telique-HELP'] = 'Le rôle télique indique le but ou la fonction du nom ou du verbe. Par exemple, couper pour couteau, scier pour scie, etc.';
$GLOBALS['FR-r_agentif'] = 'rôle agentif';
$GLOBALS['FR-r_agentif-HELP'] = 'Le rôle agentif indique le mode de création du nom. Par exemple, construire pour maison, rédiger ou imprimer pour livre, etc.';

//------

$GLOBALS['FR-GENGAME-TITLE'] = "JeuxDeMots : jeu d'associations";
$GLOBALS['FR-GENGAME-GAMETYPE'] = "Jeu d'associations";
$GLOBALS['FR-GENGAME-WARN-NEWINSTR'] = "<blink>Attention<br>nouvelle consigne</blink>";

$GLOBALS['FR-GENGAME-PASS'] = "Passer";
$GLOBALS['FR-GENGAME-BUYTIME'] = "Acheter 30 s";
$GLOBALS['FR-GENGAME-SEND'] = "Envoyer";
$GLOBALS['FR-GENGAME-TABOO'] = "Mots usagés";
$GLOBALS['FR-GENGAME-NOPROPTERM'] = "Aucun terme proposé";
$GLOBALS['FR-GENGAME-LASTPROPTERM'] = "Dernier terme proposé : ";

$GLOBALS['FR-GENGAME-PROMPT-GENERATION'] = "Création";
$GLOBALS['FR-GENGAME-PROMPT-INSTRUCTION'] = "Lecture";
$GLOBALS['FR-GENGAME-PROMPT-INGAME'] = "Temps";
$GLOBALS['FR-GENGAME-PROMPT-RES'] = "Résultats";

$GLOBALS['FR-GENGAME-REMAININGTIME'] = "Temps restant";
$GLOBALS['FR-GENGAME-PROMPT-WORDLEVEL'] = "Niveau : ";

$GLOBALS['FR-GENGAME-COUNTDOWN-SECONDS'] = " s";
$GLOBALS['FR-GENGAME-COUNTDOWN-END'] = "fini";

$GLOBALS['FR-GENGAME-TRICK'] = "Astuce";

$GLOBALS['FR-GENGAME-30SECONDS-BUTTON'] = "pics/30sB.gif";
$GLOBALS['FR-GENGAME-OK-BUTTON'] = "pics/J2M-OKB.gif";
$GLOBALS['FR-GENGAME-BONUS-IMAGE'] = "pics/JDM-bonus.gif";


//------
$GLOBALS['FR-SIGNIN-CONNECT-SUBMIT'] = "connexion";
$GLOBALS['FR-SIGNIN-CONNECT-PROMPT'] = "Connectez vous";
$GLOBALS['FR-SIGNIN-YOURNAME'] = "Votre nom :";
$GLOBALS['FR-SIGNIN-YOURPWSD'] = "Votre mot de passe (10 car max):";
$GLOBALS['FR-SIGNIN-YOUREMAIL'] = "Votre e-mail :";

$GLOBALS['FR-SIGNIN-REGISTER-SUBMIT'] = "Inscrivez-moi";
$GLOBALS['FR-SIGNIN-REGISTER-PROMPT'] = "Pas encore inscrit ?";
$GLOBALS['FR-SIGNIN-REGISTER-WARNING'] = "<BR>Attention, si votre adresse e-mail n'est pas valide, 
      nous ne serons pas en mesure de prendre contact avec vous.";

$GLOBALS['FR-SIGNIN-PWDFORGOT-SUBMIT'] = "Envoyez-le moi";
$GLOBALS['FR-SIGNIN-PWDFORGOT-PROMPT'] = "Mot de passe oublié ?";

// "bienvenue à '$Nom' qui vient de s'inscrire."
$GLOBALS['FR-SIGNIN-WELCOME'] = "bienvenue à '<0>' qui vient de s'inscrire.";
$GLOBALS['FR-SIGNIN-THX-REGISTERING']  = "Merci de vous être inscrit, <0> . - vous allez être redirigé vers l'accueil.";

$GLOBALS['FR-SIGNIN-INVALID-LOGIN'] = "login invalide - <a href=\"jdm-signin.php\">Back</a>";
$GLOBALS['FR-SIGNIN-OK-LOGIN'] = "ok - vous allez être redirigé vers l'accueil. <a href=\"jdm-accueil.php\">Accueil</a>";

$GLOBALS['FR-SIGNIN-NON-AVAILABLE'] = "Ce pseudo est déjà pris...";
$GLOBALS['FR-SIGNIN-USER-NOT-FOUND'] = "utilisateur inexistant";
$GLOBALS['FR-SIGNIN-PASSWD-FORGET-TITLE'] = "Mot de passe pour JeuxDeMots";
$GLOBALS['FR-SIGNIN-PASSWD-FORGET-BODY'] = "Votre mot de passe est <0>";
$GLOBALS['FR-SIGNIN-PASSWD-FORGET-SENT'] = "Mot de passe envoyé pour <0> <BR>";
$GLOBALS['FR-SIGNIN-PAGE-TITLE'] = "Connexion / Inscription";
$GLOBALS['FR-SIGNIN-VALID-EMPTY-EMAIL'] = "<font color='#FF0000'>L'E-Mail ne peut pas être vide</font>";
$GLOBALS['FR-SIGNIN-VALID-EMPTY-PASSWD'] = "<font color='#FF0000'>Le Mdp ne peut pas être vide</font>";
$GLOBALS['FR-SIGNIN-VALID-INVALID-EMAIL'] = "<font color='#FF0000'>L'E-Mail que vous avez fourni contient des caractères non-autorisés</font>";
$GLOBALS['FR-SIGNIN-VALID-EMPTY-USERNAME'] = "<font color='#FF0000'>Le nom ne peut pas être vide</font>";

//------
// "Le $date, $what";
$GLOBALS['FR-EVENT-TMPL-DATE'] = "Le <0>, <1>";

//------

$GLOBALS['FR-MAIL-FROM'] = "JeuxDeMots";

$GLOBALS['FR-MAIL-LEVEL'] = "\nVous avez maintenant <0> Crédits, <1> points d'honneur et vous êtes niveau <2>.";

$GLOBALS['FR-MAIL-BOTTOM'] = "    
    À bientôt.
    Cordialement,
    JeuxDeMots
    http://www.lirmm.fr/jeuxdemots
    
    Astuce : <0>
    ";

$GLOBALS['FR-MAIL-RESULT-TITLE'] = "Bonnes nouvelles de JeuxDeMots";
$GLOBALS['FR-MAIL-RESULT'] = "Bonjour <0>,
    J'ai le plaisir de vous annoncer que vous venez de gagner <1> crédits avec '<2>' 
    avec l'aide de <3>.
    
    Les réponses données par vous, <0>, pour cette partie ont été : <4> .
    Celles de <3> pour cette partie ont été : <5> .
    Les mots que vous avez en commun sont donc : <6> .
    
    La consigne était : <7> <2>
    
    De plus, vous gagnez quelques points d'honneur et votre niveau augmente légèrement.
    ";

$GLOBALS['FR-MAIL-GIFT-TITLE'] = "Une surprise de <0> vous attend à JeuxDeMots...";
$GLOBALS['FR-MAIL-GIFT'] = "Bonjour <0>,
    J'ai le plaisir de vous annoncer que vous venez de recevoir un cadeau de la part de <1>. 
    Rendez-vous dans le souk pour l'ouvrir. 
    ";

$GLOBALS['FR-MAIL-GIFT-DALTON'] = "Hey <0>, !
    Un des frères Dalton t'a fait un cadeau. Rendez-vous dans le souk pour l'ouvrir. 
    ";


$GLOBALS['FR-MAIL-PARRAINAGE-TITLE'] = "Invitation pour JeuxDeMots de la part de <0>";
$GLOBALS['FR-MAIL-PARRAINAGE'] = "Bonjour, \n\n

Si vous savez que Jean-Pierre Foucault, Thierry Beccaro, Julien Lepers et Laurence Boccolini ne sont 
pas respectivement philosophe, sportif, chanteur et plante potagère du sud de l'Italie, vous venez 
de franchir la première étape pour participer à JeuxDeMots !

Vous rêviez de pouvoir enchaîner les « neuf points gagnants », le « quatre à la  suite » et le 
« face-à-face » tout en pouvant choisir un « 50 / 50 » ou « l'appel à l'ami Google » sans devoir 
assimiler les règles de Motus et sans vous faire sortir parce que « franchement, pas connaître le 
titre de la dernière chanson de Patrick Sébastien, ben, c'est carrément être trop nul » !!!
Et bien, JeuxDeMots est fait pour VOUS !

Vous voulez faire avancer la recherche sans devoir souscrire au 3637 
[mais ce n'est pas incompatible], devenir le Maître Capello du wikipedia ou le Richard Virenque 
de l'encyclopaedia universalis ou tout simplement aider le CNRS (sans les OGM) : 
JeuxdeMots est aussi fait pour VOUS !

World of Warcraft, Second Life ou Disneyland® Resort Paris vous font encore peur ou vous n'avez pas
pris le train du Web2.0 à destination des MySpace, Skyblog, FaceBook et autres réseaux sociaux : 
JeuxDeMot est tout de même fait pour VOUS !

Rejoignez nous et montrez ainsi à toute votre famille, vos amis et même à Madame Biglon
(votre ancienne maîtresse d'école) votre valeur ! Et si vous ne voulez rien montrer,
vous pouvez toujours prendre un pseudo !

Bref, si vous voulez faire partie d'une aventure avant que Google ne la rachète : venez participer 
à JeuxDeMots et inscrivez vous : http://www.lirmm.fr/jeuxdemots\n\n

\"<0>\" pense que vous êtes digne de nous rejoindre !\n";

$GLOBALS['FR-MAIL-OTHER-PLAYER-BEWARE'] = 'ATTENTION';

$GLOBALS['FR-MAIL-OTHER-PLAYER-RULES'] = "
 En envoyant ce mail, vous acceptez :
	<br>(1) de communiquer votre adresse email au destinataire, 
	afin que ce dernier puisse vous répondre (auquel cas, vous aurez connaissance de son
	adresse) ;
	<BR>(2) de voir votre message gardé un certain temps pour vérifier les cas d'abus ;
	<BR>(3) de dépenser 500 crédits pour les frais attenants :)
	<BR> Si trop d'abus sont constatés, cette fonctionnalité sera désactivée.";

$GLOBALS['FR-MAIL-OTHER-PLAYER-FROM-TO'] = "De '<0>' à '<1>'";
$GLOBALS['FR-MAIL-OTHER-PLAYER-TITLE'] = "Titre :";
$GLOBALS['FR-MAIL-OTHER-PLAYER-MESSAGE'] = "Message :";
$GLOBALS['FR-MAIL-OTHER-PLAYER-SEND-BUTTON'] = "Envoi";
$GLOBALS['FR-MAIL-OTHER-PLAYER-MESSAGE-SENT'] = "Message envoyé";
$GLOBALS['FR-MAIL-OTHER-PLAYER-NOT-ENOUGH-CREDITS'] = "Pas assez de crédit";

//------

$GLOBALS['FR-MISC-CONTACT'] = "Contact";
$GLOBALS['FR-MISC-HOME'] = "Accueil";
$GLOBALS['FR-MISC-FORUM'] = "Forum";
$GLOBALS['FR-MISC-RANKING'] = "Classement";
$GLOBALS['FR-MISC-EVENTS'] = "Evénements";
$GLOBALS['FR-MISC-SOUK'] = "Souk";
$GLOBALS['FR-MISC-OPTIONS'] = "Options";
$GLOBALS['FR-MISC-HELP'] = "Aide";

$GLOBALS['FR-MISC-CREDIT'] = "Crédits : ";
$GLOBALS['FR-MISC-HONNOR'] = "Honneur : ";
$GLOBALS['FR-MISC-LEVEL'] = "Niveau : ";

$GLOBALS['FR-MISC-LOGIN'] = "Déconnexion";
$GLOBALS['FR-MISC-LOGOUT'] = "Connexion";

$GLOBALS['FR-MISC-PLAYAS-PLAY'] = "Jouer";
$GLOBALS['FR-MISC-PLAYAS-GUEST'] = " invité";
$GLOBALS['FR-MISC-PLAYAS-GUESTWARNING'] = "<font color=black>Vous allez jouer<br>comme invité.</font><P><a href=\"http://www.lirmm.fr/jeuxdemots/jdm-signin.php\">Connectez-vous.</a>";

//------

$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-0'] = "Donner des IDEES ASSOCIEES au terme suivant :";

$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-3'] = "Donner des THEMES/DOMAINES pour le terme suivant : <BR><font size=\"-1\">Par exemple, 'sports', 'médecine', 'cinéma', 'cuisine,' etc.</font>";
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-5'] = "Donner des SYNONYMES du terme suivant :";
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-6'] = "Donner des GENERIQUES pour le terme suivant : <BR><font size=\"-1\">Par exemple: 'véhicule' pour 'voiture', 'félin', 'animal' pour 'chat'.</font>";
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-7'] = "Donner des CONTRAIRES pour le terme suivant : <BR><font size=\"-1\">Par exemple: 'froid' pour 'chaud', 'haut' pour 'bas'.</font>";
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-8'] = "Donner des SPECIFIQUES pour le terme suivant : <BR><font size=\"-1\">Par exemple : 'chat', 'chien', 'animal de compagnie',  etc. pour 'animal' - ou encore 'voiture', 'train', 'véhicule spatial', etc. pour 'véhicule'.</font>";
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-9'] = "Donner des PARTIES du terme suivant :<BR><font size=\"-1\">Une partie est une composante de l'objet, par exemple : 'moteur', 'roue', etc. pour 'voiture' - ou encore 'couverture', 'pages', 'chapitre' etc. pour 'livre'.</font>";
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-10'] = "Donner des TOUT du terme suivant :<BR><font size=\"-1\">Le tout est ce qui englobe/contient/possède la partie, par exemple : 'corps', 'bras', etc. pour 'coude' - ou encore 'banque' pour 'guichet'.</font>";
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-11'] = "Donner des LOCUTIONS pour le terme suivant :<BR><font size=\"-1\">Par exemple : 'langue au chat', 'chat de gouttière', 'chat à neuf queues', ... pour 'chat'</font>";

$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-13'] = "Donner des SUJETS typiques pour le VERBE suivant :<BR><font size=\"-1\">Le sujet est celui qui effectue l'action, par exemple 'chat', 'animal', 'personne', ... pour 'manger'</font>";
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-14'] = "Donner des OBJETS typiques pour le VERBE suivant :<BR><font size=\"-1\">L'objet est ce qui <b>subit l'action</b>, par exemple : 'viande', 'fruit', 'bonbon', ... pour 'manger' ou encore 'personne', 'homme politique', 'otage', ... pour 'assassiner'</font>";
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-15'] = "Donner des LIEUX typiques pour le terme suivant :<BR><font size=\"-1\">Par exemple : 'pré', 'écurie', 'champs de courses', ... pour 'cheval'</font>";

$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-16'] = "Donner des INSTRUMENTS typiques pour le VERBE suivant :<BR><font size=\"-1\">L'instrument est quelque'chose avec lequel on peut effectuer l'action, par exemple : 'pelle', 'pioche', 'main', ... pour 'creuser'</font>";
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-17'] = "Donner des CARACTERISTIQUES typiques pour le terme suivant :<BR><font size=\"-1\">Par exemple : 'liquide', 'blanc', 'buvable', ... pour 'lait', ou 'coupant', 'tranchant', ... pour 'lame'</font>";

$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-20'] = "Qu'est ce qui est PLUS INTENSE que le terme suivant : <BR><font size=\"-1\">Par exemple : 'forte fièvre', 'fièvre de cheval', ... pour 'fièvre' - ou encore 'vivre intensément' pour 'vivre'</font>";
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-21'] = "Qu'est ce qui est MOINS INTENSE que le terme suivant : <BR><font size=\"-1\">Par exemple : 'maisonette', ... pour 'maison' - ou encore 'marcher lentement', 'trainer' pour 'marcher'</font>";

$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-22'] = "Donner des mots de la MEME FAMILLE (par exemple : 'chanter', 'chanteur'... pour 'chant' - ou encore 'vente', 'vendeur', 'vendu' pour 'vendre') pour le terme suivant :";

$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-23'] = "Qu'est-ce qui possède la CARACTERISTIQUE suivante (par exemple, 'eau', 'vin', 'lait' pour 'liquide') :";
		
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-24'] = "Que peut faire le SUJET suivant (par exemple, 'manger', 'dormir', 'chasser' pour 'lion') :";
		
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-25'] = "Que peut-on faire avec l'INSTRUMENT suivant (par exemple, 'écrire', 'dessiner', 'gribouiller' pour 'stylo') :";
		
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-26'] = "Que peut subir l'OBJET suivant (par exemple, ...) :";
		
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-27'] = "Donner des termes du DOMAINE suivant (par exemple, 'touche', 'penalty', 'but' pour 'Football') :";

$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-28'] = "Que trouve-t-on dans le LIEU suivant (par exemple, 'poisson', 'coquillage', 'algue' pour 'mer') :";
		
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-30'] = "Que peut-on faire dans le LIEU suivant (par exemple, 'manger', 'boire', 'commander' pour 'restaurant' -- des verbes sont demandés) :";
		
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-31'] = "Dans quels LIEUX peut-on faire l'action suivante (par exemple, 'restaurant', 'cuisine', 'fast-food' pour 'manger' -- des lieux sont demandés) :";
		
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-32'] = "A quels SENTIMENTS/EMOTIONS peut être associé le terme suivant :";
				
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-34'] = "De quelles MANIERES peut être effectuée l'action suivante :<BR><font size=\"-1\">Il s'agira d'un adverbe ou d'un équivalent, par exemple : 'rapidement', 'sur le pouce', 'goulûment', 'salement' ... pour 'manger'</font>";

$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-35'] = "Quels SENS/SIGNIFICATIONS pouvez vous donner au terme suivant :<BR><font size=\"-1\">Il s'agira de termes évoquant chacun des sens possibles, par exemple : 'forces de l'ordre', 'contrat d'assurance', 'police typographique', ... pour 'police'</font>";

$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-37'] = "Quels BUT/FONCTION (rôle télique) pouvez vous donner au terme suivant :<BR><font size=\"-1\">Il s'agira d'un verbe, par exemple : 'couper' pour 'couteau', 'lire' pour 'livre', ...</font>";
$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-38'] = "Quels MODE DE CREATION (rôle agentif) pouvez vous donner au terme suivant :<BR><font size=\"-1\">Il s'agira d'un verbe, par exemple : 'construire' pour 'maison', 'rédiger'/'imprimer' pour 'livre', ...</font>";

$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-41'] = "Quelles CONSEQUENCES (A entraine B) pouvez vous donner au terme suivant :<BR><font size=\"-1\">Il s'agira d'un verbe ou d'un nom :  'tomber' -> 'se blesser', 'faim' -> 'voler'/'dérober', ...</font>";

$GLOBALS['FR-GENGAME-FUNCTIONS-INSTR-42'] = "Quelles CAUSES (A a pour cause B) pouvez vous donner au terme suivant :<BR><font size=\"-1\">Il s'agira d'un verbe ou d'un nom : 'se blesser' -> 'tomber', 'voler'' -> 'faim', 'pauvreté', ...</font>";

$GLOBALS['FR-GENGAME-FUNCTIONS-ARRAY-3-INSTR'] = "Lista non-limitative de domaines (si aucun ne convient entrez le ou les votres, ou mettez *** s'il n'y a pas de domaine particulier) :";

$GLOBALS['FR-GENGAME-FUNCTIONS-ARRAY-3'] = array ("anatomie", "architecture", "astronomie", "audiovisuel", "automobile", "aviation", "beaux-arts", "biologie", "botanique",
"boucherie", "chemin de fer", "chimie", "chirurgie", "cinéma", "commerce", "couture", "cuisine", "droit", "économie", "électricité", "élevage", "équitation", "géographie", "géologie",
"grammaire", "histoire", "hydrologie", "imprimerie", "informatique", "jeux", "linguistique", "littérature", "marine", "mathématiques", "mécanique", "médecine", "militaire", 
"minéralogie", "musique", "mythologie", "optique", "peinture", "pharmacie", "philosophie", "photographie", "physiologie", "physique", "politique", "psychologie", "religion", 
"scolaire", "sexologie", "sociologie", "spectacle", "sports", "sylviculture", "technique", "télécommunications", "urbanisme", "versification", "vétérinaire", "viticulture", "zoologie");

$GLOBALS['FR-TERMSELECT-FUNCTIONS-NAME-TEMPLATE'] = 'Nom%';
$GLOBALS['FR-TERMSELECT-FUNCTIONS-ADJ-TEMPLATE'] = 'Adj%';
$GLOBALS['FR-TERMSELECT-FUNCTIONS-ADV-TEMPLATE'] = 'Adv%';
$GLOBALS['FR-TERMSELECT-FUNCTIONS-ADV-TEMPLATE'] = 'Ver:Inf';

$GLOBALS['FR-READ-INSTRUCTIONS'] = 'Lisez la consigne !';

//------

$GLOBALS['FR-OPTIONS-TITLE'] = "JeuxDeMots : options";
$GLOBALS['FR-OPTIONS-PROMPT'] = "Options";
$GLOBALS['FR-OPTIONS-CSS-FORM'] = 	"CSS :<BR>
	<input <0> id=\"go_css_submit\" type=\"submit\" name=\"go_css_submit\" value=\"Changer\"> l'url
	<input  id=\"go_css_url\" type=\"text\" size = \"120\" name=\"go_css_url\" value=\"<1>\">
	(par défaut mettre \"<2>\")";
$GLOBALS['FR-OPTIONS-CSS-EXAMPLE'] = "Exemple de css :";
$GLOBALS['FR-OPTIONS-CSS-MUSEUM-FORM'] = 	"CSS de votre Petit Musée :<BR>
	<input <0> id=\"go_css_museum_submit\" type=\"submit\" name=\"go_css_museum_submit\" value=\"Changer\"> l'url
	<input id=\"go_css_museum_url\" type=\"text\" size = \"120\" name=\"go_css_museum_url\" value=\"<1>\">";

$GLOBALS['FR-OPTIONS-PERSO-FORM'] = 	"Url personnelle :<BR>
	<input <0> id=\"go_perso_submit\" type=\"submit\" name=\"go_perso_submit\" value=\"Changer\"> l'url
	<input  id=\"go_perso_url\" type=\"text\" size = \"120\" name=\"go_perso_url\" value=\"<0>\">
	(par exemple mettre \"http://mapageperso.com\")";


//------

$GLOBALS['FR-HOME-TITLE'] = "JeuxDeMots : accueil";
$GLOBALS['FR-HOME-PROMPT'] = "Accueil";

$GLOBALS['FR-HOME-NEWS'] = "<P><blockquote>ANNONCE du <0>";

$GLOBALS['FR-HOME-INFO'] = "info";

$GLOBALS['FR-HOME-HOT-LINKS'] = "Hot links";


$GLOBALS['FR-HOME-INSTRUCTIONS-TITLE'] = "Comment ça marche ? Qu'est-ce qu'on doit faire ?<BR><BR>";

$GLOBALS['FR-HOME-INSTRUCTIONS'] = "Un terme va vous être présenté ainsi qu'une consigne relative à ce terme. 
Pendant une période d'une minute vous devez entrer autant de propositions que possible conformément à la consigne. 
Il s'agit en général de fournir des termes que vous associez librement au terme présenté.
Validez chaque proposition avec le bouton \"Envoyer\" ou avec la touche \"Entrée\".
D'autres joueurs vont être confrontés au même terme.
Vous gagnerez des crédits lorsque les termes que vous avez donnés correspondent à ceux des autres joueurs.<BR>
Plus un terme est précis plus vous gagnerez de points, mais l'autre joueur y aura-t-il pensé ?<BR><BR>
Pensez à lire la <a href=\"jdm-signin.php\">Charte de JeuxDeMots</a>.<P><P>";


$GLOBALS['FR-HOME-MOSTWANTED'] = 'Most Wanted';
$GLOBALS['FR-HOME-COLLECTIONS'] = 'Collections';
$GLOBALS['FR-HOME-GIFT-WAITING'] = "Des cadeaux vous attendent dans le <A href=\"/jeuxdemots/jdm-list-gift.php\">souk</a>";
$GLOBALS['FR-HOME-RELATION-COUNT'] = "<0> relations ont été produites jusqu'à présent, dont <1> tabous. Bravo !<br /><br />";
$GLOBALS['FR-HOME-TERM-COUNT'] = "... et <0> termes, dont <1> en reliant <2>.";
$GLOBALS['FR-HOME-ACTIVE-PLAYER-COUNT'] = "<br /><br /><0> joueur connecté.";
$GLOBALS['FR-HOME-ACTIVE-PLAYERS-COUNT'] = "<br /><br /><0> joueurs connectés.";
$GLOBALS['FR-HOME-OTHER-ANNOUNCES'] = "Les autres annonces.";
$GLOBALS['FR-HOME-NO-PARTICULAR-THEME-ANCHOR'] =  "Vous n'avez pas de <a href=\"souk.php\">thématique</a> particulière";
$GLOBALS['FR-HOME-PREFERRED-THEME-ANCHOR'] =   "Votre <a href=\"souk.php\">thématique</a> préférée est <0>";
$GLOBALS['FR-HOME-TRIALS-WAITING'] =  "Vous avez <0> <a href=\"jdm-list-trial.php\">procès en attente</a>";
$GLOBALS['FR-HOME-LOTTERY'] = "Loterie";
$GLOBALS['FR-HOME-EASY-WORDS'] = "Mots faciles";
$GLOBALS['FR-HOME-GAME-THEMES'] = "Jeux thématiques";

$GLOBALS['FR-HOME-PLAY-BUTTON'] = "pics/jdm-logo-jouer.png";

$GLOBALS['FR-HOME-MEAN-GAIN'] = "Vous avez un gain moyen de <0> crédits";
$GLOBALS['FR-HOME-GAMES-TOKENS'] = "Vous avez <0> parties et <1> jetons en attente";

//------

$GLOBALS['FR-SOUK-TITLE'] = "JeuxDeMots : le Souk";
$GLOBALS['FR-SOUK-PROMPT'] = "Le souk aux mille merveilles : ";

$GLOBALS['FR-SOUK-YOUR-HUMOR'] = "Votre humeur :";
$GLOBALS['FR-SOUK-SHARE-HUMOR'] = "Partager";
$GLOBALS['FR-SOUK-SHARED-HUMOR'] = "Humeur partagée !";

$GLOBALS['FR-SOUK-PREFERRED-THEME'] = "Votre thématique préférée:";
$GLOBALS['FR-SOUK-THEME-VALIDATE'] = "Valider";
$GLOBALS['FR-SOUK-VALIDATED-THEME'] = "Thématique validée";
$GLOBALS['FR-SOUK-EMPTY-FIELD-THEME'] = "(champ vide pour supprimer la thématique)";
$GLOBALS['FR-SOUK-NO-PARTICULAR-THEME'] = "Vous n'avez pas de thématique particulière";
$GLOBALS['FR-SOUK-NO-EXISTING-THEME'] = "Thème inexistant, essayez autre chose !";

$GLOBALS['FR-SOUK-PRESENTS'] = "Cadeaux";

$GLOBALS['FR-SOUK-SUBTITLE-COMP'] = "Gestion des compétences";
$GLOBALS['FR-SOUK-SUBTITLE-TREAS'] = "Gestion des trésors";
$GLOBALS['FR-SOUK-SUBTITLE-TRIALS'] = "le barreau";

$GLOBALS['FR-SOUK-SMALL-GAMES'] = 'Petits jeux'; 
$GLOBALS['FR-SOUK-TRIALS'] = 'Procès';
$GLOBALS['FR-SOUK-DUELS'] = 'Duels';
$GLOBALS['FR-SOUK-VARIOUS'] = 'Divers'; 

$GLOBALS['FR-SOUK-DEFIS-NOTERM'] = "Le terme '<0>' n'existe pas !";
$GLOBALS['FR-SOUK-DEFIS-NOTFREE'] = "Le terme <0> est libre !";
$GLOBALS['FR-SOUK-DEFIS-NOGUEST'] = "Un invité ne peut pas tenter de capturer un mot";
$GLOBALS['FR-SOUK-DEFIS-NOTENOUGHMONEY'] = "L'investissement doit être supérieure à 5 fois la valeur du mot (<1>)";
$GLOBALS['FR-SOUK-DEFIS-NOMONEY'] = "Vous n'avez pas tant de crédit (<1>)";
$GLOBALS['FR-SOUK-DEFIS-NOTNOW'] = "Vous ne pouvez pas tenter de capturer ce mot maintenant, reessayez plus tard.";

$GLOBALS['FR-SOUK-COMP-ITEM'] = "Compétence";
$GLOBALS['FR-SOUK-COMP-HONNOR-MIN'] = "Honneur min";
$GLOBALS['FR-SOUK-COMP-CREDIT'] = "Crédits";
$GLOBALS['FR-SOUK-COMP-STATUS'] = "Statut";
$GLOBALS['FR-SOUK-COMP-QUOT'] = "Cotation";
$GLOBALS['FR-SOUK-COMP-SELECPC'] = "% de sélection";
$GLOBALS['FR-SOUK-COMP-ADJUSTPC'] = "Ajustement %";

$GLOBALS['FR-SOUK-COMP-EQUIPROBABILIZE'] = "Equiprobabiliser";
$GLOBALS['FR-SOUK-COMP-ALL-COMPETENCES'] = "(5k Cr) all the competences.";

$GLOBALS['FR-SOUK-NEW-IMAGE'] = "pics/new.gif";


$GLOBALS['FR-SOUK-LIST-TRIAL-COMPLETED-FORM'] = "<li><form id=\"open_cr_trial<0>\" name=\"open_trial<0>\" method=\"post\" action=\"trialVote.php\" >
				<input <1> id=\"trial_vote_cr\" type=\"submit\" name=\"trial_vote_cr\" value=\"Visualiser\">
				<input  id=\"proc_id\" type=\"hidden\" name=\"proc_id\" value=\"<2>\">
		 		le procès <2> opposant '<3>' à '<4>' pour le terme '<5>'.
				</form></li>";

$GLOBALS['FR-SOUK-LIST-TRIAL-TODO-FORM'] ="<li><form id=\"open_trial<0>\" name=\"open_trial<0>\" method=\"post\" action=\"trialVote.php\" >
		<input id=\"trial_vote\" type=\"submit\" name=\"trial_vote\" value=\"Voter\">
		<input  id=\"proc_id\" type=\"hidden\" name=\"proc_id\" value=\"<1>\">
		 - échéance dans <2> heures.
		</form></li>";

$GLOBALS['FR-MAKE-TRIAL-FORM'] = "<form id=\"form-trial\" name=\"form-trial\" method=\"post\" action=\"generateResult_maketrial_process.php\" >
			Faire un <input <0> id=\"submit-buy-trial\" type=\"submit\" name=\"submit-buy-trial\" value=\"procès\"> 
				 (500 Cr) à ce joueur
			 <input id=\"trial_id\" type=\"hidden\" name=\"trial_id\" value=\"<1>\">
			 - indiquez le motif :
			 <textarea name=\"trial_reason\" cols=40 rows=6></textarea>
			 </form>";
$GLOBALS['FR-RESULT-MAKE-TRIAL'] = "Intenter un procès";
$GLOBALS['FR-RESULT-TRIAL'] = "procès";

//------ Le souk by MM
// global warnings
$GLOBALS['FR-SOUK-WARNING-BADPLAYER'] = "Ce joueur n'existe pas";
$GLOBALS['FR-SOUK-WARNING-BADTERM'] = "Ce terme n'existe pas";
$GLOBALS['FR-SOUK-WARNING-NOCASH'] = "Vous n'avez pas assez de crédits";
$GLOBALS['FR-SOUK-WARNING-NOHONNOR'] = "Vous n'avez pas assez d'honneur";
$GLOBALS['FR-SOUK-WARNING-NOGUEST'] = "Le joueur invité ne peut pas faire cela - inscrivez-vous.";
$GLOBALS['FR-SOUK-WARNING-NOCREDIT'] = "La maison ne fait pas crédit !";

//"le joueur '$player' a offert un cadeau au joueur '$dest'."
$GLOBALS['FR-SOUK-GIFT-EVENT'] = "le joueur &#39;<0>&#39; a offert un cadeau au joueur &#39;<1>&#39;.";
$GLOBALS['FR-SOUK-BUYCOMP-EVENT'] = "le joueur &#39;<0>&#39; a acheté une compétence.";
$GLOBALS['FR-SOUK-HYPEWORDS'] = "Mots à la mode";
$GLOBALS['FR-SOUK-INWORDS'] = "Mots tendance";
$GLOBALS['FR-SOUK-OUTWORDS'] = "Mots has been";

$GLOBALS['FR-SOUK-HYPEWORDS-VIEW-FORM'] = "<input id=\"formhypewordsumit\" type=\"submit\" name=\"formhypewordsumit\" value=\"Visualiser\"> (10 Cr)
		les mots à la mode.";

$GLOBALS['FR-SOUK-CAGNOTTE-VIEW-FORM'] = "	<input id=\"cagnotesubmit\" type=\"submit\" name=\"cagnotesubmit\" value=\"Visualiser\"> (5 Cr)
		le montant de la cagnotte.";

$GLOBALS['FR-SOUK-CAGNOTTE-DISPLAY-WARNING'] = "La cagnotte contient <0> Crédits. 
		    Les joueurs <1> et <2> sont en tête avec un score de <3> Crédits. 
		    La cagnotte tombera dans <4> jackpot(s)"; 

$GLOBALS['FR-SOUK-GAME-GIFT-FORM'] = "<input id=\"formmakegift\" type=\"submit\" name=\"formmakegift\" value=\"Offrir\"> (<2> Cr)
		une partie avec le terme 
		<input id=\"giftterm\" type=\"text\" name=\"giftterm\" value=\"<0>\">
		au joueur
		<input id=\"giftplayername\" type=\"text\" name=\"giftplayername\" value=\"<1>\"> 
		<input id=\"relation_type_gift\" type=\"hidden\" name=\"relation_type_gift\" value=\"0\">";


$GLOBALS['FR-SOUK-GAME-GIFT-WARNING-NOINPUT'] = "Entrez un terme et un nom de joueur";
$GLOBALS['FR-SOUK-GAME-GIFT-WARNING-SELFGIFT'] = "Ce serait sympa, mais on ne s'offre pas des cadeaux a soi-même";
$GLOBALS['FR-SOUK-GAME-GIFT-WARNING-NOERROR'] = "Cadeau acheté et envoyé";

$GLOBALS['FR-SOUK-GAME-GIFT-LIST'] = "Les cadeaux qui m'attendent :";
$GLOBALS['FR-SOUK-GAME-GIFT-LIST-EMPTY'] = "Personne ne vous a offert de cadeau.";

$GLOBALS['FR-SOUK-GAME-GIFT-OPEN-FORM'] = "<input id=\"opengiftbut\" type=\"submit\" name=\"opengiftbut\" value=\"Ouvrir\">
		 un cadeau offert par <0> le <1>";

$GLOBALS['FR-SOUK-GRAPH-VIEW-FORM'] = " <input id=\"viewgraph\" type=\"submit\" name=\"viewgraph\" value=\"Visualiser\"> (0 Cr) le graphe.";
$GLOBALS['FR-SOUK-TRIAL-VIEW-FORM'] = " <input id=\"viewtrial\" type=\"submit\" name=\"viewtrial\" value=\"Visualiser\"> (0 Cr) les procès.";
$GLOBALS['FR-SOUK-GIFT-VIEW-FORM'] = " <input id=\"viewgift\" type=\"submit\" name=\"viewgift\" value=\"Visualiser\"> (0 Cr) mes cadeaux.";

$GLOBALS['FR-SOUK-WORD-LOOKUP-FORM'] = "  <input id=\"gotermlist\" type=\"submit\" name=\"gotermlist\" value=\"Chercher\">
       (0 Cr) les mots contenant la chaîne 
	    <input  id=\"goterm\" type=\"text\" name=\"goterm\" value=\"<0>\">";
	    
$GLOBALS['FR-SOUK-WORDLIST-DISPLAY-WARNING-NOINPUT'] = "Entrez un mot non vide";

$GLOBALS['FR-SOUK-PARRAINAGE-FORM'] = "	  <input id=\"parainage_submit\" type=\"submit\" name=\"parainage_submit\" value=\"Parrainer\">
      (0 Cr) un joueur ayant l'adresse email suivante
	  <input  size=\"50\" id=\"parainage_email\" type=\"text\" name=\"parainage_email\" value=\"machin@bidule.truc\">
	  ";

$GLOBALS['FR-SOUK-PARRAINAGE-WARNING-NOERROR'] = "Demande de parainage envoyée (adresse email non vérifiée)";
$GLOBALS['FR-SOUK-PARRAINAGE-WARNING-WRONGEMAIL'] = "Essayez avec une autre adresse...";

$GLOBALS['FR-SOUK-GENERATE-DEFIS-FORM'] = "<input id=\"parainage_submit\" type=\"submit\" name=\"defis_submit\" value=\"Capturer\"> le mot
	  <input  size=\"20\" id=\"parainage_email\" type=\"text\" name=\"defis_word\" value=\"le mot\">
	pour un montant de 
	<input  size=\"20\" id=\"parainage_email\" type=\"text\" name=\"defis_amount\" value=\"1000\">
	Crédits.";

$GLOBALS['FR-SOUK-WORD-CAPTURE-FORM'] = "<font color=\"red\">Tentative de capture prête</font> 
<input id=\"chosengamesubmit\" type=\"submit\" name=\"chosengamesubmit\" value=\"Go !\">
		";

$GLOBALS['FR-SOUK-WORD-CAPTURE-EVENT'] = "le joueur &#39;<0>&#39; tente de capturer &#39;<1>&#39;.";

$GLOBALS['FR-SOUK-CHECK-TREASURE-FORM'] = 	"<input id=\"gochecktreasuressubmit\" type=\"submit\" name=\"gochecktreasuressubmit\" value=\"Afficher\"> les trésors du joueur
	<input  id=\"gochecktreasures\" type=\"text\" name=\"gochecktreasures\" value=\"\">";

$GLOBALS['FR-SOUK-BUYCOMP-SUBMIT'] = "Acheter";
$GLOBALS['FR-SOUK-BUYCOMP-DONE'] = "possédé";

$GLOBALS['FR-SOUK-WORD-RELATION-FORM'] = " <input id=\"gotermsubmit\" type=\"submit\" name=\"gotermsubmit\" value=\"Chercher\"> (DEBUG) le mot
	    <input  id=\"gotermrel\" type=\"text\" name=\"gotermrel\" value=\"\">";

$GLOBALS['FR-SOUK-GAMELIST-FORM'] = "<input id=\"gamelist_submit\" type=\"submit\" name=\"gamelist_submit\" value=\"parties en cours\"> (DEBUG)";

$GLOBALS['FR-SOUK-CHOSENGAME-FORM'] = "<input id=\"chosengamesubmit\" type=\"submit\" name=\"chosengamesubmit\" value=\"Jouer\">";

$GLOBALS['FR-SOUK-STATISTICS-FORM'] = "<input id=\"bd_stats_submit\" type=\"submit\" name=\"bd_stats_submit\" value=\"Statistiques\"> (DEBUG)";

$GLOBALS['FR-SOUK-CHECKOWNER-FORM'] = "	<input id=\"gotermownersubmit\" type=\"submit\" name=\"gotermownersubmit\" value=\"Vérifier\"> le statut du mot
	<input  id=\"gotermowner\" type=\"text\" name=\"gotermowner\" value=\"\">";


$GLOBALS['FR-SOUK-PROB-WARNING-NOERROR'] = "Probabilité ajustée";
$GLOBALS['FR-SOUK-COMP-WARNING-NOERROR'] = "Relation achetée";






$GLOBALS['FR-SOUK-WARNING-CANT-GIFT'] = "Vous ne pouvez pas donner ce cadeau pour l'instant.";
$GLOBALS['FR-SOUK-BUY-TRIAL-TOKEN-NOCREDIT'] = "Désolé, vous n'avez pas cette somme de <0> crédits";
$GLOBALS['FR-SOUK-BUY-TRIAL-TOKEN-CONFIRM'] = "Jeton de procès acheté";

$GLOBALS['FR-SOUK-BUY-TRIAL-TOKEN-STATUS'] = "Vous avez déja <0> jetons de procès. Vous pouvez en <input <1> id=\"submit_buy_token\" type=\"submit\" name=\"submit_buy_token\" value=\"acheter\"> un de plus pour <2> crédits.";
$GLOBALS['FR-SOUK-RELATIONS-OCCURS'] = "<0> occurrences de relations <1> (<2> - <3>)";
$GLOBALS['FR-SOUK-NODES-OCCURS'] = "<0> occurrences de noeuds <1> (<2>)";

$GLOBALS['FR-SOUK-ARTEFACTS-TITLE'] = "Artefacts";
$GLOBALS['FR-SOUK-PLAY-LOTTERY'] = "\n<input  type=\"submit\" name=\"submit\" value=\"Jouer à la loterie\"> (<0> Cr)";
$GLOBALS['FR-SOUK-CHOSE-EASY'] = "\n<input  type=\"submit\" name=\"submit\" value=\"Mots à choisir\"> facile (gratuit !)";
$GLOBALS['FR-SOUK-CHOSE-HARD'] = "\n<input  type=\"submit\" name=\"submit\" value=\"Mots à choisir\"> dur (gratuit !)";
$GLOBALS['FR-SOUK-CHOSE-THEME'] = "\n<input  type=\"submit\" name=\"submit\" value=\"Mots thématique\"> variable (gratuit !)";
$GLOBALS['FR-SOUK-CRAZY-QUESTION'] = "\n<input  type=\"submit\" name=\"submit\" value=\"Questions farfelues\"> (gratuit !)";

//------

$GLOBALS['FR-EVENT-TITLE'] = "JeuxDeMots : événements";
$GLOBALS['FR-EVENT-PROMPT'] = "Evénements";

// "les joueurs '$gamecreatorname' et '$playername' ont gagné <b>$points</b> crédits avec <b>'$fentry'</b>  pour la compétence '$relgpname'. Jackpot !"
$GLOBALS['FR-EVENT-WIN-JACKPOT'] = "les joueurs &#39;<0>&#39; et &#39;<1>&#39; ont gagné <b><2></b> crédits avec <b>'<3>'</b> 
pour la compétence '<4>'. Jackpot !";

// "les joueurs '$gamecreatorname' et '$playername' ont gagné <b>$points</b> crédits avec <b>'$fentry'</b>."
$GLOBALS['FR-EVENT-WIN'] = "les joueurs &#39;<0>&#39; et &#39;<1>&#39; ont gagné <b><2></b> crédits avec <b>&#39;<3>&#39;</b>.";

$GLOBALS['FR-EVENT-JACKPOT-TMPL'] = "Jackpot !";
$GLOBALS['FR-EVENT-TRIALWON-TMPL'] = "a gagné un procès";
$GLOBALS['FR-EVENT-TRIALNO-TMPL'] = "non-lieu pour le";
$GLOBALS['FR-EVENT-NEWPLAYER-TMPL'] = "qui vient de s'inscrire.";
$GLOBALS['FR-EVENTS-CAP'] = "<0> a fait passer un cap de <1> relations. Bravo ! + <2> H";
$GLOBALS['FR-EVENT-CAP-TMPL'] = "a fait passer un cap de";

//------

$GLOBALS['FR-TRIALVOTE-INSTRUCTIONS'] = "Au vu des réponses, le joueur a-t-il respecté la consigne ?";
$GLOBALS['FR-TRIALVOTE-TITLE'] = "vote pour un procès";
$GLOBALS['FR-TRIALVOTE-THANKS'] = "merci pour votre vote";
$GLOBALS['FR-TRIALVOTE-THANKSABS'] = "merci pour votre abstention";
$GLOBALS['FR-TRIALVOTE-INSTRWERE'] = "La consigne était :";
$GLOBALS['FR-TRIALVOTE-MAIL-TITLE'] = "Vous êtes juré d'un procès";
$GLOBALS['FR-TRIALVOTE-MAIL-BODY'] = "Bonjour <0>, 
				
Un procès a lieu et on demande votre expertise, allez sur votre page d'accueil...

";
$GLOBALS['FR-TRIALVOTE-PENDING-TRIAL'] = "Les procès qui vous attendent :";
$GLOBALS['FR-TRIALVOTE-NO-TRIAL'] = "Vous n'êtes juré d'aucun procès.";
$GLOBALS['FR-TRIALVOTE-50-LAST'] = "Les 50 derniers procès terminés :";
$GLOBALS['FR-TRIALVOTE-NO-GUEST'] = " non accessibles aux invités";
$GLOBALS['FR-TRIALVOTE-NO-TRIAL'] = "Aucun procès";
$GLOBALS['FR-TRIALVOTE-JUDGE'] = "le Juge";
$GLOBALS['FR-TRIALVOTE-JUDGE-GIFT'] = "le Juge a offert un cadeau à ";
$GLOBALS['FR-TRIALVOTE-EVENT'] = "le joueur '<0>' a gagné un procès face à '<1>' pour le mot '<2>'.";
$GLOBALS['FR-TRIALVOTE-WIN-TITLE'] = "Procès gagné";
$GLOBALS['FR-TRIALVOTE-WIN-BODY'] = "Bonjour <0>\nnous avons le plaisir de vous annoncer que vous avez gagné un procès (num <1>) face à '<2>' pour le mot '<3>'. \nVisitez le souk pour voir les votes liés à ce procès.\n";
$GLOBALS['FR-TRIALVOTE-LOSE-TITLE'] = "Procès perdu";
$GLOBALS['FR-TRIALVOTE-LOSE-BODY'] = "Bonjour <0>\nnous avons le regret de vous annoncer que vous avez perdu un procès (num <1>) face à '<2>' pour le mot '<3>'. \nVisitez le souk pour voir les votes liés à ce procès.\n";
$GLOBALS['FR-TRIALVOTE-REASON'] = "Motif du procès :";
$GLOBALS['FR-TRIALVOTE-ANSWERS'] = "Les réponses proposées par l'accusé ont été :";
$GLOBALS['FR-TRIALVOTE-DOUBT'] = "Relaxé au bénéfice du doute";
$GLOBALS['FR-TRIALVOTE-GUILTY'] = "Coupable";
$GLOBALS['FR-TRIALVOTE-INNOCENT'] = "Innocent";
$GLOBALS['FR-TRIALVOTE-ABSTAIN'] = "S'abstenir";
$GLOBALS['FR-TRIALVOTE-N-VOTES'] = "<0> voix";
$GLOBALS['FR-TRIALVOTE-INQUIRER'] = "Inquisiteur <b><0></b> versus accusé <b><1></b>";
$GLOBALS['FR-TRIALVOTE-REASON-SHORT'] = "Motif: <b><0></b><P>";
$GLOBALS['FR-TRIALVOTE-INSTR'] = "Instruction: <B><0></b><p>";
$GLOBALS['FR-TRIALVOTE-TERM'] = "Terme: <B><0></B><P>";

//------

$GLOBALS['FR-RANKING-TITLE'] = "JeuxDeMots : classement";
$GLOBALS['FR-RANKING-PROMPT'] = "Classement";
$GLOBALS['FR-RANKING-DISPLAY-SCORES'] = "scores";
$GLOBALS['FR-RANKING-DISPLAY-TRASURES'] = "trésors";

$GLOBALS['FR-RANKING-LABEL-NAME'] = "Nom";
$GLOBALS['FR-RANKING-LABEL-HONNOR'] = "Honneur";
$GLOBALS['FR-RANKING-LABEL-HONNORMAX'] = "Honneur<br>max";
$GLOBALS['FR-RANKING-LABEL-CREDITS'] = "Crédits";
$GLOBALS['FR-RANKING-LABEL-NBPLAYED'] = "Parties<br>jouées";
$GLOBALS['FR-RANKING-LABEL-LEVEL'] = "Niveau";
$GLOBALS['FR-RANKING-LABEL-EFF'] = "Efficacité";
$GLOBALS['FR-RANKING-LABEL-NBTREASURE'] = "Trésors";

$GLOBALS['FR-RANKING-BEST-SCORES'] = "Meilleurs scores";
$GLOBALS['FR-RANKING-BEST-COUPLES'] = "Meilleurs couples";
$GLOBALS['FR-RANKING-BEST-FRIENDS'] = "Vos meilleurs amis";

$GLOBALS['FR-RANKING-AND'] = " et ";
$GLOBALS['FR-RANKING-MINUTE'] = "mn";
$GLOBALS['FR-RANKING-15MINUTE'] = "1/4h";
$GLOBALS['FR-RANKING-30MINUTE'] = "1/2h";
$GLOBALS['FR-RANKING-HOUR'] = "h";
$GLOBALS['FR-RANKING-DAY'] = "j";
$GLOBALS['FR-RANKING-MONTH'] = "m";
$GLOBALS['FR-RANKING-YEAR'] = " an";
$GLOBALS['FR-RANKING-CREDITS'] = "Cr";

$GLOBALS['FR-RANKING-DISPLAY-DAY'] = "jour";
$GLOBALS['FR-RANKING-DISPLAY-DAYS'] = "jours";
$GLOBALS['FR-RANKING-DISPLAY-DAY-S'] = "jour(s)";
$GLOBALS['FR-RANKING-DISPLAY-WEEK'] = "semaine";
$GLOBALS['FR-RANKING-DISPLAY-MONTH'] = "mois";
$GLOBALS['FR-RANKING-DISPLAY-MONTHS'] = "mois";
$GLOBALS['FR-RANKING-DISPLAY-YEAR'] = "an";

 
 $GLOBALS['FR-RANKING-DISPLAY-TITLE-ALL-PLAYERS'] = "Tous les joueurs";
 $GLOBALS['FR-RANKING-DISPLAY-TITLE-ONLY-NEIGHBOURS'] = "Que les voisins";
 $GLOBALS['FR-RANKING-DISPLAY-TITLE-PLAYERS'] = "Les joueurs";
 $GLOBALS['FR-RANKING-DISPLAY-TITLE-NEIGHBOURS'] = "Les voisins";
 $GLOBALS['FR-RANKING-DISPLAY-TITLE-FRIENDS'] = "Les amis";
 $GLOBALS['FR-RANKING-DISPLAY-TITLE'] = "<0> actifs depuis <1> jour(s) sont affichés... ";

 $GLOBALS['FR-RANKING-DISPLAY-MENU'] = "Afficher tous les joueurs actifs depuis";
 $GLOBALS['FR-RANKING-DISPLAY-ONLY-NEIGHBOURS'] = "N'afficher que les voisins";

 $GLOBALS['FR-RANKING-DISPLAY-START'] = "Afficher les ";

 $GLOBALS['FR-RANKING-DISPLAY-PLAYERS'] = "joueurs";
 $GLOBALS['FR-RANKING-DISPLAY-NEIGHBOURS'] = "voisins";
 $GLOBALS['FR-RANKING-DISPLAY-FRIENDS'] = "amis";
 
 $GLOBALS['FR-RANKING-DISPLAY-ACTIVE-SINCE'] = " actifs depuis ";


//------

$GLOBALS['FR-MUSEUM-TITLE'] = "JeuxDeMots : petit musée";
$GLOBALS['FR-MUSEUM-PROMPT'] = "Le petit musée de <0>";
$GLOBALS['FR-MUSEUM-WARNING-NOTREASURE'] = "Ce joueur n'a pas de trésor";

//------ relations informations

$GLOBALS['FR-SIGNIN-YOURINFO'] = "Votre pays";

$GLOBALS['FR-RELINFO-1'] = "Brasil";
$GLOBALS['FR-RELINFO-2'] = "Portugal";

$GLOBALS['FR-RELINFO-3'] = "Angola";
$GLOBALS['FR-RELINFO-4'] = "Cabo Verde";
$GLOBALS['FR-RELINFO-5'] = "Guiné-Bissau";
$GLOBALS['FR-RELINFO-6'] = "Moçambique";
$GLOBALS['FR-RELINFO-7'] = "São Tomé e Príncipe";
$GLOBALS['FR-RELINFO-8'] = "Timor Leste";
$GLOBALS['FR-RELINFO-9'] = "Macau";
$GLOBALS['FR-RELINFO-10'] = "Outro";

//------
$GLOBALS['FR-RESULT-TITLE'] = "JeuxDeMots : résultats";
$GLOBALS['FR-RESULT-PG-PROMPT'] = "<P>Réponses données par vous (<0>) pour cette partie (<1>) : ";
$GLOBALS['FR-RESULT-NOTHING'] =  " --rien-- ";
$GLOBALS['FR-RESULT-PROMPT'] = "Résultat";

$GLOBALS['FR-RESULT-NOPROP'] = "<h2>Vous n'avez rien proposé... je suis sûr que vous allez y arriver :)</h2>";

$GLOBALS['FR-RESULT-NOGUEST'] = "Possibilité non offerte aux invités :)";

$GLOBALS['FR-RESULT-PROMPT-BUYPLAY-HEADER'] = "Vous êtes déçu des réponses de votre adversaire. Les vôtres étaient bien mieux !";
$GLOBALS['FR-RESULT-PROMPT-BUYPLAY-FORM'] = "<P>Vous pouvez racheter cette partie et la proposer à d'autres joueurs<br>";
$GLOBALS['FR-RESULT-BUY-TOKEN'] = "Achat de jetons";
$GLOBALS['FR-RESULT-TOKEN'] = "jetons";
$GLOBALS['FR-RESULT-OR'] = "ou";

$GLOBALS['FR-RESULT-PROMP-INVOKEWORD-FORM'] ="<P><font color=\"red\"> Jackpot !</font>
		<BR>Vous êtes doué. Pour vous récompenser, on vous invite à souhaiter qu'un terme de votre choix soit plus souvent proposé.";
$GLOBALS['FR-RESULT-INVOKE'] = "Invoquer";
$GLOBALS['FR-RESULT-THETERM'] = "le terme";

$GLOBALS['FR-RESULT-PROMP-ADDFRIEND-FORM'] ="<P><font color=\"red\"> Jackpot !</font>
		<BR>Vous êtes doué. Pour vous récompenser, on vous invite à ajouter un ami.";
$GLOBALS['FR-RESULT-ADD'] = "Ajouter";
$GLOBALS['FR-RESULT-ASFRIEND'] = "comme ami";

$GLOBALS['FR-RESULT-PROMPT-YOURANSWERS'] = "Réponses données par vous (<0>) pour cette partie (<1>) : ";
$GLOBALS['FR-RESULT-PROMPT-HISANSWERS'] = "Réponses données par le créateur (<0>) de cette partie (<1>) : ";
$GLOBALS['FR-RESULT-PROMPT-INTERSECTION'] = "Intersection : ";

$GLOBALS['FR-RESULT-PROMPT-CREATED-PLAY'] = "Votre partie va être proposée à deux autres joueurs. On vous écrira...";

$GLOBALS['FR-RESULT-BUYTOKEN-FORM'] = "Si vous êtes content de vous, vous pouvez la proposer à davantage de joueurs.<P>Acheter 
		<input id=\"submit5\" type=\"submit\" name=\"submit5\" value=\"5 jetons\"> (<0> Cr) ou
		<input id=\"submit10\" type=\"submit\" name=\"submit10\" value=\"10 jetons\"> (<1> Cr)";

$GLOBALS['FR-RESULT-BUY-TRIAL-TOKEN-FORM'] = "<P>Vous pouvez investir dans un jeton de procès.<br>Acheter 
		<input id=\"submit1\" type=\"submit\" name=\"submit1\" value=\"1 jeton\"> (200 Cr)";

$GLOBALS['FR-RESULT-PROMPT-GAIN'] = "<h2>Vous gagnez <0> crédits et <1> point(s) d'honneur </h2>";

$GLOBALS['FR-RESULT-PROMPT-LEVEL-GAIN'] = "Vous gagnez <br><0><br> niveau.";
$GLOBALS['FR-RESULT-PROMPT-LEVEL-LOOSE'] = "Vous perdez <br><0><br> niveau.";

$GLOBALS['FR-RESULT-PROMPT-TERMLEVEL-GAIN'] = "<br>Le terme <0> gagne <1> point de niveau.";
$GLOBALS['FR-RESULT-PROMPT-TERMLEVEL-LOOSE'] = "<br>Le terme <0> perd <1> point de niveau.";

$GLOBALS['FR-RESULT-MORE-HONNOR-POINTS'] = "<0> points d'honneur de plus !";

//------
$GLOBALS['FR-BUYPLAY-CONGRAT-INVEST'] = "Bravo pour cet investissement dans <0> jetons pour un montant de <1> crédits.";
$GLOBALS['FR-BUYPLAY-CHEAT'] = "<h1>Désolé le joueur invité ne peut pas racheter une partie. 
    	Si vous lisez ceci, c'est que la session a du expirer</h1>";
$GLOBALS['FR-BUYPLAY-EVENT'] = "le joueur &#39;<0>&#39; a racheté une partie pour <b>&#39;<1>&#39;</b>.";

//------
$GLOBALS['FR-BUYTOKEN-CONGRAT-INVEST'] = "Bravo pour cet investissement dans <0> jetons pour un montant de <1> crédits.";
$GLOBALS['FR-BUYTOKEN-CHEAT'] = "Désolé le joueur invité ne peut pas acheter des jetons. 
    	Si vous lisez ceci, c'est que la session a du expirer";
$GLOBALS['FR-BUYTOKEN-SORRY'] = "Désolé, vous n'avez pas cette somme de <0> crédits, il fallait vérifier avant :)";

// "le joueur '$login' a investi dans le mot <b>'$fentry'</b>."
$GLOBALS['FR-BUYTOKEN-EVENT'] = "le joueur &#39;<0>&#39; a investi dans <b>&#39;<1>&#39;</b>.";

//------
$GLOBALS['FR-BUY-TRIAL-TOKEN-CONGRAT-INVEST'] = "Bravo pour cet investissement dans <0> jetons de procès pour <1> crédits.";
$GLOBALS['FR-BUY-TRIAL-TOKEN-CHEAT'] = "Désolé le joueur invité ne peut pas acheter des jetons. 
    	Si vous lisez ceci, c'est que la session a du expirer";
$GLOBALS['FR-BUY-TRIAL-TOKEN-SORRY'] = "Désolé, vous n'avez pas cette somme de <0> crédits, il fallait vérifier avant :)";
$GLOBALS['FR-BUY-TRIAL-TOKEN-EVENT'] = "le joueur &#39;<0>&#39; a acheté un jeton de procès.";


//------
$GLOBALS['FR-BUYWISH-CONGRAT-INVEST'] = "Bravo pour cette invocation du terme '<0>' pour un montant de <1> crédits. Il apparaîtra un jour... peut-être.";
$GLOBALS['FR-BUYWISH-CHEAT'] = "<h1>Désolé le joueur invité ne peut pas invoquer de mots. 
    	Si vous lisez ceci, c'est que la session a du expirer</h1>";
$GLOBALS['FR-BUYWISH-SORRY'] = "<h1>Désolé, vous n'avez pas cette somme de <0> crédits, il fallait vérifier avant :)</h1>";

// "le joueur '$login' a investi dans le mot <b>'$fentry'</b>."
$GLOBALS['FR-BUYWISH-EVENT'] = "le joueur &#39;<0>&#39; a invoqué un mot...";
$GLOBALS['FR-BUYWISH-EMPTY-WORD'] = "mot vide, tant pis !";

$GLOBALS['FR-ADDFRIEND-CONGRAT-INVEST'] = "Bravo d'avoir '<0>' comme nouvel ami.";
$GLOBALS['FR-ADDFRIEND-ERROR'] = "Ce joueur n'existe pas.";
$GLOBALS['FR-ADDFRIEND-CHEAT'] = "<h1>Désolé le joueur invité ne peut pas ajouter d'ami. 
    	Si vous lisez ceci, c'est que la session a du expirer</h1>";
$GLOBALS['FR-ADDFRIEND-SORRY'] = "<h1>Désolé, vous n'avez pas cette somme de <0> crédits, il fallait vérifier avant :)</h1>";
$GLOBALS['FR-ADDFRIEND-EVENT'] = "le joueur &#39;<0>&#39; a un nouvel ami...";
$GLOBALS['FR-ADDFRIEND-EMPTY-WORD'] = "ami vide, tant pis !";

//------
$GLOBALS['FR-TRIAL-CONGRAT-INVEST'] = "Bravo pour cet investissement dans un procès.";
$GLOBALS['FR-TRIAL-CHEAT'] = "<h1>Désolé le joueur invité ne peut pas faire de procès. Si vous lisez ceci, c'est que la session a du expirer</h1>";
$GLOBALS['FR-BUYTRIAL-SORRY'] = "<h1>Désolé, vous n'avez pas cette somme de <0> crédits, il fallait vérifier avant :)</h1>";

//------
$GLOBALS['FR-PLAYER-PERM-CAGNOTTE-BINGO'] = "Bingo ! Vous faites tomber la cagnotte de <0> crédits.";
$GLOBALS['FR-PLAYER-PERM-CAGNOTTE-EVENT'] = "les joueurs '<0>' et '<1>' ont gagné la cagnotte d'un montant de <2> et se la partagent... et en font profiter d'autres";
$GLOBALS['FR-PLAYER-PERM-CAGNOTTE-MAIL'] = "Bonjour,\nla cagnotte est tombée et vous gagnez des crédits...\n";
$GLOBALS['FR-PLAYER-PERM-CAGNOTTE-MAIL-TITLE'] = "Des Crédits...";

//------

$GLOBALS['FR-REQUEST-VERB-TRANSTIVE'] = "SELECT id FROM `Nodes` WHERE w > 50 
	AND id IN (SELECT node1 FROM Relations WHERE node2 
	IN (SELECT id FROM Nodes WHERE type=18 and name LIKE 'Ver:Transitif')
	) order by w DESC LIMIT <0>";

$GLOBALS['FR-REQUEST-VERB-EASY'] = "SELECT id FROM `Nodes` WHERE w > 50 
	AND id IN (SELECT node1 FROM Relations WHERE node2 
	IN (SELECT id FROM Nodes WHERE type=4 and name LIKE 'Ver:Inf')
	) order by w DESC LIMIT <0>";


$GLOBALS['FR-REQUEST-NOUN-EASY'] = "SELECT id FROM `Nodes` WHERE w > 50 
	AND id IN (SELECT distinct node1 FROM Relations WHERE node2 
	IN (SELECT id FROM Nodes WHERE type=4 and name LIKE 'Nom:%')
	) order by w DESC LIMIT <0>";

$GLOBALS['FR-REQUEST-NOUN-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'Nom:%' and type=4); ";

$GLOBALS['FR-REQUEST-VERB-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'Ver:Inf%' and type=4); ";

$GLOBALS['FR-REQUEST-NOUN-ADJ-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'Adj:%' OR name LIKE 'Adv:%' and type=4);";

$GLOBALS['FR-REQUEST-NOUN-ADJ-VER-ADV-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE  type=4 and name LIKE 'Nom:%' OR name LIKE 'Ver:Inf' OR name LIKE 'Adj:%' OR name LIKE 'Adv:%');";

$GLOBALS['FR-REQUEST-NOUN-VER-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE  type=4 and name LIKE 'Nom:%' OR name LIKE 'Ver:Inf');";


//------
// "le joueur '$player_name' fait de la magie noire avec le mot '$fentry'."
$GLOBALS['FR-BLACK-MAGIC-EVENT'] = "le joueur '<0>' fait de la magie noire avec '<1>'.";
//"le joueur '$player_name' fait de la magie blanche avec le mot '$fentry'."
$GLOBALS['FR-WHITE-MAGIC-EVENT'] = "le joueur '<0>' fait de la magie blanche avec '<1>'.";

//------
$GLOBALS['FR-TGEN-CHOOSE-TERM'] = "Choisissez un terme qui vous plait :";

$GLOBALS['FR-RESULT-CHANGE'] = "Changer";
$GLOBALS['FR-RESULT-YOUR-NICKNAME'] = " de pseudo ";
$GLOBALS['FR-RESULT-CHANGE-YOUR-NICKNAME'] = "Vous êtes doué. Pour vous récompenser, vous pouvez changer de pseudo.";

$GLOBALS['FR-TERMCAPT-SHORT'] = "'<0>' vient d'être capturé.";
$GLOBALS['FR-TERMCAPT-LONG'] = "'<0>' (recherché pour <1> Cr) vient d'être capturé.";
$GLOBALS['FR-TERMOWN-SHORT'] = "'<0>' vient de changer de propriétaire.";
$GLOBALS['FR-TERMOWN-LONG'] = "'<0>' (recherché pour <1> Cr) vient ce changer de propriétaire.";
$GLOBALS['FR-TERMOWN-FREE'] = "le terme est libre";
$GLOBALS['FR-TERMOWN-NONFREE'] = "le terme est possédé par les joueurs '<0>' et '<1>'";

$GLOBALS['FR-NETWORK-TITLE'] = "Le Rézo";
$GLOBALS['FR-NETWORK-SEARCH-TERM'] = "<form id=\"gotermrel\" name=\"gotermrel\" method=\"post\" action=\"rezo.php\" >
	    <input id=\"gotermsubmit\" type=\"submit\" name=\"gotermsubmit\" value=\"Chercher\"> le mot
	    <input  id=\"gotermrel\" type=\"text\" name=\"gotermrel\" value=\"<0>\" size=100>
	    </form>";
$GLOBALS['FR-NETWORK-TERM-NON-EXIST'] = "<br>Le terme <0> n'existe pas !";
$GLOBALS['FR-NETWORK-TERM-HEADER'] = "'<0>' (id=<1> => <2> ; niveau = <3> ; luminosité = <4>)";
$GLOBALS['FR-NETWORK-NUMBER-RELATIONS'] = "<0> relations ";


?>
