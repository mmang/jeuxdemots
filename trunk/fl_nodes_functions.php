<?php include_once 'misc_functions.php' ; ?>

<?php 
//-- les noeuds en general



function get_node_id($w) {
    $query = "SELECT id FROM `Nodes`  WHERE  name= \"$name\" ;";
    $r =  @mysql_query($query) or die("bug in get_node_id : $query");
    $id = mysql_result($r , 0 , 0);
    return $id;;
}

//------ les relations

function make_relation($node1id, $node2id, $typerel, $val) {

    // il faut v�rif avant, car pas unique (je crois)
    //
    $query = "SELECT id FROM `Relations`  WHERE type=$typerel AND node1= '$node1id' AND node2= '$node2id';";
    $r =  @mysql_query($query) or die("bug in make_relation : $query");
    $nb = mysql_num_rows($r);
    
    if ($nb <= 0) {
	$query = "INSERT INTO Relations (node1, node2, type, w, creationdate) VALUES('$node1id', '$node2id', '$typerel', $val, CURRENT_TIMESTAMP)";
	$r =  @mysql_query($query) or die("pb in make_relation : $query");
	$id = mysql_insert_id(); 
    } else {
	$id = mysql_result($r , 0 , 0);
    }
    // on retourne l'id de la relation
    return $id;
}

function incf_relation($node1id, $node2id, $typerel, $val) {
    $query = "UPDATE `Relations` SET w = w+($val) WHERE node1= '$node1id' AND node2= '$node2id' AND type='$typerel';" ;
    $r =  @mysql_query($query) or die("pb in incf_relation : $query");
}

function get_relation_w($node1id, $node2id, $typerel) {
    $query = "SELECT w FROM `Relations`  WHERE type=$typerel AND node1= '$node1id' AND node2= '$node2id';";
    $r =  @mysql_query($query) or die("pb in get_relation_w : $query");
    return mysql_result($r , 0 , 0);
}




//------  les noeuds de fonction lexicales potentielles

function make_fl_potnode($relid) {
    // fera une erreur si cr�e plusieurs fois, mais  on s'en fout
    //
    $name = "_FL:$relid";

    $query = "INSERT INTO Nodes (name, type, w, creationdate, level) VALUES(\"$name\" , 6 , 50, CURRENT_TIMESTAMP, 0)";
    $r =  @mysql_query($query);
}

function get_fl_potnode_id($relid) {
    $name = "_FL:$relid";
    $query = "SELECT id FROM `Nodes`  WHERE type=6 AND name= '$name' ;";
    $r =  @mysql_query($query) or die("bug in get_fl_potnode_id : $query");
    $id = mysql_result($r , 0 , 0);
    return $id;;
}



function link_word_to_potnode($w, $relid, $val) {

   //debugecho("link_word_to_potnode: $w, $relid, $val");
    $wid = term_exist_in_BD_p($w);
    if ($wid > 0) {
	// on cr�e le noeud au cas o�  il n'existerait pas (peu probable)
	//
	make_fl_potnode($relid);
	// on recup l'id
	$potnodeid = get_fl_potnode_id($relid);
 
	// la relation 12 est celle g�rant les FL potentielles
	//
	make_relation($wid, $potnodeid, 12, 20);
	incf_relation($wid, $potnodeid, 12, $val);

	if (($relid != 0) and ($w > 0)) {
		link_word_to_potnode($w, 0, $val);
	}
	
	//debugecho("link_word_to_potnode : $w --> _FL:$relid : " . get_relation_w($wid, $potnodeid, 12));
    }  else {

	//debugecho("link_word_to_potnode : $w DOESN'T EXIST");
    }
}


function contaminate_link_word_to_potnode($w, $target, $relid) {
    //debugecho("contaminate_link_word_to_potnode: $w, $target, $relid");
    start_time_record("contaminate_link_word_to_potnode");
    
    if (($relid == 0)||($relid == 3)||($relid == 5)||($relid == 6)||($relid == 7)
    	||($relid == 8)||($relid == 9)||($relid == 10)
    	)
    {
	$wid = term_exist_in_BD_p($w);
	$targetid = term_exist_in_BD_p($target);
	if (($wid > 0) and  ($targetid > 0)) {
	  //  debugecho("contaminate_link_word_to_potnode: $w  and $target exist");
	    $potnodeid = get_fl_potnode_id($relid);
	   // debugecho("contaminate_link_word_to_potnode - potnodeid : $potnodeid ");
    
	    $poids = get_relation_w($wid, $potnodeid, 12);
	   // debugecho("contaminate_link_word_to_potnode - poids : $poids ");
    
	    make_relation($targetid, $potnodeid, 12, round($poids/2)+1);
	
	} else {
	  //  debugecho("contaminate_link_word_to_potnode: PB $w  and $target DO NOT exist");
	}
		if ($target != "***") {
    		if ($relid == 6) {link_word_to_potnode($target, 8, 1);}
   	 		if ($relid == 8) {link_word_to_potnode($target, 6, 1);}
   	 		
			if ($relid == 9) {link_word_to_potnode($target, 10, 1);}
   			if ($relid == 10) {link_word_to_potnode($target, 9, 1);}
		}
    } else {
		if ($target != "***") {
    		if ($relid == 16) {link_word_to_potnode($target, 25, 1);}	
    		if ($relid == 25) {link_word_to_potnode($target, 16, 1);}	
    		
    		if ($relid == 14) {link_word_to_potnode($target, 26, 1);}	
			if ($relid == 26) {link_word_to_potnode($target, 14, 1);}
			
		   	if ($relid == 17) {link_word_to_potnode($target, 23, 1);}	
			if ($relid == 23) {link_word_to_potnode($target, 17, 1);}
		
    		if ($relid == 15) {link_word_to_potnode($target, 28, 1);}	
    		if ($relid == 28) {link_word_to_potnode($target, 15, 1);}	
    		
    		if ($relid == 13) {link_word_to_potnode($target, 24, 1);}	
    		if ($relid == 24) {link_word_to_potnode($target, 13, 1);}	
    		
		   	if ($relid == 3) {link_word_to_potnode($target, 27, 1);}	
    		if ($relid == 27) {link_word_to_potnode($target, 3, 1);}
		}
	//debugecho("contaminate_link_word_to_potnode: no contamination for $relid");
    }
    debugecho("contaminate_link_word_to_potnode=" . end_time_record("contaminate_link_word_to_potnode"));
}


?>
