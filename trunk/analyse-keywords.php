<?php session_start();?>
<?php include_once "misc_functions.php" ?>
<?php include_once "intern_lexical_signature_functions.php" ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Identification de termes cl�s</title>
    
    
      </head>
<body>


<?php
openconnexion();

$_SESSION['wikibot-verbose'] == false;

function get_param($param) {
	$val=$_POST[$param];
	//echo "valpost:$param=$val ";
	if ($val == "") {$val=$_GET[$param];}
	//echo "valget:$param=$val ";
	return $val;
}

function get_signature($term) {
	//$id1 = term_exist_in_BD_p($_POST['term_1']);
	//echo "<br>check = " . check_need_update($id1);
	
	//echo "<br>sig in cache for $term == $sig";
	
		$id1 = term_exist_in_BD_p($term);
		$check1 = check_need_update($id1);
		if ($check1 == false) {
			$sig = $_SESSION['wikibot-signatures'][$term];
			if ($sig == '') {
				//echo " fetching sig for $term..."; flush();
				$sig = unserialize_sig(fetch_signature($id1));
			}
		} else {
			echo "<br>computing sig for $term ..."; flush();
			$sig = compute_full_lexical_signature_level($term, 1);
		}
		$_SESSION['wikibot-signatures'][$term] = $sig;
		
	// debug
	//
	if (count($sig) == 0) {echo "<br>sig for '$term' ($id1) EMPTY";}
		
	return $sig;
}
	
function new_delete_between_tags ($contents, $tag1, $tag2) {
	//echo "new_delete_between_tags($contents, $tag1, $tag2)";
	$pos1 = stripos($contents, $tag1, 0);
	$pos2 = stripos($contents, $tag2, $pos1 + strlen($tag1));
	if (($pos1== false) || ($pos2== false)) {
	// echo "not found!";
	 return $contents;
	}
	
	$newcontents = substr_replace($contents, ' ', $pos1, $pos2-$pos1);
	echo "<P>pos1=$pos1 pos2=$pos2 new contents : $newcontents";
	return $newcontents;
}



function delete_between_tags ($contents, $tag1, $tag2) {
	$pos1 = stripos($contents, $tag1, 0);
	$pos2 = stripos($contents, $tag2, $pos1 + strlen($tag1));
	if (($pos1== false) || ($pos2== false)) {
		//echo "<br>$contents  :: $tag1 -- $tag2  => not found!";
	 return $contents;}
	
	$contents = substr_replace($contents, '', $pos1, $pos2-$pos1);
	return $contents;
}

function extract_between_tags($contents, $tag1, $tag2) {
	//echo "extract_between_tags($contents, $tag1, $tag2)";
	$pos1 = stripos($contents, $tag1);
	$pos2 = stripos($contents, $tag2, $pos1 + strlen($tag1));
	if (($pos1 === false) || ($pos2 === false) ) {return false;}
//	echo " coucou:pos1=$pos1 et pos2=$pos2 ";
	$rez = substr($contents,$pos1+strlen($tag1), $pos2-$pos1-strlen($tag1));
	return $rez;
}

function find_tag ($contents, $tag) {
	//echo "find_tag ($contents, $tag)";
	$pos = strpos($contents, $tag);
	//echo " pos=$pos";
	return ($pos != false);
}

function process_text ($contenu) {
	process_texte_wiki($contenu);
}


function text_to_tab($page){
	//$tag = 'text_to_tab' . rand();
	//start_time_record($tag);
	
	$page = str_replace("'", "' ", $page);
	$page = str_replace("\t", " ", $page);
	$page = str_replace("[", " ", $page);
	$page = str_replace("]", " ", $page);
	$page = str_replace("�", " ", $page);
	
	$page=ereg_replace("\"|_|[|-|. |,|'|\"|~|?|#|`|_|\|^|:|;|(|)|{|}|  |�|&|=|/|<|>|�]"," ",$page);
	//$char=chr(0160); 
	//$page = str_replace("$char", " ", $page);
	//echo "<br>$page";
	$tab_mots = preg_split("/ +/",$page); // un espace ou plus
	//print_r($tab_mots);
	for($i=0;$i<(count($tab_mots));$i++){ 
		$string=ereg_replace("\"|_|[|-|. |,|'|\"|~|?|#|`|_|\|^|:|;|(|)|{|}|  |�|&|=|/|<|>|�]"," ",$tab_mots[$i]);
		$tab_mots[$i]=$string;
	}	
	//$duree = end_time_record($tag);
	//echo ("<p>2 duree pour '$tag' = $duree");
	
	return $tab_mots;
}

function text_to_tab_raw($page){
	//$tag = 'text_to_tab_raw' . rand();
	//start_time_record($tag);
	
	$page = str_replace(", ", " , ", $page);
	$page = str_replace(". ", " . ", $page);
	$page = str_replace("! ", " ! ", $page);
	$page = str_replace("? ", " ? ", $page);
	$page = str_replace("; ", " ; ", $page);
	$page = str_replace("� ", " � ", $page);
	$page = str_replace("\"", " \" ", $page);
	$page = str_replace("�", "'", $page);
	$page = str_replace("  ", " ", $page);
	$page = str_replace("  ", " ", $page);
	
	$tab_mots = preg_split("/ +/",$page); // un espace ou plus
	
	//$duree = end_time_record($tag);
	//echo ("<p>2 duree pour '$tag' = $duree");
	
	return $tab_mots;
}


function get_term_df_array ($ar_tf) {
	$Connexion = mysql_connect("mysql.lirmm.fr:13306", "lafourca", "AChanger");
	// Ouverture de la base de donn�es :
	mysql_select_db("Lafourcade_BigRezo" , $Connexion);
   
	$newtab = array(); 
	
	foreach ($ar_tf as $key => $value) {
		$df= get_term_df($key);
		if ($df == -1) {
			$val = 1;
		} else {
			$val = log($df,10);
			//$val = $df;
		}
	//	echo "<br>key = $df : $val ==> $value/$val = $newval";
		$newtab[$key] = $val;
	}	
	return $newtab;
}

function get_term_df ($term) {
	if ($term == "") {return -1;}
	//echo "<br>service=$service term=$term<br>";
	$term = addslashes($term);
	$query = "SELECT sum_in, sum_out FROM `terms` WHERE `term` = \"$term\"";
	$r =  mysql_query($query) or die("ca shitte dans get_term_df : $query");
  	$nb = mysql_num_rows($r);
  	if ($nb <= 0) {
  		return -1;
  	} else {
  		$sum_in =  mysql_result($r , 0 , 0);
  		$sum_out =  mysql_result($r , 0 , 1);
  		$val = $sum_in + $sum_out;
  		return $val;
  	}
}

function verify_term($term) {
	$query = "SELECT id FROM `Nodes` WHERE `name` = '$term'";
  	//  echo "<r> q=$query<p>";
   	$r =  mysql_query($query) or die("ca shitte dans check_term : $query");
   	$nb = mysql_num_rows($r);
   	if ($nb == 0) {
   		return -1;
   	} else {
   		$id= mysql_result($r , 0 , 0);
   		return $id;
   	}
}

function check_is_Noun($id) {
	$query = "SELECT id FROM `Relations` WHERE type=4 and node1=$id 
	and node2 in (146882, 146898, 146889, 171869, 171870, 146895, 146893, 
	146881, 161769, 148212, 148283, 146884, 147279, 
	146886, 146887, 146885 , 150563, 157023)";
  	//  echo "<r> q=$query<p>";
   	$r =  mysql_query($query) or die("ca shitte dans check_is_Noun : $query");
   	$nb = mysql_num_rows($r);
   	if ($nb == 0) {
   		return 0;
   	} else {
   		return $nb;
   	}
}

function check_is_other($id) {
	$query = "SELECT id FROM `Relations` WHERE type=4 and node1=$id 
	and node2 in (146882, 146898, 147628, 146911, 161751, 147920, 147920, 146901, 
	212235, 150504, 146896, 148598, 146889, 212241)";
  	//  echo "<r> q=$query<p>";
   	$r =  mysql_query($query) or die("ca shitte dans check_is_Noun : $query");
   	$nb = mysql_num_rows($r);
   	if ($nb == 0) {
   		return 0;
   	} else {
   		return $nb;
   	}
}

function check_cats($id) {
	$query = "SELECT id FROM `Relations` WHERE type=4 and node1=$id";
  	//  echo "<r> q=$query<p>";
   	$r =  mysql_query($query) or die("ca shitte dans check_is_Noun : $query");
   	$nb = mysql_num_rows($r);
   	if ($nb == 0) {
   		return 0;
   	} else {
   		return $nb;
   	}
}

function deal_with_s($term) {
	$l=strlen($term);
	if ($term[$l-1] == 's') {
		$newstr = substr($term, 0, ($l - 1));
		return $newstr;
	}
	return $term;
}

function process_texte_wiki ($texte) {
	$Connexion = mysql_connect("mysql.lirmm.fr", "lafourca", "newfann1");
    // Ouverture de la base de donn�es :
    mysql_select_db("LafourcadeRezoLexical" , $Connexion);
    
    
	// metrologie
	$tag = 'process_texte_wiki' . rand();
	start_time_record($tag);
	//$duree = end_time_record($tag);
	//echo ("<p>2 duree avant signature pour '$tag' = $duree");
	
	$text = trim($texte);
	$ar = text_to_tab($text);
	$ar_raw = text_to_tab_raw($text);
	$ar_lem = array();
	
	//print_r($ar_raw);
	
	$nb = count($ar);
	
	if ($_SESSION['wikibot-verbose']) {
		echo "<br># $nb terms<br>";
		$duree = end_time_record($tag);
		echo ("<p>1 duree segmentation pour '$tag' = $duree");
		flush();
	}
	
	$ar_texte = array();
	$ar_index = array();
	$ar_index_rev = array();
	$ar_freq = array();
	$ar_df = array();
	$ar_tf = array();
	
	$loc_array_tf = array();
	$loc_array_df = array();
	
	$sum_tf = 0;
	$sum_df = 0;
	
	$j = 0;
	for($i=0;$i<$nb;$i=$i+1) {
		//echo $ar[$i];
		$ar[$i] = trim($ar[$i]);
		$term = $ar[$i];
		$checkpc = stripos($term, "%");
		if (($term != "") && ($checkpc === false)) {
			$j = $j+1;
			$term_id = verify_term($term);
			$is_noun = check_is_Noun($term_id);
			$cats = check_cats($term_id);
			$othercats = max(1,check_is_other($term_id)*10);
			//$catval = $is_noun/$cats;
			// debug
			if (($term_id > 0) && ($is_noun == 0) && ($cats == 0) && ($othercats == 0))
			{
				echo "<br>  NO POS pour $term";
			}
			
			if ($term_id > 0) {
				$ar_index_rev[$term_id]=$term;
				$ar_index[$term]=$term_id;
			} else {
				$newstr = deal_with_s($term);
				//echo "<br>'$term' not found, verif '$newstr' ";
				
				if ($term_id > 0) {
				$term_id = verify_term($newstr);
				$is_noun = check_is_Noun($term_id);
				$cats = check_cats($term_id);
				$othercats = max(1,check_is_other($term_id)*15);
				
				//
				echo "<br>alternate word=$newstr == id=$term_id isnoun=$is_noun cats=$cats othercats=$othercats";
				
				$term = $newstr;
				$ar_index_rev[$term_id]=$term;
				$ar_index[$term]=$term_id;
				}
			}
			if (($term_id > 0) && ($is_noun > 0)) {
				//echo "<br>is noun $term == $is_noun / ($cats*$othercats)";
				$ar_tf[$term] = $ar_tf[$term] + $is_noun/($cats*$othercats);
				$sum_tf =  $sum_tf + $valeur;
			}
			//echo "<br>adding '$term' to array ";
			array_push($ar_texte, $term);
			array_push($ar_lem, deal_with_s($term));
		}		
	}
	
	//print_r($ar_lem);
	
	//$ar_df = get_term_df_array ($ar_tf);
	$ar_df = get_term_df_array ($ar_index);
	
	//echo "<p>arr tf = " ;
	//print_r($ar_tf);
	openconnexion();
	
	find_all_locutions ($ar, $ar_df);
	$loc_array_tf = array_merge($loc_array_tf, $_SESSION['wikibot-arr-loc-tf']);
	$loc_array_df = array_merge($loc_array_df, $_SESSION['wikibot-arr-loc-df']);
	find_all_locutions ($ar_raw, $ar_df);
	$loc_array_tf = array_merge($loc_array_tf, $_SESSION['wikibot-arr-loc-tf']);
	$loc_array_df = array_merge($loc_array_df, $_SESSION['wikibot-arr-loc-df']);
	find_all_locutions ($ar_lem, $ar_df);
	$loc_array_tf = array_merge($loc_array_tf, $_SESSION['wikibot-arr-loc-tf']);
	$loc_array_df = array_merge($loc_array_df, $_SESSION['wikibot-arr-loc-df']);
	
	echo "<p>arr LOC TF = " ; print_r($loc_array_tf);
	//echo "<p>arr DF = " ; print_r($ar_df);
	
	$ar_tf = array_merge($ar_tf, $loc_array_tf);
	$ar_df = array_merge($ar_df, $loc_array_df);
			
	//$duree = end_time_record($tag);
	//echo ("<p>2 duree avant signature pour '$tag' = $duree");
	
	//echo "<p>arr TF = " ; print_r($ar_tf);
	//echo "<p>arr DF = " ; print_r($ar_df);
	
	
	foreach ($ar_tf as $key => $value) {
		//$sig = get_signature($key);
		$ar_tf[$key] = $value / $ar_df[$key];
	}
	$norm = compute_norm($ar_tf);
	foreach ($ar_tf as $key => $value) {
		$ar_tf[$key] = $value / $norm;
	}
	
	if ($_SESSION['wikibot-verbose']) {
	/// debug
		arsort($ar_tf);
		echo "<p>arr VAL = " ; print_r($ar_tf);
		flush();
		$duree = end_time_record($tag);
		echo ("<p>3 duree avant signature pour '$tag' = $duree");
		flush();
	}
	
	$duree = end_time_record($tag);
	echo ("<p>3 duree avant signature pour '$tag' = $duree");
	flush();
		
	// calcul signatures
	//
	if (count($_SESSION['wikibot-signatures']) > 3000) {
		echo "<BR>vidage cache";
		$_SESSION['wikibot-signatures'] = array();
	}
	$sig_total = array();
	foreach ($ar_tf as $key => $value) {
		if ($value > 0.04) {
			$sig = get_signature($key);
			$sig_total = add_sig($sig_total,$sig, $value);
		}
	}
	$norm = compute_norm($sig_total);
	foreach ($sig_total as $key => $value) {
		$sig_total[$key] = $value/$norm;
	}

	//echo "<P>SIGNATURE = ";
	//decode_sig($sig_total, 100);
	//flush();
	
	//echo "<P>SIGNATURE NON DECODE = "; 
	//print_r($sig_total); 
	//flush();
	
	// comparaison avec les termes du texte
	//
	$ar_sim1 = array();
	foreach ($ar_tf as $key => $value) {
		$sig = $_SESSION['wikibot-signatures'][$key];
		$id = $ar_index[$key];
		$simi = signature_sim_biz(0, $id, $sig_total, $sig, false);
		$ar_sim1[$key] = $simi*$value;
		//echo "<br>SIM 1 avec $key ($id) = $simi"; flush();
	}
	//arsort($ar_sim1);
	//echo "<p>arr SIM 1 = " ; print_r($ar_sim1);
	
	//$duree = end_time_record($tag);
	//echo ("<p>4 duree apres ar_sim1 pour '$tag' = $duree");
	//flush();
		
	// le tri est important ici, ne pas l 'enlever
	arsort($sig_total);
	$ar_sim2 = array();
	$j=0;
	foreach ($sig_total as $key => $value) {
		//echo "<br>j=$j";
		if ($j <= 50) {
			$term = get_term_from_id($key);
			$sig = get_signature($term);
			$simi = signature_sim_biz(0, $key, $sig_total, $sig, false);
			//echo "<br>SIM 2 avec $term $key = $simi"; flush();
			$ar_sim2[$term] = $simi*$value;
		}
		$j=$j+1;
	}
	//$duree = end_time_record($tag);
	//echo ("<p>5 duree apres ar_sim2 pour '$tag' = $duree");
	//flush();
	
	//arsort($ar_sim2);
	//echo "<p>arr SIM 2 = " ; print_r($ar_sim2);
	
	// RESULTAT => fusion des deux resultats partiels
	//
	$ar_RES = array();
	foreach ($ar_sim1 as $key => $value) {
		$ar_RES[$key] = $ar_RES[$key] + $value;
	}
	foreach ($ar_sim2 as $key => $value) {
		$ar_RES[$key] = $ar_RES[$key] + $value;
	}
	arsort($ar_RES);
	$norm = compute_norm($ar_RES);
	foreach ($ar_RES as $key => $value) {
		$ar_RES[$key] = $value/$norm;
	}
	//echo "<p>RES = " ; print_r($ar_RES);
	
	echo "<P>";
	$j=0;
	foreach ($ar_RES as $key => $value) {
		if (($j < 20) 
			&& ($value > 0.04) 
			&& (is_numeric($key) == false) 
			&& (term_too_short($key) == false)
			) {
			echo " $key -";
		}
		$j++;
	}
	
	
	$duree = end_time_record($tag);
	echo("<p>duree pour '$tag' = $duree");
}


function term_too_short ($term) {
	return(strlen($term) <= 2);
}

function filter_numeric_list($arr) {
	$newarr = array();
	foreach ($arr as $key => $value) {
		$nump = is_numeric($key);
		$test = ($nump == true);
		if ($test) {
		//	echo "<br>$key is numeric";
		} else {
			$newarr[$key] = $value;
		}
	}
	return $newarr;
}

function find_all_locutions ($ar_texte, $ar_tf) {
	//print_r($ar_texte);
	$_SESSION['wikibot-arr-loc-tf'] = array();
	$_SESSION['wikibot-arr-loc-df'] = array();
	$nb = count($ar_texte);
	for($i=0;$i<$nb;$i=$i+1) {
		find_locutions_atpos ($ar_texte, $i, $ar_tf);
	}
	//echo "<P>locutions : ";
	//print_r($_SESSION['wikibot-arr-loc']);
	
}

function find_locutions_atpos ($ar_texte, $i, $ar_df) {
	$max = 100000;
	$size = 7; 
	$string = $ar_texte[$i];
	$prod = $ar_df[$string];
	if ($prod == 0) {$prod = $max;}
	
	for ($j=1;$j<$size;$j=$j+1) {
		$previous_term = $next_term;
		$next_term = $ar_texte[$i + $j];
		if ($next_term == "") {return ;}
		$string = "$string $next_term";
		//echo "<br> $string";
		$df = $ar_df[$next_term];
		if ($df == 0) {$df = $max;}
		
		$prod = min($prod,$df);
		$id = get_term_id($string);	
		$check = check_is_Noun($id);
		if (($id != -1) && ($check >  0)) {
			//if ($_SESSION['wikibot-verbose']) {
			//	echo "<br>loc found ! $string : $prod";
			//}
			$cc = $j+1;
			if (stripos($string, '\'') >0) {
				$cc=$cc+1;
			}
			//$val = $j + 1;
			$val = 0.5+log($cc,2);
			$_SESSION['wikibot-arr-loc-tf'][$string] = $_SESSION['wikibot-arr-loc-tf'][$string] + $val;
			
			if ($prod == $max) {$prod = 1;}
			$_SESSION['wikibot-arr-loc-df'][$string] = $prod;
		}
	}
}

function open_rezo_connexion() {
	$_SESSION['REZO-CONNEXION-URL'] = "mysql.lirmm.fr";
	$_SESSION['REZO-CONNEXION-USER'] = "lafourca";
	$_SESSION['REZO-CONNEXION-PSWD'] = "newfann1";
	$_SESSION['REZO-CONNEXION-DBNAME'] = "LafourcadeRezoLexical";
	
	
	// Ouverture de la connexion :  
    $connexion = mysql_connect($_SESSION['REZO-CONNEXION-URL'], $_SESSION['REZO-CONNEXION-USER'], $_SESSION['REZO-CONNEXION-PSWD']);
    // Ouverture de la base de donn�es :
    mysql_select_db($_SESSION['REZO-CONNEXION-DBNAME'] , $connexion);
}

function get_term_id($term) {
	open_rezo_connexion();
	$term = addslashes($term);
    $query =  "SELECT id FROM `Nodes` WHERE Nodes.name = \"$term\";"   ;
    $r =  @mysql_query($query) or die("bug in get_term_id : $query");
    $nb = mysql_num_rows($r);
    if ($nb == 0) {return -1;} else {return mysql_result($r , 0 , 0);}
  // si -1 alors exist po !
}


function generate_form() {
	$contenu = trim(stripslashes(get_param("wiki_page")));
				
	//echo "contenu $contenu";
	$page = str_replace(" ", "_", $page);
				
	// fait rentrer le texte
	print "<table width=\"100%\" border=1><TR>";
	print "<td>";
	print "<FORM METHOD=\"post\" ACTION=\"analyse-keywords.php\">";
	print "<center>Page � recup�rer et extraire les termes cl�s</center>";
	print "<center>";
	//print "<INPUT type=\"text\" size=\"100\" name=\"wiki_page\" value=\"$page\"/>";
	print "<TEXTAREA cols=\"100%\" rows=\"20\" id=\"wiki_page\" name=\"wiki_page\">$contenu</TEXTAREA>";
	print "<P><INPUT type=\"submit\" name=\"submit_anal\" value=\"Go !\"/>";
	print "</FORM>";
	print "<TD><CENTER<FORM><INPUT type=\"submit\" name=\"submit_empty_cache\" value=\"Vider cache\"/>
				</FORM></CENTER";
	print "</table>";
				
	if (($contenu != '') && (get_param("submit_anal") != '')) {
		//echo "<br>text = $contenu<P>";
		flush();
		process_text($contenu);
	}
	if (get_param("submit_empty_cache") != '') {
		echo "<BR>vidage cache";
		$_SESSION['wikibot-signatures'] = array();
	}
}

			
generate_form();

?>


</body>
</html>
