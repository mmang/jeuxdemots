<?php session_start();?>
<?php include_once 'misc_functions.php'; ?>
<?php include_once 'misc_duel_functions.php'; ?>
<?php
    openconnexion();

?>
<html>
 <head>
    <title><?php echo get_msg('SOUK-TITLE'); ?></title>
    <script type="text/javascript" src="mootools.js"></script>
   <script type="text/javascript" src="mootoolsmore.js"></script>
    <?php header_page_encoding(); ?>
  </head>
<?php include 'HTML-body.html' ; ?>

<?php topblock(); ?>
<?php update_souk(); ?>


<?php
function how_many_gift_given_left($playerid) {
	$query = "SELECT count(id) FROM `Gifts` WHERE `creatorid` = $playerid ";
	$r =  @mysql_query($query) or die("pb in how_many_gift_given_left: $query");
    $nb = mysql_result($r , 0 , 0);
    return $nb;
}


function update_souk() {
    $_SESSION[ssig() . 'state']=0;

    // gestion mot � la mode
    //
    if ($_POST['cagnotesubmit']!= "") {
	$cost = 5;
	if (get_player_credit($_SESSION[ssig() . 'playerid']) < $cost) {
	    $_POST['cagnotesubmit'] == "broke";} else {
	    $_POST['cagnotesubmit'] == "paid";
	    inc_player_credit($_SESSION[ssig() . 'playerid'], -$cost);
		}
    }
    
    // gestion mot � la mode
    //
    if ($_POST['formhypewordsumit']!= "") {
	$cost = 10;
	if (get_player_credit($_SESSION[ssig() . 'playerid']) < $cost) {
	    $_POST['formhypewordsumit'] == "broke";} else {
	    $_POST['formhypewordsumit'] == "paid";
	    inc_player_credit($_SESSION[ssig() . 'playerid'], -$cost);
	}
    }

    // gestion list de mots
    //
    if ($_POST['gotermlist']!= "") {
	$cost = 0;
	if (get_player_credit($_SESSION[ssig() . 'playerid']) < $cost) {
	    $_POST['gotermlist'] == "broke";} else {
	    $_POST['gotermlist'] == "paid";
	    inc_player_credit($_SESSION[ssig() . 'playerid'], -$cost);
	}
    }

    // gestion des cadeaux
    //
    if ($_POST['formmakegift']!= "") {
    	
    if ($_SESSION[ssig() . 'LASTACTION'] == "gift")
    		{$cost = 100;} else {$cost = 100;}
    $cost = 100 + how_many_gift_given_left($_SESSION[ssig() . 'playerid'])	;	
    		
	//echo "formmakegift" . $_POST['giftplayername'] . " " . $_POST['giftterm'] ;
	if (($_POST['giftterm']!= "") || ($_POST['giftplayername']!= "")) {
	    
	    if (get_player_credit($_SESSION[ssig() . 'playerid']) < $cost) {
		$_SESSION[ssig() . 'gifterror']="nocash";
		return;
		}

	    $termid = term_exist_in_BD_p($_POST['giftterm']);
	    if ($termid == 0) {
			$_SESSION[ssig() . 'gifterror']="bad term";
			//echo "bad term";
			return;
		}
		
    	if (check_proposed_word($termid)) {
    		//echo "already given";
    		$_SESSION[ssig() . 'gifterror']="already given";
    		return;
  	  	}
	    
	    if (get_player_id($_POST['giftplayername']) == get_player_id($_SESSION[ssig() . 'login'])) 
		{$_SESSION[ssig() . 'gifterror']="self gift";
		//echo "self gift";
		return;}

	    if (($_SESSION[ssig() . 'login'] == "") || (guestplayerp() =="Y")){
	    	$_SESSION[ssig() . 'gifterror']="no guest";
			//echo "no guest";
			return;
	    }

	    $destid = player_exist_p($_POST['giftplayername']);
	    if ($destid == 0) {
	    	$_SESSION[ssig() . 'gifterror']="bad player"; 
			//echo "bad player";
			return;
		}
		
		if (check_proposed_word($termid, $destid)) {
    		//echo "already given";
    		$_SESSION[ssig() . 'gifterror']="already given";
    		return;
  	  	}

	$code= make_gift($destid, $_SESSION[ssig() . 'playerid'], $termid);
	$_SESSION[ssig() . 'LASTACTION'] = "gift";
	insert_proposed_word($termid);
	
	
	if ($code > -1) {$h = 1;} else {$h = -10;}
	inc_player_credit($_SESSION[ssig() . 'playerid'], -$cost);
	inc_player_honnor($_SESSION[ssig() . 'playerid'], $h);
    
	$_SESSION[ssig() . 'gifterror']="noerror"; 
	
	$dest = $_POST['giftplayername'];
	$creatorname = get_player_name($destid);
	$destemail = get_player_email($destid);

	debugecho("$creatorname - $destemail");
	send_gift_mail($destemail, $creatorname);

	$player = $_SESSION[ssig() . 'login'];
	//make_event("le joueur '$player' a offert un cadeau au joueur '$dest'.");
	$GLOBALS[0]=$player;$GLOBALS[1]=$dest;
	make_event(get_msg('SOUK-GIFT-EVENT'));
	

	} else {$_SESSION[ssig() . 'gifterror']="noinput";}
    } else {
		$_SESSION[ssig() . 'gifterror'] = "";
    }


	
    if (($_POST['buyprobsubmit+']!= "")
    	|| ($_POST['buyprobsubmit-']!= "")
    	|| ($_POST['unik']!= "")
    	){
    
    	if ($_POST['unik']!= "") {
    		$cost = 5000;
    		echo "UNIK";
    	} else {
    		$cost = 50;}
	
	if (get_player_credit($_SESSION[ssig() . 'playerid']) < $cost) {
		$_SESSION[ssig() . 'proberror']="nocash";
		return;
		}
	if (($_SESSION[ssig() . 'login'] == "") || (guestplayerp() =="Y"))
		{$_SESSION[ssig() . 'proberror']="noguest";
		//echo "no guest";
		return;}

	inc_player_credit($_SESSION[ssig() . 'playerid'], -$cost);
	
	if ($_POST['buyprobsubmit+']!= "") {$dir = 1;}
	if ($_POST['buyprobsubmit-']!= "") {$dir = -1;}
    if ($_POST['unik']!= "") {$dir = 0;}
	adjust_player_permission($_SESSION[ssig() . 'playerid'], $_POST['buyprobrelid'], $dir);

	$_SESSION[ssig() . 'proberror']="noerror";
    } else {
	$_SESSION[ssig() . 'proberror']="";
    }
    
    
    
	if (($_POST['equiprob']!= "")){
    	$cost = 5000;
		if (get_player_credit($_SESSION[ssig() . 'playerid']) < $cost) {
			$_SESSION[ssig() . 'proberror']="nocash";
			return;
			}
		if (($_SESSION[ssig() . 'login'] == "") || (guestplayerp() =="Y"))
			{$_SESSION[ssig() . 'proberror']="noguest";
			//echo "no guest";
			return;}

		inc_player_credit($_SESSION[ssig() . 'playerid'], -$cost);
		equiprob_player_permission($_SESSION[ssig() . 'playerid']);
		$_SESSION[ssig() . 'proberror']="noerror";
    } else {
		$_SESSION[ssig() . 'proberror']="";
    }
    
    
    
    // buying comp
    if ($_POST['buycompsubmit']!= ""){
    	
	$cost = max(1000, $_POST['buycompprice']);
	$honnormin = max(100,$_POST['buycomphonnor']);

	if (($_SESSION[ssig() . 'login'] == "") || (guestplayerp() =="Y"))
		{$_SESSION[ssig() . 'comperror']="noguest";
		//echo "no guest";
		return;}

	if (get_player_honnor($_SESSION[ssig() . 'playerid']) < $honnormin) {
		$_SESSION[ssig() . 'comperror']="nohonnor";
		return;
		}

	if (get_player_credit($_SESSION[ssig() . 'playerid']) < $cost) {
		$_SESSION[ssig() . 'comperror']="nocash";
		return;
		}

	inc_player_credit($_SESSION[ssig() . 'playerid'], -$cost);

	make_player_permission($_SESSION[ssig() . 'playerid'], $_POST['buycompid'], $dir);

	$GLOBALS[0]=$_SESSION[ssig() . 'login'];
	// 	"le joueur '$login' a achet� une comp�tence
	make_event(get_msg('SOUK-BUYCOMP-EVENT'));

	$_SESSION[ssig() . 'comperror']="noerror";
    } else {
	$_SESSION[ssig() . 'comperror']="";
    }
    
}


//--------------------------------

function display_hype_words() {
	
	$playerid=$_SESSION[ssig() . 'playerid'];
	
    if ($_POST['formhypewordsumit']!= "") {
	if ($_POST['formhypewordsumit']== "broke") {
	//"La maison ne fait pas cr�dit !"
	echo get_msg('SOUK-WARNING-NOCREDIT');
	return;
	}

    echo "<br><TABLE border=\"1\" valign=\"top\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0%\">";
    echo "<TR><TH>", get_msg('SOUK-HYPEWORDS'),"<TH>";
    echo get_msg('SOUK-INWORDS'),"<TH>",get_msg('SOUK-OUTWORDS');
    echo "<TR><TH valign=\"top\">";
    $query= "(SELECT name,owner,gain,token FROM HypeWords WHERE owner = $playerid and token > 0) 
    UNION (SELECT name,owner,gain,token FROM HypeWords WHERE token > 0  ORDER BY Rand() LIMIT 50) LIMIT 100
    ";
    $r =  @mysql_query($query) or die("pb in display_hype_words: $query");
    $nb = mysql_num_rows($r);
    for ($i=0 ; $i<$nb ; $i++)
      {
      $name = mysql_result($r , $i , 0);
      $creator = mysql_result($r , $i , 1);
      $gain= mysql_result($r , $i , 2);
      $token= mysql_result($r , $i , 3);
      $ct = "black";
      if ($creator == $_SESSION[ssig() . 'playerid']) { $ct = "red";}
      if ($creator == 0) { $ct = "black";}
      
      if ($ct == "red") {
      	echo  "\n<br><span 
    		onMouseover=\"ddrivetip('jetons = $token gain = $gain','yellow', 300)\";
			onMouseout=\"hideddrivetip()\">
			<font color=\"$ct\"><small>" . $name . "</small></font></span>";
      } else {
      echo  "\n<br><font color=\"$ct\"><small>" . $name . "</small></font>";
      }
	}
      

    echo "<TH valign=\"top\">";
    $query= "(SELECT name,owner FROM HypeWords WHERE owner = $playerid and token < 0) 
    UNION (SELECT name,owner FROM HypeWords WHERE token < 0  ORDER BY Rand() LIMIT 50) LIMIT 100
    ";
    $r =  @mysql_query($query) or die("pb in display_hype_words: $query");
    $nb = mysql_num_rows($r);
    for ($i=0 ; $i<$nb ; $i++)
      {
      $name = mysql_result($r , $i , 0);
      $creator = mysql_result($r , $i , 1);
      $ct = "black";
      if ($creator == $_SESSION[ssig() . 'playerid']) { $ct = "orange";}
      if ($creator == 0) { $ct = "black";}
      echo  "\n<br><font color=\"$ct\"><small>" . $name . "</small></font>";
      }
    

    echo "<TH valign=\"top\">";
    $query= "(SELECT name,owner FROM HypeWords WHERE owner = $playerid and token = 0) 
    UNION (SELECT name,owner FROM HypeWords WHERE token = 0  ORDER BY Rand() LIMIT 50) LIMIT 100
    ";
    $r =  @mysql_query($query) or die("pb in display_hype_words: $query");
    $nb = mysql_num_rows($r);
    for ($i=0 ; $i<$nb ; $i++)
      {
      	$name = mysql_result($r , $i , 0);
      $creator = mysql_result($r , $i , 1);
      $ct = "black";
      if ($creator == $_SESSION[ssig() . 'playerid']) { $ct = "black";}
      if ($creator == 0) { $ct = "black";}
      echo  "\n<br><font color=\"$ct\"><small>" . $name . "</small></font>";
      }

     echo "</TABLE>";
    }
}

function generate_hype_words_form() {
	echo "<spacer type=\"block\" width=\"1\" height=\"20\">";
	echo "<a name=\"hypelist\">";
	echo "<form id=\"formhypeword\" name=\"formhypeword\" method=\"post\" action=\"souk.php#hypelist\" >";
	//�"<input id=\"formhypewordsumit\" type=\"submit\" name=\"formhypewordsumit\" value=\"Visualiser\"> (10 Cr)
	//	les mots � la mode."
	echo get_msg('SOUK-HYPEWORDS-VIEW-FORM');
	echo "	</form>";
	display_hype_words() ;
}

//--------------------------------

function generate_cagnote_form() {
	echo "<spacer type=\"block\" width=\"1\" height=\"20\">";
	echo "<a name=\"cagnote\">";
	echo "<form id=\"cagnote\" name=\"cagnote\" method=\"post\" action=\"souk.php#cagnote\" >";
	//	<input id=\"cagnotesubmit\" type=\"submit\" name=\"cagnotesubmit\" value=\"Visualiser\"> (5 Cr)
	//	le montant de la cagnotte.
	echo get_msg('SOUK-CAGNOTTE-VIEW-FORM');
	echo "	</form>";
	display_cagnote() ;
}

function display_cagnote() {
    if ($_POST['cagnotesubmit']!= "") {
	if ($_POST['cagnotesubmit']== "broke") {
	    display_warning(get_msg('SOUK-WARNING-NOCREDIT'));
	    return;
	}
        
    $query= "SELECT amount, gain, player1, player2, token FROM Cagnote WHERE id = 0";
    $r =  @mysql_query($query) or die("pb in display_cagnote");
    $amount = mysql_result($r , 0 , 0);
    $gain = mysql_result($r , 0 , 1);
    $player1= mysql_result($r , 0 , 2);
    $player2= mysql_result($r , 0 , 3);
    $token= mysql_result($r , 0 , 4);

    $GLOBALS[0] = $amount;
    $GLOBALS[1] = get_player_name($player1);
    $GLOBALS[2] = get_player_name($player2);
    $GLOBALS[3] = $gain;
    $GLOBALS[4] = $token;
    //"La cagnote contient <0> Cr�dits.
    //Les joueurs <1> et <2> sont en t�te avec un score de <3> Cr�dits. 
	//	    La cagnote tombera dans <4> jackpot(s)"
    display_warning(get_msg('SOUK-CAGNOTTE-DISPLAY-WARNING'));
    
    }
}



//--------------------------------

function make_gift($destid, $creatorid, $termid) {
    debugecho("make_gift : $destid - $creatorid - $termid");
    if (gift_already_given_p($destid, $creatorid, $termid)) {
    	debugecho("make_gift : $destid - $creatorid - $termid --> ALREADY GIVEN");
    	return -1;
    } else {
    $query = "INSERT INTO `Gifts` ( `id` , `destid` , `creatorid` , `termid` , `relationid` , `giftdate` )
		VALUES (
		NULL , '$destid', '$creatorid', '$termid', '0', NOW( )
		);";
    $r =  @mysql_query($query) or die("pb make_gift : $query");
    return 0;
	}
}
    
function gift_already_given_p($destid, $creatorid, $termid) {
	$query = "SELECT count(id) FROM Gifts WHERE creatorid=$creatorid 
	AND termid=$termid and destid = $destid;";
	$r =  @mysql_query($query) or die("pb gift_already_given_p : $query");
	$nb = mysql_result($r , 0 , 0);
	return ($nb > 0);
}

function send_gift_mail ($adress, $creator) {

   /*"Bonjour $creator,\n";
    "J'ai le plaisir de vous annoncer que vous venez de recevoir un cadeau. 
    Rendez-vous dans le souk pour l'ouvrir.";
    "\n\n";
    
    "A bientot.\nCordialement,\nJeuxDeMots";
    "\nhttp://www.lirmm.fr/jeuxdemots"; */
    
    
    $query = "SELECT text FROM `Tips` ORDER BY RAND() LIMIT 1";
    $r = mysql_query($query);
    $tips = mysql_result($r , 0 , 0);
        
    debugecho("adresse=$adress  / mailtext=$msg");
    $GLOBALS[0] = $_SESSION[ssig() . 'login'];
    $title = get_msg('MAIL-GIFT-TITLE');
    
    $GLOBALS[0] = $creator;
    $GLOBALS[1] = $_SESSION[ssig() . 'login'];
    $msg = get_msg('MAIL-GIFT');
    
    $GLOBALS[0] = $tips;
    $msg = $msg . get_msg('MAIL-BOTTOM');

    
    mail($adress, $title, $msg, "From: ".get_msg('MAIL-FROM')."\n" .
	    "Content-Type: text/plain; charset=\"".get_msg('ENCODING-MAIL')."\"\n");
}


function generate_make_gift_form() {


	$playername = $_POST['giftplayername'];
	$term = $_POST['giftterm'];
	$cost = 100 + how_many_gift_given_left($_SESSION[ssig() . 'playerid']);
	
	echo "<spacer type=\"block\" width=\"1\" height=\"20\">";
	echo "<form id=\"makegift\" name=\"makegift\" method=\"post\" action=\"souk.php\" >";
	
	//if (guestplayerp()=="Y") {echo " disabled ";} else { echo " ";}
	// "<input id=\"formmakegift\" type=\"submit\" name=\"formmakegift\" value=\"Offrir\"> (100 Cr)
	//	une partie avec le terme 
	//	<input id=\"giftterm\" type=\"text\" name=\"giftterm\" value=\"$term\">
	//	au joueur
	//	<input id=\"giftplayername\" type=\"text\" name=\"giftplayername\" value=\"$playername\"> 
	//	<input id=\"relation_type_gift\" type=\"hidden\" name=\"relation_type_gift\" value=\"0\"> 
	 $GLOBALS[0]=$term;
	 $GLOBALS[1]=$playername;
	 $GLOBALS[2]=$cost;
		echo get_msg('SOUK-GAME-GIFT-FORM');
	echo "	</form>";
	//Entrez un terme et un nom de joueur
	if ($_SESSION[ssig() . 'gifterror']=="noinput" ) {display_warning(get_msg('SOUK-GAME-GIFT-WARNING-NOINPUT'));}
	//Le joueur guest ne peut pas faire de cadeau - inscrivez-vous.
	if ($_SESSION[ssig() . 'gifterror']=="no guest" ) {display_warning(get_msg('SOUK-WARNING-NOGUEST'));}
	
	if ($_SESSION[ssig() . 'gifterror']=="already given" ) {display_warning(get_msg('SOUK-WARNING-CANT-GIFT'));}

	//"Ce joueur n'existe pas"
	if ($_SESSION[ssig() . 'gifterror']=="bad player" ) {display_warning(get_msg('SOUK-WARNING-BADPLAYER'));}
	//"Ce serait sympa, mais on ne s'offre pas des cadeaux a soi-m�me"
	if ($_SESSION[ssig() . 'gifterror']=="self gift" ) {display_warning(get_msg('SOUK-GAME-GIFT-WARNING-SELFGIFT'));}
	//"Ce terme n'existe pas"
	if ($_SESSION[ssig() . 'gifterror']=="bad term" ) {display_warning(get_msg('SOUK-WARNING-BADTERM'));}
	//"Vous n'avez pas assez de cr�dits"
	if ($_SESSION[ssig() . 'gifterror']=="nocash" ) {display_warning(get_msg('SOUK-WARNING-NOCASH'));}
	//"Cadeau achet� et envoy�"
	if ($_SESSION[ssig() . 'gifterror']=="noerror" ) {display_warning(get_msg('SOUK-GAME-GIFT-WARNING-NOERROR'));}
	$_SESSION[ssig() . 'gifterror'] = "";
    }


//--------------------------------

function make_graph_link() {
    echo "<spacer type=\"block\" width=\"1\" height=\"20\">";
        echo "<form id=\"gograph\" name=\"gograph\" method=\"post\" action=\"graph.php\" >";
		// <input id=\"opengiftbut\" type=\"submit\" name=\"opengiftbut\" value=\"Visualiser\">
		//�(0 Cr) le graphe.
		echo get_msg('SOUK-GRAPH-VIEW-FORM');
		echo "</form>";
    }

function make_word_link() {
    echo "<spacer type=\"block\" width=\"1\" height=\"20\">";
    echo "<a name=\"wordlink\">";
    echo "<form id=\"gotermlist\" name=\"gotermlist\" method=\"post\" action=\"souk.php#wordlink\" >";
	//    <input id=\"gotermlist\" type=\"submit\" name=\"gotermlist\" value=\"Chercher\"> (0 Cr) les mots contenant la cha�ne 
	//    <input  id=\"goterm\" type=\"text\" name=\"goterm\" value=\"$giftid\">
	$GLOBALS[0]=$giftid;
	echo get_msg('SOUK-WORD-LOOKUP-FORM');
	echo "</form>";
    
    display_word_list(trim($_POST['goterm']));
}


function display_word_list($term) {

    if ($_POST['gotermlist']!= "") {
	if ($_POST['gotermlist']== "broke") {
	display_warning(get_msg('SOUK-WARNING-NOCREDIT'));
	return;
	}
	if ($_POST['goterm']== "") {
	display_warning(get_msg('SOUK-WORDLIST-DISPLAY-WARNING-NOINPUT'));
	return;
	}

    echo "<TABLE border=\"1\" valign=\"top\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0%\">";
    echo "<TR><TH>";
    $query= "SELECT name FROM Nodes WHERE name LIKE \"%$term%\" ORDER BY name ASC LIMIT 100";
    //echo $query;
    $r =  @mysql_query($query) or die("pb in display_word_list : $query");
    $nb = mysql_num_rows($r);
    for ($i=0 ; $i<$nb ; $i++)
      {
      $name = mysql_result($r , $i , 0);
      echo  "<small>" . $name . "</small> - ";
      }

     echo "</TABLE>";
    }
}

//--------------------------------
//
function make_trial_link() {
        echo "<form id=\"gograph\" name=\"gograph\" method=\"post\" action=\"jdm-list-trial.php\" >";
        //echo "<img align=middle BORDER=0 width = \"50px\" title=\"NEW\" alt=\"NEW\" src=\"http://www.lirmm.fr/jeuxdemots/pics/new.gif\">";
		// <input id=\"opengiftbut\" type=\"submit\" name=\"opengiftbut\" value=\"Visualiser\">
		//�(0 Cr) le graphe.
		echo get_msg('SOUK-TRIAL-VIEW-FORM');
		echo "</form>";
    }
    
function make_gift_link() {
        echo "<form id=\"gograph\" name=\"gograph\" method=\"post\" action=\"jdm-list-gift.php\" >";
        // <input id=\"opengiftbut\" type=\"submit\" name=\"opengiftbut\" value=\"Visualiser\">
		//�(0 Cr) le graphe.
		echo get_msg('SOUK-GIFT-VIEW-FORM');
		echo "</form>";
    }
    
//--------------------------------
// Parainage

function generate_parainage_form() {
    echo "<form id=\"parainage_form\" name=\"parainage_form\" method=\"post\" action=\"souk.php\" >";
//	  <input id=\"parainage_submit\" type=\"submit\" name=\"parainage_submit\" value=\"Parrainer\"> (0 Cr) un joueur
//	  ayant l'adresse email suivante
//	  <input  size=\"50\" id=\"parainage_email\" type=\"text\" name=\"parainage_email\" value=\"machin@bidule.truc\">
	echo get_msg('SOUK-PARRAINAGE-FORM');
	echo "    </form>";
    process_parainage();
}


function process_parainage() {
    if ($_POST['parainage_submit']!= "") {

	$playerid = $_SESSION[ssig() . 'playerid'];
	$email= $_POST['parainage_email'];

	$res = make_parainage($playerid, $email);
	if ($res != -1) {display_warning(get_msg('SOUK-PARRAINAGE-WARNING-NOERROR'));}
	    else
	{display_warning(get_msg('SOUK-PARRAINAGE-WARNING-WRONGEMAIL'));}
    }
}


//--------------------------------
// DEFIS

function generate_defis_form() {

    echo "<spacer type=\"block\" width=\"1\" height=\"20\">";
    echo "<form id=\"parainage_form\" name=\"parainage_form\" method=\"post\" action=\"souk.php\" >";
	//  <input id=\"parainage_submit\" type=\"submit\" name=\"defis_submit\" value=\"Capturer\"> le mot
	//  <input  size=\"20\" id=\"parainage_email\" type=\"text\" name=\"defis_word\" value=\"le mot\">
	//pour un montant de 
	//<input  size=\"20\" id=\"parainage_email\" type=\"text\" name=\"defis_amount\" value=\"1000\">
	//Cr�dits.
	echo get_msg('SOUK-GENERATE-DEFIS-FORM');
    echo "</form>";

    form_make_defis();
}

function capturable_word_p($id,$playerid,$ip) {
	$query = "SELECT PG.*  FROM PendingGames as PG, `PendingGameData` as PGD 
		WHERE PGD.data = $id and PGD.dataType = 0 and PGD.gameId = PG.id 
		and PG.creator != $playerid AND PG.IP != '$ip' ORDER BY RAND() LIMIT 4;";
	$r =  @mysql_query($query) or die("bug in form_make_defis : $query");
	$n = mysql_num_rows($r);
	if ($n <= 3) {return True;} else {return False;}
}

function form_make_defis() {
    if ($_POST['defis_submit']!= "") {
	//display_warning("defis lanc�");

    $defis_factor = 5;
    		
	if (guestplayerp() == "Y") {display_warning(get_msg('SOUK-DEFIS-NOGUEST')); return;}

	$term = $_POST['defis_word'];
	$id = term_exist_in_BD_p($term);
	if ($id == 0) {$GLOBALS[0]=$term;display_warning(get_msg('SOUK-DEFIS-NOTERM')); return;}

	$value = max(get_term_value($id), 50);
	//if ($value == -1) {display_warning(get_msg('SOUK-DEFIS-NOTFREE')); return;}

	$amount = round($_POST['defis_amount']);

	if ($amount <= ($defis_factor * $value)) {$GLOBALS[1]=$value; display_warning(get_msg('SOUK-DEFIS-NOTENOUGHMONEY')); return;}

	if (get_player_credit($_SESSION[ssig() . 'playerid']) < $amount) {$GLOBALS[1]=$amount;display_warning(get_msg('SOUK-DEFIS-NOMONEY')); return;}
	
	$ip=$_SERVER['REMOTE_ADDR'];
	$playerid =$_SESSION[ssig() . 'playerid'];
	$player_name = $_SESSION[ssig() . 'login'];

	$query = "SELECT PG.*  FROM PendingGames as PG, `PendingGameData` as PGD 
		WHERE PGD.data = $id and PGD.dataType = 0 and PGD.gameId = PG.id 
		and PG.creator != $playerid AND PG.IP != '$ip' ORDER BY RAND();";
	$r =  @mysql_query($query) or die("bug in form_make_defis : $query");
    
	$n = mysql_num_rows($r);

	if ($n <= 3) {
	    display_warning(get_msg('SOUK-DEFIS-NOTNOW')); return;
	} else {
	    
	    inc_player_credit($_SESSION[ssig() . 'playerid'], -$amount);
	    //$_SESSION[ssig() . 'game_bonus'] 
	    $game_bonus = round (($amount-($defis_factor * $value))/50);

	    $game_id = mysql_result($r , 0 , 0);
	    $_SESSION[ssig() . 'chosengame'] = $game_id;

	    echo  "<small><form id=\"chooseplaydefis\" name=\"chooseplaydefis\" method=\"post\" action=\"generateGames.php\" >
		<input  id=\"chosengame\" type=\"hidden\" name=\"chosengame\" value=\"$game_id\">
		<input  id=\"chosengamebonus\" type=\"hidden\" name=\"chosengamebonus\" value=\"$game_bonus\">";
		//<font color=\"red\">Tentative de capture pr�te</font> <input id=\"chosengamesubmit\" type=\"submit\" name=\"chosengamesubmit\" value=\"Go !\">
		echo get_msg('SOUK-WORD-CAPTURE-FORM');
		echo "</form></small>";

		$GLOBALS[0] = $player_name;
		$GLOBALS[1] = $term;
	    make_event(get_msg('SOUK-WORD-CAPTURE-EVENT'));

	}
    }
}


function make_checkowner_form() {

    echo "<form id=\"gotermowner\" name=\"gotermowner\" method=\"post\" action=\"souk.php\" >";
	//<input id=\"gotermownersubmit\" type=\"submit\" name=\"gotermownersubmit\" value=\"V�rifier\"> le status du mot
	//<input  id=\"gotermowner\" type=\"text\" name=\"gotermowner\" value=\"\">
	echo get_msg('SOUK-CHECKOWNER-FORM');
	echo "</form>";

    form_display_term_owner(trim($_POST['gotermowner']));	
    
}

function form_display_term_owner($term) {
    //echo "<P>in display_wordrelation_list value :" . $_POST['gotermsubmit'] . "--" . $_POST['gotermrel'];
    if ($_POST['gotermownersubmit']!= "") {
	$term = $_POST['gotermowner'];
	$id = term_exist_in_BD_p($term);
	if ($id == 0) {display_warning(get_msg('SOUK-DEFIS-NOTERM'));}
	    else {
	    display_warning(display_term_owners($id));
	    }
    echo "<p>";
    }
}

function make_checktreasures_form() {

    echo "<form id=\"gochecktreasures\" name=\"gochecktreasures\" method=\"post\" action=\"souk.php\" >";
	//<input id=\"gochecktreasuressubmit\" type=\"submit\" name=\"gochecktreasuressubmit\" value=\"Afficher\"> les tr�sors du joueur
	//<input  id=\"gochecktreasures\" type=\"text\" name=\"gochecktreasures\" value=\"\">
	echo get_msg('SOUK-CHECK-TREASURE-FORM');
	echo "</form>";

    form_display_owner_treasures(trim($_POST['gochecktreasures']));	
    
}

function form_display_owner_treasures($term) {
    //echo "<P>in display_wordrelation_list value :" . $_POST['gotermsubmit'] . "--" . $_POST['gotermrel'];
    if ($_POST['gochecktreasuressubmit']!= "") {
	$p = $_POST['gochecktreasures'];
	if (player_exist_p($p) == 0) {
	    display_warning(get_msg('SOUK-WARNING-BADPLAYER'));}
	else {
	    $id = get_player_id($p);
	    display_warning(display_owner_terms($id));
	}
    echo "<p>";
    }
}


?>

<?php

function nb_relation_bought($relid) {

    $query = "SELECT count(*)  FROM `PlayerPermissions` WHERE `relid` = $relid";
    $r =  @mysql_query($query) or die("pb in nb_relation_bought : $query");
    $nb = mysql_result($r , 0 , 0);

    return $nb;
}

function compute_comp_price($relid) {
	$playerid = $_SESSION[ssig() . 'playerid'];
	
	$query = "SELECT price FROM RelationTypes WHERE id = '$id'";
    $r =  @mysql_query($query) or die("pb in compute_comp_price : $query");
    $price = mysql_result($r , 0 , 0);
    
    $f = player_permission_number($playerid);    
    $price = max(1000 * $f,$pricemin);
    return $price;
}

function generate_buy_relation_line($id, $n) {
    //echo "<br>generate_buy_relation_line : $n";
    $playerid = $_SESSION[ssig() . 'playerid'];

    $query = "SELECT id, name, quot, quotmin, quotmax, price FROM RelationTypes WHERE id = '$id'";
    $r =  @mysql_query($query) or die("pb in generate_buy_relation_line : $query");

    $relid = mysql_result($r , 0 , 0);
    $name = mysql_result($r ,  0 , 1);
    $gpname = get_msg($name);
    $cotation = mysql_result($r ,  0 , 2);

    $pricemin = mysql_result($r ,  0 , 5);
	$help = get_msg($name . '-HELP');

    $f = player_permission_number($playerid);
    
    $price = max(1000 * $f,$pricemin);
    $honnor = max(100 * $f,$pricemin/10);
    
    $poss = check_player_permission($playerid,$relid);

		
    echo "<TR>";
    echo "<TH>
    		<a name=\"complist$id\"><span 
    		onMouseover=\"ddrivetip('$help','yellow', 300)\";
			onMouseout=\"hideddrivetip()\">
    		<div class=\"jdm-relation-name\">$gpname" ;
    if (test_playerp()=="Y") {
	echo "(" . nb_relation_bought($relid) . ")" ;
    }
	echo "</div></span></a>";
	
    if ($poss=="Y") {echo "<TH>-";} else {echo "<TH>$honnor";}
    if ($poss=="Y") {echo "<TH>-";} else {echo "<TH>$price";}

    if ($poss=="Y") {
	echo "<TH>";
	echo get_msg('SOUK-BUYCOMP-DONE');
	} else {
	echo "<TH><form id=\"buycomp\" name=\"buycomp\" method=\"post\" action=\"souk.php#complist$id\" >
		<input id=\"buycompsubmit\" type=\"submit\" name=\"buycompsubmit\" value=\"",
		get_msg('SOUK-BUYCOMP-SUBMIT'), "\">
		<input  id=\"buycompid\" type=\"hidden\" name=\"buycompid\" value=\"$relid\">
		<input  id=\"buycompprice\" type=\"hidden\" name=\"buycompprice\" value=\"$price\">
		<input  id=\"buycomphonnor\" type=\"hidden\" name=\"buycomphonnor\" value=\"$honnor\">
		</form>";
		//$_SESSION[ssig() . 'buycompid'] = $relid;
		//$_SESSION[ssig() . 'buycompprice'] = $price;
		//$_SESSION[ssig() . 'buycomphonnor'] = $honnor;
	}
	
    echo "<TH>" .  round($cotation/1000, 2);

    if (check_player_permission($playerid, $relid)=="Y")  {$tag="";} else {$tag="disabled";}
    if (player_permission_number($playerid) <= 1) {$tag="disabled";}

    $prob=round(compute_player_permission_pc($playerid, $relid),2);
    echo "<TH>$prob%";
	
    echo "<TH>";
    echo "<form id=\"buyprobrel\" name=\"buyprobrel\" method=\"post\" action=\"souk.php#complist$id\" >
		<input $tag id=\"buyprobsubmit\" type=\"submit\" name=\"buyprobsubmit+\" value=\"+\">  
		<input $tag id=\"buyprobsubmit\" type=\"submit\" name=\"buyprobsubmit-\" value=\"-\"> (50 Cr)
		<input  id=\"buyprobrelid\" type=\"hidden\" name=\"buyprobrelid\" value=\"$relid\">";
    echo "<input $tag id=\"buyprobsubmit\" type=\"submit\" name=\"unik\" value=\"unik\"> (5k Cr)";
    echo "</form>";
    
}

function get_relation_name($relid) {
    $query = "SELECT name FROM `RelationTypes`  WHERE id= '$relid' ;";
    $r =  @mysql_query($query) or die("bug in get_relation_name : $query");
    $name = mysql_result($r , 0 , 0);
    return $name;
}




function make_SOUK_buy_trial_token_form() {
	//
	$msg = "";
	$playerid = $_SESSION[ssig() . 'playerid'];
	if (($playerid != 0) && ($_POST['submit_buy_token'] <> '')) {
		$nb = get_player_nb_trial_tokens($playerid);
		$cost = 1000 * ($nb + 1);
		if (get_player_credit($playerid) >= $cost) {		
			inc_player_credit($playerid, -$cost);
			inc_player_trial_tokens($playerid, 1);
			$msg = get_msg("SOUK-BUY-TRIAL-TOKEN-CONFIRM");
			$GLOBALS[0]=get_player_name($playerid);
    		make_event(get_msg('BUY-TRIAL-TOKEN-EVENT'));
		} else {
			$GLOBALS[0]=$cost;
			$msg = get_msg("SOUK-BUY-TRIAL-TOKEN-NOCREDIT");
		}
	}
	
	if ($playerid == 0) {$tag = "disabled";} else {$tag = "";}
	$nb = get_player_nb_trial_tokens($_SESSION[ssig() . 'playerid']);
	$cost = 1000 * ($nb + 1);
	echo "<form id=\"form_souk_buy_token\" name=\"form_souk_buy_token\" method=\"post\" action=\"souk.php\" >";
	$GLOBALS[0] = $nb;
	$GLOBALS[1] = $tag;
	$GLOBALS[2] = $cost;
	echo get_msg("SOUK-BUY-TRIAL-TOKEN-STATUS");
	
	echo "</form>";
	display_warning($msg);
}


function make_wordrelation_form() {
    if (test_playerp()=="Y") {
	echo "<form id=\"gotermrel\" name=\"gotermrel\" method=\"post\" action=\"souk.php\" >";
	echo get_msg('SOUK-WORD-RELATION-FORM');   
	echo "    </form>";
    
	display_wordrelation_list(trim($_POST['goterm']));	
    }
}


function display_wordrelation_list($term) {
    //echo "<P>in display_wordrelation_list value :" . $_POST['gotermsubmit'] . "--" . $_POST['gotermrel'];
    if ($_POST['gotermsubmit']!= "") {
	$term = $_POST['gotermrel'];
	$id = term_exist_in_BD_p($term);
	if ($id == 0) {display_warning(get_msg('SOUK-DEFIS-NOTERM'));}
	    else {
	    echo "<br>'$term'";

	    $query= "SELECT depart.name, RelationTypes.name, arrivee.name,
		    Relations.w FROM `Nodes` AS depart, `Relations` , `RelationTypes` , `Nodes` AS arrivee
		    WHERE depart.id = $id
		    AND depart.id = Relations.node1
		    AND Relations.type = RelationTypes.id
		    AND arrivee.id = Relations.node2 ORDER BY Relations.w DESC";
	    //echo "<br>$query<BR>";
	    echo "<br>relations ==><UL>";
	    $r =  @mysql_query($query) or die("pb in display_wordrelation_list : $query");
	    $nb = mysql_num_rows($r);
	    for ($i=0 ; $i<$nb ; $i++) {
			$n1 = mysql_result($r , $i , 0);
			$rel = mysql_result($r , $i , 1);
			$n2 = mysql_result($r , $i , 2);
			$w = mysql_result($r , $i , 3);
	      echo "<li>" . $n1 , " <tt>---" , $rel , ":", $w, "--></tt>  " , $n2 , "<BR>";
	      }
	    echo"</UL>";

	    $query= "SELECT depart.name, RelationTypes.name, arrivee.name,
		    Relations.w FROM `Nodes` AS depart, `Relations` , `RelationTypes` , `Nodes` AS arrivee
		    WHERE arrivee.id = $id
		    AND depart.id = Relations.node1
		    AND Relations.type = RelationTypes.id
		    AND arrivee.id = Relations.node2 ORDER BY Relations.w DESC";
	    //echo "<br>$query<BR>";
	    echo "<br>relations <== <UL>";
	    $r =  @mysql_query($query) or die("pb in display_wordrelation_list : $query");
	    $nb = mysql_num_rows($r);
	    for ($i=0 ; $i<$nb ; $i++) {
			$n1 = mysql_result($r , $i , 0);
			$rel = mysql_result($r , $i , 1);
			$n2 = mysql_result($r , $i , 2);
			$w = mysql_result($r , $i , 3);
	      echo "<li>" . $n1 , " <tt>---" , $rel , ":", $w, "--></tt>  " , $n2 , "<BR>";
	      }
	    echo"</UL>";
	   
	    }
    }
}


function make_gamelist_form() {
    if (test_playerp()=="Y") {
	echo  "<P>";
	echo "<form id=\"gamelist\" name=\"gamelist\" method=\"post\" action=\"souk.php\" >";
	//    <input id=\"gamelist_submit\" type=\"submit\" name=\"gamelist_submit\" value=\"parties en cours\"> (DEBUG)
	echo get_msg('SOUK-GAMELIST-FORM');
	echo "    </form>";
    
	generate_game_list();	
    }
}


function generate_game_list() {
    if ($_POST['gamelist_submit']!= "") {
	echo  "<blockquote>";
	

	$query = "SELECT * FROM PendingGames ORDER by creationdate LIMIT 3000 ;";
	$r =  @mysql_query($query) or die("pb in generate_game_list : $query");
	$nb = mysql_num_rows($r);
	for ($i=0 ; $i<$nb ; $i++)
	    {
	    $id = mysql_result($r , $i , 0);
	    $type = mysql_result($r , $i , 1);
	    $creator = mysql_result($r , $i , 2);
	    $creationdate = mysql_result($r , $i , 3);
	    $token = mysql_result($r , $i , 4);
	    $ip = mysql_result($r , $i , 5);

	    $creatorname = get_player_name($creator);

	    $rel = mysql_result($r , $i , 6);

	    if ($token > 10) {
	    	$query = "UPDATE `PendingGames` SET token = 10 WHERE id= '$id';" ;
			$r1 =  @mysql_query($query) or die("pb in normlization");
	    }

	    $query = "SELECT N.id, N.name, N.level, PGD2.data FROM `PendingGameData` AS PGD, Nodes AS N,
		`PendingGameData` AS PGD2
		    WHERE 	     PGD.gameid = $id AND PGD.dataType = '0' AND N.id = PGD.data
			AND  PGD2.gameid = $id AND PGD2.dataType = '1' " ;

	    $r2 =  @mysql_query($query) or die ("bug in generateAnswerGame 2 : $query");

	    $entryid = mysql_result($r2 , 0 , 0);
	    $entry = mysql_result($r2 , 0 , 1);
	    $entrylevel = mysql_result($r2 , 0 , 2);
	    $gamerelationtype = mysql_result($r2 , 0 , 3);

	    $relname = get_relation_name($rel);
    
	    echo  "<P><small><form id=\"chooseplay$i\" name=\"chooseplay$i\" method=\"post\" action=\"generateGames.php\" >";
		echo "$i: ";
		echo get_msg('SOUK-CHOSENGAME-FORM');
		echo "<input  id=\"chosengame\" type=\"hidden\" name=\"chosengame\" value=\"$id\">
		$id $entry ($entryid $entrylevel) $type $relname($rel) token=$token $creatorname($creator) $creationdate $ip";
		echo "</form></small>";

    
	}
	echo  "</blockquote>";

    }}

function make_bd_stats_form() {
    if (test_playerp()=="Y") {
	echo  "<P>";
	echo "<form id=\"bd_stats\" name=\"bd_stats\" method=\"post\" action=\"souk.php\" >";
	//    <input id=\"bd_stats_submit\" type=\"submit\" name=\"bd_stats_submit\" value=\"Statistiques\"> (DEBUG)
	 echo get_msg('SOUK-STATISTICS-FORM');
	 echo "   </form>";
    
	generate_bd_stats();	
    }
}

function generate_bd_stats() {
    if ((test_playerp()=="Y") AND ($_POST['bd_stats_submit']!= "")) {
	echo "<P>Statistiques relations: ";
	$query = "SELECT id, name, gpname FROM RelationTypes;";
	$r =  @mysql_query($query) or die("pb in generate_bd_stats 1: $query");
	$nb = mysql_num_rows($r);
	for ($i=0 ; $i<$nb ; $i++) {
	    $reltype = mysql_result($r , $i , 0);
	    $relname = mysql_result($r , $i , 1);
	    $relgpname = mysql_result($r , $i , 2);
	    $query = "SELECT COUNT(*) FROM Relations WHERE type = $reltype;";
	    $r2 =  @mysql_query($query) or die("pb in generate_bd_stats 2 : $query");
	    $count = mysql_result($r2 , 0 , 0);
	    $GLOBALS[0] = $count;
	    $GLOBALS[1] = $relname;
	    $GLOBALS[2] = $reltype;
	    $GLOBALS[3] = $relgpname;
	    echo "<br>" . get_msg("SOUK-RELATIONS-OCCURS");
	}

	echo "<P>Statistiques noeuds: ";
	$query = "SELECT id, name FROM NodeTypes;";
	$r =  @mysql_query($query) or die("pb in generate_bd_stats 3 : $query");
	$nb = mysql_num_rows($r);
	for ($i=0 ; $i<$nb ; $i++) {
	    $nodetype = mysql_result($r , $i , 0);
	    $nodename = mysql_result($r , $i , 1);
	    $query = "SELECT COUNT(*) FROM Nodes WHERE type = $nodetype;";
	    $r2 =  @mysql_query($query) or die("pb in generate_bd_stats 4 : $query");
	    $count = mysql_result($r2 , 0 , 0);
	    $GLOBALS[0] = $count;
	    $GLOBALS[1] = $nodename;
	    $GLOBALS[2] = $nodetype;
	    echo "<br>"  . get_msg("SOUK-NODES-OCCURS");
	}
	
	echo "<P>100 most frequents terms<UL>";
	$query= "SELECT name, w FROM `Nodes`  ORDER BY `Nodes`.`w`  DESC limit 100";
	$r =  @mysql_query($query) or die("pb in display_wordrelation_list : $query");
	$nb = mysql_num_rows($r);
	for ($i=0 ; $i<$nb ; $i++) {
		$name = mysql_result($r , $i , 0);
		$w = mysql_result($r , $i , 1);
	    echo "<br>" . $name , " -- " , $w;
	}
	}
}
?>

<?php include 'tip-javascript.php' ; ?>
<div class="jdm-level1-block">
	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
    <?php echo get_msg('SOUK-PROMPT'); ?>
    </div>
	</div>

    <div class="jdm-login-block">
    <?php  loginblock(); ?>
    </div>
</div>
<?php
echo "\n<img width=101% STYLE=\"opacity:0.4;height:900px;z-index:-1;position:absolute;top:-400px;left:0px;\"  src=\"pics/circle.png\">";
//echo "\n<img STYLE=\"opacity:0.4;width:1000px;height:200px;z-index:-1;position:absolute;top:-50px;left:-500px;\"  src=\"pics/circle.png\">";
?>
<div class="jdm-level2-block">
<TABLE border="0" width="100%" cellspacing="" cellpadding="5"
	style="opacity:0.85;"
	summary="jeuxdemots">

<TR><TH valign="top" width="200pts" >
    <TH align="left" bgcolor="white">
	<?php 
	display_ask_twit_form();
	display_ask_thema_form();
	?>
	<TH valign="top" width="200">
	
<TR><TH valign="top" width="200pts">
    <TH align="left" bgcolor="#FFFFCC" >
    <div class="jdm-souk-section" style="opacity:1;"><a name="complist">
    <?php 
    	echo get_msg('SOUK-SUBTITLE-COMP');
		handle_toggle('souk_comp_toggle', 'souk.php');
  		?>
    </div></a>
    <?php 
    if ($_SESSION[ssig() . 'souk_comp_toggle']) {
    	
    echo "<P>";
   	echo "<TABLE border=\"1\" valign=\"middle\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0%\>";
   	echo "<TR bgcolor=\"#EEF6F8\">";
    
	echo "<TH>"; echo get_msg('SOUK-COMP-ITEM'); 
	echo "<TH>"; echo get_msg('SOUK-COMP-HONNOR-MIN');
	echo "<TH>"; echo get_msg('SOUK-COMP-CREDIT'); 
	echo "<TH>"; echo get_msg('SOUK-COMP-STATUS'); 
	echo "<TH>"; echo get_msg('SOUK-COMP-QUOT');
	echo "<TH>"; echo get_msg('SOUK-COMP-SELECPC'); 
	echo "<TH>"; echo get_msg('SOUK-COMP-ADJUSTPC');
    
	$l=active_relations();
	$nb = count($l);
	for ($i=0 ; $i<$nb ; $i++) {
	    generate_buy_relation_line($l[$i], $nb);
	}
	echo "</TABLE>";
	echo "<P>";
   
    echo "<center><form id=\"equiprob\" name=\"buyprobrel\" method=\"post\" action=\"souk.php#complist$id\" >
    	<input $tag id=\"equiprob\" type=\"submit\" name=\"equiprob\" value=\"",get_msg('SOUK-COMP-EQUIPROBABILIZE'),"\">
		",get_msg('SOUK-COMP-ALL-COMPETENCES');
    echo "</form></center>";
    
    
    if ($_SESSION[ssig() . 'proberror']=="noerror" ) {display_warning(get_msg('SOUK-PROB-WARNING-NOERROR'));}
    if ($_SESSION[ssig() . 'proberror']=="nocash" ) {display_warning(get_msg('SOUK-WARNING-NOCASH'));}
    if ($_SESSION[ssig() . 'proberror']=="noguest" ) {display_warning(get_msg('SOUK-WARNING-NOGUEST'));}
    $_SESSION[ssig() . 'proberror'] = "";
    if ($_SESSION[ssig() . 'comperror']=="noerror" ) {display_warning(get_msg('SOUK-COMP-WARNING-NOERROR'));}
    if ($_SESSION[ssig() . 'comperror']=="nohonnor" ) {display_warning(get_msg('SOUK-WARNING-NOHONNOR'));}
    if ($_SESSION[ssig() . 'comperror']=="nocash" ) {display_warning(get_msg('SOUK-WARNING-NOCREDIT'));}
    if ($_SESSION[ssig() . 'comperror']=="noguest" ) {display_warning(get_msg('SOUK-WARNING-NOGUEST'));}
    $_SESSION[ssig() . 'comperror'] = "";
    
    }
    ?>
    <TH valign="top" width="200">
	


<TR><TH >
    <TH align="left" bgcolor="#EEF6F8">
    <div class="jdm-souk-section">
    <?php 
    	echo get_msg('SOUK-PRESENTS'); 
    	handle_toggle('souk_gift_toggle', 'souk.php');
    ?>
    </div>
    <blockquote>
    <?php 
    if ($_SESSION[ssig() . 'souk_gift_toggle']) {
		generate_make_gift_form();
		make_gift_link();
    }
	?>
	</blockquote>
	
	<div class="jdm-souk-section">
	<?php 
		echo get_msg('SOUK-SUBTITLE-TREAS');
		handle_toggle('souk_treas_toggle', 'souk.php');
	?>
	 </div>
	 <blockquote>
	<?php 
	if ($_SESSION[ssig() . 'souk_treas_toggle']) {
		generate_defis_form();
    	make_checkowner_form();
		make_checktreasures_form();
	}
    ?>
    </blockquote>
    
    <a name="artefacts"></a>
    <div class="jdm-souk-section">
    <?php 
    	echo get_msg("SOUK-ARTEFACTS-TITLE"); 
    	handle_toggle('souk_art_toggle', 'souk.php');
    ?>
    </div>
    <blockquote>
    <?php 
    if ($_SESSION[ssig() . 'souk_art_toggle']) {
    	display_artefact_info(0) ;
   		display_artefact_info(1) ;
    	display_artefact_info(2) ;
    	display_artefact_info(3) ;
    	display_artefact_info(4) ;
    }
    ?>
	</blockquote>
    <TH >


<TR><TH align="left" >
    <TH align="left" bgcolor="#DCFFFF">
    <?php
function souk_produce_loterie_play_form () {
	$playerid = $_SESSION[ssig() . 'playerid'];
	$cost = get_loterie_price($playerid);
	echo "\n<form  method=\"post\" action=\"GEN-loterie.php\" >";
	$GLOBALS[0] = $cost;
 	echo get_msg("SOUK-PLAY-LOTTERY");
 	echo "\n</form>";
}
function souk_produce_TGEN_easy_play_form () {
	echo "\n<form  method=\"post\" action=\"TGEN.php\" >";
 	echo get_msg("SOUK-CHOSE-EASY");	
 	echo " <img src=\"pics/JDM-bonus-dot.gif\">";
    echo "\n</form>";
}
function souk_produce_TGEN_hard_play_form () {
	echo "\n<form  method=\"post\" action=\"TGEN_hard.php\" >";
 	echo get_msg("SOUK-CHOSE-HARD");		
    echo "\n</form>";
}

function souk_produce_THEMA_play_form () {
	echo "\n<form  method=\"post\" action=\"TGEN-domain.php\" >";
	echo "<img align=middle BORDER=0 width=\"50px\" title=\"NEW\" alt=\"NEW\" src=\"",get_msg('SOUK-NEW-IMAGE'),"\">";
	echo get_msg("SOUK-CHOSE-THEME");
    echo "\n</form>";
}

function souk_produce_QGEN_play_form () {
	echo "\n<form  method=\"post\" action=\"QGEN.php\" >";
 	echo get_msg("SOUK-CRAZY-QUESTION");
    echo "\n</form>";
}
    
    ?>
    <div class="jdm-souk-section">
    <?php 
    	echo get_msg('SOUK-SMALL-GAMES');
    	handle_toggle('souk_loterie_toggle', 'souk.php');
    ?>
    </div>
     <blockquote>
    <?php
    if ($_SESSION[ssig() . 'souk_loterie_toggle']) {
		souk_produce_loterie_play_form ();
		souk_produce_TGEN_easy_play_form ();
		souk_produce_TGEN_hard_play_form ();
		souk_produce_QGEN_play_form();
		
		souk_produce_THEMA_play_form ();
    }
    ?>
    
    </blockquote>
    <TH>

<TR><TH  valign="top" align="center" width="200pts">
    <TH bgcolor="#FFFFCC" align="left">
    <div class="jdm-souk-section">
    <?php 
    	echo get_msg('SOUK-TRIALS'); 
    	handle_toggle('souk_trial_toggle', 'souk.php');
    ?>
    </div>
    <blockquote>
    <?php
     if ($_SESSION[ssig() . 'souk_trial_toggle']) {
		make_trial_link();
		make_SOUK_buy_trial_token_form();
     }
    ?>
    </blockquote>
    <TH >
    
<TR><TH align="left">
    <TH align="left" bgcolor="#EEF6F8">
    <div class="jdm-souk-section">
    <?php 
    	echo get_msg('SOUK-DUELS'); 
    	handle_toggle('souk_duel_toggle', 'souk.php');
    ?>
    </div>
    <blockquote>
    <?php
    if ($_SESSION[ssig() . 'souk_duel_toggle']) {		
		display_ask_duel_form();
    }
    ?>
    </blockquote>
    <TH align="left" >
    
<TR><TH align="left" >
    <TH align="left" bgcolor="#DCFFFF">
    <div class="jdm-souk-section">
    <?php 
    	echo get_msg('SOUK-VARIOUS');
    	handle_toggle('souk_various_toggle', 'souk.php');
    ?>
    </div>
    <blockquote>
    <?php
    if ($_SESSION[ssig() . 'souk_various_toggle']) {
	//	make_word_link();
	
		generate_hype_words_form();
	
		//make_graph_link();
	
		generate_cagnote_form();	
    }
    ?>
    </blockquote>
    <TH align="left">

<!--
<TR><TH valign="top" align="center" width="200pts">
    <TH bgcolor="#EEF6F8" align="left">
    <?php
//	generate_parainage_form();
    ?>
    <TH>
    -->
    

<TR><TH valign="top" width="200pts">
    <TH align="left">
    <?php
	    make_wordrelation_form();
	    make_gamelist_form();
	    make_bd_stats_form();
    ?>
    <TH>
</TABLE>
</div>

<div class="jdm-playas-block-1">
<?php produceplayasform(); ?>
</div>

<?php playerinfoblock($_SESSION[ssig() . 'playerid']) ?>
<?php 
    bottomblock();
    closeconnexion();
?>

  </body>
</html>
