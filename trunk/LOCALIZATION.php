<?php session_start();?>
<?php include_once 'misc_functions.php'; ?>
<?php
    openconnexion();

    $_SESSION[ssig() . 'state']=0;
?>
<html>
 <head>
    <title>JeuxDeMots: How to Localize JeuxDeMots</title>
    <?php header_page_encoding(); ?>
  </head>
<?php include 'HTML-body.html' ; ?>
<?php topblock(); ?>

<TABLE	border="0"
	width="100%"
	cellspacing="0" cellpadding="0%"
	summary="en-t�te" bgcolor="white"
	>


<TR><TH bgcolor="black" align="center" height="40" width="200"> <?php jdmLogo();?>
    <TH bgcolor="#D3E5F8" colspan="1" align="left">
	
	<div class="jdm-prompt">
	Help for localizing JeuxDeMots
	</div>


    <TH bgcolor="#D3E5F8"  width="150">
    <div class="jdm-login-block">
    <?php  loginblock(); ?>
    </div>

<TR><TH>
    <TH>    <spacer type="block" width="0" height="30"> 
    <TH>    <?php playerinfoblock($_SESSION[ssig() . 'playerid']) ?>
</TABLE>

<TABLE	border="0"
	width="100%"
	cellspacing="0" cellpadding="0%"
	summary="aide" bgcolor="white"
	>
<TR><TH width="200">
    <TH>
    <TH width="150">

<TR valign= "top">
    <TH align="right"><h2>General<BR>principles</h2>
    <TH align="left"> <blockquote>
	    The main files for localization are <tt>PARAM.php</tt>, <tt>LOC-??.php</tt> and <tt>HELP-??.php</tt>
	   <p>
	   PARAM.php contains general parameter for JeuxDeMots. The LOC file contains the differents messages and strings
	   of JeuxDeMots to be translated in a given language. The HELP file contains the user help, also to be
	   translated.
	   <p>
	   The mecanism used by JeuxDeMots is to refer always to a Country Code (CC) plus a string name
	   to access to the proper message. The CC is set in the PARAM file.
	   <p>
	   
	   WARNING: two differents should not be confused. Playing with JeuxDeMots with a given lexicon (of french, english, thai, etc)
	   and with a user interface in a given language. These are two different things.
	   We assume here that because of the nature of the game, the  interface language and
	   the lexicon (database) language are always the same.
	   Technically speaking the two aspects are not whatsoever related.
	   
	    </blockquote>
	    <p>
    <TH>

<TR valign= "top">
    <TH align="right"><h2>PARAM file</h2>
    <TH align="left"> <blockquote>
	    PARAM.php contains the general parameters :
	    
	    <blockquote><tt>
	    $_SESSION[ssig() . 'LANG'] the country code (aka CC)<br>
	   	$_SESSION[ssig() . 'CONNEXION-URL'] = the url for the mysdql server (like "mysql.lirmm.fr")<br>
		$_SESSION[ssig() . 'CONNEXION-USER'] the loging of the owner of the database<br>
		$_SESSION[ssig() . 'CONNEXION-PSWD'] the password for opening the connexion of the database<br>
		$_SESSION[ssig() . 'CONNEXION-DBNAME'] the database name<br>
		$_SESSION[ssig() . 'DEFAULT-CSS-URL'] the url of the default CSS, if none is given by the player<br>
		$_SESSION[ssig() . 'CSS-URL'] the url of a given CSS proposed to the user (usually equal to the default CSS)<br>
</tt></blockquote>
    
	    </blockquote>
	    <p>
    <TH>
    
    <TR valign= "top">
    <TH align="right"><h2>LOC files</h2>
    <TH align="left"> <blockquote>
	    The LOCalization files contains all messages (strings with or without parameters)
	    in a given language. This is the file that should be translated to have a user interface
	    localized in a given language.<p>
	    
	    The file name should be of the form "LOC-<var>CC</var>.php. For example, for French, we should the CC as being "FR", 
	    thus the LOC file for French has the name "LOC-FR.php".<P>
	    
	    The contents of this file is a sequence of php globals containing strings like:
	   <blockquote><tt>
	    $GLOBALS['EN-GENGAME-TITLE'] = "JeuxDeMots: Association Game";<br>
		$GLOBALS['EN-GENGAME-GAMETYPE'] = "Association Game";<br>
		...
  		</tt></blockquote>
  		
  		All string names (like <tt>'EN-GENGAME-TITLE'</tt>) should start with the CC followed by a "-"
  		(the <tt>'EN-'</tt> part for our example with english). If the string is missing, an error message will be displayed
  		when the html page is generated.
  		
  		
	    </blockquote>
	    <p>
<?php
// code for testing if a localization file is complete and not missing a message.

/*
	include_once('LOC-FR.php');
	include_once('LOC-EN.php');
	include_once('LOC-JP.php');
	$LOCS = array();
	foreach ($GLOBALS as $key => $value) {
		if (!empty($key) && strlen($key)> 3 && strncmp($key, "FR-", 3) == 0) {
			$cle = substr($key,3);
			$LOCS{$cle} = 'FR-';
		}
	}
	foreach ($GLOBALS as $key => $value) {
		if (!empty($key) && strlen($key)> 3 && strncmp($key, "EN-", 3) == 0) {
			$cle = substr($key,3);
			$LOCS{$cle} .= 'EN-';
		}
	}
	foreach ($GLOBALS as $key => $value) {
		if (!empty($key) && strlen($key)> 3 && strncmp($key, "JP-", 3) == 0) {
			$cle = substr($key,3);
			$LOCS{$cle} .= 'JP-';
		}
	}
	foreach ($LOCS as $key => $value) {
		if ($value !== 'FR-EN-JP-') {
			echo '<p>Missing localization: ', $key, ' in languages ', $value, ' only! </p>';
		}
	}
*/
?>
    <TH>
    
    <TR valign= "top">
    <TH align="right"><h2>HELP files</h2>
    <TH align="left"> <blockquote>
	    The HELP files contains the help for the user.<p>
	    
	    The file name should be of the form "HELP-<var>CC</var>.php. For example, for French, we should the CC as being "FR", 
	    thus the HELP file for French has the name 'HELP-FR.php".<P>
	    
  		
  		
	    </blockquote>
	    <p>
     <TH>
    
    <TR valign= "top">
    <TH align="right"><h2>CHARTE files</h2>
    <TH align="left"> <blockquote>
	    The CHARTE files contains the charter of the game and produced data.<p>
	    
	    The file name should be of the form "CHARTE-<var>CC</var>.php. For example, for French, we should the CC as being "FR", 
	    thus the CHARTE file for French has the name 'CHARTE-FR.php".<P>
	    
  		
  		
	    </blockquote>
	    <p>
    <TH>
    
    <TR valign= "top">
    <TH align="right"><h2>OTHER Strings</h2>
    <TH align="left"> <blockquote>
	    Some string are contained in the datebase. This is the case for the relation
	    names and descriptions. The localization should be done in the database.
	    This mightertainly change in the future for having someting more coherent.
  		
	    </blockquote>
	    <p>
    <TH>

</TABLE>
<P>


<?php 
    bottomblock();
    closeconnexion();
?>

  </body>
</html>
