<? session_start();?>
<? include_once 'misc_functions.php'; ?>
<?
    openconnexion();

    $_SESSION['state']=0;
?>
<html>
�<head>
����<title>JeuxDeMots : aide</title>
    <? header_page_encoding(); ?>
��</head>
<? include 'HTML-body.html' ; ?>
<? topblock(); ?>

<TABLE	border="0"
	width="100%"
	cellspacing="0" cellpadding="0%"
	summary="en-t�te" bgcolor="white"
	>
<TR><TH bgcolor="black" align="center" height="40" width="200"> <?jdmLogo();?>
    <TH bgcolor="#D3E5F8" colspan="1" align="left">

	<div class="jdm-prompt">
	Aide de JeuxDeMots
	</div>

    <TH bgcolor="#D3E5F8"  width="150">
    <div class="jdm-login-block">
    <?  loginblock(); ?>
    </div>

<TR><TH>
    <TH>    <spacer type="block" width="0" height="30"> 
    <TH>    <? playerinfoblock($_SESSION['playerid']) ?>
</TABLE>

<TABLE	border="0"
	width="100%"
	cellspacing="0" cellpadding="0%"
	summary="aide" bgcolor="white"
	>
<TR><TH width="200">
    <TH>
    <TH width="150">

<TR valign= "top">
    <TH align="right"><h2>Principe<BR>g�n�ral</h2>
    <TH align="left"> <blockquote>
	    Lorsqu'une partie d�marre une consigne concernant un type de comp�tence (synonymes, contraires, etc) est affich�e. 
	    Plus vous aurez de r�ponses en commun avec un autre joueur, meilleurs seront vos gains. Les r�ponses sp�cifiques et peu rencontr�es rapportent plus 
	    que des r�ponses g�n�rales.
	    <br><br>
	    Pour pouvoir jouer, vous devez avoir pris connaissance et accept� la charte de JeuxDeMots.
	    </blockquote>
	<spacer type="block" width="1" height="30">
    <TH>

<TR valign= "top">
    <TH align="right"><h2>Histoire<br>d'honneur</h2>
    <TH align="left"><blockquote>
    L'honneur repr�sente votre performance. Lors d'une partie vous pouvez gagner ou perdre de l'honneur. Ne pas r�pondre n'est pas tr�s honorable.
    L'honneur � tendance � s'effriter avec le temps.
	    </blockquote>

    <spacer type="block" width="1" height="30">
    <TH>

<TR valign= "top">
    <TH align="right"><h2>Histoire<br>d'argent</h2>
    <TH align="left"><blockquote>
	 L'argent virtuel de JeuxDeMots sont les cr�dits. Ceux-ci sont gagn�s lors des parties, en fonction de la pertinence de vos r�ponses. 
	    Les cr�dits vous servent � plusieurs choses, notemment � acheter du temps lors d'une partie, mais aussi � d�bloquer l'acc�s � certaines comp�tences.
	</blockquote>
	<spacer type="block" width="1" height="30">
    <TH>

<TR valign= "top">
    <TH align="right"><h2>Histoire<br>de comp�tences</h2>
    <TH align="left"><blockquote>
	 Un nouveau joueur n'a acc�s au d�part qu'� un seul type de consigne (on parlera de comp�tence). 
	    Dans le souk, vous pouvez acheter le droit de joueur avec d'autres comp�tences. Sachez qu'une comp�tence � une cotation qui influence 
	(� la baisse ou � la hausse) vos gains. Plus une comp�tence est jou�, plus cette cotation baisse. 
	    Les comp�tences rares et difficiles valent donc plus que les autres. Toujours dans le souk, vous pouvez ajuster la probabilit�
	    qu'une partie en cr�ation corresponde � une de vos comp�tences. Ainsi, il vous est possible de joueur pr�f�rentiellement avec les comp�tences
	    qui vous rapportent le plus ou que vous pr�f�rez. Vous n'influencez pas la proportion d'apparition des parties en atente.
	</blockquote>
	<spacer type="block" width="1" height="30">
    <TH>

<TR valign= "top">
    <TH align="right"><h2>Histoire<br>de niveaux</h2>
    <TH align="left"><blockquote>
	 Les joueurs ont un niveau d'expertise et les mots un niveau de difficult�. 
	Plus le niveau du joueur est �lev�, plus il a de chance d'�tre confront� � des mots difficiles. 
	Faire des points face � un mot difficile fait augmenter son propre niveau, mais ne pas faire de points face � un mot de faible niveau
	fait baisser son propre niveau. 
	Le niveau de difficult� des mots s'ajuste selon l'activit� des joueurs.
	</blockquote>
	<spacer type="block" width="1" height="30">
    <TH>


<TR valign= "top">
    <TH align="right"><h2>Histoire<br>de r�ponses</h2>
    <TH align="left"><blockquote>
	Le bouton "passer" permet de finir la partie avant l'�ch�ance de temps. Si vous pensez qu'il n'y a pas du tout de r�ponse � la consigne donn�e
	vous pouvez mettre *** comme r�ponse. Si votre partenaire pense comme vous, vous aurez des gains. Attention, une telle r�ponse termine imm�diatement la partie 
	en cours et annule les r�ponses que vous auriez pu donner avant. <BR>
	Les mots tabous ne sont pas interdits, mais ils vous rapporteront bien peu... t�chez d'en trouver d'autres.
	</blockquote>
	<spacer type="block" width="1" height="30">
    <TH>

<TR valign= "top">
    <TH align="right"><h2>Foire<br>aux questions</h2>
    <TH align="left"><blockquote>
	Q: Apr�s 10 ou 11 propositions le chronom�tre tombe spontan�ment � "termin�". Normal ?
	<br>R : oui, c'est normal, le nombre de propositions est "plus ou moins" limit� a 10.
	<P>
	Q: J'ai ajust� une de mes comp�tences � 100% et pourtant j'ai parfois des consignes correspondant � d'autres comp�tences. Normal ?
	<br>R : oui, c'est normal, l'ajustement ne concerne que les parties en cr�ation, et pas celles � finir (o� on obtient le r�sultat imm�diatement).
	</blockquote>
	<spacer type="block" width="1" height="30">
    <TH>

<TR><TH>
    <TH align="left">
    <HR width=100%>
    <spacer type="block" width="1" height="30">
    <? produceplayasform(); ?>

</TABLE>
<P>


<? 
    bottomblock();
    closeconnexion();
?>

��</body>
</html>