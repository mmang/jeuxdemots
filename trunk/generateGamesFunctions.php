<?php include_once 'misc_functions.php' ; ?>
<?php include_once 'term_selection_functions.php' ; ?>

<?php


function select_rand_in_tab_with_weight($r) {
    $n = mysql_num_rows($r);
   // debugecho("select_rand_in_tab_with_weight : n = $n");

    if ($n <= 0) {return -1;}

    // on calcul la zone pour l'�tendu du rand
    //
    $range = 0;
    for ($i=0 ; $i<$n ; $i++) {
	$poids = max(0,mysql_result($r , $i , 1));
	$range = $range + $poids;
    }
    

    $rand = rand(0,$range-1);

   // debugecho("select_rand_in_tab_with_weight : range = $range RAND = $rand");

    $sum= 0;
    for ($i=0 ; $i<$n ; $i++) {
	$poids = max(0,mysql_result($r , $i , 1));
	$sum = $sum + $poids;
	if ($rand < $sum) {return $i;}
    }
}


function make_free_relation_game() {
    select_instructions();
    select_term_general();
}


function make_SYN_game() {
    select_instructions();
    
    $rand = rand(0,100);
    if ($rand > 50) {
	select_potential_word();
    } else {       
	select_random_word_N_V_ADJ_ADV();
    }
}

function make_DOMAIN_game() {
    select_instructions();	
    

    $rand = rand(0,100);
    if ($rand > 50) {
	select_potential_word();
    } else {       
	select_term_general();
    }
}

function make_ANTO_game() {
    select_instructions();          

    $rand = rand(0,100);
    if ($rand > 70) {
	select_potential_word();
    } else {       
	select_random_word_ADJ_ADV();
    }
}


function make_GENER_game() {
    select_instructions();   
    $rand = rand(0,100);
    if ($rand > 70) {
	select_potential_word();
    } else {
	select_easy_word_N();
    }
}

function make_HYPO_game() {
    select_instructions();                 
     $rand = rand(0,100);
    if ($rand > 70) {
	select_potential_word();
    } else {
	select_easy_word_N();
    }
}

function make_PLACE_game() {
    select_instructions();                 
     $rand = rand(0,100);
    if ($rand > 90) {
	select_potential_word();
    } else {
	select_easy_word_N();
    }
}

function make_PARTOF_game() {
    select_instructions();                 
     $rand = rand(0,100);
    if ($rand > 10) {
	select_potential_word();
    } else {
	select_easy_word_N();
    }
}

function make_HOLO_game() {
    select_instructions();                 
     $rand = rand(0,100);
    if ($rand > 10) {
	select_potential_word();
    } else {
	select_easy_word_N();
    }
}

function make_LOCU_game() {
   select_instructions();                 
   select_easy_word_N();
}

function make_CARAC_game() {
    select_instructions();                 
     $rand = rand(0,100);
    if ($rand > 80) {
	select_potential_word();
    } else {
	select_easy_word_N();
    }
}

function make_AGENT_game() {
    select_instructions();                 
    $rand = rand(0,100);
    switch(term_wrong_in_BD_p($w)) 
	{
  			case ($rand < 20): 
  			select_potential_word();
    		break;

  			case ($rand < 50): 
  			select_easy_word_V();
   			break;

 			case ($rand >= 50): 
 			select_random_word_V();
    		break;
			} 
}

function make_MANNER_game() {
    select_instructions();                 
    $rand = rand(0,100);
    switch(term_wrong_in_BD_p($w)) 
	{
  			case ($rand < 20): 
  			select_potential_word();
    		break;

  			case ($rand < 50): 
  			select_easy_word_V();
   			break;

 			case ($rand >= 50): 
 			select_random_word_V();
    		break;
    		
    		default: 
 			select_random_word_V();
    		break;
			} 
}

function make_MEANING_game() {
    select_instructions();                 
    $rand = rand(0,100);
    switch(term_wrong_in_BD_p($w)) 
	{
  			case ($rand < 5): 
  			selectRandomWord();
    		break;

  			case ($rand >= 10): 
  			selectFrequentWord();
   			break;

 			default: 
 			selectFrequentWord();
    		break;
			} 
}

function make_PATIENT_game() {
    select_instructions();                 
    $rand = rand(0,100);
    if ($rand > 100) {
	select_potential_word();
    } else {
	select_V_transitive();
    }
}

function make_INSTR_game() {
    $rand = rand(0,100);
    switch(term_wrong_in_BD_p($w)) 
	{
  			case ($rand < 0): 
  			select_potential_word();
    		break;

  			case ($rand < 50): 
  			select_easy_word_V();
   			break;

 			case ($rand >= 50): 
 			select_random_word_V();
    		break;
			} 
}

function make_MAGN_game() {
    select_instructions();                 
     $rand = rand(0,100);
    if ($rand > 100) {
	select_potential_word();
    } else {
	select_easy_word_NV();
    }
}

function make_ANTIMAGN_game() {
    select_instructions();                 
     $rand = rand(0,100);
    if ($rand > 100) {
	select_potential_word();
    } else {
	select_easy_word_NV();
    }
}

function make_FAMILLY_game() {
    select_instructions();                 
     $rand = rand(0,100);
    if ($rand > 50) {
	select_potential_word();
    } else {
	selectFrequentWord();
    }
}

function make_EMOTION_game() {
    select_instructions();                 
    $rand = rand(0,100);
    if ($rand > 90) {
	select_potential_word();
    } else {
	selectFrequentWord();
    }
}

function make_TELIC_game() {
    select_instructions();                 
	select_easy_word_N();
}

function make_AGENTIF_game() {
    select_instructions();                 
	select_easy_word_N();
}

function make_CAUSATIF_game() {
    select_instructions();                 
	select_random_word_N_V();
}

function make_gift_game() {
   select_instructions();                 
   select_gift_term();
}

function select_instructions() {
    $type = $_SESSION[ssig() . 'gamerelationtype'];
    switch (true){
	case ($type == 0):
		// par default - relation generique
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 3):
		// domaine
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 5):
		// synonyme
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 6):
		// generique - is-a
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 7):
		// contraire
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 8):
		// specifique
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 9):
		// partie
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 10):
		// tout
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

    
	case ($type == 11):
		// locution
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;


	case ($type == 13):
		// sujet
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 14):
		// object - patient
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 15):
		// lieux
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 16):
		// instr
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;


	case ($type == 17):
		// carac
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 20):
		// magn
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 21):
		// anti magn
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 22):
		// famille
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;
		
	case ($type == 23):
		// carac -1
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Qu'est-ce qui poss�de la CARACTERISTIQUE suivante (par exemple, 'eau', 'vin', 'lait' pour 'liquide') :";
		break;
	

	case ($type == 24):
		// agent -1
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Que peut faire le SUJET suivant (par exemple, 'manger', 'dormir', 'chasser' pour 'lion') :";
		break;
		
	case ($type == 25):
		// instrument -1
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Que peut-on faire avec l'INSTRUMENT suivant (par exemple, '�crire', 'dessiner', 'gribouiller' pour 'stylo') :";
		break;
		
	case ($type == 26):
		// patient -1 � modifier
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Que peut subir l'OBJET suivant (par exemple, ...) :";
		break;
		
	case ($type == 27):
		// patient -1
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Donner des termes du DOMAINE suivant (par exemple, 'touche', 'penalty', 'but' pour 'Football') :";
		break;
		
	case ($type == 28):
		// lieu -1 
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Que trouve-t-on dans le LIEU suivant (par exemple, 'poisson', 'coquillage', 'algue' pour 'mer') :";
		break;
		
	case ($type == 30):
		// lieu--action 
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Que peut-on faire dans le LIEU suivant (par exemple, 'manger', 'boire', 'commander' pour 'restaurant' -- des verbes sont demand�s) :";
		break;
		
	case ($type == 31):
		// action--lieu
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Dans quels LIEUX peut-on faire l'action suivante (par exemple, 'restaurant', 'cuisine', 'fast-food' pour 'manger' -- des lieux sont demand�s) :";
		break;
		
		
	case ($type == 32):
		// sentiment
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Quels SENTIMENTS/EMOTIONS �voque pour vous le terme suivant :";
		break;
		
	case ($type == 34):
		// sentiment
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "De quel mani�re peut on effectuer l'action suivante";
		break;
		
	case ($type == 35):
		// sentiment
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Quels SENS existent-ils pour le terme suivant";
		break;
   
	case ($type == 37):
		// sentiment
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Quels SENS existent-ils pour le terme suivant";
		break;
    
    
    case ($type == 38):
		// sentiment
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Quels SENS existent-ils pour le terme suivant";
		break;
    
    case ($type == 41):
		// sentiment
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Quels SENS existent-ils pour le terme suivant";
		break;
		
	case ($type == 42):
		// sentiment
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Quels SENS existent-ils pour le terme suivant";
		break;
    }
}

function generatePendingGame($user) {
	$_SESSION[ssig() . 'old_game'] = False;
    $_SESSION[ssig() . 'termselectionmethod']="undefined";

    // on selectionne le type de jeux
    // et la (ou les) relation(s) qui sont concern�es
    $_SESSION[ssig() . 'gametype']=1;			// jeux d'association - a voir pour la suite


    if ($_SESSION[ssig() . 'termgift'] != "") {
    	if ($_SESSION[ssig() . 'relation_type_gift']=="") {$_SESSION[ssig() . 'relation_type_gift'] = 0;}
		$_SESSION[ssig() . 'gamerelationtype']=$_SESSION[ssig() . 'relation_type_gift'];
	
		make_gift_game();
	
    } else {
	selectRelationType();
	$_SESSION[ssig() . 'old_game'] = False;
	switch (true){
	    case ($_SESSION[ssig() . 'gamerelationtype'] == 0):
		    // relation libre
		    make_free_relation_game();
		    break;

    	    case ($_SESSION[ssig() . 'gamerelationtype'] == 3):
		    // domaine
		    make_DOMAIN_game();
		    break;

	    case ($_SESSION[ssig() . 'gamerelationtype'] == 5):
		    // synonyme
		    make_SYN_game();
		    break;
    
	    case ($_SESSION[ssig() . 'gamerelationtype'] == 6):
		    // generique
		    make_GENER_game();
		    break;
    
	    case ($_SESSION[ssig() . 'gamerelationtype'] == 7):
		    //  contraire-antonyme
		    make_ANTO_game();
 		    break;
   
	    case ($_SESSION[ssig() . 'gamerelationtype'] == 8):
		    //  contraire-antonyme
		    make_HYPO_game();
		    break;

	    case ($_SESSION[ssig() . 'gamerelationtype'] == 9):
		    //  part of
		    make_PARTOF_game();
		    break;

	    case ($_SESSION[ssig() . 'gamerelationtype'] == 10):
		    //   tout (inverse de part of)
		    make_HOLO_game();
		    break;

	    case ($_SESSION[ssig() . 'gamerelationtype'] == 11):
		    //   locution
		    make_LOCU_game();
		    break;
	    
	    case ($_SESSION[ssig() . 'gamerelationtype'] == 13):
		    //   locution
		    make_AGENT_game();
		    break;

    	    case ($_SESSION[ssig() . 'gamerelationtype'] == 14):
		    //   locution
		    make_PATIENT_game();
		    break;

	    case ($_SESSION[ssig() . 'gamerelationtype'] == 15):
		    //   locution
		    make_PLACE_game();
		    break;

    	    case ($_SESSION[ssig() . 'gamerelationtype'] == 16):
		    //   locution
		    make_INSTR_game();
		    break;

	    case ($_SESSION[ssig() . 'gamerelationtype'] == 17):
		    //   locution
		    make_CARAC_game();
		    break;

	    case ($_SESSION[ssig() . 'gamerelationtype'] == 20):
		    //   locution
		    make_MAGN_game();
		    break;

	    case ($_SESSION[ssig() . 'gamerelationtype'] == 21):
		    //   locution
		    make_ANTIMAGN_game();
		    break;

	    case ($_SESSION[ssig() . 'gamerelationtype'] == 22):
		    //   locution
		    make_FAMILLY_game();
		    break;
		    
		case ($_SESSION[ssig() . 'gamerelationtype'] == 32):
		    //   emotion/sentiment
		    make_EMOTION_game();
		    break;
		    
		case ($_SESSION[ssig() . 'gamerelationtype'] == 34):
		    //   emotion/sentiment
		    make_MANNER_game();
		    break;
		    
		case ($_SESSION[ssig() . 'gamerelationtype'] == 35):
		    //   emotion/sentiment
		    make_MEANING_game();
		    break;
		    
		case ($_SESSION[ssig() . 'gamerelationtype'] == 37):
		    //   emotion/sentiment
		    make_TELIC_game();
		    break;
		    
		case ($_SESSION[ssig() . 'gamerelationtype'] == 38):
		    //   emotion/sentiment
		    make_AGENTIF_game();
		    break;
		    
		case ($_SESSION[ssig() . 'gamerelationtype'] == 41):
		    //   emotion/sentiment
		    make_CAUSATIF_game();
		    break;
		    
		case ($_SESSION[ssig() . 'gamerelationtype'] == 42):
		    //   emotion/sentiment
		    make_CAUSATIF_game();
		    break;
	}
    }

    if (($_SESSION[ssig() . 'gameentryid'] == 0) || ($_SESSION[ssig() . 'gamerelationtype'] === ""))
	{
	    $entryid = $_SESSION[ssig() . 'gameentryid'];
	    
	    if (check_proposed_word($entryid)) {
   			 } else {
    	insert_proposed_word($entryid);
    	}
	    
	    $gameentry = $_SESSION[ssig() . 'gameentry'] ;
	    $entrylevel= $_SESSION[ssig() . 'gameentrylevel'] ;
	    $relationtype = $_SESSION[ssig() . 'gamerelationtype'];
	    $gamecreatorid = $_SESSION[ssig() . 'playerid'];
	    $termselectionmethod = $_SESSION[ssig() . 'termselectionmethod'];

	    echo "<P>BUG generatePendingGame : send mail to ",$_SESSION[ssig().'CONTACT-EMAIL'], " with the following data:
		    <BR>entryid: $entryid gameentry: \"$gameentry\" entrylevel: $entrylevel gamecreatorid: $gamecreatorid gamerelationtype: $relationtype
		    termselectionmethod: $termselectionmethod
		    " ;
//break;
	}
    
    
}


function selectRelationType() {
    // tres provisoire

    // on ne veut pas changer de type de partie trop souvent
    // on gere un token de type
    // si � zero alors on reselectionne au hasard
    //
    
    if (($_SESSION[ssig() . 'typetoken'] <= 0) || 
    	($_SESSION[ssig() . 'typetoken'] == "") || 
    	($_SESSION[ssig() . 'gamerelationtype'] == "") || 
    	($_SESSION[ssig() . 'gamerelationtype'] === NULL)) {
	
		$_SESSION[ssig() . 'typetoken'] == 5 + rand(0,5);
		
		
		$relid = randomize_player_permission($_SESSION[ssig() . 'playerid']);
		if (rand(0,100) <= 5) {
			$_SESSION[ssig() . 'old_game'] = True;
		} else {
			$_SESSION[ssig() . 'old_game'] = False;
		}
		
		$_SESSION[ssig() . 'gamerelationtype']=$relid;	    
		
    } else {
	// le token pas � zero, on continue sur le type courant
	// on diminue le token
	//
	$_SESSION[ssig() . 'typetoken'] == $_SESSION[ssig() . 'typetoken'] - 1;
    }
}

function select_old_game($playerid) {
	$query = "SELECT max(id) FROM PendingGames";
	$r =  @mysql_query($query) or die("pb select_old_game : $query");
	$lastid = mysql_result($r , 0 , 0);
	
	$th = round($lastid / 4);
	
	$query = "SELECT * FROM `PendingGames` WHERE id < $th and creator !=  '$playerid'";
	
	return $query;
	
}


function generateAnswerGame($user) {
    start_time_record("generateAnswerGame");
   // debugecho("in generateAnswerGame($user)");
   // debugecho("generateAnswerGame chosengame =" . $_SESSION[ssig() . 'chosengame']);
    
    $_SESSION[ssig() . 'termselectionmethod']="NA";

    $_SESSION[ssig() . 'termcolor'] = "red";
    $ip=$_SERVER['REMOTE_ADDR'];
    $playerid =$_SESSION[ssig() . 'playerid'];
    
    if ($_SESSION[ssig() . 'gamerelationtype'] === NULL) {
    	selectRelationType();
    }
    
    $gamerelationtype = $_SESSION[ssig() . 'gamerelationtype'];

    if ($_SESSION[ssig() . 'chosengame'] != "") {
    	$_SESSION[ssig() . 'fixed_term']="Y";
		//debugecho("generateAnswerGame chosengame =" . $_SESSION[ssig() . 'chosengame']);
		$chosenid=$_SESSION[ssig() . 'chosengame'];
		$query = "SELECT id,type,creator,creationdate,token  FROM `PendingGames` WHERE id=$chosenid";
	} else {
		if ($_SESSION[ssig() . 'old_game'] == True) {
			$query = select_old_game($playerid);
		} else {
			$query = "SELECT PG.id,PG.type,PG.creator,PG.creationdate,PG.token  FROM `PendingGames` AS PG
			WHERE PG.creator !=  '$playerid' 
			AND PG.rel = '$gamerelationtype'
			AND PG.id NOT IN (SELECT PROP.gameID FROM ProposedGames AS PROP WHERE playerID = '$playerid')";
			//AND PG.IP != '$ip' -> removed by ceramisch to allow games between different users with same IP (Concurso JdM UFRGS)
		}
	}
	
    if (test_playerp() == "Y") {$_SESSION[ssig() . 'deb-message'] =  "<br>q=" . $query ; };
    //debugecho("generateAnswerGame q1 =" . $query);

    $r =  @mysql_query($query) or die ("bug in generateAnswerGame 1 : $query");
    $id = select_rand_in_tab($r);

    // y a pus
    // bug en fait
    //
    if ($id ==-1) {
		$_SESSION[ssig() . 'gamestatus']="PENDING";
		//debugecho("GAME MISSING");
		generatePendingGame($_SESSION[ssig() . 'login']);
		return;
    }

    $gameid = mysql_result($r ,$id , 0);
    $gametype = mysql_result($r ,$id , 1);
    $gamecreatorid = mysql_result($r ,$id , 2);
    $datecreation = mysql_result($r ,$id , 3);
    $token = mysql_result($r , $id , 4);

    start_time_record("recup donn�es PG");
    $query = "SELECT N.id, N.name, N.level, N.annotation, N.gloss, PGD2.data FROM `PendingGameData` AS PGD, Nodes AS N,
		`PendingGameData` AS PGD2
		WHERE 	     PGD.gameid = $gameid AND PGD.dataType = '0' AND N.id = PGD.data
			AND  PGD2.gameid = $gameid AND PGD2.dataType = '1' " ;

    $r =  @mysql_query($query) or die ("bug in generateAnswerGame 2 : $query");
    start_time_record("recup donn�es PG");

    $entryid = mysql_result($r , 0 , 0);
    $entry = mysql_result($r , 0 , 1);
    $entrylevel = mysql_result($r , 0 , 2);
    $entryannotation = mysql_result($r , 0 , 3);
    $entrygloss = mysql_result($r , 0 , 4);
    $gamerelationtype = mysql_result($r , 0 , 5);

    // evitons de proposer trop de fois le mm mot
    //
    if ($_SESSION[ssig() . 'fixed_term'] != "Y") {
    	if (check_proposed_word($entryid)) {
    		//debugecho("$entry est un TERME DEJA PROPOSE --> PENDING");
    		$_SESSION[ssig() . 'gamestatus']="PENDING";
    		generatePendingGame($_SESSION[ssig() . 'login']);
    		return;
  	 	} else {
    		insert_proposed_word($entryid);
  	  	}
    }
    
    $_SESSION[ssig() . 'gameid']=$gameid;
    $_SESSION[ssig() . 'gameentryid'] = $entryid;
    $_SESSION[ssig() . 'gameentry'] = $entry;
    $_SESSION[ssig() . 'gameentrylevel'] = $entrylevel;
    $_SESSION[ssig() . 'gameentryannotation'] = $entryannotation;
    $_SESSION[ssig() . 'gameentrygloss'] = $entrygloss;

    $_SESSION[ssig() . 'gamecreatorid']=$gamecreatorid;
    	
    $_SESSION[ssig() . 'gametype']=$gametype;
    $_SESSION[ssig() . 'gamerelationtype'] = $gamerelationtype;


    //echo "<BR>grt=$gamerelationtype";

    select_instructions();

    if ($_SESSION[ssig() . 'gameentryid'] == 0) 
	{echo "<P>BUG generateAnswerGame : send mail to ",$_SESSION[ssig().'CONTACT-EMAIL'], " with the following data:
		<BR>gameid: $gameid entryid: $entryid entrylevel: $entrylevel gamecreatorid: $gamecreatorid gamerelationtype: $gamerelationtype";
	}

    end_time_record("generateAnswerGame");
}



function generateGame() {	
    start_time_record("generateGame");
    //debugecho("in generateGame");
    
    $login = $_SESSION[ssig() . 'login'];
    if (guestplayerp()=="Y") {$login="guest";}
    $gamerelationtype = $_SESSION[ssig() . 'gamerelationtype'];
    $ip = $_SERVER['REMOTE_ADDR'];
    $playerid = $_SESSION[ssig() . 'playerid'];

    start_time_record("counting pending games");
    $query = "SELECT COUNT(PG.id) FROM `PendingGames` AS PG
		WHERE PG.creator !=  '$playerid'
		AND PG.rel = '$gamerelationtype'
		AND PG.id NOT IN (SELECT PROP.gameID FROM ProposedGames AS PROP WHERE playerID = '$playerid')" ;
    // AND PG.IP != '$ip' -> removed by ceramisch to allow games between different users with same IP (Concurso JdM UFRGS)
    $r =  @mysql_query($query) or die ("bug in generateGame : $query");
    //echodebug("q=" . $query);
    end_time_record("counting pending games");
    
    $nb = mysql_result($r , 0 , 0);
    
    // provisoire
    // 99% de chance d'avoir une partie � finir
    // sauf si pas assez
    if ($nb >= 3) {$prob = 99;} else {$prob = 0;}
	if ($nb > 1000) {$prob = 100;}
    $rand = rand(0,100);

    // for debug
    // if 100 then only creation games
    // if 0 then only pending games
    // if (test_playerp() == "Y") {$prob=0;}
	//debugecho("in generateGame : _SESSION['termgift'] == " . $_SESSION[ssig() . 'termgift']);
	//debugecho("in generateGame : _SESSION['chosengame'] == " . $_SESSION[ssig() . 'chosengame']);

    $_SESSION[ssig() . 'fixed_term'] = "N";
    if ($_SESSION[ssig() . 'termgift'] != "") {$rand=100; $_SESSION[ssig() . 'fixed_term']="Y";}
    if ($_SESSION[ssig() . 'chosengame'] != "") {$rand=0; $_SESSION[ssig() . 'fixed_term']="Y";}

    // un guest joue toujours des partie a finit
    // sauf s'il n'y en a pas assez
    //
    if (($nb > 0) && (guestplayerp()=="Y")) {$rand=0;}

    $_SESSION[ssig() . 'message'] = "($nb) $prob% de chance d'avoir un jeu � finir (tirage=$rand)";
    
    if ($rand >= $prob) {
	$_SESSION[ssig() . 'gamestatus']="PENDING";    // ANSWER or PENDING
	} else {
	$_SESSION[ssig() . 'gamestatus']="ANSWER";
	}


    if ($_SESSION[ssig() . 'gamestatus']=="PENDING") {
	// generate a new game - that will be pending
	generatePendingGame($login);
	}
    else {
	// generate a game that is an answer to a pending game
	generateAnswerGame($login);
	}
	
	// cas des noeuds anon
	
	// fin
	if (anon_entry ($_SESSION[ssig() . 'gameentry'])) {
		$_SESSION[ssig() . 'gameinstruction'] = "R�pondez � la question sp�ciale suivante :";
	}
	
	debugecho("generateGame=" . end_time_record("generateGame"));
}


?>
