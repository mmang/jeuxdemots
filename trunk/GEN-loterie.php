<?php session_start();?>
<?php include_once 'misc_functions.php'; ?>
<?php
if (test_playerp()) { } else {$_SESSION[ssig() . 'session_tag'] = "";}
    openconnexion();

?>
<html>
 <head>
    <title><?php 	echo "Loterie " ; 
				echo get_msg('SOUK-TITLE');
				echo " underground"; ?></title>
    <?php header_page_encoding(); ?>
  </head>
<?php include 'HTML-body.html' ; ?>

<?php topblock(); ?>
<?php check_referer(); ?>

<?php

//-----------

function get_game_instructions($rel) {
    $type = $rel;
    switch (true){
	case ($type == 0):
		// par default - relation generique
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 3):
		// domaine
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 5):
		// synonyme
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 6):
		// generique - is-a
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 7):
		// contraire
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 8):
		// specifique
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 9):
		// partie
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 10):
		// tout
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

    
	case ($type == 11):
		// locution
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;


	case ($type == 13):
		// sujet
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 14):
		// object - patient
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 15):
		// lieux
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 16):
		// instr
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;


	case ($type == 17):
		// carac
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 20):
		// magn
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 21):
		// anti magn
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;

	case ($type == 22):
		// famille
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		break;
		
	case ($type == 23):
		// carac -1
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Qu'est-ce qui poss�de la CARACTERISTIQUE suivante (par exemple, 'eau', 'vin', 'lait' pour 'liquide') :";
		break;
	

	case ($type == 24):
		// agent -1
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Que peut faire le SUJET suivant (par exemple, 'manger', 'dormir', 'chasser' pour 'lion') :";
		break;
		
	case ($type == 25):
		// instrument -1
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Que peut-on faire avec l'INSTRUMENT suivant (par exemple, '�crire', 'dessiner', 'gribouiller' pour 'stylo') :";
		break;
		
	case ($type == 26):
		// patient -1 � modifier
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Que peut subir l'OBJET suivant (par exemple, ...) :";
		break;
		
	case ($type == 27):
		// patient -1
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Donner des termes du DOMAINE suivant (par exemple, 'touche', 'penalty', 'but' pour 'Football') :";
		break;
		
	case ($type == 28):
		// lieu -1 
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Que trouve-t-on dans le LIEU suivant (par exemple, 'poisson', 'coquillage', 'algue' pour 'mer') :";
		break;
		
	case ($type == 30):
		// lieu--action 
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Que peut-on faire dans le LIEU suivant (par exemple, 'manger', 'boire', 'commander' pour 'restaurant' -- des verbes sont demand�s) :";
		break;
		
	case ($type == 31):
		// action--lieu
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Dans quels LIEUX peut-on faire l'action suivante (par exemple, 'restaurant', 'cuisine', 'fast-food' pour 'manger' -- des lieux sont demand�s) :";
		break;
		
		
	case ($type == 32):
		// sentiment
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Quels SENTIMENTS/EMOTIONS �voque pour vous le terme suivant :";
		break;
		
	case ($type == 34):
		// sentiment
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "De quel mani�re peut on effectuer l'action suivante";
		break;
		
	case ($type == 35):
		// sentiment
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Quels SENS existent-ils pour le terme suivant";
		break;
    
    
	case ($type == 37):
		// sentiment
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Quels SENS existent-ils pour le terme suivant";
		break;
    
    
	case ($type == 38):
		// sentiment
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Quels SENS existent-ils pour le terme suivant";
		break;
    
    
	case ($type == 41):
		// sentiment
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Quels SENS existent-ils pour le terme suivant";
		break;
    
    
	case ($type == 42):
		// sentiment
		$_SESSION[ssig() . 'gameinstruction'] = get_msg('GENGAME-FUNCTIONS-INSTR-' . $type);
		//$_SESSION[ssig() . 'gameinstruction'] = "Quels SENS existent-ils pour le terme suivant";
		break;
    }

}


//-----------

function solve_loterie() {
	if ($_SESSION[ssig() . 'loterie_last'] == "result") {
		$_GET['gid'] = "";
		return;
	}
	
	
	
	$_SESSION[ssig() . 'loterie_last'] = "result";
	$_SESSION[ssig() . 'loterie_result_message'] = "";
	
	
	$creator_game_id = round($_GET['gid']);
	if ($creator_game_id == 0) {return;}
	
	// what is the other player
	//
	$query = "SELECT creator,rel FROM PendingGames WHERE id=$creator_game_id;";
	$r =  @mysql_query($query) or die("pb solve_loterie 1 : $query");
	$_SESSION[ssig() . 'loterie_player_id'] = mysql_result($r , 0 , 0);
	$_SESSION[ssig() . 'loterie_player'] = get_player_name($_SESSION[ssig() . 'loterie_player_id']);
	$_SESSION[ssig() . 'loterie_rel'] = mysql_result($r , 0 , 1);
	
	// what was the term
	//
	$query = "SELECT data FROM PendingGameData WHERE gameId=$creator_game_id and dataType=0;";
	$r =  @mysql_query($query) or die("pb solve_loterie 2 : $query");
	$termid = mysql_result($r , 0 , 0);
	$_SESSION[ssig() . 'loterie_term'] = format_entry(get_term_from_id($termid));
	
	if (check_proposed_word($termid)==true) {
		echo "acc�s invalide... pas possible";
		return;
	}
	insert_proposed_word($termid);
	
	// select a player game, on the same term
	//
	$playerid = $_SESSION[ssig() . 'playerid'];
	$rel = $_SESSION[ssig() . 'loterie_rel'];
	
	$_SESSION[ssig() . 'gamerelationtype'] = $rel;
	get_game_instructions($rel);
	
	$query = "SELECT P.id  FROM PendingGames as P, PendingGameData as D 
	WHERE P.rel=$rel and P.creator = $playerid
	and P.id = D.gameId and D.dataType = 0 and D.data = $termid
	ORDER BY Rand() limit 1";
	$r =  @mysql_query($query) or die("pb solve_loterie 3 : $query");
    $nb = mysql_num_rows($r);
    if ($nb == 0) {echo "Pas de chance !"; return;}
    $player_game_id = mysql_result($r , 0 , 0);
    
    // getting the intersection
    //
    $instr = $_SESSION[ssig() . 'gameinstruction'];
    $term = $_SESSION[ssig() . 'loterie_term'];
    $msg = "<P><font size=-1>La consigne (partie $player_game_id) �tait '$instr' pour
    <P><font color=\"red\" size=+2>$term</font><font><p>r�sultat...</font>";
    
	$_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . "<div class = \"jdm-intersection-block\">" . $msg;
	$_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . "<P>";
	
	// les rep de l'autre
	//
	$query = "SELECT DISTINCT PA.answer FROM `PendingAnswers` AS PA
	       WHERE PA.gameId = '$creator_game_id'
	       AND PA.answer NOT IN (SELECT DISTINCT P.answer FROM `PendingAnswers` AS P
	       				WHERE P.gameId = '$player_game_id')";
	$r =  @mysql_query($query) or die("pb solve_loterie 4-1 :$query");
	$nb2 = mysql_num_rows($r);
	for ($i=0 ; $i<$nb2 ; $i++) {
		$w = mysql_result($r , $i , 0);
		$tag = " - ";
		if ($i == 0) {$tag = "ses r�ponses &gt; "; }
		$_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . "$tag <font size=+2 color=grey>" . format_entry($w) . "</font>";
	}
	
	$query = "SELECT DISTINCT PA1.answer FROM `PendingAnswers` AS PA1, `PendingAnswers` AS PA2
	       WHERE PA1.gameId = '$creator_game_id' AND PA2.gameId = '$player_game_id' 
	       AND PA2.answer = PA1.answer ORDER BY 'PA1.answer' DESC";
	$r =  @mysql_query($query) or die("pb solve_loterie 4 :$query");
	$meanlevel = (get_player_level($gamecreatorid)+get_player_level($playerid))/2;
	$points = 0;
	$nb = mysql_num_rows($r);
	if ($nb == 0) {
		$_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . "<P>-- rien -- ";
		}
	
	$interseclist = "";
	for ($i=0 ; $i<$nb ; $i++) {
		$w = mysql_result($r , $i , 0);
		$tag = " - ";
		if ($i == 0) {$tag = "<p>"; }
		if (term_exist_in_BD_p($w) == 0) {
			$_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . "$tag<div class = \"jdm-term-intersect-notinbase\"><font size=+2>" . format_entry($w) . "</font></div>";
		} else {
			$_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . "$tag<div class = \"jdm-term-intersect-inbase\"><font size=+2>" . format_entry($w) . "</font></div>";
		}
		update_network_relation($w, $termid, $rel, 50);
	}
	// vos reponses
	$query = "SELECT DISTINCT PA.answer FROM `PendingAnswers` AS PA
	       WHERE PA.gameId = '$player_game_id'
	       AND PA.answer NOT IN (SELECT DISTINCT P.answer FROM `PendingAnswers` AS P
	       				WHERE P.gameId = '$creator_game_id')";
	$r =  @mysql_query($query) or die("pb solve_loterie 4-1 :$query");
	$nb3 = mysql_num_rows($r);
	for ($i=0 ; $i<$nb3 ; $i++) {
		$w = mysql_result($r , $i , 0);
		$tag = " - ";
		if ($i == 0) {$tag = "<p>"; }
		$_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . "$tag <font size=+2 color=grey>" . format_entry($w) . "</font>";
	}
	$_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . " &lt; vos r�ponses<P><br>";
	
	if (($nb + $nb2) == 0) {echo "<P>delete $creator_game_id"; delete_game($creator_game_id);}
	if (($nb + $nb3) == 0) {echo "<P>delete $player_game_id"; delete_game($player_game_id);}
	
	// IMPORTANT
	// on decremente le jeton du joueur courant DE TOUTE FACON
	// celui de l'autre seulement si intersection
	// ou 2  r�ponses ou moins (partie merdique)
	//
	decf_play_token ($player_game_id);
	inc_player_nbplayed($_SESSION[ssig() . 'playerid'], 1);
	if (($nb > 0) || ($nb2 <= 2)) {
		decf_play_token ($creator_game_id);
    	inc_player_nbplayed($_SESSION[ssig() . 'loterie_player_id'], 1);
	}
	
	$val = $nb*$nb;
	$val2 = $nb*50;
	//inc_player_credit($_SESSION[ssig() . 'playerid'], $val2);
	
	$_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . "</div>";
		
	$_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . "<P>Vous gagnez $val points d'honneur.";
	//$_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . "<P>Vous avez regagn� $val2 credits.";
	//$_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . "<P>(debug: $creator_game_id / $player_game_id).";
	
	inc_player_honnor($_SESSION[ssig() . 'playerid'], $val);
	inc_player_honnor($_SESSION[ssig() . 'loterie_player_id'], $val);
	
	if ($nb > 0) {
		$p1 = get_player_name($_SESSION[ssig() . 'playerid']);
		$p2 = $_SESSION[ssig() . 'loterie_player'];
		$term = $_SESSION[ssig() . 'loterie_term'];
		make_event("'$p1' a gagn� $val point d'honneur avec '$p2' � la loterie pour '$term'.");
	} else {
		$p1 = get_player_name($_SESSION[ssig() . 'playerid']);
		$p2 = $_SESSION[ssig() . 'loterie_player'];
		$term = $_SESSION[ssig() . 'loterie_term'];
		make_event("'$p1' n'a rien gagn� avec '$p2' � la loterie pour '$term'.");
		
	}
	produce_loterie_play_form ();
	
}

function produce_loterie_play_form () {
	$playerid = $_SESSION[ssig() . 'playerid'];
	$cost = get_loterie_price($playerid);
	$_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . "\n<form  method=\"post\" action=\"GEN-loterie.php\" >";
 	$_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . "\n<input  type=\"submit\" name=\"submit\" value=\"Relancer\">";
    $_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . "\n($cost Cr)";
    $_SESSION[ssig() . 'loterie_result_message'] = $_SESSION[ssig() . 'loterie_result_message'] . "\n</form>";
}

//-----------

function generate_random_special_TERM_form(){
	if ($_SESSION[ssig() . 'playerid'] == 0) {
		//echo "Nan !"; return;
		echo "<div class=\"jdm-notice1-block\"><div class=\"jdm-notice1\">";
		echo "L'invit� ne peut pas jouer � la loterie.<BR><br>Connectez-vous.";
		echo "\n</div>\n</div>";
		return;
	}
	$_POST['gener_loterie_submit'] = "xxx";

	//PRED+AGT => PATIENT ?
	//echo  "<form id=\"gener_q\" name=\"gener_q\" method=\"post\" action=\"QGEN.php\" >
	//		<input id=\"gener_q_submit\" type=\"submit\" name=\"gener_q_submit\" value=\"Generer des questions\">
	//		</form>";
	if ($_POST['gener_loterie_submit'] != "") {
		generate_random_loterie();
	}
	
}

function check_game_loterie ($playerid, $entryid, $rel=0) {
	$query = "SELECT count(P.id)
	FROM PendingGames as P, PendingGameData as D 
	WHERE P.rel=$rel and P.creator = $playerid 
	and P.id = D.gameId and D.dataType = 0 and D.data = $entryid;";
	$r =  @mysql_query($query) or die("pb check_game_loterie 1 : $query");
	return mysql_result($r , 0 , 0);
}


function generate_random_loterie(){
	$playerid = $_SESSION[ssig() . 'playerid'];
	$cost = get_loterie_price($playerid);
	
	$_SESSION[ssig() . 'loterie_last'] = "init";
	
	
	if (get_player_credit($playerid) < $cost) {
		echo "<div class=\"jdm-notice1-block\"><div class=\"jdm-notice1\">";
		echo "Pas assez de cr�dit disponibles...";
		echo "\n</div>\n</div>";
		echo "<div class=\"jdm-playas-block-1\">";
		echo produceplayasform();
		echo "</div>";
		return;
	}

	$nbcol = 3;
	$size = 3;
	$nb = $nbcol * $size * 1000;
	
	//$query = "SELECT id, creator, target, token, ip, rel
//	FROM PendingGames as P 
	//WHERE  P.creator != $playerid and P.creator != 0 
//	ORDER BY Rand() limit $nb";
	
	
//	$query = "SELECT P.id, P.creator, P.target, P.token, P.ip, P.rel
//	FROM PendingGames as P, PendingGames as P2
//	WHERE  P.creator != $playerid and P.creator != 0 
//AND P2.creator = $playerid
//AND P.target = P2.target AND P.rel = P2.rel
//	ORDER BY Rand() limit  $nb";
	
		$query = "SELECT P.id, P.creator, P.target, P.token, P.ip, P.rel
	  FROM `PendingGames` as P WHERE `creator` != $playerid and `creator` != 0 and token > 1
AND target in (SELECT distinct(target) FROM PendingGames WHERE creator = $playerid and rel = P.rel)
AND target not in (select term_id FROM ProposedWords WHERE player_id = $playerid)
AND IP not in (select IP FROM PendingGames WHERE creator = $playerid) LIMIT 10";

	$r =  @mysql_query($query) or die("pb generate_random_loterie 1 : $query");
    $nb= mysql_num_rows($r);
  //  echo "<br>debug:$query == $nb";
    $j = -1;
    echo "<CENTER><TABLE width=\"100%\">";
    for ($i=0 ; $i<mysql_num_rows($r) ; $i++) {
    	$gameid = mysql_result($r , $i , 0);
    	$creator = mysql_result($r , $i , 1);
    	$data = mysql_result($r , $i , 2);
    	$token = mysql_result($r , $i , 3);
    	$ip = mysql_result($r , $i , 4);
    	$rel = mysql_result($r , $i , 5);
    	
    	$cname = get_player_name($creator);
    	$fentry = format_entry(get_term_from_id($data));
    	
    	$test = check_game_loterie($playerid, $data, $rel);
    	//$test2 = check_proposed_word($data);
    	//$test3 = check_IP_table($playerid, $ip);
    	
    	//$test = 1;
    	$test2 = false;
    	$test3 = false;
    	
    	if ($token > 10) {normalize_play_token ($gameid);}
    	
    	if ($j >= ($nbcol * $size)-1) {break;}
    	
    	if (($test2 == false) && ($test3 == false) && ($j < ($nbcol * $size)-1) && ($test > 0)) {
    		$j++;
    		if ($j%$nbcol == 0) {echo "<TR>";}
    		
    		if (rand(0,1)== 0) {
    			$cname = '???';
    		}
    		if (rand(0,1)== 0) {
    			$fentry = '???';
    		}
    		
    		echo "<TH>";
    		echo "\n<div class=\"jdm-special-block\">";
			echo "\n<img BORDER=0 width=\"100%\" height=\"70\" 
							src=\"http://www.lirmm.fr/jeuxdemots/pics/postits/postitdeg.png\">";
			echo "\n<div class=\"jdm-special-question\">";
			echo "\n<a href=\"GEN-loterie.php?gid=$gameid\">'$fentry'<br><font size=-1>avec '$cname'</font></a>";
			echo "\n</div>";
			echo "\n</div>";
   			}
    }
    echo "</TABLE></CENTER>";
    if ($j == -1) {
    	echo "<div class=\"jdm-notice1-block\"><div class=\"jdm-notice1\">";
		echo "Vous n'avez pas assez de parties pour pr�tendre � la loterie... investissez d'avantage !";
		echo "\n</div>\n</div>";
		
		echo "<div class=\"jdm-playas-block-1\">";
		echo produceplayasform();
		echo "</div>";
    
    } else {
    	inc_player_credit($_SESSION[ssig() . 'playerid'], -$cost);
    	echo "<p>Votre compte a �t� d�bit� de $cost cr�dits.";
    }
}







?>

<?php 
solve_loterie(); 

?>

<div class="jdm-level1-block">
	
	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
    <?php if ($_GET['gid'] == "") {
    	echo "Choisissez une partie..."; 
    } else {
    	$t = $_SESSION[ssig() . 'loterie_term'];
    	$p = $_SESSION[ssig() . 'loterie_player'];
    	echo "Loterie pour '$t' avec '$p'";
    }
    if ($_SESSION[ssig() . 'FRAUD'] == true) {
    	echo " :: acc�s frauduleux...";
    }
    
    ?>
    </div>
	</div>

    <div class="jdm-login-block">
    <?php  loginblock(); ?>
    </div>
</div>

<div class="jdm-level2-block">
<TABLE border="0" width="100%" cellspacing="0" cellpadding="0%" summary="jeuxdemots">
<TR><TH bgcolor="#EEF6F8">
    <TH align="left" bgcolor="#EEF6F8">
    <TH bgcolor="#EEF6F8">

<TR><TH  align="left" bgcolor="#DCFFFF">
    <TH align="left" bgcolor="#DCFFFF" align="left" size=200px>
   	<TH>
    <?php
    
    //echo "_GET['gid']==" . $_GET['gid'];
    if ($_GET['gid'] == "") {
   		generate_random_special_TERM_form();
    } else {
    	echo $_SESSION[ssig() . 'loterie_result_message'];
		
    }
    ?>
    
   
    <TH bgcolor="#DCFFFF">
	
</TABLE>
</div>

<?php playerinfoblock($_SESSION[ssig() . 'playerid']); ?>

<?php 
    bottomblock();
    closeconnexion();
?>

  </body>
</html>
