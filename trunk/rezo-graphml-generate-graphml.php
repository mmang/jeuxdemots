<?php session_start();?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>';?>
<?php include_once 'misc_functions.php'; ?>
<?php openconnexion();?>
<?php

//echo "<BR>arg=" . fetch_arg('aff_typerel');
//echo "<BR>arg=" . fetch_arg('aff_w');

function fetch_arg($name, $default = '') {
	$arg = trim($_POST[$name]);
	if ($arg == "") {
		$arg = trim($_GET[$name]);	
	}
	if ($arg == '') {$arg = $default;}
	return $arg;
}

function generate_nodes() {
	foreach ($_SESSION['rg_node_list'] as $nodeid => $name) {
		
		$utf8_name = utf8_encode($name);
		
		echo "\n<node id=\"n$nodeid\">
      <data key=\"d0\"/>
      <data key=\"d1\">
        <y:ShapeNode>
          <y:NodeLabel>$utf8_name</y:NodeLabel>
        </y:ShapeNode>
      </data>
    </node>";
	}
}

function generate_links() {
foreach ($_SESSION['rg_rel_list'] as $relid => $val) {
		if ($val == '-1') {
			$query = "SELECT id, node1, node2, w, type FROM Relations WHERE id=$relid;";
			$r =  @mysql_query($query) or die("pb in generate_links : $query");
			$id = mysql_result($r , 0 , 0);
			$node1 = mysql_result($r , 0 , 1);
			$node2 = mysql_result($r , 0 , 2);
			$w = mysql_result($r , 0 , 3);
			$type = mysql_result($r , 0 , 4);
		
			
		} else {
			$split = explode(':', $relid);
			$node1 = $split[0];
			$node2 = $split[1];
			$w = $val;
			$type = '';
		}
		$arg1 = fetch_arg('aff_typerel');
		if ($type == '') {$arg1 = '';}
		$arg2 = fetch_arg('aff_w');
		if (($arg1 != '') && ($arg2 != '')) {$tag = "$type:$w";}
		if (($arg1 == '') && ($arg2 != '')) {$tag = "$w";}
		if (($arg1 != '') && ($arg2 == '')) {$tag = "$type";}
		if (($arg1 == '') && ($arg2 == '')) {$tag = '';}
		echo "\n<edge id=\"e$id\" source=\"n$node1\" target=\"n$node2\">
      <data key=\"d2\"/>
      <data key=\"d3\">
        <y:PolyLineEdge>
          <y:Arrows source=\"none\" target=\"standard\"/>
          <y:EdgeLabel>$tag</y:EdgeLabel>
        </y:PolyLineEdge>
      </data>
    </edge>";
	}
}

?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns/graphml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:y="http://www.yworks.com/xml/graphml" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns/graphml http://www.yworks.com/xml/schema/graphml/1.0/ygraphml.xsd">
  <key attr.name="description" attr.type="string" for="node" id="d0"/>
  <key for="node" id="d1" yfiles.type="nodegraphics"/>
  <key attr.name="description" attr.type="string" for="edge" id="d2"/>
  <key for="edge" id="d3" yfiles.type="edgegraphics"/>
  <key for="graphml" id="d4" yfiles.type="resources"/>
  <graph edgedefault="directed" id="G" parse.edges="50" parse.nodes="43" parse.order="free">
  <?php generate_nodes(); generate_links(); ?>
  </graph>
  <data key="d4">
    <y:Resources/>
  </data>
</graphml>
