<?php
    first_inits();
    include_once 'PARAM.php';

    include_once 'player_perm_functions.php' ; 
    include_once 'fl_nodes_functions.php';
    //include_once 'challenge_functions.php';
    include_once 'parainage_functions.php';

    include_once 'term_owning_functions.php';

    include_once 'mail_functions.php';
    
    include_once 'trial_functions.php';
    
    include_once 'BOT_gift_functions.php';
    
    include_once 'POS_functions.php';
    
    include_once 'artefact_functions.php' ;
    
    include_once 'http_request_functions.php' ;

    include_once 'LOC-' . $_SESSION["LANG"] . '.php';
    
    include_once 'Q_DATA_GEN-Functions.php'; 
    
    include_once 'misc_duel_functions.php';
  
  // à ajouter après le chargement du fichier de LOC !
   include_once 'aide_FL_functions.php';

    
   // include_once 'jdm_twik_script.js';
//error_reporting(E_ALL);
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);


function start_debugecho() {
	$_SESSION[ssig() . 'DEBUGECHO'] = "<div class=\"jdm-debug-block\">";
}

function debugecho($s) {
    if (test_playerp() == "Y") {
		$_SESSION[ssig() . 'DEBUGECHO'] = $_SESSION[ssig() . 'DEBUGECHO'] .  "\n<div class=\"jdm-debug\">DG:" . $s . "</div>";
    }
}

function end_debugecho() {
	$_SESSION[ssig() . 'DEBUGECHO'] = $_SESSION[ssig() . 'DEBUGECHO'] . "</div>";
	echo $_SESSION[ssig() . 'DEBUGECHO'];
}



//---------------------  PAGE HEADERS

function header_page_encoding() {
    $cur_url = get_css_url($_SESSION[ssig() . 'playerid']);
    if ($cur_url == "") {$cur_url = $_SESSION[ssig() . 'DEFAULT-CSS-URL'];}
    echo get_msg('ENCODING-GENERAL');
    //echo  "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\"/>";
    // echo  "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
    echo  "\n";
    echo  "\n<link rel=\"stylesheet\" type=\"text/css\" href=\"$cur_url\"/>";
   	echo  "\n<link rel=\"stylesheet\" type=\"text/css\" href=\"CSS/smoothbox.css\"/>";
    header_metatags();
  	echo "\n<script type=\"text/javascript\" src=\"jsscripts/mootools.js\"></script>";
  	echo "\n<script type=\"text/javascript\" src=\"jsscripts/mootoolsmore.js\"></script>";
  	echo "\n<script type=\"text/javascript\" src=\"jsscripts/jdm_twik_script.js\"></script>";
   	echo "\n<script type=\"text/javascript\" src=\"jsscripts/jdm_duel_script.js\"></script>";
   	echo "\n<script type=\"text/javascript\" src=\"jsscripts/jdm_shake_script.js\"></script>";
   	echo "\n<script type=\"text/javascript\" src=\"jsscripts/jdm_beat_script.js\"></script>";
   	echo "\n<script type=\"text/javascript\" src=\"jsscripts/smoothbox.js\"></script>";
   	
   	echo "<link rel=\"shortcut icon\" href=\"pics/favicon-Je-32-bis.ico\" type=\"image/x-icon\" />";
}

function header_museum_page_encoding($owner) {
    $cur_url = get_css_museum_url($owner);
    //if ($cur_url == "") {$cur_url = $_SESSION[ssig() . 'DEFAULT-CSS-URL'];}
    if ($cur_url == "") {$cur_url = " http://www.lirmm.fr/jeuxdemots/jdm-museum.css";}   
   
    echo get_msg('ENCODING-MUSEUM');
    //echo  "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">";
    // echo  "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
    echo  "\n";
    echo  "<link rel=\"stylesheet\" type=\"text/css\" href=\"$cur_url\"/>";
    header_metatags();
}

function set_css_url($playerid, $url) {
    if ($playerid == 0) {return;}
    $query = "UPDATE `Players` SET css_url = \"$url\" WHERE id= '$playerid' ;";
    $r =  @mysql_query($query) or die("bug in set_css_url : $query");
    }

function get_css_url($playerid) {
    $query = "SELECT css_url FROM `Players`  WHERE id= '$playerid' ;";
    $r =  @mysql_query($query) or die("bug in get_css_url : $query");
    $name = mysql_result($r , 0 , 0);
    return $name;
}

function set_css_museum_url($playerid, $url) {
	//echo "set_css_museum_url";
    if ($playerid == 0) {return;}
    $query = "UPDATE `Players` SET css_mus_url = \"$url\" WHERE id= '$playerid' ;";
    $r =  @mysql_query($query) or die("bug in set_css_museum_url : $query");
    }

function get_css_museum_url($playerid) {
	//echo "get_css_museum_url";
    $query = "SELECT css_mus_url FROM `Players`  WHERE id= '$playerid' ;";
    $r =  @mysql_query($query) or die("bug in set_css_museum_url : $query");
    $name = mysql_result($r , 0 , 0);
    return $name;
}


function set_perso_url($playerid, $url) {
    if ($playerid == 0) {return;}
    $query = "UPDATE `Players` SET perso_url = \"$url\" WHERE id= '$playerid' ;";
    $r =  @mysql_query($query) or die("bug in set_perso_url : $query");
    }

function get_perso_url($playerid) {
    $query = "SELECT perso_url FROM `Players`  WHERE id= '$playerid' ;";
    $r =  @mysql_query($query) or die("bug in get_perso_url : $query");
    $name = mysql_result($r , 0 , 0);
    return $name;
}


//----------------------------

function normalize_candidate($s) {
	$news = htmlspecialchars(addslashes(trim($s)));
	$oldstr = array("&gt;", "\\");
	$newstr = array(">", ""); 
	$newss = str_replace($oldstr, $newstr, $news);
	return $newss;
}

function produce_candidate_class($cand, $norm=true, $value=-1) {
	if ($norm) {$w = normalize_candidate($cand);} else {$w = $cand;}
	if ($value == -1) {
		$value = term_wrong_in_BD_p($w);
	}
	switch($value) 
	{
  			case -1: // si $var vaut 1
    		return "<div class = \"jdm-term-prop-notinbase\">" . $w  . "</div>";
   			break;

  			case 0: // si $var vaut 2
   			return "<div class = \"jdm-term-prop-inbase\">" . $w . "</div>";
    		break;

 			case 1: // on met bien : après la valeur
    		return "<div class = \"jdm-term-prop-wrong-inbase\">" . $w . "</div>";
    		break;
	} 
}


function header_metatags() {
	echo  "\n";
	echo  "<meta name=\"description\" content=\"Jogo de associação de termos, muito divertido. Colecione e capture palavras. Encontre as associações mais relevantes.\"/>\n";
	echo  "<meta name=\"keywords\" lang=\"fr\" content=\"jogo, palavra, expressão, coleção, associação, funções lexicais, divertido, rede lexical, PLN, língua, linguagem, léxico, dicionário\"/>\n";
	echo  "<meta http-equiv=\"content-language\" content=\"pt\"/>\n";
	echo  "<meta name=\"reply-to\" content=\"",$_SESSION[ssig().'CONTACT-EMAIL'],"\"/>\n";
	echo  "<meta name=\"category\" content=\"Jogos\"/>\n";
	echo  "<meta name=\"robots\" content=\"index\"/>\n";
	echo  "<meta name=\"distribution\" content=\"global\"/>\n";
	echo  "<meta name=\"revisit-after\" content=\"7 days\"/>";
	echo  "<meta name=\"author\" lang=\"pt\" content=\"LICIA, LIG, Carlos Ramisch, Mathieu Mangeot, Mathieu Lafourcade\"/>\n";
	echo  "<meta name=\"copyright\" content=\"LIRMM, Mathieu Lafourcade\"/>\n";
	echo  "<meta name=\"generator\" content=\"\"/>\n";
	echo  "<meta name=\"identifier-url\" content=\"http://jeuxdemots.liglab.fr/por/\"/>\n";
	echo  "<meta name=\"expires\" content=\"never\"/>\n";
	echo  "<meta name=\"Date-Creation-yyyymmdd\" content=\"mai 2006\"/>\n";
	echo  "<meta name=\"Date-Revision-yyyymmdd\" content=\"novembre 2011\"/>\n";	
	
	generate_no_script();
}

//---------------------  GESTION CONNEXION

function openconnexion() {
    // Ouverture de la connexion :  
    $connexion = mysql_connect($_SESSION[ssig() . 'CONNEXION-URL'], $_SESSION[ssig() . 'CONNEXION-USER'], $_SESSION[ssig() . 'CONNEXION-PSWD']);
    
    if (empty($connexion)) {
    	die('Database connection error: '. mysql_error());
    }
    // Ouverture de la base de données :
    $result = mysql_select_db($_SESSION[ssig() . 'CONNEXION-DBNAME'] , $connexion);
    if (empty($result)) {
    	die('Database error: '. mysql_error());
    }
    if (get_msg('ENCODING-DB')=='utf8') {
    	mysql_query('SET NAMES UTF8', $connexion);
    }
    start_debugecho();
    }	

function closeconnexion() {
    //mysql_close();
    
	end_debugecho();
    }	

   
//--------------------- 

function open_session() {
   // $_SESSION[ssig() . 'session_tag'] = microtime();
   $_SESSION[ssig() . 'session_tag'] = $_SESSION["LANG"];
}

function session_down_p() {
	//debugecho("GAME_INSTANCE = '" . $_SESSION[ssig() . 'GAME_INSTANCE'] . "'");
	//debugecho("session_tag = '" . $_SESSION[ssig() . 'session_tag'] . "'");
	if ((empty($_SESSION[ssig() . 'GAME_INSTANCE']))
		OR ($_SESSION[ssig() . 'session_tag'] == "")
		) {
		$_SESSION[ssig() . 'GAME_INSTANCE'] = dirname(__FILE__);
		return "Y";
	} else if (($_SESSION[ssig() . 'GAME_INSTANCE'] !== dirname(__FILE__))
				OR ($_SESSION[ssig() . 'session_tag'] != $_SESSION["LANG"])
			) {
		// Là tu rediriges vers l'accueil en vidant tous les SESSION
		session_unset();
		return "Y"; 
		//session_destroy(); 
	}
	return "N";
}
    
function first_inits() {
	// pour eviter de stupides warnings
	//
/*	$GLOBALS['<0>'] = '';
    $GLOBALS['<1>'] = '';
    $GLOBALS['<2>'] = '';
    $GLOBALS['<3>'] = '';
    $GLOBALS['<4>'] = '';
    $GLOBALS['<5>'] = '';
    $GLOBALS['<6>'] = '';
    $GLOBALS['<7>'] = '';
    $GLOBALS['<8>'] = '';
    $GLOBALS['<9>'] = ''; */
	$GLOBALS[0] = '';
    $GLOBALS[1] = '';
    $GLOBALS[2] = '';
    $GLOBALS[3] = '';
    $GLOBALS[4] = '';
    $GLOBALS[5] = '';
    $GLOBALS[6] = '';
    $GLOBALS[7] = '';
    $GLOBALS[8] = '';
    $GLOBALS[9] = '';
	$GLOBALS[10] = '';
    $GLOBALS[11] = '';
    $GLOBALS[12] = '';
    $GLOBALS[13] = '';
    $GLOBALS[14] = '';
    $GLOBALS[15] = '';
    $GLOBALS[16] = '';
    $GLOBALS[17] = '';
    $GLOBALS[18] = '';
    $GLOBALS[19] = '';

    }

//---------------------  DEBUG GESTION DE TEMPS
function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

function start_time_record($tag) {
    $t = microtime_float();
    $_SESSION[$tag] = $t;
}

function end_time_record($tag) {
    $t = microtime_float();
    $duree = $t - $_SESSION[$tag];

    //debugecho("duree pour '$tag' = $duree");
    return $duree;
}

//---------------------  


function select_rand_in_array($array) {
    $n = count($array);
    //debugecho("select_rand_in_tab : n = $n");
    if ($n != 0) {
	$rand = rand(0,$n-1);
	return $array[$rand];
    } else {return -1;}
}

//---------------------  STAMPS

// pas de gestion de connexion
//
function generateStamp() {
    $_SESSION[ssig() . 'stamp'] = $_SESSION[ssig() . 'login'] . "-" . $_SESSION[ssig() . 'playerid'] . "-" .  $_SESSION[ssig() . 'gameid'] . "-" . $_SESSION[ssig() . 'gameentryid'] . "_" .  rand(0,100000000);
}

// pas de gestion de connexion
//
function checkStamp() {
    //openconnexion();
    $stamp = $_SESSION[ssig() . 'stamp'];
    $query = "SELECT tag, date FROM `Stamps` WHERE tag= '$stamp' ;";
    //echo $query;
    
   $r =  @mysql_query($query) or die("bug in checkStamp : $stamp");
    //$r =  mysql_query($query) ;
    $n = mysql_num_rows($r);
    //closeconnexion();
    if ($n == 0) {return 0;} else {return 1;}
}

//
function insertStamp() {
    //openconnexion();
    $stamp = $_SESSION[ssig() . 'stamp'];
    $query = "INSERT INTO `Stamps` (tag) VALUES ('$stamp');";
    $r =  @mysql_query($query) or die("bug in insertStamp : $stamp");
    //closeconnexion();
}


function fraudRedirect() {
    echo "<a href=\"jdm-accueil.php\">Accueil</a>";

    echo "<script language=\"JavaScript1.2\"> 
    window.setTimeout(\"location=('jdm-accueil.php');\",1) 
    </script>  ";

    echo "<script language=\"JavaScript1.2\">
	    window.onload=start_countdown
	    </script>";

}

//---------------------

function term_wrong_in_BD_p($term) {
	$id = term_exist_in_BD_p($term);
	if ($id == 0){return -1;}	
    $query =  "SELECT count(id) FROM `Relations` WHERE node1= $id and type = 33"   ;
    $r =  @mysql_query($query) or die ("bug in term_wrong_in_BD_p : $query");
    $nb = mysql_result($r , 0 , 0);
    if ($nb == 0) {return 0;} else {return 1;}

    // si 0 alors exist po !
}

function wrong_term_suggestions($term) {
	$suggestions = "";
	$id = term_exist_in_BD_p($term);
	if ($id == 0){return;}	
    $query =  "SELECT node2 FROM `Relations` WHERE node1= $id and type = 33"   ;
    $r =  @mysql_query($query) or die ("bug in term_wrong_in_BD_p : $query");
    $nb =  mysql_num_rows($r);
    for ($i=0 ; $i<$nb ; $i++) {
    	$id = mysql_result($r , $i , 0);
    	$sug = get_term_from_id($id);
    	if ($i == 0) {
    			$suggestions = "$sug";
    		} else {
    			$suggestions = "$suggestions, $id $sug";
    		}
    }
    //echo $suggestions;
	return $suggestions;
}

function term_exist_in_BD_p($term) {
    $query =  "SELECT id FROM `Nodes` WHERE Nodes.name = \"$term\";"   ;
    //echo $query;
    $r =  @mysql_query($query) or die("bug in term_exist_in_BD_p : $query");
    $nb = mysql_num_rows($r);
    if ($nb == 0) {return $nb;} else {return mysql_result($r , 0 , 0);}

    // si 0 alors exist po !
}

function get_term_from_id($id) {
    $query =  "SELECT name FROM `Nodes` WHERE Nodes.id = \"$id\";"   ;
    $r =  @mysql_query($query) or die("bug in get_term_id : $query");
    $nb = mysql_num_rows($r);
    if ($nb == 0) {return -1;} else {return mysql_result($r , 0 , 0);}

    // si -1 alors exist po !
}

//--------------------- DISPLAY


function display_warning ($text) {
    echo "<div class=\"jdm-warning\">$text</div>";
}

function check_busted() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], "MSIE") <> "") 
    {
    $_SESSION[ssig() . 'BUSTED'] = "Y";
    //$_SESSION[ssig() . 'BUSTED'] = "Y";
    //echo "BUSTED!";
    }
}

function busted_warning() {
    if ($_SESSION[ssig() . 'BUSTED'] <> "") {
    echo "<h1 style='text-align:center;'>";
    display_warning("Ce jeu ne fonctionne pas avec Internet Explorer - Utilisez <a href=\"http://www.mozilla.org/\">Firefox</a>");
    echo "</h1>";
    }
}

//---------------------
// obsolete
//
function myspacer ($w, $h) {
    if (strpos($_SERVER['HTTP_USER_AGENT'], "MSIE") <> "") 
    {
	echo "<IMG SRC=\"SPACE.GIF\" NOSAVE BORDER=0 HEIGHT=$h WIDTH=$w>";
    } else {
	echo "<spacer type=\"block\" width=\"$w\" height=\"$h\">";
    }
}

//---------------------

function jdmLogo_old(){
	echo "<div class=\"jdm-logo-block\">";
    echo "<a href=\"jdm-accueil.php\"><font size=\"5\"><font color=\"#ADD8E6\">J</font><font color=\"red\">e</font><font color=\"yellow\">u</font><font color=\"#ADD8E6\">x</font>
<font color=\"lightgreen\">D</font><font color=\"red\">e</font>
<font color=\"#ADD8E6\">M</font><font color=\"red\">o</font><font color=\"yellow\">t</font><font color=\"#ADD8E6\">s</font></font></a>";

    echo "</div\">";
    }

function jdmLogo(){
	echo "\n<div class=\"jdm-logo-block\">";
	echo "\n<div class=\"jdm-logo\">";
	echo "<a BORDER=0 href=\"world-of-jeuxdemots.php\">";
	$url = $_SESSION[ssig() . 'JDM-LOGO-URL'];
	//echo "<img BORDER=0 width=\"200\" alt=\"JeuxDeMots\" src=\"$url\">";
	echo "&nbsp;";
	//echo "Salut le monde";
	echo "</a>";
    echo "\n</div>";
    echo "\n</div>";
    }

    
function playerinfoblock($playerid) {
	echo "\n<div id=\"jdm-player-info-block-shadow\">";
    echo "\n<div id=\"jdm-player-info-block\">";
    echo "\n<div id=\"jdm-player-info-block-contents\">";
	playerinfoblock_contents($playerid);
    echo "\n</div>";
    echo "\n</div>";
    echo "\n</div>";
    
    echo "\n<div class=\"jdm-pin-player-stats\">";
	echo "</div>";
}

function playerinfoblock_contents($playerid, $utf8=false) {

	$rez = '';
    // direct pour reaison d'efficacité
    //
    $query = "SELECT credit,honnor,level,name FROM `Players`  WHERE id= '$playerid' ;";
    $r =  @mysql_query($query) or die("bug in playerinfoblock : $query");
    $credit = mysql_result($r , 0 , 0);
    $honnor = mysql_result($r , 0 , 1);
    $level = mysql_result($r , 0 , 2);
	$name = mysql_result($r , 0 , 3);
	
	if ($_SERVER['PHP_SELF'] == '/jeuxdemots/jdm-accueil.php') {
	 $query = "SELECT sum(honnor)FROM `Players` WHERE 1";
   	 $r2 =  @mysql_query($query) or die("bug in playerinfoblock : $query");
	 $sum = mysql_result($r2 , 0 , 0);
	 $percent = ($honnor/$sum)*100;
	} 
	
    $rez = $rez . $name;
    $rez = $rez . "<BR/>";
    $rez = $rez . get_msg('MISC-CREDIT');
    $rez = $rez . $credit; 
    $rez = $rez . "<BR/>"; 
    $rez = $rez . get_msg('MISC-HONNOR');
    if ($_SERVER['PHP_SELF'] == '/jeuxdemots/jdm-accueil.php') {
   		$rez = $rez . "<span onMouseover=\"ddrivetip('$percent%','yellow', 200)\";
				onMouseout=\"hideddrivetip()\">";
    } 
    $rez = $rez . $honnor;
    if ($_SERVER['PHP_SELF'] == '/jeuxdemots/jdm-accueil.php') {
    	$rez = $rez . "</span>";
    }
    $rez = $rez . "<BR/>"; 
    $rez = $rez . get_msg('MISC-LEVEL');
    $rez = $rez . round($level);
    
    if ($utf8) {
    	echo utf8_encode($rez);
    } else {
    		echo $rez;
    }
}


//---------------------

function target_term_info_block() {
    echo "<div class=\"jdm-target-info-block\">";
    echo get_msg('GENGAME-PROMPT-WORDLEVEL') . round($_SESSION[ssig() . 'gameentrylevel']) . "";
    echo "</div>";
}

//---------------------
function display_target() {
	    if ($_SESSION[ssig() . 'hideterm'] == "Y") {
		    		echo  "<div id=\"jdm-gameentry-block\">&nbsp;</div>"; 
		} 
		else {
			$color = $_SESSION[ssig() . 'termcolor'];
								
		$annotation = '';
		if (!empty($_SESSION[ssig() . 'gameannotation'])) {
			$annotation = $_SESSION[ssig() . 'gameannotation'];
		}
		$entry = format_entry($_SESSION[ssig() . 'gameentry']);
		if ($entry == $annotation) {
			$annotation = '';
		}
		if (test_playerp() !== "Y") {
			echo "<font color=\"$color\">";
		} 
		echo "<div id=\"jdm-gameentry-block\"><div id=\"jdm-gameentry-block-text\">";
		if (!empty($annotation)) {
			echo "<ruby><rb>";
		}
		if (!empty($_SESSION[ssig() . 'gamegloss'])) {
			echo "<abbr title=\"",$_SESSION[ssig() . 'gamegloss'],"\">";
		}
		echo "&nbsp;",$entry,"&nbsp;";
		if (!empty($_SESSION[ssig() . 'gamegloss'])) {
			echo "</abbr>";
		}
		if (!empty($annotation)) {
			echo "</rb>";
			echo "<rp>(</rp><rt>",$annotation,"</rt><rp>)</rp>";
			echo "</ruby>";
		}
		echo "</div></div>";
		if (test_playerp() !== "Y") {
			echo "</font>";
		} 
	}
}


function anon_entry ($entry) {
	return (strpos($entry, '::') === 0);
}


function select_gender_number($id) {
	$query = "SELECT count(id) FROM Relations WHERE 
	type = 4
	and node1 = $id
	and node2 in (SELECT id FROM Nodes WHERE type = 4 and name LIKE \"Nom:%PL\")
	";
	$r =  @mysql_query($query) or die("pb select_gender_number 1 : $query");
    $nb= mysql_result($r , 0 , 0);
    if ($nb > 0) {return "des";}
        
	$query = "SELECT count(id) FROM Relations WHERE 
	type = 4
	and node1 = $id
	and node2 in (SELECT id FROM Nodes WHERE type = 4 and name like \"Nom:Fem%SG\" or name like \"Adj:Fem%SG\" )";
	$r =  @mysql_query($query) or die("pb select_gender_number 1 : $query");
    $nb= mysql_result($r , 0 , 0);
    if ($nb > 0) {return "une";}
    
	$query = "SELECT count(id) FROM Relations WHERE 
	type = 4
	and node1 = $id
	and node2 in (SELECT id FROM Nodes WHERE type = 4 and name LIKE \"Nom:Mas%SG\" or name like \"Adj:Mas%SG\")";
	$r =  @mysql_query($query) or die("pb select_gender_number 1 : $query");
    $nb= mysql_result($r , 0 , 0);
    if ($nb > 0) {return "un";}
    

    
	$query = "SELECT count(id) FROM Relations WHERE 
	type = 4
	and node1 = $id
	and node2 in (SELECT id FROM Nodes WHERE type = 4 and name LIKE \"Nom:%Mas%SG\" or name LIKE \"Nom:%Fem%SG\"
	or name LIKE \"Nom:%InvGen%SG\"
	)";
	$r =  @mysql_query($query) or die("pb select_gender_number 1 : $query");
    $nb= mysql_result($r , 0 , 0);
    if ($nb > 0) {return "un(e)";}
    
    return "";
}

function format_entry_wordlist ($entry) {
	
}

function format_entry_colon ($entry) {
	if (strpos($entry, '::?') === 0) {
		return format_entry_wordlist ($entry);
	}
	if (strpos($entry, '::') === 0) {
		$pieces = explode('>', $entry);
		$count = count($pieces);
		//print_r($pieces);
		$s = "";
		// cas ternaire
		if ($count == 4) {
			$r1_pieces = explode(':', $pieces[1]);
			$r2_pieces = explode(':', $pieces[2]);
			//print_r($r1_pieces);
			//print_r($r2_pieces);
			//echo  $r1_pieces[0];
			//echo $r2_pieces[0];
			//echo $pieces[3];
			
			$article = select_gender_number($r1_pieces[1]);
			if ($article == "des") {
				$conjonc = "que des ";
				$pouvoir = "pourraient";
			} else {
				$conjonc = "qu'" . $article;
				$pouvoir = "pourrait";
			}
			
			
			
			$oldstr = array("qu' quelqu'un", "un quelqu'un", "qu'quelqu'un");
			$newstr = array("quelqu'un", "quelqu'un", "que quelqu'un"); 
			
			// PRED=29 AGT=13 PATIENT=14 INSTR=16 LIEU=15
			// AGENT+PRED
			// Q = PATIENT
			if (($r1_pieces[0] == '13') and ($r2_pieces[0] == '29') and ($pieces[3] == '14')) {
				$agt = get_term_from_id($r1_pieces[1]);
				$pred = get_term_from_id($r2_pieces[1]);
				$s = "Qu'est-ce $conjonc $agt $pouvoir $pred ?";
				//debugecho("S1 = $s");
				$s = str_replace($oldstr, $newstr, $s);
				//debugecho("S2 = $s");
				return $s;	
			}
			// Q = INSTR
			if (($r1_pieces[0] == '13') and ($r2_pieces[0] == '29') and ($pieces[3] == '16')) {
				$agt = get_term_from_id($r1_pieces[1]);
				$pred = get_term_from_id($r2_pieces[1]);
				$s = "Avec quoi $article $agt $pouvoir $pred ?";
				$s = str_replace($oldstr, $newstr, $s);
				return $s;	
			}
			// Q = LIEU
			if (($r1_pieces[0] == '13') and ($r2_pieces[0] == '29') and ($pieces[3] == '15')) {
				$agt = get_term_from_id($r1_pieces[1]);
				$pred = get_term_from_id($r2_pieces[1]);
				$s = "Où $article $agt $pouvoir $pred ?";
				$s = str_replace($oldstr, $newstr, $s);
				return $s;	
			}
			// Q = MANIERE
			if (($r1_pieces[0] == '13') and ($r2_pieces[0] == '29') and ($pieces[3] == '34')) {
				$agt = get_term_from_id($r1_pieces[1]);
				$pred = get_term_from_id($r2_pieces[1]);
				$s = "De quelle manière $article $agt $pouvoir $pred ?";
				$s = str_replace($oldstr, $newstr, $s);
				return $s;	
			}
			
			// PATIENT+PRED
			// Q=AGENT
			if (($r1_pieces[0] == '14') and ($r2_pieces[0] == '29') and ($pieces[3] == '13')) {
				$patient = get_term_from_id($r1_pieces[1]);
				$pred = get_term_from_id($r2_pieces[1]);
				$s = "Qui pourrait $pred $article $patient ?";
				
				$s = str_replace($oldstr, $newstr, $s);
				return $s;	
			}
			// Q=INSTR
			if (($r1_pieces[0] == '14') and ($r2_pieces[0] == '29') and ($pieces[3] == '16')) {
				$patient = get_term_from_id($r1_pieces[1]);
				$pred = get_term_from_id($r2_pieces[1]);
				$s = "Avec quoi pourrait-on $pred $article $patient ?";
				$s = str_replace($oldstr, $newstr, $s);
				return $s;	
			}
			// Q=LIEU
			if (($r1_pieces[0] == '14') and ($r2_pieces[0] == '29') and ($pieces[3] == '15')) {
				$patient = get_term_from_id($r1_pieces[1]);
				$pred = get_term_from_id($r2_pieces[1]);
				$s = "Où pourrait-on $pred $article $patient ?";
				//debugecho($s);
				$s = str_replace($oldstr, $newstr, $s);
				//debugecho($s);
				return $s;	
			}
			// Q=MANIERE
			if (($r1_pieces[0] == '14') and ($r2_pieces[0] == '29') and ($pieces[3] == '34')) {
				$patient = get_term_from_id($r1_pieces[1]);
				$pred = get_term_from_id($r2_pieces[1]);
				$s = "De quelle manière pourrait-on $pred $article $patient ?";
				//debugecho($s);
				$s = str_replace($oldstr, $newstr, $s);
				//debugecho($s);
				return $s;	
			}
			
			// INSTR+PRED
			// Q=AGENT
			if (($r1_pieces[0] == '16') and ($r2_pieces[0] == '29') and ($pieces[3] == '13')) {
				$instr = get_term_from_id($r1_pieces[1]);
				$pred = get_term_from_id($r2_pieces[1]);
				$s = "Qui pourrait $pred avec $article $instr ?";
				$s = str_replace($oldstr, $newstr, $s);
				return $s;	
			}
			// Q=PATIENT
			if (($r1_pieces[0] == '16') and ($r2_pieces[0] == '29') and ($pieces[3] == '14')) {
				$instr = get_term_from_id($r1_pieces[1]);
				$pred = get_term_from_id($r2_pieces[1]);
				$s = "Que pourrait-on $pred avec $article $instr ?";
				$s = str_replace($oldstr, $newstr, $s);
				return $s;	
			}
			// Q=LIEU
			if (($r1_pieces[0] == '16') and ($r2_pieces[0] == '29') and ($pieces[3] == '15')) {
				$instr = get_term_from_id($r1_pieces[1]);
				$pred = get_term_from_id($r2_pieces[1]);
				$s = "Où pourrait-on $pred avec $article $instr ?";
				$s = str_replace($oldstr, $newstr, $s);
				return $s;	
			}
			// Q=MANIERE
			if (($r1_pieces[0] == '16') and ($r2_pieces[0] == '29') and ($pieces[3] == '34')) {
				$instr = get_term_from_id($r1_pieces[1]);
				$pred = get_term_from_id($r2_pieces[1]);
				$s = "De quelle manière pourrait-on $pred avec $article $instr ?";
				$s = str_replace($oldstr, $newstr, $s);
				return $s;	
			}
			
			return "--wrong formating--";
		}
		
		
	}
}


function generate_before_tag($pos, $posid) {
	//debugecho("generate_before_tag $pos, $posid");
	if ($posid == 146881) {
		return "une/la";
	}
	if ($posid == 146885) {
		return "un/le";
	}
	
	if ("$pos" == "Nom:Mas+InvPL") {return "un/des";}
	if ("$pos" == "Nom:Fem+InvPL") {return "une/des";}
	if ("$pos" == "Nom:InvGen+SG") {return "un/une";}
	if ("$pos" == "Nom:InvGen+PL") {return "des";}
	
	if ((strpos("$pos", "Nom:") === 0) &&
		(strpos("$pos", "PL") >= 0)) {
		return "des";
		}
	return '';
}


function normalize_POS_tag($pos) {
	if (strpos("$pos", "Adj:") === 0) {return "Adj";}
	if (strpos("$pos", "Ver:") === 0) {return "Ver";}
	if ("$pos" == "Int") {return " !!! ";}
	if (strpos("$pos", "Det:") === 0) {return "Déterminant";}
	if (strpos("$pos", "Pro:") === 0) {return "Pronom";}
	if ("$pos" == "Pre") {return "Préposition";}
	if ("$pos" == "Con") {return "Conjonction";}
	
	return $pos;
}

// formating for game prompt
//
function format_entry($entry) {
	
	if (strpos($entry, '::') === 0) {
		return format_entry_colon ($entry);	
	}
	
	if (strpos($entry, '>')) {
		$pieces = explode('>', $entry);
		$count = count($pieces);
		//print_r($pieces);
		//return $pieces[0] . " (" . get_term_from_id($pieces[1]) . ")";
		$s = $pieces[0] . " <div class=\"jdm-term-context\">" ;
		$prems = true;
		for ($i = 1; $i < $count; $i++) {
			$pos = get_term_from_id($pieces[$i]);
			$tag = generate_before_tag($pos, $pieces[$i]);
			if ($tag != '') {
				$s = " <div class=\"jdm-term-context\">" . $tag . "</div> " . $s;
			} else {
				//echo "coucou";
				if ($prems) {$par =" ("; $prems = false;} else {$par =", ";}
    			$s = $s . $par . normalize_POS_tag($pos);
			}
		}
		if ($prems) {
			$s = $s . "</div>";
		} else {
			$s = $s . ")</div>";
		}
		
		$oldstr = array("\\");
		$newstr = array(""); 
		$news = str_replace($oldstr, $newstr, $s);
		return $s ;	
	}	
	return $entry;
}

// formatting for event and mail
//
function format2_entry($entry) {
	if (strpos($entry, '::') === 0) {
		return format_entry_colon ($entry);
	}
	//echo "entry='$entry'<P>";
	if (strpos($entry, '>')) {
		$pieces = explode('>', $entry);
		$count = count($pieces);
		//print_r($pieces);
		//return $pieces[0] . " (" . get_term_from_id($pieces[1]) . ")";
		$s = $pieces[0] . ">" . get_term_from_id($pieces[1]);
		
		for ($i = 2; $i < $count; $i++) {
    		$s = $s . ">" . get_term_from_id($pieces[$i]);
		}
		return $s ;	
	}
		
	return $entry;
	
}

//---------------------

function loginblock()
    {if (guestplayerp() != "Y") {
	    //$GLOBALS[0]=$_SESSION[ssig() . 'login'];
	    //echo "<div class=\"jdm-login-block\">" ; 
	    echo "<a href=\"jdm-accueil.php?logout=1\">" . get_msg('MISC-LOGIN') . "</a>"; 
	    //echo "</div>";
	} else {
		//echo "<div class=\"jdm-login-block\">" ; 
	    echo "<a href=\"jdm-signin.php\">" . get_msg('MISC-LOGOUT') . "</a>";
	   // echo "</div>";
	}
    }

    
function make_splach() {
	$x = rand(0,90);
	$y = rand(0,90);
	$size = rand(20,400);
	$n = rand(1, 11);
	echo "\n<div class=\"jdm-splatch\"
	style=\"position:fixed;left:$x%;top:$y%;z-index:1000;opacity:0.8\">\n";
	echo "<img HEIGHT=\"$size\"  
		src=\"pics/taches/clean_muddy_ink_stain$n.png\">";
	echo "\n</div>\n";
}


//---------------------
function topblock_old() {
    if (session_down_p() == "Y") {
    echo "<script language=\"JavaScript1.2\"> 
	    window.setTimeout(\"location=('jdm-accueil.php');\",1) 
	    </script>  ";
	return "session-finished";
    }
    // <img height=\"148\" width=\"4\" title=\"LIRMM\" alt=\"LIRMM\" rc=\"\">
    // src=\"http://www.lirmm.fr/xml/images2/home/bleujaune.gif\"
   $logo_url = $_SESSION[ssig() . 'JDM-LAB-LOGO-URL'];
   
   echo "\n<div class=\"jdm-top-block\">\n";
   
   echo "\n
   		<div class=\"jdm-lirmm-block\">
		<div id=\"jdm-lirmm-logo\">
		  <a BORDER=0 href=\"http://www.lirmm.fr\">
		  <img BORDER=0 width = \"62\" title=\"LIRMM\" alt=\"LIRMM\" src=\"$logo_url\">
		</a>
		</div>
		</div>";
   
	echo "\n<div class=\"jdm-top-menu-block\">";
    jdm_menu("T");
    echo "\n</div>";
    echo "\n<td width=\"100\" align=\"left\">
</table>" ;
	echo "\n</div>";
}

function topblock() {
	start_time_record("page_loading_time");
	if ($_SESSION[ssig() . 'redirect_if_session_finished'] == 'N') {} else {
    	if (session_down_p() == "Y") {
    	echo "<script language=\"JavaScript1.2\"> 
		    window.setTimeout(\"location=('jdm-accueil.php');\",1) 
		    </script>  ";
		return "session-finished";
   		}
	}

   $logo_url = $_SESSION[ssig() . 'JDM-LAB-LOGO-URL'];
   

  
   echo "\n<div id=\"jdm-top-block-shadow\">\n";
   echo "\n<div id=\"jdm-top-block\">\n";
   
   echo "\n
   		<div class=\"jdm-lirmm-logo-block\">
		<div id=\"jdm-lirmm-logo\">
		<a BORDER=0 href=\"http://www.lirmm.fr\">
		<img BORDER=0 width = \"62\" title=\"LIRMM\" alt=\"LIRMM\" src=\"$logo_url\">
		</a>
		</div>
		</div>
		";
   
   	jdmLogo();
   	
	echo "\n<div class=\"jdm-top-menu-block\">";
    jdm_menu("T");
    echo "\n</div>";
	echo "\n</div>";
	echo "\n</div>";
	//guest_arrows();
	//
	echo "\n<img STYLE=\"opacity:0.4;width:1000px;height:200px;z-index:-1;position:absolute;top:-50px;left:-500px;\"  src=\"pics/circle.png\">";
	
}


function guest_arrows() {
	if (guestplayerp() == "Y") {
      	echo "\n<div style=\"position:absolute;top:50px;right:20%;\">";
		echo "<img src=\"pics/flechegauchecentre.png\" width=\"100px\">";
		echo "\n</div>";
	}
}

function jdm_menu($tag) {
    $file =  $_SERVER['PHP_SELF'];
 
    $lang = $_SESSION["LANG"];

    echo "\n<div class=\"jdm-menu\">\n";
    if ($file != "/jeuxdemots/jdm-accueil.php") {
    	echo "<div id=\"jdm-menu-item-home-$tag\"><nobr><a href=\"jdm-accueil.php\">" . get_msg('MISC-HOME') . "</a></nobr></div>\n";   
    }
    if (stripos($file, "generateRanking") === false) {
    	echo "<div id=\"jdm-menu-item-ranking-$tag\"><nobr><a href=\"generateRanking-4.php\">" . get_msg('MISC-RANKING') . "</a></nobr></div>\n"; 
    }
    
    
    if ($file != "/jeuxdemots/generateEvents.php") {
    	echo "<div id=\"jdm-menu-item-events-$tag\"><nobr><a href=\"generateEvents.php\">" . get_msg('MISC-EVENTS') . "</a></nobr></div>\n"; 
    }
    if ($file != "/jeuxdemots/souk.php") {
    	echo "<div id=\"jdm-menu-item-souk-$tag\"><nobr><a href=\"souk.php\">" . get_msg('MISC-SOUK') . "</a></nobr></div>\n"; 
    }
    echo "<br><div id=\"jdm-menu-item-contact-$tag\"><nobr><a href=\"mailto:" .  $_SESSION[ssig() . 'CONTACT-EMAIL'] . "?subject=JeuxDeMots fedback\" >" . get_msg('MISC-CONTACT') . "</a></nobr></div>\n";
    echo "<div id=\"jdm-menu-item-forum-$tag\"><nobr><a href=\"" . $_SESSION[ssig() . 'FORUM-URL'] . "\" >" . get_msg('MISC-FORUM') . "</a></nobr></div>\n"; 
    
    if ($file != "/jeuxdemots/jdm-options.php") {
    	echo "<div id=\"jdm-menu-item-options-$tag\"><nobr><a href=\"jdm-options.php\">" . get_msg('MISC-OPTIONS') . "</a></nobr></div>\n"; 
    }
    if (stripos($file, "HELP") === false) {
    	echo "<div id=\"jdm-menu-item-help-$tag\"><nobr><a href=\"HELP-$lang.php\">" . get_msg('MISC-HELP') . "</a></nobr></div>\n"; 
    }
    echo "</div>\n";

    if ($_SESSION[ssig() . 'debugmode'] != "") 
	{display_warning(" --- some maintenance is undertaken - things may behave strangely...");}

    if ($_SESSION[ssig() . 'message'] != "") {
	//echo "<br>Message : " . $_SESSION[ssig() . 'message'] . "";
	}
}

//---------------------

function curPageName() {
 return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
}

function bottomblock() {

	//echo "<div class=\"jdm-bottom-block\">\n";
   // echo "<div class=\"jdm-bottom-menu-block\">\n";
   // jdm_menu("B");
   // echo "</div>\n";
   // echo "</div>\n";
    // code analytics
    //
    //if (curPageName() != "generateGames.php") {
	//	include_once  'ANALYTICS-' . $_SESSION["LANG"] . '.php';
    //}
	
	//echo curPageName();
	
    $file =  $_SERVER['PHP_SELF'];
    if (stripos($file, "generateGames") === false) {
    	include_once('ANALYTICS.php');
    	display_twick();
    }
    
    echo "<div id=\"jdm-debug-block-time\">";
    echo round(end_time_record("page_loading_time"), 3);
    echo " s</div>";
}

function insert_graphic_div($n, $tag) {
	for ($i=0 ; $i<$n ; $i++) {
		echo "\n<div class=\"jdm-pin-$tag-$i\">";
		echo "</div>";
	}
	for ($i=0 ; $i<$n ; $i++) {
		echo "\n<div class=\"jdm-graphic-$tag-$i\">";
		echo "</div>";
	}
}
//---------------------

function produceplayasform ($loc='normal') {
    $tag = "";
    if (($_SESSION[ssig() . 'harddebugmode']=="Y") or ($_SESSION[ssig() . 'BUSTED'] <> "")) {
	if (test_playerp() !="Y")
	    {$tag = "disabled"; } 
    }
	
    if ($loc == 'normal') {echo "<div id=\"playas-form\">";}
	if ($loc == 'home') {echo "<div id=\"playas-form-home\">";}
	// Thème désactivé. À étudier comment ça marche
    /*$rand = rand(0,100);
    if ($_SESSION[ssig() . 'playerid'] == 0) {$rand = 100;}
    
    $thema = get_thema($_SESSION[ssig() . 'playerid']);
    if ($thema != -1) {
    	$rand = 9;
    	$thema_target = get_term_from_id($thema);
    } else {
    	$thema_target = "oiseau";
    }
 	switch(true) 
	{
  		case ($rand < 2): 
  		echo "\n<form  method=\"post\" action=\"QGEN.php\" >";
		$_SESSION[ssig() . 'special_question_permisson'] = True;
   		break;

  		case ($rand < 4): 
  		echo "\n<form  method=\"post\" action=\"TGEN.php\" >";
			$_SESSION[ssig() . 'special_question_permisson'] = True;
   		break;
   		
   		case ($rand < 6): 
  		echo "\n<form  method=\"post\" action=\"TGEN-concept.php\" >";
			$_SESSION[ssig() . 'special_question_permisson'] = True;
   		break;
   		
   		case ($rand < 10): 
  		echo "\n<form  method=\"post\" action=\"TGEN-domain.php?thema=$thema_target\" >";
			$_SESSION[ssig() . 'special_question_permisson'] = True;
   		break;

  		default: 
 		echo "\n<form  method=\"post\" action=\"generateGames.php\" >";
    	break;
    	
    	
	}
    */
     		echo "\n<form  method=\"post\" action=\"generateGames.php\" >";

	
    //echo "\n<input $tag type=\"submit\" name=\"generateGames\" value=\"" . get_msg('MISC-PLAYAS-PLAY');
 //$('anotherElement').beat(2,30);
 	$msg = get_msg('MISC-PLAYAS-PLAY');
	echo "\n<div id=\"jdm_play_button\" onmouseover=\"$('jdm_play_button').shake(3);return false;\">";
	echo "<input $tag type=\"image\" style=\"width:80px;\" SRC=\"",get_msg('HOME-PLAY-BUTTON'),"\" name=\"generateGames\" value=\"$msg\">";
	echo "</div>";
	
    echo "\n<input $tag type=\"hidden\" name=\"playasname\" value=\"" . $_SESSION[ssig() . 'login'] . "\">";
    echo "\n</form>";
    echo "\n</div>";
    
    if ($_GET['playasname'] != $_SESSION[ssig() . 'login']) {
	//echo "<P><font color=\"red\" size=\"2\">";
	//echo "Vous n'êtes plus connecté.";
	//echo "</font>";
    }
    if (guestplayerp() != "Y") {
       // $_SESSION[ssig() . 'login'] = "guest";
	} else	{
		if ($loc == 'home') {
			echo "\n<div id=\"jdm-guest-warning-block\">";
			echo "\n<div id=\"jdm-guest-warning\">";
			echo get_msg('MISC-PLAYAS-GUESTWARNING');
    		echo "\n</div>\n</div>";
		}
	}
}


//--------------------- RELATIONS AND COTATIONS
//

function get_relation_gpname($relid) {
    $query = "SELECT name FROM `RelationTypes`  WHERE id= '$relid' ;";
    $r =  @mysql_query($query) or die("bug in get_relation_gpname : $query");
    $name = mysql_result($r , 0 , 0);
    return get_msg($name);
}

function active_relations() {
    $l = array(0, 5, 8, 3, 6, 7, 9, 10, 15, 17, 11, 22, 20, 21, 34, 13, 14, 16, 32, 35, 41, 42, 37, 38);
    return $l;
    }

function all_relations() {
    $l = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    	10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
    	20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
    	30, 31, 32, 33, 34, 35, 36, 37, 38, 39);
    return $l;
    }

function adjust_relation_cotations($relid, $f) {
    // echo "adjust_relation_cotations($relid, $f)";
    
    $l = active_relations();
    $nb = count($l);

    $query = "SELECT quot, quotmin, quotmax FROM RelationTypes WHERE id= '$relid' ;";
    $r =  @mysql_query($query) or die("pb in adjust_cotation 1 : $query");
    $quot = mysql_result($r , 0 , 0);
    $quotmin = mysql_result($r , 0 , 1);
	$quotmax = mysql_result($r , 0 , 2);
	
    $adjust = (2 + (2 * $f));
    //if (($quot >= $quotmin) AND ($quot <= $quotax)) {

		$query = "UPDATE `RelationTypes` SET quot = quot-$adjust WHERE id= '$relid' ;";
		$r =  @mysql_query($query) or die("pb in adjust_cotation 2 : $query");
		$val = (1 + $f) / $nb ;
			
		for ($i=0 ; $i<$nb ; $i++) {
	     	$rel = $l[$i];
	     	if ($rel != $relid){
				$query = "UPDATE `RelationTypes` SET quot = quot + $val WHERE id= '$rel' ;";
				$r =  @mysql_query($query) or die("pb in adjust_cotation 3 : $query");
	   	 }
		}
    //}
    
	$l = all_relations();
    $nb = count($l);
	for ($i=0 ; $i<$nb ; $i++) {
		$rel = $l[$i];
	    $query = "SELECT quot, quotmin, quotmax FROM RelationTypes WHERE id= '$rel' ;";
   		$r =  @mysql_query($query) or die("pb in adjust_cotation 1 : $query");
	    $nbrows = mysql_num_rows($r);
	    if ($nbrows > 0) {
    		$quot = mysql_result($r , 0 , 0);
    		$quotmin = mysql_result($r , 0 , 1);
			$quotmax = mysql_result($r , 0 , 2);
	     	
	  		if ($quot < $quotmin) {
    			$query = "UPDATE `RelationTypes` SET quot = $quotmin WHERE id= '$rel' ;";
				$r =  @mysql_query($query) or die("pb in adjust_cotation 3 : $query");
   			}
    		if ($quot > $quotmax) {
    			$query = "UPDATE `RelationTypes` SET quot = $quotmax WHERE id= '$rel' ;";
				$r =  @mysql_query($query) or die("pb in adjust_cotation 3 : $query");
    		}
	    }
	}
	
 }


function compute_rel_cotation_ratio($relid) {
    $query = "SELECT quot FROM RelationTypes WHERE id = '$relid'";
    $r =  @mysql_query($query) or die("pb in compute_rel_cotation_ratio : $query");
    $cotation = mysql_result($r , 0 , 0);

    return $cotation/1000;
}


//---------------------

//--------------------- EVENTS


function make_event($string) {
    $query = "INSERT INTO `Events` (`what`) VALUES (\"$string\");";
    $r =  @mysql_query($query) or die("merdum in make_event : $query");
    
    add_twit('event', $string);
    }



//--------------------- MESSAGES AND LOCALIZATION

function safe_echo ($str) {
	echo htmlentities($str);
}

function get_msg($s){
	
    $tab = array("\'", 
    			"<0>", "<1>", "<2>", "<3>", "<4>", "<5>", "<6>", "<7>", "<8>", "<9>",
    			 "<10>", "<11>", "<12>", "<13>", "<14>", "<15>", "<16>", "<17>", "<18>", "<19>");
    $glob = array("&#39;",
    				 $GLOBALS[0], $GLOBALS[1], $GLOBALS[2], $GLOBALS[3], $GLOBALS[4], 
    				$GLOBALS[5],$GLOBALS[6], $GLOBALS[7], $GLOBALS[8],$GLOBALS[9],
    				$GLOBALS[10], $GLOBALS[11], $GLOBALS[12], $GLOBALS[13], $GLOBALS[14], 
    				$GLOBALS[15],$GLOBALS[16], $GLOBALS[17], $GLOBALS[18],$GLOBALS[19]);

	$tag = $_SESSION["LANG"]."-".$s;
    $string = $GLOBALS[$tag];
    
    if ($string === NULL) {
    	$string = "[get_msg:string not found:$tag]";
    } else {
    	$string = str_replace($tab,$glob,$string);
    }
    return $string;
}


//--------------------- MOST WANTED TERMS


function display_mostwanted () {
	
	$time = microtime_float();
	//echo $time;
	$rtime = round($time);
	//echo $rtime;
	$query = "SELECT count(id) FROM WantedWords WHERE (date-$rtime)>0";
    $r =  @mysql_query($query) or die("pb display_mostwanted 0 : $query");
	$nb = mysql_result($r , 0 , 0);
	if ($nb < 7) {
		$query = "SELECT id FROM WantedWords WHERE (date-$rtime)<0 ORDER BY Rand() Limit 1";
    	$r =  @mysql_query($query) or die("pb display_mostwanted 0bis : $query");
    	$id = mysql_result($r , 0 , 0);
    	//echo "<br>$id";
    	
    	$value = 50000;
    	$endtime = round($time + 60*$value);
    	$query = "UPDATE `WantedWords` SET date = $endtime WHERE id= $id";
    	$r =  @mysql_query($query) or die("pb display_mostwanted 0bis : $query");
	}
	
	
	$query = "SELECT id,date FROM WantedWords ORDER BY `date` DESC ";
    $r =  @mysql_query($query) or die("pb display_mostwanted 1 : $query");
    
    
	for ($i=0 ; $i<mysql_num_rows($r) ; $i++) {
      	$termid = mysql_result($r , $i , 0);
    	$date = mysql_result($r , $i , 1);
 
    	$notfree = check_term_owners($termid);
    	if ($notfree) {
    		$tag = "déjà capturé";
    	} else {
    		$tag = "libre";
    	}
    	
    	$value = round(($date - $time)/60);
    	$term = get_term_from_id($termid);
    	if ($value > 0){
    		echo"<TR><TH width=\"100%\"><span 
				onMouseover=\"ddrivetip('$value Cr - $tag','yellow', 200)\";
				onMouseout=\"hideddrivetip()\"><div class=\"jdm-postit-mw-term\">'$term'</div>
				</span>";
    	}
	}
}

function check_mostwanted($termid) {
	$query = "SELECT id, date FROM `WantedWords` WHERE id=$termid;";
	//echo "query=$query";
	$r =  @mysql_query($query) or die("pb check_mostwanted 1: $query");
	$nb = mysql_num_rows($r);
	if ($nb <= 0) {return 0 ;}
	
    $date = mysql_result($r , 0 , 1);	
	$time = microtime_float();
	$value = round(($date - $time)/60);
	if ($value <= 0) {return 0 ;}
	return $value;
}

function display_collection() {
	$query = "SELECT id, name,
			t1, t2, t3, t4, t5, t6, t7, t8, t9, t10 
			FROM Collections ORDER BY name";
    $r =  @mysql_query($query) or die("pb display_collection 1 : $query");
	for ($i=0 ; $i<mysql_num_rows($r) ; $i++) {
      	$cid = mysql_result($r , $i , 0);
    	$cname = mysql_result($r , $i , 1);
    	$t1 = mysql_result($r , $i , 2);
    	$t2 = mysql_result($r , $i , 3);
    	$t3 = mysql_result($r , $i , 4);
    	$t4 = mysql_result($r , $i , 5);
    	$t5 = mysql_result($r , $i , 6);
    	$t6 = mysql_result($r , $i , 7);
    	$t7 = mysql_result($r , $i , 8);
    	$t8 = mysql_result($r , $i , 9);
    	$t9 = mysql_result($r , $i , 10);
    	$t10 = mysql_result($r , $i , 11);
    	
    	$string = "";
    	for ($j=2 ; $j<=11 ; $j++) {
    		$s = mysql_result($r , $i , $j);
    		if ($s != "") {
    			$string = $string . $s . " - ";
    		}
    	}
    
    	//$string = trim("$t1 $t2 $t3 $t4 $t5 $t6 $t7 $t8 $t9 $t10");
    	//echo "$string";
    	
		echo"
<span onMouseover=\"ddrivetip('$string','yellow', 300)\";
	  onMouseout=\"hideddrivetip()\">
<div class=\"jdm-postit-collection-item\">'$cname'</div>
</span>\n";
    	
	}
}

//--------------------- INVOCATION - HYPERWORDS

function get_invok_number($playerid) {
	$query = "SELECT COUNT(name)  FROM `HypeWords` WHERE `owner` = $playerid and token != 0";
	$r =  @mysql_query($query) or die("bug in get_invok_number : $query");
	return mysql_result($r , 0 , 0);
}

function reward_invok_term($entry, $relid) {
	$query = "SELECT owner, token, gain, relationType FROM `HypeWords` WHERE `name` = \"$entry\"";
	$r =  @mysql_query($query) or die("bug in reward_invok_term : $query");
	
	$nb = mysql_num_rows($r);
	if ($nb <= 0) {return;}
	
	$owner = mysql_result($r , 0 , 0);
	$token = mysql_result($r , 0 , 1);
	$gain = mysql_result($r , 0 , 2);
	$relationType = mysql_result($r , 0 , 3);
	
	if (($token > 0)&&($relid==$relationType)){
		$gain = $gain + 1;
		$token = $token - 1;
		
		$query = "UPDATE `HypeWords` SET token = $token, gain=$gain WHERE name=\"$entry\";" ;
		$r =  @mysql_query($query) or die("Bug in reward_invok_term 3");
		
		inc_player_honnor($owner, 1);
	}
}


//--------------------- STATS

function get_relation_number() {
	//$query = "SELECT COUNT(id)  FROM `Relations` WHERE `type` != 1 AND `type` != 2 AND `type` != 3 AND `type` != 4  AND `type` != 12 AND `type` != 18 AND `type` != 19 AND `type` != 36";
	$query = "SELECT COUNT(id) FROM `Relations` WHERE
	`type` not in (1, 2, 4, 12, 18, 19, 36)";
	
	$r =  @mysql_query($query) or die("bug in get_relation_number : $query");
	return mysql_result($r , 0 , 0);
}

function get_relation_wsum() {
	$query = "SELECT SUM(w)  FROM `Relations` WHERE `type` != 1 
		AND `type` != 2 AND `type` != 3 AND `type` != 4 
		AND `type` != 12 AND `type` != 18 AND `type` != 19";
	$r =  @mysql_query($query) or die("bug in get_relation_number : $query");
	return mysql_result($r , 0 , 0);
}

function get_off_relation_number() {
	$th = $_SESSION[ssig() . 'offlimits_thresh'];
	$query = "SELECT COUNT(id) FROM `Relations` WHERE 
		w >= $th
		AND
		`type` not in (1, 2, 4, 12, 18, 19, 36)
		";
	$r =  @mysql_query($query) or die("bug in get_off_relation_number : $query");
	return mysql_result($r , 0 , 0);
}



function get_node_number() {
	$query = "SELECT COUNT(id)  FROM `Nodes` WHERE `type` = 1";
	$r =  @mysql_query($query) or die("bug in get_node_number : $query");
	return mysql_result($r , 0 , 0);
}

function get_out_relation_number() {
	$query = "SELECT count(distinct(node1)) FROM `Relations` WHERE type in (0, 5, 8, 3, 6, 7, 9, 10, 15, 17, 11, 22, 20, 21, 13, 14, 16, 32)";
	$r =  @mysql_query($query) or die("bug in get_out_relation_number : $query");
	return mysql_result($r , 0 , 0);
}

function get_in_relation_number() {
	$query = "SELECT count(distinct(node2)) FROM `Relations` WHERE type in (0, 5, 8, 3, 6, 7, 9, 10, 15, 17, 11, 22, 20, 21, 13, 14, 16, 32)";
	$r =  @mysql_query($query) or die("bug in get_in_relation_number : $query");
	return mysql_result($r , 0 , 0);
}



function get_relation_id_number($id) {
	$query = "SELECT COUNT(id)  FROM `Relations` WHERE `type` = $id";
	$r =  @mysql_query($query) or die("bug in get_relation_number : $query");
	return mysql_result($r , 0 , 0);
}






function make_stats_string() {
	$l=active_relations();
	$nb = count($l);
	$string = "";
	for ($i=0 ; $i<$nb ; $i++) {
	   $string = $string . $l[$i] . ":" . get_relation_id_number($l[$i]) . ";";
	}
	
	$string = $string . "w:". get_relation_wsum() . ";" ;
	
	$time = round(microtime_float()*1000);
	$query = "INSERT INTO `Stats` (`id` , `data`) VALUES ('$time', '$string');";
	$r =  @mysql_query($query) or die("pb make_stats_string: $query");
}


function check_gifts() {
	$playerid = $_SESSION[ssig() . 'playerid'];
    $query = "SELECT * FROM Gifts WHERE destid='$playerid'";
    $r =  @mysql_query($query) or die("pb check_gifts : $query");
    $nb = mysql_num_rows($r);

    if ($nb>0) {return true;} else {return false;}
}


function game_bonus_factor($id) {
	//debugecho("in game_bonus_factor($id)");
	
	if ($id <= 0) {return 1;}
	$query = "SELECT max(id) FROM PendingGames";
	$r =  @mysql_query($query) or die("pb game_bonus_factor : $query");
	$lastid = mysql_result($r , 0 , 0);
	$f = round(max(1, min(30,round($lastid/$id,1))))*10;
	
	if ($f < 10) {$f = 1;}
	if (count($_SESSION[ssig() . 'offlimits']) <= 0) {$f = 1;}
	
	//debugecho("in game_bonus_factor id=$id lastid=$lastid F=$f");
	
	return $f;
}

function display_game_bonus_factor(){
	if ($_SESSION[ssig() . 'bonus-factor'] > 1) {
		echo "\n<div class=\"jdm-pastille\">";
		echo "\n<div class=\"jdm-pastille-text\">";
		echo "+" . ($_SESSION[ssig() . 'bonus-factor']*1) . "%";
		echo "</div></div>";
	}
}

function display_antik_tag(){
	if ($_SESSION[ssig() . 'old_game'] == True) {
		echo "\n<div class=\"jdm-antik-tag\">";
		echo "&nbsp;</div>";
	}
}

//--------------------- PROPOSED WORDS

function insert_proposed_word($termid, $playerid=-1) {
	if ($playerid == -1) {
		$playerid = $_SESSION[ssig() . 'playerid'];
	}
	$time = microtime_float();

    if (check_proposed_word($termid)) {
		// rien
    } else {
		$query = "INSERT INTO `ProposedWords` (`player_id` , `term_id`, `date`) 
			VALUES ('$playerid', '$termid','$time');";
		$r =  @mysql_query($query) or die("pb insert_proposed_word 2: $query");
    }
    
    // on vire les trop vieux si nb > 200
    $query = "SELECT count(term_id) FROM ProposedWords WHERE player_id='$playerid'";
    $r =  @mysql_query($query) or die("pb insert_proposed_word 3 : $query");
    $nb = mysql_result($r , 0 , 0);
    $thresh = max(1000, 250*get_player_eff($playerid));
    if ($nb > $thresh) {  	
		$query = "SELECT date FROM ProposedWords 
			WHERE player_id='$playerid' ORDER by DATE DESC LIMIT $thresh";
		$r =  @mysql_query($query) or die("pb insert_proposed_word 4: $query");
		$nb2= mysql_num_rows($r);
		$mindate = mysql_result($r , ($thresh-1) , 0);
		if ($mindate > 0) {
			$query = "DELETE FROM `ProposedWords` WHERE date < '$mindate' and player_id='$playerid' ;";
	    	$r =  @mysql_query($query) or die("pb insert_proposed_word 5: $query");
			}
    	}
}

function check_proposed_word($term_id, $playerid=-1) {
	if ($playerid == -1) {
		$playerid = $_SESSION[ssig() . 'playerid'];
	}
	$query = "SELECT player_id,term_id FROM ProposedWords 
			WHERE player_id='$playerid' AND term_id='$term_id'";
    $r =  @mysql_query($query) or die("pb check_proposed_word : $query");
    $nb= mysql_num_rows($r);
    if ($nb<=0) {
		return false;
    } else {
    	return true;
    }
}

//--------------------- LAST PLAYER

function insert_last_player($player2_id) {
	start_time_record("insert_last_player");
			
	if ($player2_id == "") {echo "<P>bug in insert_last_player : player2_id is null<P>"; return;}
	$player1_id = $_SESSION[ssig() . 'playerid'];
    if (check_last_player($term_id)) {
    	// mise a jour
    	$query = "UPDATE `LastPlayer` SET player2_id = $player2_id WHERE player1_id= '$player1_id' ;";
	    $r =  @mysql_query($query) or die("pb insert_last_player 1: $query");
    } else {
    	// creation
		$query = "INSERT INTO `LastPlayer` (`player1_id` , `player2_id`) 
			VALUES ('$player1_id', '$player2_id');";
		$r =  @mysql_query($query) or die("pb insert_last_player 2: $query");
    }
    debugecho("insert_last_player=" . end_time_record("insert_last_player"));
}

function check_last_player() {
	$player1_id = $_SESSION[ssig() . 'playerid'];
	$query = "SELECT player1_id, player2_id FROM LastPlayer WHERE player1_id= '$player1_id'";
    $r =  @mysql_query($query) or die("pb check_last_player : $query");
    $nb = mysql_num_rows($r);
    if ($nb<=0) {
		return false;
    } else {
    	return true;
    }
}

function get_last_player() {
	$player1_id = $_SESSION[ssig() . 'playerid'];
	if ($player1_id == 0) {return -1;}
	$query = "SELECT player1_id, player2_id FROM LastPlayer WHERE player1_id= '$player1_id'";
    $r =  @mysql_query($query) or die("pb get_last_player : $query");
    $nb = mysql_num_rows($r);
    if ($nb<=0) {
		return -1;
    } else {
    	return mysql_result($r , 0 , 1);;
    }
}


function insert_friend($player1_id, $player2_id) {
	//start_time_record("insert_last_player");
			
	if ($player1_id == "") {//echo "<P>bug in insert_friend : player1_id is null<P>"; 
		return;}
	if ($player2_id == "") {//echo "<P>bug in insert_friend : player2_id is null<P>"; 
		return;}
		
	$query = "SELECT playerid, friendid FROM PlayerFriends WHERE playerid= '$player1_id'
	and friendid= '$player2_id';";
    $r =  @mysql_query($query) or die("pb insert_friend : $query");
    $nb = mysql_num_rows($r);
    if ($nb > 0) {return;}
    
	$query = "INSERT INTO `PlayerFriends` (playerid, friendid) 
			VALUES ('$player1_id', '$player2_id');";
	$r =  @mysql_query($query) or die("pb insert_friend 2: $query");
    
    //debugecho("insert_last_player=" . end_time_record("insert_last_player"));
}

function make_friend_list($playerid) {
	$str = '';
	$query = "SELECT friendid FROM PlayerFriends WHERE playerid= '$playerid';";
    $r =  @mysql_query($query) or die("pb insert_friend : $query");
    $nb = mysql_num_rows($r);
    if ($nb > 0) {
    	$id = mysql_result($r , 0 , 0);
    	$str = "($id";
    	for ($i=1 ; $i<mysql_num_rows($r) ; $i++) {
			$id = mysql_result($r , $i , 0);
			$str = $str . ", $id";
    	}
    	$str = $str . ")";
    }
    return $str;
}



function randomselection($c, $sql) {
    // run the query
    $result=mysql_query($sql) or die($sql);   
    // get the upper limit for rand()
    $up = mysql_num_rows($result)-1;
   
    // check that there is more data than rows required
    if ($up <= $c) {
        while ($row=mysql_fetch_assoc($result)) {
            $selection[] = $row;
        }
        return $selection;
    }
   
    // set up array to hold primary keys and elliminate duplicates
    // since random DOES NOT mean unique
    // or the query may not return DISTINCT rows
    $keys = array();
    // get random selection
    while (count($keys) < $c) {
        // get random number for index
           $i = rand(0, $up);
           // move pointer to that row number and fetch the data
           mysql_data_seek($result, $i);
           $row = mysql_fetch_assoc($result);
          //  check if the primary key has already been fetched
          if (!in_array($row['id'], $keys)) {
             // store the row
             $selection[] = $row;
            // store the id to prevent duplication
            $keys[] = $row['id'];
        }
    }
    return $selection;
}


function make_raffinement_link_down($term) {
	if ($_SESSION[ssig() . 'playerid'] != 11) {return "";} 
	if (has_some_raffinement_down($term) > 0) {
    	return " > ";
    }
}

function make_raffinement_link_up($term) {
	if ($_SESSION[ssig() . 'playerid'] != 11) {return "";} 
	if (has_some_raffinement_up($term) > 0) {
    	return " < ";
    }
}

function has_some_raffinement_down($node1_name) {
	$id1 = term_exist_in_BD_p($node1_name);
	if ($id1 == 0) {return 0;}
	$query = "SELECT count(id) FROM Relations WHERE type=1 
		AND node1 = $id1";
	$r =  @mysql_query($query) or die("pb has_some_raffinement_down 1: $query");
	$nb= mysql_result($r , 0 , 0);
	return $nb;
}

function has_some_raffinement_up($node1_name) {
	$id1 = term_exist_in_BD_p($node1_name);
	if ($id1 == 0) {return 0;}
	$query = "SELECT count(id) FROM Relations WHERE type=1 
		AND node2 = $id1";
	$r =  @mysql_query($query) or die("pb has_some_raffinement_up 1: $query");
	$nb= mysql_result($r , 0 , 0);
	return $nb;
}


function display_toggle($bool) {
	if ($bool) {return "<img border=0 width=\"12px\" src=\"pics/icon-minus.png\">" ;} 
	else {return "<img border=0 width=\"12px\" src=\"pics/icon-plus.png\">";}
}

function handle_toggle($toggle_tag, $url) {
	echo "<a name=\"$toggle_tag\">";
	
	if ($_GET[$toggle_tag]=="1") { $_SESSION[ssig() . $toggle_tag] = true;}
	if ($_GET[$toggle_tag]=="0") { $_SESSION[ssig() . $toggle_tag] = false;}
	if ($_SESSION[ssig() . $toggle_tag]) {$val=0;} else {$val=1;}
	$tag = display_toggle($_SESSION[ssig() . $toggle_tag]);
  	echo " <div class=\"jdm-toggle-block\"><a href=\"$url?$toggle_tag=$val#$toggle_tag\">$tag</a></div>";
}

function get_nb_token_term_comp($playerid, $termid, $compid) {
	$query = "SELECT SUM(P.token) FROM `PendingGames` as P, `PendingGameData` as D 
		WHERE P.creator = $playerid and P.id = D.gameId and D.data = $termid and D.dataType = $compid;";
	$r =  @mysql_query($query) or die("pb in get_nb_token_term_comp : $query");
	$nb = mysql_result($r , 0 , 0);
	return $nb;
}


function decf_play_token ($gameid) {
	// combien de jeton pour cette partie ?
	//
	$query = " SELECT token FROM `PendingGames` WHERE id= '$gameid' ;";
	$r =  @mysql_query($query) or die("pb decf_play_token 1");
	$nb = mysql_num_rows($r);
	if ($nb > 0) {
		$token = mysql_result($r , 0 , 0);
		if ($token > 1) {
			// on normalise... en cas de pb ou de triche
			//
			if ($token > 10) {
	    		$query = "UPDATE `PendingGames` SET token = 10 WHERE id= '$gameid';" ;
				$r1 =  @mysql_query($query) or die("pb decf_play_token 2");
	    	}
			
			$query = "UPDATE `PendingGames` SET token = token-1 WHERE id= '$gameid' ;";
			$r =  @mysql_query($query) or die("pb decf_play_token 3");
		} else {
			// si plus de jeton, on degage la partie et tout ce qui s'y rapporte
			//
			delete_game ($gameid);
			
		}
	}
}


function normalize_play_token ($gameid) {
	// combien de jeton pour cette partie ?
	//
	$query = " SELECT token FROM `PendingGames` WHERE id= '$gameid' ;";
	$r =  @mysql_query($query) or die("pb normalize_play_token 1");
	$nb = mysql_num_rows($r);
	if ($nb > 0) {
		if ($token > 10) {
	    		$query = "UPDATE `PendingGames` SET token = 10 WHERE id= '$gameid';" ;
				$r1 =  @mysql_query($query) or die("pb normalize_play_token 2");
	    	}
		}
	}

function delete_game($gameid) {
	$query = "DELETE FROM `PendingGames` WHERE id= '$gameid' ;";
	$r =  @mysql_query($query) or die("pb delete_game 4");

	$query = "DELETE FROM `PendingAnswers` WHERE  gameId = '$gameid'";
	$r =  @mysql_query($query) or die("pb delete_game 5");

	$query = "DELETE FROM `PendingGameData` WHERE  gameId = '$gameid'";
	$r =  @mysql_query($query) or die("pb delete_game 6");

	$query = "DELETE FROM `ProposedGames` WHERE  gameId = '$gameid'";
	$r =  @mysql_query($query) or die("pb delete_game 7");
}


function update_network_relation ($w, $entryid, $gamerelationtype, $meanlevel=50, $infop1, $infop2) {
	start_time_record("update_network_relation");
	$points = 0;
	//$allrelnb_before=get_relation_number();
	$query =  "SELECT id FROM `Nodes` WHERE Nodes.name = \"$w\";"   ;
	//echo $query;
	$r2 =  @mysql_query($query) or die("pb in update_network_relation 1 : $query");
	$nb2 = mysql_num_rows($r2);
	if ($nb2 > 0) {
		// le mot est deja dans la base
		// on met à jour
		$wid = mysql_result($r2 , 0 , 0);
		//echo "<BR>updating word $wid $w";
		$query = "UPDATE `Nodes` SET w = w+1 WHERE Nodes.name = \"$w\" LIMIT 1;";
		//echo $query;
		$r =  @mysql_query($query) or die("pb in update_network_relation 2 : $query");

		// Gestion de la relation
		$query = "SELECT id,w FROM `Relations` WHERE node1 = '$entryid' AND node2 = '$wid' 
					AND type='$gamerelationtype';"  ;
		$r3 =  @mysql_query($query) or die("pb in update_network_relation 3 : $query");
		$nb3 = mysql_num_rows($r3);

			if ($nb3 > 0) {
				// la relation existe deja
			    $idRel = mysql_result($r3 , 0 , 0);

				$poids = mysql_result($r3 , 0 , 1);
				$points = $points+round((1000-$poids)/20);

				$query = "UPDATE `Relations` SET w = w+10 WHERE node1= '$entryid' AND node2= '$wid' 
							AND type='$gamerelationtype';" ;
				$r =  @mysql_query($query) or die("pb update_network_relation 4 : $query");

				$query = "UPDATE `RelationInfo` SET `$infop1` = `$infop1`+1 WHERE id= '$idRel';" ;
				$r =  @mysql_query($query) or die("pb update_network_relation 5 : $query");

				$query = "UPDATE `RelationInfo` SET `$infop2` = `$infop2`+1 WHERE id= '$idRel';" ;
				$r =  @mysql_query($query) or die("pb update_network_relation 6 : $query");

				$query = "UPDATE `Nodes` SET w = w+1 WHERE id= '$wid';" ;
				$r =  @mysql_query($query) or die("pb in update_network_relation 7 : $query");

			} else {
				// la relation n existe pas -> on insere
				//echo "<BR>inserting relation between $entry and $w";
				$query = "INSERT INTO Relations (node1, node2, type, w, creationdate) 
							VALUES('$entryid', '$wid', '$gamerelationtype', 50, CURRENT_TIMESTAMP)";
				$r =  @mysql_query($query) or die("pb in update_network_relation 8 : $query");

				// Gestion de la relation
				$query = "SELECT id,w FROM `Relations` WHERE node1 = '$entryid' AND node2 = '$wid' 
					AND type='$gamerelationtype';"  ;
				$r =  @mysql_query($query) or die("pb in update_network_relation 9 : $query");
				$idRel = mysql_result($r , 0 , 0);
				
				$query = "INSERT INTO `RelationInfo` (id,`$infop1`) VALUES ('$idRel', 1);" ;
				$r =  @mysql_query($query) or die("pb in update_network_relation 10 : $query");	

				$query = "UPDATE `RelationInfo` SET `$infop2`=`$infop2`+1 WHERE id='$idRel';" ;
				$r =  @mysql_query($query) or die("pb in update_network_relation 12 : $query");	

				$query = "UPDATE `Nodes` SET w = w+1 WHERE id= '$wid';" ;
				$r =  @mysql_query($query) or die("pb in update_network_relation 13 : $query");

				$points = $points+50;
			}
		} else {
			//echo "<BR>inserting word $w";
			// bonus pour un mot nouveau
			$query = "INSERT INTO Nodes (name, type, w, creationdate, level) 
						VALUES(\"$w\" , 1 , 50, CURRENT_TIMESTAMP, $meanlevel)";
			$r =  @mysql_query($query) or die("pb in update_network_relation inserting new node : $query");
			$LASTPGID = mysql_insert_id();
			$wid = $LASTPGID;
			$points = $points+20;

			// insertion de la relation
			//
			//echo "<BR>inserting relation between $entry and $w";
			$query = "INSERT INTO Relations (node1, node2, type, w, creationdate) VALUES('$entryid', '$LASTPGID', '$gamerelationtype', 50, CURRENT_TIMESTAMP)";
			$r =  @mysql_query($query) or die("pb in update_network_relation inserting rel after new node : $query");
			$points = $points+50;
		}
		
		//$allrelnb_after=get_relation_number();
		//process_relation_increase($allrelnb_before,$allrelnb_after);
		
		$factor = term_get_log_freq ($wid);
				
		debugecho("update_network_relation" . end_time_record("update_network_relation"));
		return round($points/$factor);
}

function process_relation_increase($nb_before,$nb_after) {
	$th = 100000;
	$g = 2500;
	$mod1_before = floor($nb_before/$th);
	$mod1_after = floor($nb_after/$th);
	if ($mod1_after > $mod1_before) {
		$playerid = $_SESSION[ssig() . 'playerid'];
		$playername = get_player_name($playerid);
		$GLOBALS[0] = $playername;
		$GLOBALS[1] = $th;
		$GLOBALS[2] = $g;
		$msg = get_msg("EVENTS-CAP");
		make_event($msg);
		inc_player_honnor($playerid, $g);
		return;
	}

	$th = 10000;
	$g = 500;
	$mod1_before = floor($nb_before/$th);
	$mod1_after = floor($nb_after/$th);
	if ($mod1_after > $mod1_before) {
		$playerid = $_SESSION[ssig() . 'playerid'];
		$playername = get_player_name($playerid);
		$GLOBALS[0] = $playername;
		$GLOBALS[1] = $th;
		$GLOBALS[2] = $g;
		$msg = get_msg("EVENTS-CAP");
		make_event($msg);
		inc_player_honnor($playerid, $g);
		return;
	}
	
	$th = 1000;
	$g = 100;
	$mod1_before = floor($nb_before/$th);
	$mod1_after = floor($nb_after/$th);
	if ($mod1_after > $mod1_before) {
		$playerid = $_SESSION[ssig() . 'playerid'];
		$playername = get_player_name($playerid);
		$GLOBALS[0] = $playername;
		$GLOBALS[1] = $th;
		$GLOBALS[2] = $g;
		$msg = get_msg("EVENTS-CAP");
		make_event($msg);
		inc_player_honnor($playerid, $g);
		return;
	}
	
	$th = 100;
	$g = 20;
	$mod1_before = floor($nb_before/$th);
	$mod1_after = floor($nb_after/$th);
	if ($mod1_after > $mod1_before) {
		$playerid = $_SESSION[ssig() . 'playerid'];
		$playername = get_player_name($playerid);
		$GLOBALS[0] = $playername;
		$GLOBALS[1] = $th;
		$GLOBALS[2] = $g;
		$msg = get_msg("EVENTS-CAP");
		make_event($msg);
		inc_player_honnor($playerid, $g);
		return;
	}
}


//----------------------

function update_honnor_score($playerid) {
	$h=get_player_honnor($playerid);
	$query = "INSERT INTO `Scores` (id, score, date) VALUES ('$playerid','$h', CURRENT_TIMESTAMP);";
	//echo $query;
	$r =  @mysql_query($query) or die("bug dans update_honnor_score :$query");
}


function update_some_honnor_scores() {
	$query =  "SELECT id FROM `Players` WHERE TO_DAYS(NOW()) - TO_DAYS(lastlogin) <= 30
    AND id NOT IN (SELECT id FROM `Scores` WHERE (UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(date))/3600 < 24)
    ORDER BY Rand() LIMIT 50;";
    $r =  @mysql_query($query) or die("bug dans update_some_honnor_scores :$query");
    $nb = mysql_num_rows($r);
	for ($i=0 ; $i<$nb ; $i++) {
	   $playerid = mysql_result($r , $i , 0);
	   update_honnor_score($playerid);
	}	
}



function check_referer() {
	$_SESSION[ssig() . 'FRAUD'] = false;
	
	if ($_SESSION[ssig() . 'playerid'] == get_admin_id()) {
		print "<pre>\n";
		print "\nContents of \$_SERVER:\n";
   		foreach ($_SERVER as $k => $v) {
     		print "   $k = $v\n";
   		}
   		print "</pre>\n";
	}
	
	if ($_SERVER['HTTP_REFERER']=="") {
		$_SESSION[ssig() . 'FRAUD'] = true;
   		echo "<P>Accès frauduleux... retournez à l'accueil";
   		
   		$h = get_player_honnor($_SESSION[ssig() . 'playerid']);
   		$c = get_player_credit($_SESSION[ssig() . 'playerid']);
   		//inc_player_honnor($_SESSION[ssig() . 'playerid'], -1*round(0.1*$h));
   		//inc_player_credit($_SESSION[ssig() . 'playerid'], -1*round(0.1*$c));
   		
   		$p1 = get_player_name($_SESSION[ssig() . 'playerid']);
		//make_event("'$p1' tente de tricher...");
   	}
}



function term_comp_no_relation_p ($termid, $relid) {
	start_time_record("term_comp_no_relation_p");
	$query = "Select id FROM Relations WHERE
		node1 = $termid AND type=$relid LIMIT 1";
	$r =  @mysql_query($query) or die("pb term_comp_no_relation_p 1 : $query");
    $nb= mysql_num_rows($r);
    debugecho("term_comp_no_relation_p=" . end_time_record("term_comp_no_relation_p"));
    if ($nb == 0) {
    	return true;
    } else {
    	return false;
    }
}

function term_get_log_freq ($termid) {
	$query = "Select w FROM Nodes WHERE id = $termid";
	$r =  @mysql_query($query) or die("pb term_get_log_freq 1 : $query");
    $poids = mysql_result($r , 0 , 0);
    return max(1, log($poids/100, 10));
}


function my_marquee() {
	
	echo "<div class=\"jdm-niou**\">";
	echo "<marquee  BEHAVIOR=\"alternate\" SCROLLAMOUNT=\"2\" SCROLLDELAY=\"25\">";
	echo "top du top : n@t";
	echo "&nbsp;&nbsp;&bull;&nbsp;&nbsp; joueur plus innovant : luciole";
	echo "&nbsp;&nbsp;&bull;&nbsp;&nbsp; joueur le plus efficace : enc...moi";
	echo "&nbsp;&nbsp;&bull;&nbsp;&nbsp; joueur le plus riche : agnes";
	echo "&nbsp;&nbsp;&bull;&nbsp;&nbsp; joueur le plus collectionneur : Exelmans";

	echo "</marquee>";
	echo "</div>";
	
}


function generate_no_script(){
	echo "<noscript>
	<div style=\"position:absolute;z-index:10000;font-size:32pt;\">
	<CENTER>Javascript doit fonctionner ! Activez-le et rechargez cette page.
	<br>
	<IMG src=\"pics/error.png\">
	</CENTER>
	<div>
	<meta HTTP-EQUIV=\"Refresh\" content=\"1;URL=http://jeuxdemots.org/\">
</noscript>";
}

function display_twick() {
	//if ($_SESSION[ssig() . 'playerid'] != 11) {return "";} 
	echo "<div id=\"jdm-twik-block\">";
	echo "<div id=\"jdm-twik-block-text\">";
	echo "" . $_SESSION[ssig() . 'current-twik'];
	echo "</div></div>";
	echo "<script language=\"javascript\">
function script_update_twik()
{
jdm_ajax_update_twik();


/*$('jdm-twik-block-text').fade(0);
var myEffect = new Fx.Morph($('jdm-twik-block-text'), { 
  duration: 2000 
}); 
myEffect.start({ 
  opacity: 1 //Transition opacity to 1 
}); 
$('jdm-twik-block-text').fade(1);*/

setTimeout(\"script_update_twik()\",10000);
}

window.onload=script_update_twik;
</script>";
}


function make_postit_stats_contents ($utf8=false) {
		$rez = "<div id=\"jdm-postit-stats\">";
		echo $rez;
		
		include('jdm-stats.txt');
		
		// Au lieu de cela calculer une première fois les stats avec jdm-about.php
    /*	$GLOBALS[0]=get_relation_number();
		$GLOBALS[1]=get_off_relation_number();
		//$GLOBALS[2]=get_relation_wsum();
    	$rez = $rez .  get_msg('HOME-RELATION-COUNT');
 
    	$GLOBALS[0]=get_node_number();
    	$GLOBALS[1]=get_out_relation_number();
    	$GLOBALS[2]=get_in_relation_number();
    	$rez = $rez . get_msg('HOME-TERM-COUNT');
    	*/
		
		$rez = '';
    	$GLOBALS[0]=count_recent();
    	$playerCount = $GLOBALS[0] > 1 ? get_msg('HOME-ACTIVE-PLAYERS-COUNT') : get_msg('HOME-ACTIVE-PLAYER-COUNT');
    	$rez = $rez . $playerCount . "</div>";
    	
    	add_twit('stats', $rez);
    	
    	$player_string = display_recent_player_string_mooquee();
    	//$rez = $rez . "<marquee>$player_string</marquee>";
    	$rez = $rez . "\n<div id=\"mooquee\" style=\"text-align:center;width:175px;height:25px;border:0px solid;position:relative;top:210px;left:20px;\">  
    	$player_string
    	</div>\n";
    	
    	$rez = $rez . "<script type=\"text/javascript\">
		mookieExampleOne = new Mooquee({
			element:'mooquee',
			direction:'left',
			duration:3,
			pause:1
		});	
	</script>";
    	
    	
    	
    	if ($utf8) {
    		//echo utf8_encode($rez);
    		$S_SESSION['compteur'] = $S_SESSION['compteur'] + 1;
    		echo "camerde" . $S_SESSION['compteur'];
    	} else {
    		echo $rez;
    	}
}


function update_postit_stats_contents ($utf8=false) {
		
		$rez = '';
		
    	$GLOBALS[0]=get_relation_number();
		$GLOBALS[1]=get_off_relation_number();
		//$GLOBALS[2]=get_relation_wsum();
    	$rez = $rez .  get_msg('HOME-RELATION-COUNT');
 
    	$GLOBALS[0]=get_node_number();
    	$GLOBALS[1]=get_out_relation_number();
    	$GLOBALS[2]=get_in_relation_number();
    	$rez = $rez . get_msg('HOME-TERM-COUNT');
    	
		
		/*
    	$GLOBALS[0]=count_recent();
    	$rez = $rez . get_msg('HOME-ACTIVE-PLAYER-COUNT');
    	$rez = $rez . "</div>";
    	
    	add_twit('stats', $rez);
    	
    	$player_string = display_recent_player_string_mooquee();
    	//$rez = $rez . "<marquee>$player_string</marquee>";
    	$rez = $rez . "\n<div id=\"mooquee\" style=\"text-align:center;width:175px;height:25px;border:0px solid;position:relative;top:210px;left:20px;\">  
    	$player_string
    	</div>\n";
    	
    	$rez = $rez . "<script type=\"text/javascript\">
		mookieExampleOne = new Mooquee({
			element:'mooquee',
			direction:'left',
			duration:3,
			pause:1
		});	
	</script>";
    	
    	*/
    	
    	if ($utf8) {
    		//echo utf8_encode($rez);
    		$S_SESSION['compteur'] = $S_SESSION['compteur'] + 1;
    		echo "camerde" . $S_SESSION['compteur'];
    	} else {
    		$file = fopen('jdm-stats.txt', "w+");
    		fwrite($file,$rez);
    		fclose($file);
    	}
}


function add_twit($tag, $text) {
	$query = "DELETE FROM `Twits` WHERE  tag = '$tag'";
    $r =  @mysql_query($query) or die("bug dans add_twit :$query");
    
    $text = trim($text);
    if ($text != "") {
    	$text = addslashes($text);
		$query = "INSERT INTO `Twits` (tag, text) VALUES ('$tag','$text');";
		//echo $query;
		$r =  @mysql_query($query) or die("bug dans add_twit :$query");
    }
}

function get_twit($tag) {
	$query = "SELECT text FROM `Twits` WHERE  tag = '$tag'";
    $r =  @mysql_query($query) or die("bug dans get_twit :$query");
    return stripslashes(mysql_result($r , 0 , 0));
}

function display_ask_twit_form() {
	$text = $_POST['twit_text'];

	echo "<form id=\"make_twit\" name=\"make_twit\" method=\"post\" action=\"souk.php\" >";
	echo get_msg('SOUK-YOUR-HUMOR'),"<br/>
	<input id=\"twit_text\" size=\"100px\" type=\"text\" name=\"twit_text\" value=\"$text\">
	<input id=\"submit_make_twit\" type=\"submit\" name=\"submit_make_twit\" value=\"",get_msg('SOUK-SHARE-HUMOR'),"\">	
	";
	echo "	</form>";
	
	if ($_POST['submit_make_twit']!= "") {
		add_twit($_SESSION[ssig() . 'playerid'], get_player_name($_SESSION[ssig() . 'playerid']) . ' ' . $text);
	}
	if ($_POST['submit_make_twit']!= "")
		 {display_warning(get_msg('SOUK-SHARED-HUMOR'));}
	
	
}

function display_ask_thema_form() {
	$playerid = $_SESSION[ssig() . 'playerid'];
	if ($_POST['submit_make_thema'] == "") {
		$id = get_thema($playerid);
		if ($id != -1) {
			$text = get_term_from_id($id);
		}
	} else {
		$text = trim($_POST['thema_text']);
	}
	$text = stripslashes($text);
	$tag = "";
	if ($playerid == 0) {$tag = "disabled";}
	
	echo "<form id=\"make_thema\" name=\"make_thema\" method=\"post\" action=\"souk.php\" >";
	echo get_msg('SOUK-PREFERRED-THEME'), "<br/>
	<input id=\"thema_text\" size=\"50px\" type=\"text\" name=\"thema_text\" value=\"$text\">
	<input $tag id=\"submit_make_thema\" type=\"submit\" name=\"submit_make_thema\" value=\"",get_msg('SOUK-THEME-VALIDATE'),"\">",	
	 get_msg('SOUK-EMPTY-FIELD-THEME');
	echo "	</form>";
	
	if ($_POST['submit_make_thema']!= "") {
		$id = add_thema($_SESSION[ssig() . 'playerid'], $text);
		//echo "<br>id=$id";
		if ($id == -1) {
			display_warning(get_msg('SOUK-NO-EXISTING-THEME'));
		}
		if ($id == 0) {
			display_warning(get_msg('SOUK-NO-PARTICULAR-THEME'));
		}
		if ($id > 0) {
			display_warning(get_msg('SOUK-VALIDATED-THEME'));
		}
	}
}


function add_thema($playerid, $term) {
	$query = "DELETE FROM `Thema` WHERE  playerid = $playerid";
   	$r =  @mysql_query($query) or die("bug dans add_thema :$query");
   	
   	
   	$term = trim($term);
   	if ($term == "") {
   	//echo "term=$term" ; 
   	return 0;}
   	
    $term = trim($term);
   	$termid = term_exist_in_BD_p($term);
   	if ($termid == 0) {return -1;}
    if (($term != "") && ($playerid != 0)) {
		$query = "INSERT INTO `Thema` (playerid, termid) VALUES ($playerid,'$termid');";
		//echo $query;
		$r =  @mysql_query($query) or die("bug dans add_thema :$query");
		return $termid;
    } else {
    	$query = "DELETE FROM `Thema` WHERE  playerid = $playerid";
   	 	$r =  @mysql_query($query) or die("bug dans add_thema :$query");
   	 	return 0;
    }
}

function get_thema($playerid) {
	$query = "SELECT termid FROM `Thema` WHERE  playerid = '$playerid'";
    $r =  @mysql_query($query) or die("bug dans get_thema :$query");
   	$nb = mysql_num_rows($r);
    if ($nb<=0) {
		return -1;
    } else {
    	return mysql_result($r , 0 , 0);;
    }
}

function learn_more_link ($term) {
	$link = compute_learn_more_link($term);
	if ($link != "") {
		echo "<div id=\"jdm-learn-more-block\">";
		echo "<div id=\"jdm-learn-more\">";
		echo "<a border=0 target=blank title=\"Inspirateur\" alt=\"Inspirateur\" BORDER=0 href=\"$link\">";
		echo "En savoir plus";
		echo "</a>";
		echo "</div></div>";
	}
}


function compute_learn_more_link($term) {
	if (strpos($term, '::') === 0) {return;}
	if (strpos($term, '>') != false) {return;}
	
	$term = str_replace(" ", "_", $term);

		// Astro le createur
		// Meuh
		if (($_SESSION[ssig() . 'gamecreatorid'] == 889) ||
			($_SESSION[ssig() . 'gamecreatorid'] == 1466) 
			)
		{
			$link = "http://fr.wikipedia.org/wiki/$term"; return $link;
		}
	
		//syn
		if ($_SESSION[ssig() . 'gamerelationtype'] == 5) {$link = "http://www.cnrtl.fr/synonymie/$term";
		 	return $link;}
		// anto
		if ($_SESSION[ssig() . 'gamerelationtype'] == 7) {$link = "http://www.cnrtl.fr/antonymie/$term"; 
			return $link;}	
		
		if ((strtolower($term) == $term) &&
			(strpos($term,"_") === false)) {
			$rand = rand(0,1);
			if ($rand == 0) {$link = "http://fr.wiktionary.org/wiki/$term"; }
			if ($rand == 1) {$link = "http://www.cnrtl.fr/lexicographie/$term"; }
			return $link;
		}
		
		// default
		$link = "http://fr.wikipedia.org/wiki/$term"; return $link;
}


function select_rand_in_tab($r) {
    $n = mysql_num_rows($r);
    //debugecho("select_rand_in_tab : n = $n");
    if ($n != 0) {
	$rand = rand(0,$n-1);
	return $rand;
    } else {return -1;}
}


?>
