<?php session_start();?>
<?php include_once 'misc_functions.php'; ?>
<?php
    openconnexion();
	$_SESSION[ssig() . 'state']=0;
?>
<html>
 <head>
    <title>JeuxDeMots : ayuda</title>
    <?php header_page_encoding(); ?>
  </head>
<?php include 'HTML-body.html' ; ?>
<?php topblock(); ?>
<div class="jdm-level1-block">>

	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
    <?php echo "Aide de JeuxDeMots"; ?>
    </div>
	</div>

    <div class="jdm-login-block">
    <?php  loginblock(); ?>
    </div>
</div>

<div class="jdm-level2-block">
<TABLE	border="0"
	width="100%"
	cellspacing="30px" cellpadding="0%"
	summary="aide">

<TR valign= "top">
    <TH align="right"><h2>Principio<BR>general</h2>
    <TH align="left"> 
    <P><br>
		Cuando empieza una partida, una consigna que concierne un tipo de competencia (sin�nimo, contrario,�) aparece.
	    Cuanto m�s tendr� usted respuestas en com�n con otro jugador, mejor ser�n su ganancia. Respuestas espec�ficas y poco encontradas reportan m�s que respuestas generales.
	    <br><br>
	    Para jugar, usted tiene que enterarse de la carta de JeuxDeMots y aceptarla.
	   <P>
	<img border="1" src="pics/jdm-ecranaide-ES.png" alt="image d'aide JeuxDeMots">
	<br><br>
    <TH>

<TR valign= "top">
    <TH align="right"><h2>Historia<br>de honor</h2>
    <TH align="left">
    <P><br>
    El honor representa su prestaci�n. Durante una partida se puede ganar o perder honor. No responder no es honorable.
    El honor tiende a pulverizarse con el tiempo.
    <spacer type="block" width="1" height="30">
    <TH>

<TR valign= "top">
    <TH align="right"><h2>Historia<br>de dinero</h2>
    <TH align="left">
    <P><br>
	  El dinero virtual de JeuxDeMots son los cr�ditos. Se ganan durante partidas seg�n la pertinencia de sus respuestas. 
	  Los cr�ditos le sirven para varias cosas como comprar tiempo durante una partida, pero tambi�n para desbloquear el acceso a algunas competencias.
	<spacer type="block" width="1" height="30">
    <TH>

<TR valign= "top">
    <TH align="right"><h2>Historia<br>de competencias</h2>
    <TH align="left">
    <P><br>
	 Un nuevo jugador no tiene acceso sino a un solo tipo de consigna (se hablar� de competencia). 
	    En el zoco, se puede comprar el derecho de jugar con otras competencias. Una competencia tiene una cotizaci�n que influye su ganancia (a la baja o a la alza). Cuanto m�s se juega una competencia, m�s su cotizaci�n baja.  
	    Pues las competencias escasas y dif�ciles valen m�s que las dem�s. Todav�a en el zoco, se puede ajustar la probabilidad que una partida en construcci�n corresponda a una de sus competencias.
		As� se puede jugar preferentemente con las competencias que le reporta m�s o que usted prefiere. 
	    No influye la proporci�n de aparici�n  de las partidas en espera.
	<spacer type="block" width="1" height="30">
    <TH>

<TR valign= "top">
    <TH align="right"><h2>Historia<br>de niveles</h2>
    <TH align="left">
    <P><br>
	 Los jugadores tienen un nivel de peritaje y los t�rminos tienen un nivel de dificultad.  
	Cuanto m�s el nivel del jugador es alto, m�s tendr� probabilidades/posibilidades de confrontarse a t�rminos dif�ciles.  
	Ganar puntos frente a un t�rmino dif�cil permite aumentar su propio nivel mientras que no ganar puntos frente a un t�rmino f�cil hace bajar su nivel. 
	Se ajusta el nivel de dificultad de los t�rminos seg�n la actividad de los jugadores.
    <TH>


<TR valign= "top">
    <TH align="right"><h2>Historia<br>de respuestas</h2>
    <TH align="left">
	<P><br>
	El bot�n �pasar� permite terminar la partida antes el plazo del tiempo. Si usted considera que no existe respuesta a la consigna propuesta,
	puede entrar *** como respuesta. Si el otro jugador piensa en lo mismo, ganar�n puntos. Cuidado, tal respuesta termina inmediatamente la partida 
	pendiente y anula todas las respuestas propuestas antes. <BR>
	Los t�rminos tab� reportan s�lo un poquito� intente proponer otros t�rminos.
	<spacer type="block" width="1" height="30">
    <TH>

<TR valign= "top">
    <TH align="right"><h2>FAQ<br>Preguntas de Uso Frecuente</h2>
    <TH align="left">
	<P><br>P: Tras 10 o 11 propuestas, el cron�metro anuncia �tiempo terminado�. �Es normal?
	<br>R: S� lo es ya que el n�mero de propuestas se limita a 10 m�s o menos.
	<P>
	P: Ajust� una de mis competencias a 100% pero a veces tengo consignas que corresponden a otras competencias. �Es normal?
	<br>R: S� lo es ya que el ajuste concierne s�lo las partidas en construcci�n y no las partidas que se terminan (las que nos permiten obtener el resultado inmediatamente).
	<spacer type="block" width="1" height="30">
    <TH>

</TABLE>
</div>

<?php playerinfoblock($_SESSION[ssig() . 'playerid']) ?>
<?php 
    bottomblock();
    closeconnexion();
?>

  </body>
</html>
