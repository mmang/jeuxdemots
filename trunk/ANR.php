<?php session_start();?>
<?php include_once 'misc_functions.php'; ?>
<?php
    openconnexion();
	$_SESSION[ssig() . 'redirect_if_session_finished'] = 'N';
?>
<html>
 <head>
    <title><?php echo "Devine"; ?></title>
    <?php header_page_encoding(); ?>
  </head>
<?php include 'HTML-body.html' ; ?>
<?php topblock(); ?>

<?php
if (!isset($_SESSION[ssig() . 'nbtries'])) {
	anr_reinit();
}


function make_anr_form() {

	//echo "'". $_POST['gotermrel'] . "'";
	//echo "'". $_GET['gotermrel'] . "'";
	
	$term = trim($_POST['go_anr']);
	if ($term == "") {
		$term = trim($_GET['go_anr']);	
	}
	echo "<form id=\"go_anr_form\" name=\"go_anr_form\" method=\"post\" action=\"ANR.php\" >
	    <input id=\"go_anr_submit\" type=\"submit\" name=\"go_anr_submit\" value=\"Soumettre\"> l'indice
	    <input  id=\"go_anr\" type=\"text\" name=\"go_anr\" value=\"$term\" size=100>
	    </form>";
    
	if (($_POST['go_anr_submit']!= "") || ($_GET['go_anr_submit']!= "")){
		make_anr_guess($term);
	}
	if (($_POST['go_anr_reinit']!= "") || ($_GET['go_anr_reinit']!= "")){
		anr_reinit($term);
	}
}

function anr_reinit() {
	$_SESSION[ssig() . 'assoc'] = array();
	$_SESSION[ssig() . 'clues'] = array();
	$_SESSION[ssig() . 'guesses'] = array();
	$_SESSION[ssig() . 'propositions'] = array();
	$_SESSION[ssig() . 'nbtries'] = 0;
}

function make_anr_guess($term) {
    //echo "<P>in display_wordrelation_list value :" . $_POST['gotermsubmit'] . "--" . $_POST['gotermrel'];
    if (($_POST['go_anr_submit']!= "") || ($_GET['go_anr_submit']!= "")){
    	
    if 	($term == "") {
    	display_warning("<br>entrez un terme !");
    	return;
    }
	$id = term_exist_in_BD_p($term);
	
	if ($id == 0) {display_warning("<br>Le terme $term n'existe pas !");}
	else {
		$_SESSION[ssig() . 'clues'][$_SESSION[ssig() . 'nbtries']] = $term;
		
		$_SESSION[ssig() . 'assoc']["n"] = array();
		$_SESSION[ssig() . 'assoc']["w"] = array();
		
	    $formated_term = format_entry($term);

	    $query= "SELECT N1.name, R.w from Relations as R, Nodes as N1 WHERE
	    R.node1 = N1.id and R.node2 = \"$id\"
	    and N1.id != \"$id\" and
	    R.type not in (1, 2, 4, 12, 18, 29, 36)
	    and N1.name not like \"::%\"
	    ORDER by R.w DESC";
	    //echo "<br>$query<BR>";
	   
	    $r =  @mysql_query($query) or die("pb in display_wordrelation_list : $query");
	    $nb = mysql_num_rows($r);
	    for ($i=0 ; $i<$nb ; $i++) {
			$name = mysql_result($r , $i , 0);
			$w = mysql_result($r , $i , 1);
			$_SESSION[ssig() . 'assoc']['n'][$i] = $name;
			$_SESSION[ssig() . 'assoc']['w'][$i] = 1*$w;
			
	      //echo "<li>$name - $w<BR>";
	      }
	    //echo "array name";
	  	//	print_r($_SESSION[ssig() . 'assoc']['n']);
	   // print_r($_SESSION[ssig() . 'assoc']['w']);
	    
	    $newar1 = my_array_combine($_SESSION[ssig() . 'assoc']['n'], $_SESSION[ssig() . 'assoc']['w']);
	   // print_r($newar1);
	    
	    //--------------------
	    $_SESSION[ssig() . 'assoc']["n"] = array();
		$_SESSION[ssig() . 'assoc']["w"] = array();
		
	    $formated_term = format_entry($term);

	    $query= "SELECT N1.name, R.w from Relations as R, Nodes as N1 WHERE
	    R.node2 = N1.id and R.node1 = \"$id\"
	    and N1.id != \"$id\" and
	    R.type not in (1, 2, 4, 12, 18, 29, 36)
	    and N1.name not like \"::%\"
	    ORDER by R.w DESC";
	    //echo "<br>$query<BR>";
	   
	    $r =  @mysql_query($query) or die("pb in display_wordrelation_list : $query");
	    $nb = mysql_num_rows($r);
	    for ($i=0 ; $i<$nb ; $i++) {
			$name = mysql_result($r , $i , 0);
			$w = mysql_result($r , $i , 1);
			$_SESSION[ssig() . 'assoc']['n'][$i] = $name;
			$_SESSION[ssig() . 'assoc']['w'][$i] = $w;
			
	      //echo "<li>$name - $w<BR>";
	      }
	    //echo "<p>array name";
	  	//	print_r($_SESSION[ssig() . 'assoc']['n']);
	   // print_r($_SESSION[ssig() . 'assoc']['w']);
	    
	    $newar2 = my_array_combine($_SESSION[ssig() . 'assoc']['n'], $_SESSION[ssig() . 'assoc']['w']);
	    //ksort($newar2);
	    //print_r($newar2);
	    
	    
	    $newar3 = my_array_fusion($newar1, $newar2) ;
	    arsort($newar3);
	  //  print_r($newar3);
	    
	    
	    
		$nbt = $_SESSION[ssig() . 'nbtries'];
		$_SESSION[ssig() . 'cand'][$nbt] = $newar3;
		
	//	echo "<p>candidats == ";
	//	print_r($newar3);
		
		
		if ($nbt > 0) {
			$_SESSION[ssig() . 'guesses'][$nbt] = my_array_intersect($_SESSION[ssig() . 'guesses'][$nbt-1],
												            $_SESSION[ssig() . 'cand'][$nbt]);
		} else {
			$_SESSION[ssig() . 'guesses'][0] = $_SESSION[ssig() . 'cand'][0];
			
		}
		arsort($_SESSION[ssig() . 'guesses'][$nbt]);
		//echo "<p>essai num $nbt";
	//	echo "<p>suggestions == ";
		//print_r($_SESSION[ssig() . 'guesses'][$nbt]);
		
		
		$tab = array_keys($_SESSION[ssig() . 'guesses'][$nbt]);
		$proposition = $tab[0];
		
		$nbtbis = $nbt+1;
		if ($proposition == "") {
		echo "<p><br><br>Apr�s $nbtbis indice(s), je suis perdu, il vaut mieux recommencer !";	
		} else {
		echo "<p><br><br>Apr�s $nbtbis indice(s), je pense qu'il s'agit de : <center><h1>$proposition</h1></center>";
		$_SESSION[ssig() . 'propositions'][$nbt] = $proposition;
		$_SESSION[ssig() . 'guesses'][$nbt][$proposition] = 0;
		}
		
		$_SESSION[ssig() . 'nbtries'] = $_SESSION[ssig() . 'nbtries'] +1 ;
	    }
    }
}



function my_array_combine ($ar1, $ar2) {
	$newar = array();
	$nb = count($ar1);
	for ($i=0 ; $i<$nb ; $i++) {
		if ($newar[$ar1[$i]] == 0) {
			$newar[$ar1[$i]] = $ar2[$i];
		} else {
			$newar[$ar1[$i]] = $newar[$ar1[$i]] + $ar2[$i];
		}
	}
	return $newar;
}

function my_array_fusion($ar1, $ar2) {
	
	$newar = array();
	$tab = array_keys($ar1);
	$nb = count($tab);
	for ($i=0 ; $i<$nb ; $i++) {
		$newar[$tab[$i]] = $ar1[$tab[$i]];
	}

	$tab = array_keys($ar2);
	$nb = count($tab);
	for ($i=0 ; $i<$nb ; $i++) {
		if ($newar[$tab[$i]] == 0) {
			$newar[$tab[$i]] = $ar2[$tab[$i]];
		} else {
			$newar[$tab[$i]] = ($newar[$tab[$i]] + $ar2[$tab[$i]]);
		}
	}
	return $newar;
}

function my_array_intersect($ar1, $ar2) {
	//print_r($ar1);
	//print_r($ar2);
	//echo "dans array intersect";
	$newar = array();
	$tab = array_keys($ar1);
	$nb = count($tab);
	for ($i=0 ; $i<$nb ; $i++) {
		if ($ar1[$tab[$i]]>0 && $ar2[$tab[$i]]>0) {
			$newar[$tab[$i]] = sqrt($ar1[$tab[$i]] * $ar2[$tab[$i]]);
		}
	}
//	print_r($newar);
	return $newar;
}

?>

<div class="jdm-level1-block">
	
	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
    <?php echo "Pense � un mot et je vais tenter de le deviner..."; ?>
    </div>
	</div>

    <div class="jdm-login-block">
    <?php  loginblock(); ?>
    </div>
</div>

<div class="jdm-level2-block">
<TABLE border="0" width="100%" cellspacing="0" cellpadding="0%"
	summary="jeuxdemots" bgcolor="white">

<TR><TH bgcolor="#FFFFCC" valign="top" width="200pts">

<?php
	echo "<form id=\"go_anr_form\" name=\"go_anr_form\" method=\"post\" action=\"ANR.php\" >
		Je veux<br>
	    <input id=\"go_anr_reinit\" type=\"submit\" name=\"go_anr_reinit\" value=\"recommencer\">
	    </form><P>";
	

?>
	<TH bgcolor="#FFFFCC" align="center">
	<BR>
    <?php
	    make_anr_form();
    ?>
    <P><BR>
    <TH bgcolor="#FFFFCC" valign="top" width="200pts">
   

<TR><TH bgcolor="#FFFFCC" valign="top" width="200pts">
	<TH bgcolor="#FFFFCC" align="center">
	<TABLE border="1" width="70%" cellspacing="0" cellpadding="10%"
	summary="jeuxdemots" bgcolor="white">
	<TR><TH bgcolor="#FFFFCC" align="right" width="40%">
		Vos indices
		<TH bgcolor="#FFFFCC" align="left" width="40%">
		Mes propositions
	<TR><TH bgcolor="#FFFFCC" align="right" width="40%">
	 <?php
	$nb = count($_SESSION[ssig() . 'clues']);
	if ($nb >= 0) {
	}
	for ($i=0 ; $i<$nb ; $i++) {
		$clue = $_SESSION[ssig() . 'clues'][$i];
		echo "<font size=\"-1\">$clue</font><br>";
	}
	?>
	<TH bgcolor="#FFFFCC" align="left" width="40%">
	
	<?php 
	$nb = count($_SESSION[ssig() . 'propositions']);
	if ($nb >= 0) {
	
	}
	for ($i=0 ; $i<$nb ; $i++) {
		$prop = $_SESSION[ssig() . 'propositions'][$i];
		echo "<font size=\"-1\">$prop</font><br>";
	}
	?>

</TABLE>
	<TH bgcolor="#FFFFCC" valign="top" width="200pts">
	
</TABLE>
</div>

<?php playerinfoblock($_SESSION[ssig() . 'playerid']) ?>

<?php 
    bottomblock();
    closeconnexion();
?>

  </body>
</html>
