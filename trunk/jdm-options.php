<?php session_start();?>
<?php include 'misc_functions.php' ; ?>
<?php 	
    // imp�rativement en premier
    // NE RIEN METTRE AVANT
    //
    open_session();
    openconnexion();
    check_busted();
    busted_warning();
    ?>

<html>
  <head>
    <title><?php echo get_msg('OPTIONS-TITLE'); ?></title>
    <?php header_page_encoding(); ?>
  </head>
<?php include 'HTML-body.html' ; ?>
<?php topblock(); ?>

<?php
echo "\n<img width=101% STYLE=\"opacity:0.4;height:900px;z-index:-1;position:absolute;top:-400px;left:0px;\"  src=\"pics/circle.png\">";
//echo "\n<img STYLE=\"opacity:0.4;width:1000px;height:200px;z-index:-1;position:absolute;top:-50px;left:-500px;\"  src=\"pics/circle.png\">";
?>

<div class="jdm-level1-block">
	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
    <?php echo get_msg('OPTIONS-PROMPT'); ?>
    </div>
    </div>

    <div class="jdm-login-block">
    <?php  loginblock(); ?>
    </div>
</div>

<div class="jdm-level2-block">
<TABLE	border="0"
	width="100%"
	cellspacing="0" cellpadding="0%"
	summary="jeuxdemot">
	
<TR><TH width="200">
    <TH align= "left"><font size="3">
           <?php make_css_url_form(); ?>
           <P>
    <TH>

<TR><TH width="200">
    <TH align= "left"><font size="3">
    <P>
           <?php make_css_museum_url_form(); ?>
    <TH>
    
<TR><TH width="200">
    <TH align= "left"><font size="3">
    <P><P><P>
           <?php make_perso_url_form(); ?>
    <TH>

<TR><TH width="200">
    <TH>
    <HR>
    <p><BR><BR>

    <?php 	if ($_SESSION[ssig() . 'harddebugmode'] == "Y") {
	    echo "<P><font size=\"7\" color=\"red\"><b>JeuxdeMots <P>is not available</b></font>";
	}
    ?>
    <P><?php produceplayasform(); ?>
    
    <TH align="center" width="100">
  
<TR><TH>
    <TH align= "left"><font size="3">
    <TH>
   
</TABLE>
</div>
<?php

function make_css_url_form() {
    $cur_url = get_css_url($_SESSION[ssig() . 'playerid']);
    if ($cur_url == "") {$cur_url = $_SESSION[ssig() . 'DEFAULT-CSS-URL'];}

    $def_url = $_SESSION[ssig() . 'DEFAULT-CSS-URL'];
    if ($_SESSION[ssig() . 'playerid'] == 0) {$tag="disabled";}
    echo "<div class=\"jdm_css_form\">
	<form id=\"cssform\" name=\"cssform\" method=\"post\" action=\"jdm-options.php\" >";
	// 	CSS :<BR>
	//<input $tag id=\"go_css_submit\" type=\"submit\" name=\"go_css_submit\" value=\"Changer\"> l'url
	//<input  id=\"go_css_url\" type=\"text\" size = \"120\" name=\"go_css_url\" value=\"$cur_url\">
	//(par d�faut mettre \"$def_url\")
	$GLOBALS[0] = $tag;
	$GLOBALS[1] = $cur_url;
	$GLOBALS[2] = $def_url;
	echo get_msg('OPTIONS-CSS-FORM');
	echo "</form></div>";

 	echo get_msg('OPTIONS-CSS-EXAMPLE'), "http://www.lirmm.fr/jeuxdemots/jdm.css";

    form_display_css_url(trim($_POST['go_css_url']));	
    
}

function form_display_css_url($css_url) {
    //echo $_POST['go_css_submit'];
    if ($_POST['go_css_submit'] != "") {
		$url = $_POST['go_css_url'];
		set_css_url($_SESSION[ssig() . 'playerid'], $url);
	echo "<BR>Changement ok : $url ";

	echo "<script language=\"JavaScript1.2\"> 
	    window.setTimeout(\"location=('jdm-options.php');\",1000) 
	    </script>  ";

	echo "<script language=\"JavaScript1.2\">
		    window.onload=start_countdown
		    </script>";
    }
}

//------

function make_css_museum_url_form() {
    $cur_url = get_css_museum_url($_SESSION[ssig() . 'playerid']);
	if ($cur_url == "") {$cur_url = "http://www.lirmm.fr/jeuxdemots/jdm-museum.css";}

    if ($_SESSION[ssig() . 'playerid'] == 0) {$tag="disabled";}
    
    
    echo "<div class=\"jdm_css_form\">
	<form id=\"css_museum_form\" name=\"css_museum_form\" method=\"post\" action=\"jdm-options.php\" >";
//	CSS de votre Petit Mus�e :<BR>
//	<input $tag id=\"go_css_museum_submit\" type=\"submit\" name=\"go_css_museum_submit\" value=\"Changer\"> l'url
//	<input  id=\"go_css_museum_url\" type=\"text\" size = \"120\" name=\"go_css_museum_url\" value=\"$cur_url\">
	$GLOBALS[0] = $tag;
	$GLOBALS[1] = $cur_url;
	echo get_msg('OPTIONS-CSS-MUSEUM-FORM');
	echo "</form></div>";

    echo get_msg('OPTIONS-CSS-EXAMPLE'), "http://www.lirmm.fr/jeuxdemots/jdm-museum.css";
    echo "  ---  mettre - pour fermer";
    
    form_display_css_museum_url(trim($_POST['go_css_museum_url']));	
    
}

function form_display_css_museum_url($css_url) {
    //echo $_POST['go_css_museum_submit'];
    if ($_POST['go_css_museum_submit'] != "") {
		$url = $_POST['go_css_museum_url'];
		set_css_museum_url($_SESSION[ssig() . 'playerid'], $url);
		echo "<BR>Changement ok : $url ";

		echo "<script language=\"JavaScript1.2\"> 
	    window.setTimeout(\"location=('jdm-options.php');\",1000) 
	    </script>  ";

		echo "<script language=\"JavaScript1.2\">
		    window.onload=start_countdown
		    </script>";
    }
}

//-----

function make_perso_url_form() {
    $cur_url = get_perso_url($_SESSION[ssig() . 'playerid']);
    
    if ($_SESSION[ssig() . 'playerid'] == 0) {$tag="disabled";}
    echo "<div class=\"jdm_perso_form\">
	<form id=\"persoform\" name=\"persoform\" method=\"post\" action=\"jdm-options.php\" >";

	$GLOBALS[0] = $cur_url;
	echo get_msg('OPTIONS-PERSO-FORM');
	echo "</form></div>";
    form_perso_css_url(trim($_POST['go_perso_url']));	
    
}

function form_perso_css_url($css_url) {
    //echo $_POST['go_css_submit'];
    if ($_POST['go_perso_submit'] != "") {
		$url = $_POST['go_perso_url'];
		set_perso_url($_SESSION[ssig() . 'playerid'], $url);
	echo "<BR>Changement ok : $url ";

	echo "<script language=\"JavaScript1.2\"> 
	    window.setTimeout(\"location=('jdm-options.php');\",1000) 
	    </script>  ";

	echo "<script language=\"JavaScript1.2\">
		    window.onload=start_countdown
		    </script>";
    }
}


?>

<?php playerinfoblock($_SESSION[ssig() . 'playerid']) ?>
<?php bottomblock(); ?>
<?php 
    busted_warning();
    closeconnexion();
?>
  </body>
</html>
