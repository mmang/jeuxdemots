<?php session_start();?>
<?php include_once 'misc_functions.php'; ?>
<?php
    openconnexion();
	$_SESSION[ssig() . 'state']=0;
?>
<html>
 <head>
    <title>JeuxDeMots: help</title>
    <?php header_page_encoding(); ?>
  </head>
<?php include 'HTML-body.html' ; ?>
<?php topblock(); ?>
<div class="jdm-level1-block">

	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
    <?php echo "JeuxDeMots help (to be translated)"; ?>
    </div>
	</div>

    <div class="jdm-login-block">
    <?php  loginblock(); ?>
    </div>
</div>

<div class="jdm-level2-block">

<TABLE	border="0"
	width="100%"
	cellspacing="0" cellpadding="0%"
	summary="aide" bgcolor="white"
	>
<TR><TH width="200">
    <TH>
    <TH width="150">

<TR valign= "top">
    <TH align="right"><h2>Principe<BR>général</h2>
    <TH align="left"> <blockquote>
	    Lorsqu'une partie démarre une consigne concernant un type de compétence (synonymes, contraires, etc) est affichée. 
	    Plus vous aurez de réponses en commun avec un autre joueur, meilleurs seront vos gains. Les réponses spécifiques et peu rencontrées rapportent plus 
	    que des réponses générales.
	   <br><br>
	    Pour pouvoir jouer, vous devez avoir pris connaissance et accepté la charte de JeuxDeMots.
	    </blockquote>
	<spacer type="block" width="1" height="30">
    <TH>

<TR valign= "top">
    <TH align="right"><h2>Histoire<br>d'honneur</h2>
    <TH align="left"><blockquote>
    L'honneur représente votre performance. Lors d'une partie vous pouvez gagner ou perdre de l'honneur. Ne pas répondre n'est pas très honorable.
    L'honneur à tendance à s'effriter avec le temps.
	    </blockquote>

    <spacer type="block" width="1" height="30">
    <TH>

<TR valign= "top">
    <TH align="right"><h2>Histoire<br>d'argent</h2>
    <TH align="left"><blockquote>
	 L'argent virtuel de JeuxDeMots sont les crédits. Ceux-ci sont gagnés lors des parties, en fonction de la pertinence de vos réponses. 
	    Les crédits vous servent à plusieurs choses, notemment à acheter du temps lors d'une partie, mais aussi à débloquer l'accès à certaines compétences.
	</blockquote>
	<spacer type="block" width="1" height="30">
    <TH>

<TR valign= "top">
    <TH align="right"><h2>Histoire<br>de compétences</h2>
    <TH align="left"><blockquote>
	 Un nouveau joueur n'a accès au départ qu'à un seul type de consigne (on parlera de compétence). 
	    Dans le souk, vous pouvez acheter le droit de joueur avec d'autres compétences. Sachez qu'une compétence à une cotation qui influence 
	(à la baisse ou à la hausse) vos gains. Plus une compétence est joué, plus cette cotation baisse. 
	    Les compétences rares et difficiles valent donc plus que les autres. Toujours dans le souk, vous pouvez ajuster la probabilité
	    qu'une partie en création corresponde à une de vos compétences. Ainsi, il vous est possible de joueur préférentiellement avec les compétences
	    qui vous rapportent le plus ou que vous préférez. Vous n'influencez pas la proportion d'apparition des parties en atente.
	</blockquote>
	<spacer type="block" width="1" height="30">
    <TH>

<TR valign= "top">
    <TH align="right"><h2>Histoire<br>de niveaux</h2>
    <TH align="left"><blockquote>
	 Les joueurs ont un niveau d'expertise et les mots un niveau de difficulté. 
	Plus le niveau du joueur est élevé, plus il a de chance d'être confronté à des mots difficiles. 
	Faire des points face à un mot difficile fait augmenter son propre niveau, mais ne pas faire de points face à un mot de faible niveau
	fait baisser son propre niveau. 
	Le niveau de difficulté des mots s'ajuste selon l'activité des joueurs.
	</blockquote>
	<spacer type="block" width="1" height="30">
    <TH>


<TR valign= "top">
    <TH align="right"><h2>Histoire<br>de réponses</h2>
    <TH align="left"><blockquote>
	Le bouton "passer" permet de finir la partie avant l'échéance de temps. Si vous pensez qu'il n'y a pas du tout de réponse à la consigne donnée
	vous pouvez mettre *** comme réponse. Si votre partenaire pense comme vous, vous aurez des gains. Attention, une telle réponse termine immédiatement la partie 
	en cours et annule les réponses que vous auriez pu donner avant. <BR>
	Les mots tabous ne sont pas interdits, mais ils vous rapporteront bien peu... tâchez d'en trouver d'autres.
	</blockquote>
	<spacer type="block" width="1" height="30">
    <TH>

<TR valign= "top">
    <TH align="right"><h2>Foire<br>aux questions</h2>
    <TH align="left"><blockquote>
	Q: Après 10 ou 11 propositions le chronomètre tombe spontanément à "terminé". Normal ?
	<br>R : oui, c'est normal, le nombre de propositions est "plus ou moins" limité a 10.
	<P>
	Q: J'ai ajusté une de mes compétences à 100% et pourtant j'ai parfois des consignes correspondant à d'autres compétences. Normal ?
	<br>R : oui, c'est normal, l'ajustement ne concerne que les parties en création, et pas celles à finir (où on obtient le résultat immédiatement).
	</blockquote>
	<spacer type="block" width="1" height="30">
    <TH>

<TR><TH>
    <TH align="left">
    <HR width=100%>
    <spacer type="block" width="1" height="30">
    <?php produceplayasform(); ?>

</TABLE>
</div>
<P>


<?php 
    bottomblock();
    closeconnexion();
?>

  </body>
</html>
