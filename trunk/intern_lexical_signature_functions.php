<?php session_start();?>
<?php include_once 'misc_functions.php'; ?>

<?php

$_SESSION['rel'] = '(666, 19, 0, 3, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 20, 21, 22, 23, 24, 25, 26, 27, 30, 31, 33, 37, 45, 46, 47, 48)';

function fetch_arg($s) {
	$arg = $_POST[$s];
	if ($arg != '') {return $arg;}
	$arg = $_GET[$s];
	return $arg;
}



function store_signature($sig, $termid, $level=0){
	if ($termid == 0) {return;}
	$string = serialize_sig($sig);
	if (trim($string) == '') {return;}
	//echo "serial=$string";
	// on verif l'existence
	$query = "SELECT count(id) FROM `LexSig` WHERE termid = '$termid'"  ;
	$r =  @mysql_query($query) or die("pb1 in store_signature : $query");
	$nb = mysql_result($r , 0 , 0);

	if ($nb > 0) {
		// existe deja => maj
		$query = "UPDATE `LexSig` SET sig=\"$string\",date=now(),iter=$level WHERE termid=$termid";
		$r =  @mysql_query($query) or die("pb2 in store_signature : $query");
	} else {
		// n'existe pas => creation
		$query = "INSERT INTO LexSig (termid, sig, date, iter) VALUES('$termid', '$string', now(), $level);";
		$r =  @mysql_query($query) or die("pb3 in store_signature : $query");
	}
}

function fetch_signature($termid){
	$query = "SELECT sig FROM `LexSig` WHERE termid = '$termid'"  ;
	$r =  @mysql_query($query) or die("pb1 in fetch_signature : $query");
	$nb =  mysql_num_rows($r);
	if ($nb > 0) {
		return mysql_result($r , 0 , 0);
	} else {
		return -1;
	}
}



function get_sum_w($termid) {
	$rel = $_SESSION['rel'];
	$query = "SELECT sum(w) FROM Relations WHERE node2 = $termid and type in $rel;";
	$r =  @mysql_query($query) or die("bug in get_sum_w : $query");
	return mysql_result($r , 0 , 0);	
}

function get_lum($termid) {
	$query = "SELECT w FROM Nodes WHERE id = $termid;";
	$r =  @mysql_query($query) or die("bug in get_sum_w : $query");
	return mysql_result($r , 0 , 0);	
}

function compute_lexical_signature($term, $dir, $limit=500) {
	//echo "<P>compute_lexical_signature($term)";
	//$limit = 500;
	$nbturn=1;
	$rel = $_SESSION['rel'];
	
	$id = term_exist_in_BD_p($term);
	if ($id <= 0) {return;}
	$sig = array();
	$sig[$id]=1;
	//print_r($sig);
	
	$newsig = array();
	for ($i=0 ; $i<$nbturn ; $i++) {
		foreach ($sig as $key => $value) {
		//	echo "<P>key=$key";
			flush();
			$norm = 0;
			if ($dir == 0) {
    			$query = "SELECT node1, sum(w) FROM Relations WHERE node2 = $key and type in $rel group by node1 ORDER BY w DESC LIMIT $limit;";
			} else {
				$query = "SELECT node2, sum(w) FROM Relations WHERE node1 = $key and type in $rel group by node2 ORDER BY w DESC LIMIT $limit;";
			}
			
			$r =  @mysql_query($query) or die("bug in compute_lexical_signature : $query");
			$nb =  mysql_num_rows($r);
    		for ($j=0 ; $j<$nb ; $j++) {
    			$node2_id = mysql_result($r , $j , 0);
    			$w = mysql_result($r , $j , 1);
    			
    			$sum = get_sum_w($node2_id);
    			//$lum = max(10,get_lum($node2_id));
    			//$sum = 1;
    			//echo "<br>id=$node2_id w=$w sum=$sum";
    			//$val = ($w/$sum * log($sum,10));
    			$val = $w;
    			
    			$newsig[$node2_id]=$newsig[$node2_id]+ $val;
    			
    			$norm = $norm + ($val*$val);
    			}
		}
		
	}

	$norm = sqrt($norm);
	//echo "<P>norm= $norm";
	
	foreach ($newsig as $key => $value) {
		$newsig[$key] = $value/$norm;
	}
	//echo "<P>Result $term = ";
	//arsort($newsig);
	//print_r($newsig);
	//decode_sig($newsig);
	
	return $newsig;
}

function compute_lexical_signature_bothdir($term, $limit=500) {
	//echo "<P>compute_lexical_signature_bothdir($term)";
	
	$rel = $_SESSION['rel'];
	
	$key = term_exist_in_BD_p($term);
	if ($key <= 0) {return;}

	$newsig = array();
	
	$query = "SELECT node1, sum(w) FROM Relations WHERE node2 = $key and type in $rel group by node1 
	ORDER BY w DESC LIMIT $limit;";
	$r =  @mysql_query($query) or die("bug in compute_lexical_signature : $query");
	$nb =  mysql_num_rows($r);
    for ($j=0 ; $j<$nb ; $j++) {
    	$node2_id = mysql_result($r , $j , 0);
    	$w = mysql_result($r , $j , 1);
    	//$sum = get_sum_w($node2_id);
    	$newsig[$node2_id]=$newsig[$node2_id]+ $w;
    }
    $query = "SELECT node2, sum(w) FROM Relations WHERE node1 = $key and type in $rel group by node2 
    ORDER BY w DESC LIMIT $limit;";
	$r =  @mysql_query($query) or die("bug in compute_lexical_signature : $query");
	$nb =  mysql_num_rows($r);
    for ($j=0 ; $j<$nb ; $j++) {
    	$node2_id = mysql_result($r , $j , 0);
    	$w = mysql_result($r , $j , 1);
    	//$sum = get_sum_w($node2_id);
    	$newsig[$node2_id]=$newsig[$node2_id]+ $w;
    }

	$norm = compute_norm($newsig);
	//echo "<P>norm= $norm";
	foreach ($newsig as $key => $value) {
		$newsig[$key] = $value/$norm;
	}
	//echo "<P>Result $term = ";
	//arsort($newsig);
	//print_r($newsig);
	//decode_sig($newsig);
	
	return $newsig;
}

function check_need_update($term_id) {
	//$tag = 'check_need_update' . rand();
	//start_time_record($tag);
	//return false;
	//$rel = $_SESSION['rel'];
	$query = "SELECT max(touchdate) FROM Relations WHERE node1=$term_id";
	//$query = "SELECT touchdate FROM Relations WHERE node1 = $term_id or node2=$term_id and type in $rel ORDER BY touchdate DESC LIMIT 1;";
	$r1 =  @mysql_query($query) or die("bug 1 in check_need_update($term_id) : $query");
	$date11 = mysql_result($r1 , 0 , 0);
	//echo "<p>date relations $date1";
	$query = "SELECT max(touchdate) FROM Relations WHERE node2=$term_id";
	$r1 =  @mysql_query($query) or die("bug 2 in check_need_update($term_id) : $query");
	$date12 = mysql_result($r1 , 0 , 0);
	
	$date1 = max($date11, $date12);
	
	$query = "SELECT date FROM LexSig WHERE termid = $term_id;";
	$r2 =  @mysql_query($query) or die("bug 3 in check_need_update($term_id) : $query");
	$date2 = mysql_result($r2 , 0 , 0);
	//echo "<p>date sig $date2";
	//$duree = end_time_record($tag);
	//echo ("<p>2 duree pour '$tag' = $duree");
	
	return ($date1 > $date2);
}

function compute_norm($sig) {
	$sum = 0;
	foreach ($sig as $key => $value) {
		$sum = $sum + ($value*$value);
	}
	return sqrt($sum);
}

function decode_sig($sig, $max=1000) {
	//$j = 0;
	arsort($sig);
	$j=0;
	foreach ($sig as $key => $value) {
		if ($j < $max) {
			$term = get_term_from_id($key);
			$val = round($value, 2);
			echo "$term:$val ";
			//echo utf8_encode("$term");
			//echo "$term  ";
			$j++;
		} else {echo ' ... '; return;}
	}
}

function decode_sig_nosort($sig, $max=1000) {
	//$j = 0;
	$j=0;
	foreach ($sig as $key => $value) {
		if ($j < $max) {
			$term = get_term_from_id($key);
			$val = round($value, 2);
			echo "$term:$val ";
			//echo utf8_encode("$term");
			//echo "$term  ";
			$j++;
		} else {echo ' ... '; return;}
	}
}

function serialize_sig($sig) {
	$j = 0;
	arsort($sig);
	$serial = '';
	foreach ($sig as $key => $value) {
		$term = get_term_from_id($key);
		$val = round($value, 2);
		if ($j==0) {
			$serial = "$key=$val";
		} else { 
			$serial = $serial . '&' . "$key=$val";
		}
		//echo utf8_encode("$term");
		//echo "$term  ";
		$j++;
	}
	return $serial;
}

function unserialize_sig($sig_str) {
	if ($sig_str == -1) {return -1;}
	$sig = array();
	$tab=explode('&',$sig_str);
	$nb=count($tab);
	for ($i=0 ; $i<$nb ; $i++) {
		$ar = explode('=',$tab[$i]);
		$sig[$ar[0]] = $ar[1];
	}
	return $sig;
}

function signature_sim($sig1,$sig2) {
	$n2 = compute_norm($sig1);
	$n1 = compute_norm($sig2);
	
	//echo "<p>n1=$n1 n2=$n2";
	
	$sim = 0;
	foreach ($sig1 as $key => $value) {
		$sim = $sim + ($value * $sig2[$key]);
	}
	//echo "<p>sum=$sim";
	return sqrt($sim)/($n2 * $n1);
}

function signature_sim_biz($id1, $id2, $sig1, $sig2, $verbose=true) {
	$n2 = compute_norm($sig1);
	$n1 = compute_norm($sig2);
	
	$sim1 = $sig1[$id2];
	$sim2 = $sig2[$id1];
	//echo "<p>n1=$n1 n2=$n2";
	
	$sim = 0;
	foreach ($sig1 as $key => $value) {
		// $key = 151591 ==> ***
		if ($key != 151591) {
			$sim = $sim + ($value * $sig2[$key]);
		}
	}
	//echo "<p>sum=$sim";
	$sim3 =  sqrt($sim)/($n2 * $n1);
	
	if ($verbose == true) {echo "<br>1:$sim1 2:$sim2 3:$sim3";}
	return max(max($sim1,$sim2),$sim3);
}

function signature_sim_biz_new($id1, $id2, $s1, $s2, $verbose=true) {
	$n2 = compute_norm($s2);
	$n1 = compute_norm($s1);
	
	$sim1 = $s1[$id2]; $sim2 = $s2[$id1];
	//echo "<p>n1=$n1 n2=$n2";
	
	$c1 = count($sig1);$c2 = count($sig2);
	if ($c1 < $c2) {
		$sig1 = $s1; $sig2 = $s2;
	} else {
		$sig1 = $s2; $sig2 = $s1;
	}
	
	$sim = 0;
	foreach ($sig1 as $key => $value) {
		// $key = 151591 ==> ***
		if ($key != 151591) {
			$sim = $sim + ($value * $sig2[$key]);
		}
	}
	//echo "<p>sum=$sim";
	$sim3 =  sqrt($sim)/($n2 * $n1);
	
	if ($verbose == true) {echo "<br>1:$sim1 2:$sim2 3:$sim3";}
	return max(max($sim1,$sim2),$sim3);
}


function add_sig($sig1,$sig2, $poids=1) {
	$newsig = array();
	foreach ($sig1 as $key => $value) {
		$newsig[$key]=$newsig[$key]+$value;
	}
	foreach ($sig2 as $key => $value) {
		$newsig[$key]=$newsig[$key]+$value*$poids;
	}
	return $newsig;
}

function add_sig_boolean($sig1,$sig2) {
	$newsig = array();
	foreach ($sig1 as $key => $value) {
		$newsig[$key]=1;
	}
	foreach ($sig2 as $key => $value) {
		$newsig[$key]=1;
	}
	return $newsig;
}

function add_sig_count($sig1,$sig2, $poids=1) {
	$newsig = array();
	foreach ($sig1 as $key => $value) {
		$newsig[$key]=$newsig[$key]+max($poids,$value);
	}
	foreach ($sig2 as $key => $value) {
		$newsig[$key]=$newsig[$key]+max($poids,$value);
	}
	return $newsig;
}


function compute_full_lexical_signature($term, $limit=500) {
	$term=stripslashes(trim($term));
	/*$id = term_exist_in_BD_p($term);
	$sig1 = compute_lexical_signature($term, 1);
	$sig2 = compute_lexical_signature($term, 0);
	$sig = add_sig($sig1,$sig2,1);
	
	$norm = compute_norm($sig);
	
	foreach ($sig as $key => $value) {
		$sig[$key] = $value/$norm;
	}*/
	
	$sig = compute_lexical_signature_bothdir($term, $limit);
	//store_signature($sig,$id);
	return $sig;
}

function compute_full_lexical_signature_level ($term, $level=0) {
	//echo "<br>compute_full_lexical_signature_level ($term, $level) ";
	//flush();
	
	$term=stripslashes(trim($term));
	$id = term_exist_in_BD_p($term);
	
	if ($level == 0) {
		$sig = compute_full_lexical_signature($term);	
	} else {
		$target_sig = compute_full_lexical_signature_level($term, $level-1);
		$newsig = array();
		if (count($target_sig) < 10) {
			foreach ($target_sig as $key => $value) {
				$subterm = get_term_from_id($key);
			 	$subsig = compute_full_lexical_signature_level($subterm, $level-1);
			 	$newsig = add_sig($newsig,$subsig,1);
			}
			
			unset($newsig[$id]);
			// on  normalize subsig
			$norm = compute_norm($newsig);
			foreach ($newsig as $key => $value) {
				$newsig[$key] = $value/$norm;
			}
		
			$sig = add_sig($target_sig,$newsig,pow(1/2,$level));
			// on  normalize subsig
			unset($sig[$id]);
			$norm = compute_norm($sig);
			foreach ($sig as $key => $value) {
				$sig[$key] = $value/$norm;
				}
		} else {
			$sig = $target_sig;
		}
	}
	
	store_signature($sig,$id);
	return $sig;
}









?>
