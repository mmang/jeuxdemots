<?php session_start();?>
<?php include_once 'misc_functions.php'; ?>
<?php openconnexion();?>
<?php
function fetch_arg($name, $default = '') {
	$arg = trim($_POST[$name]);
	if ($arg == "") {
		$arg = trim($_GET[$name]);	
	}
	if ($arg == '') {$arg = $default;}
	return $arg;
}

function generate_nodes() {
	foreach ($_SESSION['rg_node_list'] as $nodeid => $name) {
		//$utf8_name = utf8_encode($name);
		//$name = utf8_encode(addslashes($name));
		$name = addslashes($name);
		echo "\r\n$nodeid [shape=ellipse,label=\"$name\"];";
	}
}

function generate_links() {
	foreach ($_SESSION['rg_rel_list'] as $relid => $val) {
		if ($val == '-1') {
			$query = "SELECT id, node1, node2, w, type FROM Relations WHERE id=$relid;";
			$r =  @mysql_query($query) or die("pb in generate_links : $query");
			$id = mysql_result($r , 0 , 0);
			$node1 = mysql_result($r , 0 , 1);
			$node2 = mysql_result($r , 0 , 2);
			$w = mysql_result($r , 0 , 3);
			$type = mysql_result($r , 0 , 4);
		
			
		} else {
			$split = explode(':', $relid);
			$node1 = $split[0];
			$node2 = $split[1];
			$w = $val;
			$type = '';
		}
		$arg1 = fetch_arg('aff_typerel');
		if ($type == '') {$arg1 = '';}
		$arg2 = fetch_arg('aff_w');
		if (($arg1 != '') && ($arg2 != '')) {$tag = "$type:$w";}
		if (($arg1 == '') && ($arg2 != '')) {$tag = "$w";}
		if (($arg1 != '') && ($arg2 == '')) {$tag = "$type";}
		if (($arg1 == '') && ($arg2 == '')) {$tag = '';}
		echo "\r\n$node1 -> $node2 [label=\"$tag\"];";
	}
}

?>
digraph G {
  <?php generate_nodes(); generate_links(); ?>
  
  
  }
  
