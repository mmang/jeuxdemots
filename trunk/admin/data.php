<?php
	session_start();
	include 'admin_functions.php';
	include 'data_export_functions.php';
	include 'data_import_functions.php';
	$step = 'DATA_FORM';
	if (file_exists('../PARAM.php')) {
		include '../misc_functions.php';
		open_session();
	 	if (!empty($_SESSION[ssig() . 'playerid'])) {
			openconnexion();
	 		if (get_player_admin($_SESSION[ssig() . 'playerid'])) {
		 		$_SESSION[ssig() . 'ADMIN'] = $_SESSION[ssig() . 'login'];
	 		}
	 	}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>JeuxDeMots admin interface</title>
    <link rel="stylesheet" type="text/css" href="admin.css"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  </head>
<body>     
	<h1>JeuxDeMots admin interface</h1>
	<div id="header">
<ul>
	<li><a href="index.php">Parameters</a></li>
	<li id="selected"><a href="data.php">Data</a></li>
	<li><a href="announces.php">Announces</a></li>
	<li><a href="tips.php">Tips</a></li>
	<li><a href="users.php">Users</a></li>
	<li><a href="localize.php">Localization</a></li>
</ul>

</div>

	<div id="content">
 <?php

	/* ADMIN INTERFACE */
	if (!empty($_REQUEST['step'])) {
		$step = $_REQUEST['step'];	
	}
	
	if (empty($_SESSION[ssig() . 'ADMIN']) || empty($_SESSION[ssig() . 'playerid'])) {
		echo 'You are not admin. Try to log in from the <a href="../index.html">index</a> or 
		delete the PARAM.php file in the root directory';
		exit;
	}
	
	if ($step == 'ACTION_DB') {
		if (!empty($_REQUEST['BACKUP_DB'])) {
			$step = 'BACKUP_DB';
		}
		if (!empty($_REQUEST['RESTORE_DB'])) {
			$step = 'RESTORE_DB';
		}
		if (!empty($_REQUEST['UPLOAD_DATA'])) {
			$step = 'UPLOAD_DATA';
		}
		if (!empty($_REQUEST['IMPORT_DATA'])) {
			$step = 'IMPORT_DATA';
		}
		if (!empty($_REQUEST['DELETE_DATA'])) {
			$step = 'DELETE_DATA';
		}
		if (!empty($_REQUEST['EXPORT_TEXT'])) {
			$step = 'EXPORT_TEXT';
		}
		if (!empty($_REQUEST['EXPORT_XML'])) {
			$step = 'EXPORT_XML';
		}
	}

	
	if ($step == 'BACKUP_DB') {
		echo 'Backuping database...<br/>';
		$dbname = $_SESSION[ssig() . 'CONNEXION-DBNAME'];
		$backupdir = $_SESSION[ssig() . 'BACKUPDIR'];
		if ($backupdir !== '' && substr($backupdir, -1) !== '/') {
			$backupdir .= '/';
		}
		$backupFile = $backupdir . $dbname . date("Y-m-d-H-i-s") . '.gz';
		$dbhost = $_SESSION[ssig() . 'CONNEXION-URL'];
		$dbuser = $_SESSION[ssig() . 'CONNEXION-USER'];
		$dbpass = $_SESSION[ssig() . 'CONNEXION-PSWD'];
		$command = "mysqldump  --host=$dbhost --user=$dbuser --password=$dbpass $dbname | gzip > $backupFile";
		system($command);
		echo '<p>See <a href="',$_SESSION[ssig() . 'BACKUPDIR'],'">backup dir</a>  (you can change it in the settings)</p>';
	}


	if ($step == 'RESTORE_DB' && !empty($_REQUEST['RESTORE_DB_FILE'])) {
		echo 'Restoring database...<br/>';
		$dbname = $_SESSION[ssig() . 'CONNEXION-DBNAME'];
		$backupdir = $_SESSION[ssig() . 'BACKUPDIR'];
		if ($backupdir !== '' && substr($backupdir, -1) !== '/') {
			$backupdir .= '/';
		}
		load_sql_file($backupdir . $_REQUEST['RESTORE_DB_FILE'],'gzip');
		echo '<p>Database restored!</p>';
	}
	
	if ($step == 'UPLOAD_DATA' && !empty($_FILES['UPLOAD_DATA_FILE'])) {
		$datadir = $_SESSION[ssig() . 'DATADIR'];
		if ($datadir !== '' && substr($datadir, -1) !== '/') {
			$datadir .= '/';
		}
		echo 'Uploading data...<br/>';
		upload_file($_FILES['UPLOAD_DATA_FILE']['tmp_name'],$_FILES['UPLOAD_DATA_FILE']['name'],$datadir);
		echo '<p>File uploaded</p>';
	}
		
	if ($step == 'IMPORT_DATA'  && !empty($_REQUEST['IMPORT_DATA_FILE'])) {
		$datadir = $_SESSION[ssig() . 'DATADIR'];
		if ($datadir !== '' && substr($datadir, -1) !== '/') {
			$datadir .= '/';
		}
		echo 'Importing data...<br/>';
		$format='text';
		if (findext($_REQUEST['IMPORT_DATA_FILE'])=='gz') {
			$format='gzip';
		}
		jdm_import_data($datadir . $_REQUEST['IMPORT_DATA_FILE'],$format);
		echo '<p>Data imported, now you can <a href="..">play</a>!</p>';
	}
	
	if ($step == 'DELETE_DATA') {
		$query= 'TRUNCATE table Nodes; ';
		$result = mysql_query($query);
		$query = 'TRUNCATE table PendingAnswers; ';
		$result = mysql_query($query);
		$query = 'TRUNCATE table PendingGameData; ';
		$result = mysql_query($query);
		$query = 'TRUNCATE table PendingGames; ';
		$result = mysql_query($query);
		$query = 'TRUNCATE table ProposedGames; ';
		$result = mysql_query($query);
		$query = 'TRUNCATE table ProposedWords; ';
		$result = mysql_query($query);
		$query = 'TRUNCATE table Relations; ';
		$result = mysql_query($query);
		$query = 'TRUNCATE table TrialProposedWords; ';
		$result = mysql_query($query);
		$query = 'TRUNCATE table WantedWords;';
		$result = mysql_query($query);
		$query = 'TRUNCATE table RelationInfo;';
		$result = mysql_query($query);
        if ($result) {
			echo '<p>Data deleted</p>';
		}
		else {
			echo 'Error in the database: ', mysql_error();
		}
	}
	if ($step == 'EXPORT_TEXT') {
		echo 'Outputing data...<br/>';
		jdm_outputing_data($_SESSION[ssig() . 'EXPORTDIR'],'text');
		echo '<p>See <a href="',$_SESSION[ssig() . 'EXPORTDIR'],'">export dir</a></p>';
	}
	
	if ($step == 'EXPORT_XML') {
		echo 'Outputing data...<br/>';
		jdm_outputing_data($_SESSION[ssig() . 'EXPORTDIR'],'xml');
		echo '<p>See <a href="',$_SESSION[ssig() . 'EXPORTDIR'],'">export dir</a></p>';
	}
	
	// STEP EDIT_PARAM
	if ($step == 'DATA_FORM') {
		echo '
			<form action="#" method="post" enctype="multipart/form-data">
				<fieldset name="data management">
				<p>Backup the database <input type="submit" name="BACKUP_DB" value="backup database" />
				<p>Restore the database from <select name="RESTORE_DB_FILE">',dir_files($_SESSION[ssig() . 'BACKUPDIR']),'</select>
					<input type="submit" name="RESTORE_DB" value="restore database" /></p>
				<p>Import word list in text or gzip (.gz) format.
				Each word must be on one line: (word;annotation;gloss#pos).
				annotation and gloss are optional but if you must put both when you want to put only one. 
				annotation can be used for transcriptions or pronunciation. 
				gloss can be used for translation.
				The file must be encoded in UTF-8 (without BOM) and stored in the data dir
				(you can change it in the settings).</p>
				<p>There is already a data set for English called <code>english-word-list.txt.gz</code>
				for testing purposes. You can import it into the database and try to play directly.</p>
				<p>Be patient when importing a big file. It takes several minutes. When the import 
				is finished, you can <a href="..">play</a>!.</p>
				Eg for English:<pre>
abjure#verb
ablative#adj
ablative#noun
ablaut#noun
ablaze#adj
ablaze#adv</pre>

Eg for Japanese:<pre>
図案;ずあん;a design#verb
図画;ずが;drawing#noun
図解;ずかい;schematic#adj
図鑑;ずかん;illustrated reference book#noun
図形;ずけい;figure#noun
</pre>			
	Upload file: <input type="file" name="UPLOAD_DATA_FILE" /> <input type="submit" name="UPLOAD_DATA" value="Upload data" /><br/>
	Import file : <select name="IMPORT_DATA_FILE">',dir_files($_SESSION[ssig() . 'DATADIR']),'</select>
				<input type="submit" name="IMPORT_DATA" value="Import data" /><br/>
	Delete all the data:
				<input type="submit" name="DELETE_DATA" value="Delete data" onclick="confirm(\'Do you want to delete ALL the data?\');"/>
				<p>Export the data network in text format <input type="submit" name="EXPORT_TEXT" value="Export text" />
				<p>Export the data network in GraphML format <input type="submit" name="EXPORT_XML" value="Export XML" />
					<input name="step" type="hidden" value="ACTION_DB"></p>
					<p>See <a href="',$_SESSION[ssig() . 'EXPORTDIR'],'">export dir</a> (you can change it in the settings)</p>
				</fieldset>
			</form>';
	}
	
?>
</div>
	</body>
	</html>
