<?php
	session_start();
	include 'admin_functions.php';
	$step = '';
	if (file_exists('../PARAM.php')) {
		include '../misc_functions.php';
		open_session();
	 	if (!empty($_SESSION[ssig() . 'playerid'])) {
			openconnexion();
	 		if (get_player_admin($_SESSION[ssig() . 'playerid'])) {
		 		$_SESSION[ssig() . 'ADMIN'] = $_SESSION[ssig() . 'login'];
	 		}
	 	}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>JeuxDeMots admin interface</title>
    <link rel="stylesheet" type="text/css" href="admin.css"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  </head>
<body>     
	<h1>JeuxDeMots admin interface</h1>
	<div id="header">
<ul>
	<li><a href="index.php">Parameters</a></li>
	<li><a href="data.php">Data</a></li>
	<li><a href=""announces.php">Announces</a></li>
	<li><a href="tips.php">Tips</a></li>
	<li id="selected"><a href="users.php">Users</a></li>
	<li><a href="localize.php">Localization</a></li>
</ul>

</div>

	<div id="content">
 <?php
	
	if (empty($_SESSION[ssig() . 'ADMIN']) || empty($_SESSION[ssig() . 'playerid'])) {
		echo 'You are not admin. Try to log in from the <a href="../index.html">index</a> or 
		delete the PARAM.php file in the root directory';
		exit;
	}
	
	if (!empty($_REQUEST['search_user'])) {
		if (!empty($_REQUEST['USER_LOGIN'])) {
			$query = 'SELECT * FROM Players WHERE name like \''.$_REQUEST['USER_LOGIN'].'%\'';
			$result = mysql_query($query);
			if (!$result) {
				echo 'Error in looking for a user: ', mysql_error();
			}
		}
		else if (!empty($_REQUEST['USER_EMAIL'])) {
			$query = 'SELECT * FROM Players WHERE email like \''.$_REQUEST['USER_EMAIL'].'%\'';
			$result = mysql_query($query);
			if (!$result) {
				echo 'Error in looking for a user: ', mysql_error();
			}
		}	
		while ($user = mysql_fetch_object($result)) {
			echo '<form method="post">
					<fieldset name="Edit an existing user">
					<legend>Edit an existing user</legend>
					<input type="hidden" name="USER_ID" value="',$user->id,'" />
					Login: <input type="text" name="USER_LOGIN" value="',$user->name,'"/><br/>
					Password: <input type="text" name="USER_PASSWORD" value="',$user->pwd,'"/><br/>
					Email: <input type="text" name="USER_EMAIL" size="30" value="',$user->email,'"/>
					Mailing-list: <input type="checkbox" name="USER_LIST" value="Y" ', ($user->receive_mail_p=='Y')?'checked="checked"':'','"/><br/>
					<input type="submit" name="edit_user" value="Edit" />
					<input type="submit" name="delete_user" value="Delete" />
					</fieldset>
				</form>
				';
		}
	}
	
	if (!empty($_REQUEST['edit_user']) && !empty($_REQUEST['USER_ID'])) {
		//$receiveMail = ($_REQUEST['USER_LIST']=='Y') ? 'Y': '';
		$query = 'UPDATE Players set name="'.$_REQUEST['USER_LOGIN'].'", pwd="'.$_REQUEST['USER_PASSWORD'].'", email="'.$_REQUEST['USER_EMAIL'].'", receive_mail_p="'.$_REQUEST['USER_LIST'].'" WHERE id="'.$_REQUEST['USER_ID'].'"';
		$result = mysql_query($query);
		if (!$result) {
			echo 'Error in updating an existing user: ', mysql_error();
		}	
	}
	
	if (!empty($_REQUEST['delete_user']) && !empty($_REQUEST['USER_ID'])) {
		$query = 'DELETE FROM Players WHERE id="'.$_REQUEST['USER_ID'].'"';
		$result = mysql_query($query);
		if (!$result) {
			echo 'Error in deleting an existing user: ', mysql_error();
		}	
	}
	
	echo '<form method="post">
					<fieldset name="Lookup an existing user">
					<legend>Lookup an existing user</legend>
					Login: <input type="text" name="USER_LOGIN" value=""/><br/>
					or Email: <input type="text" name="USER_EMAIL" value=""/><br/>
					<input type="submit" name="search_user" value="Search" />
					</fieldset>
				</form>
				';
?>
</div>
	</body>
	</html>
