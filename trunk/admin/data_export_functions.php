<?php
function generate_output_stats($format) {
	
	$scom = '//';
	$lcom = '//';
	$ecom = '';
	if ($format=='xml') {
		$scom = '<!--';
		$lcom = '';
		$ecom = '-->';
	}
	
	$string = "";
	
	
	$string =  $string . "$scom Relation stats\n";
	$query = "SELECT id, name FROM RelationTypes;";
	$r =  @mysql_query($query) or die("pb in generate_bd_stats 1: $query:" . mysql_error());
	$nb = mysql_num_rows($r);
	for ($i=0 ; $i<$nb ; $i++) {
	    $reltype = mysql_result($r , $i , 0);
	    $relname = mysql_result($r , $i , 1);
	    $query = "SELECT COUNT(*) FROM Relations WHERE type = $reltype;";
	    $r2 =  @mysql_query($query) or die("pb in generate_bd_stats 2 : $query");
	    $count = mysql_result($r2 , 0 , 0);
	    $string =  $string .  "\n$lcom $count occurrences of relations $relname (t=$reltype)";
	}
	$string =  $string .  "\n$ecom\n";
	$string =  $string .  "$scom Node stats\n";
	$query = "SELECT id, name FROM NodeTypes;";
	$r =  @mysql_query($query) or die("pb in generate_bd_stats 3 : $query");
	$nb = mysql_num_rows($r);
	for ($i=0 ; $i<$nb ; $i++) {
	    $nodetype = mysql_result($r , $i , 0);
	    $nodename = mysql_result($r , $i , 1);
	    $query = "SELECT COUNT(*) FROM Nodes WHERE type = $nodetype;";
	    $r2 =  @mysql_query($query) or die("pb in generate_bd_stats 4 : $query");
	    $count = mysql_result($r2 , 0 , 0);
	    $string =  $string .  "\n$lcom $count occurrences of nodes $nodename (t=$nodetype)";
	}
	
	$string =  $string .  "\n$ecom\n";
	$string =  $string .  "$scom 50 most frequents terms\n";
	$query= "SELECT name, w FROM `Nodes`  ORDER BY `Nodes`.`w`  DESC limit 50";
	$r =  @mysql_query($query) or die("pb in display_wordrelation_list : $query");
	$nb = mysql_num_rows($r);
	for ($i=0 ; $i<$nb ; $i++) {
		$name = mysql_result($r , $i , 0);
		$w = mysql_result($r , $i , 1);
	    $string =  $string .  "\n$lcom n=\"" . $name . "\":w=" . $w;
	}
	$string =  $string .  "\n$ecom\n";

	return($string);
}

function jdm_outputing_data($outputdir,$format = 'text') {

	$realdate = date('l dS \of F Y h:i:s A');
	
	$date  = date("Ymd");
	$extension = '.txt';
	if ($format=='xml') {
		$extension = '.xml';
	}
	$name = $date . '-LEXICALNET-JEUXDEMOTS-'.$_SESSION['LANG'].$extension;
	if ($outputdir !== '' && substr($outputdir, -1) !== '/') {
		$outputdir .= '/';
	}
	$filename = $outputdir . $name . '.gz';
	
	$result_count=mysql_query("SELECT COUNT(*) FROM Nodes");
	$nodesNumber=mysql_result($result_count, 0);
	$result_count=mysql_query("SELECT COUNT(*) FROM Relations");
	$relationsNumber=mysql_result($result_count, 0);

	$entete = '';
	
	if ($format=='xml') {
		$entete = '<?xml version="1.0" encoding="UTF-8"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns"  
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns 
                                http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key id="nn" for="node" attr.name="name" attr.type="string"/>
  <key id="nw" for="node" attr.name="weight" attr.type="int"/>
  <key id="nt" for="node" attr.name="type" attr.type="int"/>
  <key id="na" for="node" attr.name="annotation" attr.type="string"/>
  <key id="ng" for="node" attr.name="gloss" attr.type="string"/>
  <key id="ew" for="edge" attr.name="weight" attr.type="int"/>
  <key id="et" for="edge" attr.name="type" attr.type="int"/>
  <graph id="G" edgedefault="directed" 
            parse.nodes="'.$nodesNumber.'" parse.edges="'.$relationsNumber.'" 
            parse.nodeids="free" parse.edgeids="free" 
            parse.order="nodesfirst">
<!-- ';
	}
	$entete .= '
//JEUXDEMOT LEXICAL NET
//
// Data of JeuxDeMots. (c) Carlos Ramisch & Mathieu Mangeot, 2011
// 
//
// Filename: '.$name.'
// File available at: http://jeuxdemots.liglab.fr/por/admin/export/JDM-LEXICALNET-PT/
// jeuxdeMots : http://jeuxdemots.org/
//
// Licença CreativeCommons Atribuição - CompartilhaIgual 3.0 Portugal
// http://creativecommons.org/licenses/by-sa/3.0/pt/
// 
//
// 
// Aknowledgments
// Main author: Mathieu Lafourcade
// 
// Many many thanks to all players of JeuxDeMots Portuguese.
//
//
//   Carlos Ramisch
//   '.$realdate.'
//
// Équipe GETALP - LIG
// 385 rue de la Bibliothèque F-38041 GRENOBLE CEDEX 9 - FRANCE
// '.$_SESSION[ssig().'CONTACT-EMAIL'].'
// http://jeuxdemots.org/
// http://jeuxdemots.org/world-of-jeuxdemots.php
';
	if ($format=='xml') {
		$entete .= '-->
';
	}

    // Dans notre exemple, nous ouvrons le fichier $filename en mode d'ajout
    // Le pointeur de fichier est placé à la fin du fichier
    if (file_exists($filename)) {
    	unlink($filename);
    }
    if (!$handle = gzopen($filename, 'w9')) {
        echo "Impossible d'ouvrir le fichier ($filename)";
        exit;
    }
 	if (gzwrite($handle, $entete) === FALSE) {
        echo "Impossible d'écrire dans le fichier ($filename)";
		exit;
   	}

    $entete_stats = "\n\n// STATS\n\n";
	if ($format=='xml') {
		$entete_stats = "\n\n<!-- STATS -->\n\n";
	}
 	gzwrite($handle, $entete_stats);
   	$s = generate_output_stats($format);
   	echo "STATS= " . $s;
   	gzwrite($handle, $s);
   	
   	
	$entete_nodes = "\n\n// -- NODES\n\n" 	;
	if ($format=='xml') {
		$entete_nodes = "\n\n<!-- NODES -->\n\n";
	}
 	gzwrite($handle, $entete_nodes);
 	 
 	$offset = 0;
	$limit = 1000;
	echo "(nodes ";
	while ($limit == 1000) {
		$query = "SELECT id,name, annotation, gloss, w,type FROM Nodes ORDER BY name ASC LIMIT $limit OFFSET $offset;";
    	$r =  @mysql_query($query) or die("pb jdm_outputing_data 1 : $query");
		$offset += $limit;
		$limit = mysql_num_rows($r);
		if ($limit >0) {
	 		for ($j=0 ; $j<$limit ; $j++) {
	 			//echo "$id ";
	 			$node = mysql_fetch_object($r);
	   			// Ecrivons quelque chose dans notre fichier.
	   			if ($format == 'xml') {
					$data = "    <node id='n". $node->id . "'>";
					$data .= "<data key='nn'>". $node->name . "</data>";
					if (!empty($node->annotation)) {
						$data .= "<data key='na'>". $node->annotation . "</data>";
					}
					if (!empty($node->gloss)) {
						$data .= "<data key='ng'>". $node->gloss . "</data>";
					}
					$data .= "<data key='nt'>" . $node->type. "</data>";
					$data .= "<data key='nw'>" . $node->w . "</data>";
					$data .= "</node>\n";
	   			}
	   			else {
	   				$data = 'eid='.$node->id.':n="' . $node->name. '":';
					if (!empty($node->annotation)) {
						$data .= "a=". $node->annotation . ":";
					}
					if (!empty($node->gloss)) {
						$data .= "g=". $node->gloss . ":";
					}
					$data .= "t=". $node->type . ":";
					$data .= "w=". $node->w . "\n";
	   			}
	 			if (gzwrite($handle, $data) === FALSE) {
       				echo "Impossible d'écrire dans le fichier ($filename)";
	 			}		
	 		}
		 echo "block $offset<br/>";
		}
	}
	echo ")<br/>";
	
	$entete_relations = "\n\n// -- RELATIONS\n\n" ;
	if ($format=='xml') {
		$entete_relations = "\n\n<!-- RELATIONS -->\n\n";
	}
 	 gzwrite($handle, $entete_relations);
	
 	$offset = 0;
	$limit = 1000;
	echo "(relations ";
	while ($limit == 1000) {
		$query = "SELECT id,node1,node2,type,w FROM Relations ORDER BY id ASC LIMIT $limit OFFSET $offset;";
    	$r =  @mysql_query($query) or die("pb jdm_outputing_data 2 : $query");
		$offset += $limit;
		$limit = mysql_num_rows($r);
		if ($limit >0) {
	 		for ($j=0 ; $j<$limit ; $j++) {
	 			//echo "$id ";
	 			$id = mysql_result($r , $j , 0);
				$node1 = mysql_result($r , $j , 1);
				$node2 = mysql_result($r , $j , 2);
				$type = mysql_result($r , $j , 3);
				$w = mysql_result($r , $j , 4);
	   			// Ecrivons quelque chose dans notre fichier.
				if ($format == 'xml') {
	   				$data = "    <edge id='e$id' source='n$node1' target='n$node2'><data key='et'>$type</data><data key='ew'>$w</data></edge>\n";
	   			}
	   			else {
					$data = "rid=$id:n1=$node1:n2=$node2:t=$type:w=$w\n";
	   			}
	 			if (gzwrite($handle, $data) === FALSE) {
       				echo "Impossible d'écrire dans le fichier ($filename)";
	 			}		
	 		}
		 echo "block $offset<br/>";
		}
	}
	echo ")<br/>";
 	 
 	 
	$epilogue = "\n\n//EOF";
	if ($format=='xml') {
		$epilogue = "  </graph>\n</graphml>\n";
	}
 	 gzwrite($handle, $epilogue);
 	 
 	 echo "L'écriture des donnees dans le fichier ($filename) a réussi";
	 gzclose($handle);
}

?>