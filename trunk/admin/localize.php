<?php
	if (!empty($_REQUEST['step'])) {
		$step = $_REQUEST['step'];	
	}
	else {
		$step ='';
	}
	if (($step=='SEND_FILE') && !empty($_REQUEST['DOWN_FILE'])) {
		$envoiFichier = dirname(dirname(__FILE__)).'/'.$_REQUEST['DOWN_FILE'];
		//send_file($envoiFichier);
		header('Content-disposition: attachment; filename='.$_REQUEST['DOWN_FILE']);
		header('Content-type: text/plain');
		readfile($envoiFichier);
		exit();
	}
	session_start();
	include 'admin_functions.php';
	if (file_exists('../PARAM.php')) {
		include '../misc_functions.php';
		open_session();
	 	if (!empty($_SESSION[ssig() . 'playerid'])) {
			openconnexion();
	 		if (get_player_admin($_SESSION[ssig() . 'playerid'])) {
		 		$_SESSION[ssig() . 'ADMIN'] = $_SESSION[ssig() . 'login'];
	 		}
	 	}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>JeuxDeMots admin interface</title>
    <link rel="stylesheet" type="text/css" href="admin.css"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  </head>
<body>     
	<h1>JeuxDeMots admin interface</h1>
	<div id="header">
<ul>
	<li><a href="index.php">Parameters</a></li>
	<li><a href="data.php">Data</a></li>
	<li><a href="announces.php">Announces</a></li>
	<li id="selected"><a href="localize.php">Localization</a></li>
</ul>

</div>

	<div id="content">
 <?php
	$suffixe = '-'.$_SESSION['LANG'].'.php';

	/* LOCALIZATION INTERFACE */
	
	if (empty($_SESSION[ssig() . 'ADMIN']) || empty($_SESSION[ssig() . 'playerid'])) {
		echo 'You are not admin. Try to log in from the <a href="../index.html">index</a> or 
		delete the PARAM.php file in the root directory';
		exit;
	}
	
	if (($step=='RECEIVE_FILE') && !empty($_FILES['UP_FILE'])) {
		$nomFichier = $_FILES['UP_FILE']['name'];
		if ($nomFichier==$_REQUEST['UP_FILE_NAME']) {
			$cheminLocalFichier = $_FILES['UP_FILE']['tmp_name'];
			$nouveauFichier = dirname(dirname(__FILE__)).'/'.$_REQUEST['UP_FILE_NAME'];
			if (move_uploaded_file($cheminLocalFichier, $nouveauFichier)) {
				echo "File <code>$nomFichier</code> uploaded!";	
			}
			else {
				echo "<span style='color:red;'>Error while uploading the file <code>$nomFichier</code> !</span>";	
			}
		}
		else {
			echo '<span style="color:red;">Your file does not match the correct name: <strong><code>',
			$_REQUEST['UP_FILE_NAME'], '</code></strong></span><br/>';
		}
	}
	if (!empty ($_FILES['photo'])) {
		echo 'pas vide';
	}
	
	// STEP DEFAULT
	
	echo '<p>
		For more explanation about the localization of the files, please read the <a href="../LOCALIZATION.php">
		LOCALIZATION.php</a> file.
	</p>';
	
	echo '
			<form action="#" method="post">
				<fieldset name="Download a parameter file to localize">
					<legend>Download a parameter file to localize</legend>
					<p><select id="DOWN_FILE" name="DOWN_FILE">
						<option value="LOC',$suffixe,'">Localization: LOC',$suffixe,'</option>
						<option value="HELP',$suffixe,'">Help: HELP',$suffixe,'</option>
						<option value="CHARTE',$suffixe,'">Charter: CHARTER',$suffixe,'</option>
					</select>
						<input type="submit" name="download" value="Download" />
					<input name="step" type="hidden" value="SEND_FILE" /></p>
				</fieldset>
			</form>';

	echo '
			<form action="#" method="post" enctype="multipart/form-data">
				<fieldset name="Upload a localized file">
					<legend>Upload a localized file</legend>
					<p><select id="UP_FILE_NAME" name="UP_FILE_NAME">
						<option value="LOC',$suffixe,'">Localization: LOC',$suffixe,'</option>
						<option value="HELP',$suffixe,'">Help: HELP',$suffixe,'</option>
						<option value="CHARTE',$suffixe,'">Charter: CHARTER',$suffixe,'</option>
					</select>
					
					<input type="file" id="UP_FILE" name="UP_FILE" />
						<input type="submit" name="upload" value="Upload" />
					<input name="step" type="hidden" value="RECEIVE_FILE" /></p>
				</fieldset>
			</form>';
?>
</div>
	</body>
	</html>