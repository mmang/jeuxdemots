-- phpMyAdmin SQL Dump
-- version 2.11.9.6
-- http://www.phpmyadmin.net
--
-- Server version: 4.1.22
-- PHP version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `LafourcadeRezoLexical`
--

-- --------------------------------------------------------

--
-- Structure of the table `Anonces`
--

INSERT INTO `Anonces` (text) VALUES ('JeuxDeMots is up and running. Enjoy the game!');

-- --------------------------------------------------------

--
-- Structure of the table `Artefacts`
--

-- --------------------------------------------------------

--
-- Structure of the table `Cagnote`
--

INSERt INTO `Cagnote`  (amount, player1,  player2, token,  gain, id) VALUES (0,0,0,50,0,0);

-- --------------------------------------------------------

--
-- Structure of the table `Collections`
--

-- --------------------------------------------------------

--
-- Structure of the table `Duels`
--

-- --------------------------------------------------------

--
-- Structure of the table `Events`
--


-- --------------------------------------------------------

--
-- Structure of the table `Gifts`
--


-- --------------------------------------------------------

--
-- Structure of the table `HypeWords`
--


-- --------------------------------------------------------

--
-- Structure of the table `LastPlayer`
--


-- --------------------------------------------------------

--
-- Structure of the table `LastScores`
--


-- --------------------------------------------------------

--
-- Structure of the table `LexSig`
--


-- --------------------------------------------------------

--
-- Structure of the table `Nodes`
--


-- --------------------------------------------------------

--
-- Structure of the table `NodeTypes`
--
INSERT INTO `NodeTypes` (`id`, `Name`) VALUES
(0, 'n_generic'),
(1, 'n_term'),
(2, 'n_acception'),
(3, 'n_definition'),
(4, 'n_pos'),
(5, 'n_concept'),
(6, 'n_flpot'),
(7, 'n_hub'),
(18, 'n_data');

-- --------------------------------------------------------

--
-- Structure of the table `PendingAnswers`
--


-- --------------------------------------------------------

--
-- Structure of the table `PendingGameData`
--


-- --------------------------------------------------------

--
-- Structure of the table `PendingGames`
--

-- --------------------------------------------------------

--
-- Structure of the table `PlayerFriends`
--

-- --------------------------------------------------------

--
-- Structure of the table `PlayerPermissions`
--


-- --------------------------------------------------------

--
-- Structure of the table `Players`
--

INSERT INTO `Players` (name,pwd) VALUES ('test','test');
INSERT INTO `Players` (name) VALUES ('guest');
-- --------------------------------------------------------

--
-- Structure of the table `ProposedGames`
--


-- --------------------------------------------------------

--
-- Structure of the table `ProposedWords`
--


-- --------------------------------------------------------

--
-- Structure of the table `QPOSdata`
--


-- --------------------------------------------------------

--
-- Structure of the table `QPOSdata2`
--


-- --------------------------------------------------------

--
-- Structure of the table `QPOSquestion`
--


-- --------------------------------------------------------

--
-- Structure of the table `Relations`
--


-- --------------------------------------------------------

--
-- Structure of the table `RelationTypes`
--

INSERT INTO `RelationTypes` VALUES (0, 'r_associated', 998.942, 1000, 1000, 0);
INSERT INTO `RelationTypes` VALUES (1, 'r_acception', 1000, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (2, 'r_definition', 1000, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (3, 'r_domain', 1492.03, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (4, 'r_pos', 1000, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (5, 'r_syn', 548.505, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (6, 'r_isa', 1663.75, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (7, 'r_anto', 585.916, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (8, 'r_hypo', 979.731, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (9, 'r_has_part', 1195.85, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (10, 'r_holo', 1474.55, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (11, 'r_locution', 2062.63, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (13, 'r_agent', 2885.57, 500, 2000, 30000);
INSERT INTO `RelationTypes` VALUES (14, 'r_patient', 2929.5, 500, 2000, 30000);
INSERT INTO `RelationTypes` VALUES (12, 'r_flpot', 1000, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (15, 'r_lieu', 852.611, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (16, 'r_instr', 2751.98, 500, 2000, 30000);
INSERT INTO `RelationTypes` VALUES (17, 'r_carac', 1952.61, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (18, 'r_data', 1000, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (19, 'r_lemma', 1000, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (20, 'r_magn', 2726.88, 500, 2000, 20000);
INSERT INTO `RelationTypes` VALUES (21, 'r_antimagn', 2846.59, 500, 2000, 20000);
INSERT INTO `RelationTypes` VALUES (22, 'r_familly', 2130.82, 500, 2000, 10000);
INSERT INTO `RelationTypes` VALUES (23, 'r_carac-1', 1000, 1000, 1000, 0);
INSERT INTO `RelationTypes` VALUES (24, 'r_agent-1', 1000, 1000, 1000, 0);
INSERT INTO `RelationTypes` VALUES (25, 'r_instr-1', 1000, 1000, 1000, 0);
INSERT INTO `RelationTypes` VALUES (26, 'r-patient-1', 1000, 1000, 1000, 0);
INSERT INTO `RelationTypes` VALUES (27, 'r_domain-1', 1000, 1000, 1000, 0);
INSERT INTO `RelationTypes` VALUES (28, 'r_lieu-1', 1000, 1000, 1000, 0);
INSERT INTO `RelationTypes` VALUES (29, 'r_pred', 1000, 500, 2000, 0);
INSERT INTO `RelationTypes` VALUES (30, 'r_lieu_action', 1000, 1000, 1000, 0);
INSERT INTO `RelationTypes` VALUES (31, 'r_action_lieu', 1000, 1000, 1000, 0);
INSERT INTO `RelationTypes` VALUES (32, 'r_sentiment', 1000, 1000, 1000, 0);
INSERT INTO `RelationTypes` VALUES (34, 'r_maniere', 1000, 1000, 1000, 0);
INSERT INTO `RelationTypes` VALUES (35, 'r_sens', 1000, 1000, 1000, 0);
INSERT INTO `RelationTypes` VALUES (37, 'r_telique', 1000, 1000, 1000, 0);
INSERT INTO `RelationTypes` VALUES (38, 'r_agentif', 1000, 1000, 1000, 0);
INSERT INTO `RelationTypes` VALUES (41, 'r_consequence', 1000, 1000, 1000, 0);
INSERT INTO `RelationTypes` VALUES (42, 'r_cause', 1000, 1000, 1000, 0);


-- --------------------------------------------------------

--
-- Structure of the table `Reportings`
--


-- --------------------------------------------------------

--
-- Structure of the table `Scores`
--


-- --------------------------------------------------------

--
-- Structure of the table `Sponsors`
--


-- --------------------------------------------------------

--
-- Structure of the table `Stamps`
--


-- --------------------------------------------------------

--
-- Structure of the table `Stats`
--


-- --------------------------------------------------------

--
-- Structure of the table `TermOwners`
--


-- --------------------------------------------------------

--
-- Structure of the table `Thema`
--


-- --------------------------------------------------------

--
-- Structure of the table `Themes`
--


-- --------------------------------------------------------

--
-- Structure of the table `Tips`
--
INSERT INTO `Tips` (text) VALUES ('Everyday, points are decreasing!');



-- --------------------------------------------------------

--
-- Structure of the table `TrialProposedWords`
--


-- --------------------------------------------------------

--
-- Structure of the table `Trials`
--


-- --------------------------------------------------------

--
-- Structure of the table `TrialVotes`
--


-- --------------------------------------------------------

--
-- Structure of the table `Twits`
--


-- --------------------------------------------------------

--
-- Structure of the table `WantedWords`
--

