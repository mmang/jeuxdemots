-- phpMyAdmin SQL Dump
-- version 2.11.9.6
-- http://www.phpmyadmin.net
--
-- Server version: 4.1.22
-- PHP version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `jeuxdemots`
--

-- --------------------------------------------------------

--
-- Structure of the table `Anonces`
--
CREATE TABLE IF NOT EXISTS `Anonces` (
 `id` int(11) NOT NULL auto_increment,
 `date` timestamp NOT NULL default CURRENT_TIMESTAMP,
 `text` mediumtext NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure of the table `Artefacts`
--

CREATE TABLE IF NOT EXISTS `Artefacts` (
 `id` bigint(20) NOT NULL default '0',
 `owner` bigint(20) NOT NULL default '-1',
 `value` bigint(20) NOT NULL default '0',
 `icon_url` varchar(200) NOT NULL default '',
 `help` text NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Cagnote`
--


CREATE TABLE IF NOT EXISTS `Cagnote` (
 `id` int(11) NOT NULL default '0',
 `amount` int(11) NOT NULL default '0',
 `player1` int(11) NOT NULL default '0',
 `player2` int(11) NOT NULL default '0',
 `gain` bigint(20) NOT NULL default '0',
 `token` bigint(20) NOT NULL default '5'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Collections`
--

CREATE TABLE IF NOT EXISTS `Collections` (
 `id` bigint(20) NOT NULL default '0',
 `name` varchar(100) NOT NULL default '',
 `owner_id` bigint(20) NOT NULL default '0',
 `t1` varchar(100) NOT NULL default '0',
 `t2` varchar(100) NOT NULL default '0',
 `t3` varchar(100) NOT NULL default '0',
 `t4` varchar(100) NOT NULL default '0',
 `t5` varchar(100) NOT NULL default '0',
 `t6` varchar(100) NOT NULL default '0',
 `t7` varchar(100) NOT NULL default '0',
 `t8` varchar(100) NOT NULL default '0',
 `t9` varchar(100) NOT NULL default '0',
 `t10` varchar(100) NOT NULL default '0',
 UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Duels`
--

CREATE TABLE IF NOT EXISTS `Duels` (
 `id` int(11) NOT NULL auto_increment,
 `player1` bigint(20) NOT NULL default '0',
 `player2` bigint(20) NOT NULL default '0',
 `nb_cp_1` int(11) NOT NULL default '0',
 `nb_cp_2` int(11) NOT NULL default '0',
 `date_1` datetime NOT NULL default '0000-00-00 00:00:00',
 `date_2` datetime NOT NULL default '0000-00-00 00:00:00',
 `score_1` int(11) NOT NULL default '0',
 `score_2` int(11) NOT NULL default '0',
 `max_cp` int(11) NOT NULL default '0',
 PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Events`
--

CREATE TABLE IF NOT EXISTS `Events` (
 `id` bigint(20) NOT NULL auto_increment,
 `date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
 `what` text character set utf8 collate utf8_unicode_ci NOT NULL,
 PRIMARY KEY (`id`),
 KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Gifts`
--

CREATE TABLE IF NOT EXISTS `Gifts` (
 `id` bigint(20) NOT NULL auto_increment,
 `destid` bigint(20) NOT NULL default '0',
 `creatorid` bigint(20) NOT NULL default '0',
 `termid` bigint(20) NOT NULL default '0',
 `relationid` int(11) NOT NULL default '0',
 `giftdate` timestamp NOT NULL default '0000-00-00 00:00:00' on update CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 KEY `playerid` (`destid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `HypeWords`
--

CREATE TABLE IF NOT EXISTS `HypeWords` (
 `name` varchar(120) character set utf8 collate utf8_bin NOT NULL default '',
 `token` bigint(20) NOT NULL default '5',
 `gameType` bigint(20) NOT NULL default '0',
 `relationType` bigint(20) NOT NULL default '0',
 `owner` bigint(20) NOT NULL default '0',
 `gain` bigint(20) NOT NULL default '0',
 PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `LastPlayer`
--

CREATE TABLE IF NOT EXISTS `LastPlayer` (
 `player1_id` bigint(20) NOT NULL default '0',
 `player2_id` bigint(20) NOT NULL default '0',
 UNIQUE KEY `player_id` (`player1_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `LastScores`
--

CREATE TABLE IF NOT EXISTS `LastScores` (
 `id` bigint(20) NOT NULL auto_increment,
 `Score` bigint(20) NOT NULL default '0',
 `p1_id` bigint(20) NOT NULL default '0',
 `p2_id` bigint(20) NOT NULL default '0',
 `date` bigint(20) default '0',
 PRIMARY KEY (`id`),
 KEY `date` (`date`),
 KEY `p1_id` (`p1_id`),
 KEY `p2_id` (`p2_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Structure of the table `LexSig`
--

CREATE TABLE IF NOT EXISTS `LexSig` (
 `id` bigint(20) NOT NULL auto_increment,
 `termid` bigint(20) NOT NULL default '0',
 `sig` blob NOT NULL,
 `date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
 `iter` int(11) NOT NULL default '0',
 UNIQUE KEY `id` (`id`),
 KEY `termid` (`termid`),
 KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Nodes`
--
CREATE TABLE IF NOT EXISTS `Nodes` (
 `id` bigint(20) NOT NULL auto_increment,
 `name` varchar(200) character set utf8 collate utf8_bin NOT NULL default '',
 `type` int(11) NOT NULL default '0',
 `w` bigint(20) NOT NULL default '0',
 `c` bigint(20) NOT NULL default '0',
 `level` float NOT NULL default '50',
 `infoid` bigint(20) default NULL,
 `creationdate` date NOT NULL default '0000-00-00',
 `touchdate` timestamp NOT NULL default '0000-00-00 00:00:00' on update CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 UNIQUE KEY `name` (`name`),
 KEY `type` (`type`),
 KEY `touchdate` (`touchdate`),
 KEY `w` (`w`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE Nodes ADD `annotation` varchar(200) character set utf8 collate utf8_bin NOT NULL default '';
ALTER TABLE Nodes ADD `gloss` varchar(200) character set utf8 collate utf8_bin NOT NULL default '';

-- --------------------------------------------------------

--
-- Structure of the table `NodeTypes`
--

CREATE TABLE IF NOT EXISTS `NodeTypes` (
 `id` int(11) NOT NULL auto_increment,
 `Name` varchar(32) collate utf8_unicode_ci NOT NULL default '',
 UNIQUE KEY `id` (`id`,`Name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure of the table `PendingAnswers`
--

CREATE TABLE IF NOT EXISTS `PendingAnswers` (
 `gameId` bigint(20) NOT NULL default '0',
 `playerId` bigint(20) NOT NULL default '0',
 `answer` varchar(120) collate utf8_bin NOT NULL default '',
 KEY `playerId` (`playerId`),
 KEY `gameId` (`gameId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure of the table `PendingGameData`
--

CREATE TABLE IF NOT EXISTS `PendingGameData` (
 `gameId` bigint(20) NOT NULL default '0',
 `dataType` bigint(20) NOT NULL default '0',
 `data` bigint(20) NOT NULL default '0',
 KEY `gameId` (`gameId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `PendingGames`
--

CREATE TABLE IF NOT EXISTS `PendingGames` (
 `id` bigint(20) NOT NULL auto_increment,
 `type` int(11) NOT NULL default '0',
 `creator` bigint(20) NOT NULL default '0',
 `creationdate` timestamp NOT NULL default CURRENT_TIMESTAMP,
 `token` int(11) NOT NULL default '2',
 `IP` varchar(64) character set utf8 collate utf8_unicode_ci NOT NULL default '',
 `rel` int(11) NOT NULL default '0',
 `target` bigint(20) NOT NULL default '0',
 PRIMARY KEY (`id`),
 KEY `creator` (`creator`),
 KEY `rel` (`rel`),
 KEY `IP` (`IP`),
 KEY `creationdate` (`creationdate`),
 KEY `target` (`target`),
 KEY `token` (`token`),
 KEY `creator_2` (`creator`,`target`,`rel`,`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Structure of the table `PlayerFriends`
--

CREATE TABLE IF NOT EXISTS `PlayerFriends` (
 `playerid` bigint(20) NOT NULL default '0',
 `friendid` bigint(20) NOT NULL default '0',
 KEY `friendid` (`friendid`),
 KEY `playerid` (`playerid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `PlayerPermissions`
--


CREATE TABLE IF NOT EXISTS `PlayerPermissions` (
 `playerid` int(11) NOT NULL default '0',
 `relid` int(11) NOT NULL default '0',
 `value` float NOT NULL default '1000',
 KEY `playerid` (`playerid`,`relid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Players`
--

CREATE TABLE IF NOT EXISTS `Players` (
 `id` bigint(20) NOT NULL auto_increment,
 `name` varchar(32) character set utf8 collate utf8_unicode_ci NOT NULL default '',
 `pwd` varchar(10) character set utf8 collate utf8_bin NOT NULL default '',
 `email` varchar(128) character set utf8 collate utf8_unicode_ci NOT NULL default '',
 `credit` bigint(20) NOT NULL default '0',
 `honnor` bigint(20) NOT NULL default '0',
 `accu` float NOT NULL default '0.5',
 `maxhonnor` bigint(20) NOT NULL default '0',
 `nbplayed` bigint(20) NOT NULL default '0',
 `level` float NOT NULL default '30',
 `lasttimeplayed` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
 `lastlogin` timestamp NOT NULL default '0000-00-00 00:00:00',
 `nb_tres` int(11) NOT NULL default '0',
 `nb_tres2` bigint(20) NOT NULL default '0',
 `nb_trial_tokens` bigint(20) NOT NULL default '0',
 `css_url` text NOT NULL,
 `css_mus_url` text NOT NULL,
 `perso_url` text NOT NULL,
 `receive_mail_p` char(1) NOT NULL default 'Y',
 PRIMARY KEY (`id`),
 UNIQUE KEY `name_2` (`name`),
 KEY `credit` (`credit`,`honnor`,`accu`),
 KEY `maxcredit` (`maxhonnor`,`nbplayed`),
 KEY `last_touch` (`last_touch`),
 FULLTEXT KEY `name` (`name`,`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


ALTER TABLE Players ADD `admin` char(1) NOT NULL default '0';
ALTER TABLE Players ADD `last_touch` bigint(20) NOT NULL default '0';


-- --------------------------------------------------------

--
-- Structure of the table `ProposedGames`
--

CREATE TABLE IF NOT EXISTS `ProposedGames` (
 `gameId` bigint(20) NOT NULL default '0',
 `playerId` bigint(20) NOT NULL default '0',
 KEY `gameId` (`gameId`,`playerId`),
 KEY `playerId` (`playerId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `ProposedWords`
--

CREATE TABLE IF NOT EXISTS `ProposedWords` (
 `player_id` bigint(20) NOT NULL default '0',
 `term_id` bigint(20) NOT NULL default '0',
 `date` bigint(20) NOT NULL default '0',
 KEY `player_id` (`player_id`),
 KEY `term_id` (`term_id`),
 KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure of the table `QPOSdata`
--

CREATE TABLE IF NOT EXISTS `QPOSdata` (
 `id` bigint(20) NOT NULL auto_increment,
 `term_id` bigint(20) NOT NULL default '0',
 `q_id` bigint(20) NOT NULL default '0',
 `player_id` bigint(20) NOT NULL default '0',
 `answer` int(11) NOT NULL default '0',
 KEY `id` (`id`,`term_id`,`q_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `QPOSdata2`
--

CREATE TABLE IF NOT EXISTS `QPOSdata2` (
 `id` bigint(20) NOT NULL auto_increment,
 `term_id` bigint(20) NOT NULL default '0',
 `q_id` bigint(20) NOT NULL default '0',
 `player_id` bigint(20) NOT NULL default '0',
 `answer` int(11) NOT NULL default '0',
 KEY `id` (`id`,`term_id`,`q_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `QPOSquestion`
--

CREATE TABLE IF NOT EXISTS `QPOSquestion` (
 `id` bigint(20) NOT NULL auto_increment,
 `question` varchar(200) NOT NULL default '',
 PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Relations`
--

CREATE TABLE IF NOT EXISTS `Relations` (
 `id` bigint(20) NOT NULL auto_increment,
 `node1` bigint(20) NOT NULL default '0',
 `node2` bigint(20) NOT NULL default '0',
 `type` int(11) NOT NULL default '0',
 `w` float NOT NULL default '0',
 `c` float NOT NULL default '0',
 `infoid` bigint(20) NOT NULL default '0',
 `creationdate` date NOT NULL default '0000-00-00',
 `touchdate` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 KEY `node2` (`node2`),
 KEY `type` (`type`),
 KEY `node1` (`node1`),
 KEY `w` (`w`),
 KEY `two-nodes` (`node1`,`node2`),
 KEY `touchdate` (`touchdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure of the table `RelationTypes`
--

CREATE TABLE IF NOT EXISTS `RelationTypes` (
 `id` int(11) NOT NULL default '0',
 `name` varchar(32) collate utf8_unicode_ci NOT NULL default '',
 `quot` float NOT NULL default '1000',
 `quotmin` float NOT NULL default '500',
 `quotmax` float NOT NULL default '2000',
 `price` int(11) NOT NULL default '0',
 UNIQUE KEY `id` (`id`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure of the table `Reportings`
--

CREATE TABLE IF NOT EXISTS `Reportings` (
 `id` bigint(20) NOT NULL auto_increment,
 `term_id` bigint(20) NOT NULL default '0',
 `term` varchar(120) NOT NULL default '0',
 `player` bigint(20) NOT NULL default '0',
 `honnor` bigint(20) NOT NULL default '0',
 `correction` varchar(120) NOT NULL default '',
 PRIMARY KEY (`id`),
 KEY `term` (`term`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Scores`
--

CREATE TABLE IF NOT EXISTS `Scores` (
 `id` bigint(20) NOT NULL default '0',
 `score` bigint(20) NOT NULL default '0',
 `date` timestamp NOT NULL default '0000-00-00 00:00:00',
 KEY `id` (`id`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Sponsors`
--


CREATE TABLE IF NOT EXISTS `Sponsors` (
 `sponsorid` bigint(20) NOT NULL default '0',
 `email` varchar(120) character set utf8 collate utf8_bin NOT NULL default '',
 `date` timestamp NOT NULL default CURRENT_TIMESTAMP,
 UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Stamps`
--

CREATE TABLE IF NOT EXISTS `Stamps` (
 `tag` varchar(100) character set utf8 collate utf8_bin NOT NULL default '',
 `date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
 UNIQUE KEY `tag` (`tag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Stats`
--

CREATE TABLE IF NOT EXISTS `Stats` (
 `id` bigint(20) NOT NULL default '0',
 `data` varchar(200) NOT NULL default '',
 PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure of the table `TermOwners`
--

CREATE TABLE IF NOT EXISTS `TermOwners` (
 `term_id` bigint(20) NOT NULL default '0',
 `owner1_id` bigint(20) NOT NULL default '0',
 `owner2_id` bigint(20) NOT NULL default '0',
 `value` int(11) NOT NULL default '0',
 PRIMARY KEY (`term_id`),
 KEY `owner1_id` (`owner1_id`),
 KEY `owner2_id` (`owner2_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Thema`
--

CREATE TABLE IF NOT EXISTS `Thema` (
 `playerid` bigint(20) NOT NULL default '0',
 `termid` bigint(20) NOT NULL default '0',
 PRIMARY KEY (`playerid`),
 KEY `termid` (`termid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Themes`
--

CREATE TABLE IF NOT EXISTS `Themes` (
 `termid` bigint(20) NOT NULL default '0',
 `w` bigint(20) NOT NULL default '0',
 UNIQUE KEY `termid` (`termid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Tips`
--

CREATE TABLE IF NOT EXISTS `Tips` (
 `id` bigint(20) NOT NULL auto_increment,
 `text` text character set utf8 collate utf8_bin NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `TrialProposedWords`
--

CREATE TABLE IF NOT EXISTS `TrialProposedWords` (
 `id` bigint(20) NOT NULL default '0',
 `player_id` bigint(20) NOT NULL default '0',
 `word` varchar(100) character set utf8 collate utf8_bin NOT NULL default '',
 KEY `id` (`id`,`player_id`),
 KEY `player_id` (`player_id`),
 KEY `word` (`word`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Trials`
--

CREATE TABLE IF NOT EXISTS `Trials` (
 `id` bigint(20) NOT NULL auto_increment,
 `instructions` text NOT NULL,
 `term` varchar(200) NOT NULL default '',
 `player1_id` bigint(20) NOT NULL default '0',
 `player2_id` bigint(20) NOT NULL default '0',
 `date` bigint(20) NOT NULL default '0',
 `date_term` bigint(20) NOT NULL default '0',
 `p1_score` float NOT NULL default '0',
 `p2_score` float NOT NULL default '0',
 `reason` blob NOT NULL,
 PRIMARY KEY (`id`),
 KEY `player1_id` (`player1_id`,`player2_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `TrialVotes`
--

CREATE TABLE IF NOT EXISTS `TrialVotes` (
 `id` bigint(20) NOT NULL auto_increment,
 `trial_id` bigint(20) NOT NULL default '0',
 `player_id` bigint(20) NOT NULL default '0',
 `vote` char(1) NOT NULL default '',
 PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure of the table `Twits`
--

CREATE TABLE IF NOT EXISTS `Twits` (
 `id` bigint(20) NOT NULL auto_increment,
 `tag` varchar(100) character set utf8 NOT NULL default '',
 `text` text character set utf8 NOT NULL,
 PRIMARY KEY (`id`),
 KEY `tag` (`tag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure of the table `WantedWords`
--

CREATE TABLE IF NOT EXISTS `WantedWords` (
 `id` bigint(20) NOT NULL default '0',
 `date` bigint(20) NOT NULL default '0',
 PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
