<?php
	session_start();
	include 'admin_functions.php';
	$step = '';
	if (file_exists('../PARAM.php')) {
		include '../misc_functions.php';
		open_session();
	 	if (!empty($_SESSION[ssig() . 'playerid'])) {
			openconnexion();
	 		if (get_player_admin($_SESSION[ssig() . 'playerid'])) {
		 		$_SESSION[ssig() . 'ADMIN'] = $_SESSION[ssig() . 'login'];
	 		}
	 	}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>JeuxDeMots admin interface</title>
    <link rel="stylesheet" type="text/css" href="admin.css"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  </head>
<body>     
	<h1>JeuxDeMots admin interface</h1>
	<div id="header">
<ul>
	<li><a href="index.php">Parameters</a></li>
	<li><a href="data.php">Data</a></li>
	<li id="selected"><a href="announces.php">Announces</a></li>
	<li><a href="tips.php">Tips</a></li>
	<li><a href="localize.php">Localization</a></li>
</ul>

</div>

	<div id="content">
 <?php

	/* ADMIN INTERFACE */
	if (!empty($_REQUEST['step'])) {
		$step = $_REQUEST['step'];	
	}
	
	if (empty($_SESSION[ssig() . 'ADMIN']) || empty($_SESSION[ssig() . 'playerid'])) {
		echo 'You are not admin. Try to log in from the <a href="../index.html">index</a> or 
		delete the PARAM.php file in the root directory';
		exit;
	}
	
	if ($step=='CREATE_ANNOUNCE' && !empty($_REQUEST['ANNOUNCE_TEXT'])) {
		$query = 'INSERT INTO Anonces (text) VALUES (\''.$_REQUEST['ANNOUNCE_TEXT'].'\')';
		$result = mysql_query($query);
		if (!$result) {
			echo 'Error in creating a new announce: ', mysql_error();
		}	
	}
	
	if ($step=='EDIT_ANNOUNCE' && !empty($_REQUEST['delete_announce'])) {
		$query = 'DELETE FROM Anonces WHERE id='.$_REQUEST['ANNOUNCE'].';';
		$result = mysql_query($query);
		if (!$result) {
			echo 'Error in deleting an existing announce: ', mysql_error();
		}	
	}
	
	if ($step=='EDIT_ANNOUNCE' && !empty($_REQUEST['edit_announce'])) {
		$query = 'UPDATE Anonces SET text=\''.$_REQUEST['ANNOUNCE_TEXT'].'\' WHERE id='.$_REQUEST['ANNOUNCE'].';';
		$result = mysql_query($query);
		if (!$result) {
			echo 'Error in updating an existing announce: ', mysql_error();
		}	
	}
	
	// STEP EDIT_PARAM
	echo '
			<form action="#" method="post">
				<fieldset name="Create a new announce">
					<legend>Create a new announce</legend>
					<p>Text: <textarea ROWS="5" COLS="40" name="ANNOUNCE_TEXT"></textarea>
						<input type="submit" name="create_announce" value="Create" />
					<input name="step" type="hidden" value="CREATE_ANNOUNCE"></p>
				</fieldset>
			</form>';
	$query = 'SELECT * FROM Anonces ORDER BY date DESC;';
	$result = mysql_query($query);
	if ($result) {
		while ($announce = mysql_fetch_object($result)) {
			echo '<form action="#" method="get">
					<fieldset name="Edit an existing announce">
					<legend>Edit an existing announce</legend>
					<input type="hidden" name="ANNOUNCE" value="',$announce->id,'" />
					Date: ',$announce->date,'<br/>
					Text: <textarea name="ANNOUNCE_TEXT" ROWS="5" COLS="40">',$announce->text,'</textarea>
					<input type="submit" name="edit_announce" value="Edit" />
					<input type="submit" name="delete_announce" value="Delete" />
					<input name="step" type="hidden" value="EDIT_ANNOUNCE"></p>
				</fieldset>
				</form>
				';
		}
	}
	
?>
</div>
	</body>
	</html>