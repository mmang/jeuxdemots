<?php

// keep the double quotes!
//
$_SESSION["LANG"] = "EN";
$_SESSION["GAME"] = "JDM";

function ssig() {
	return ($_SESSION["GAME"] . $_SESSION["LANG"] . '-');
}

// Y normal
// H hard quand y a que test qui a accès
$_SESSION[ssig() . 'debugmode']="";
$_SESSION[ssig() . 'harddebugmode']="";

$_SESSION[ssig() . 'version']="2.6.0";



$_SESSION[ssig() . 'CONNEXION-URL'] = "localhost";
$_SESSION[ssig() . 'CONNEXION-USER'] = "root";
$_SESSION[ssig() . 'CONNEXION-PSWD'] = "";
$_SESSION[ssig() . 'CONNEXION-DBNAME'] = "jeuxdemots";

$_SESSION[ssig() . 'DATADIR'] = "data";
$_SESSION[ssig() . 'BACKUPDIR'] = "backup";
$_SESSION[ssig() . 'EXPORTDIR'] = "export";



$_SESSION[ssig() . 'CONTACT-EMAIL'] = "jeux.de.meaux@gmail.com";

$_SESSION[ssig() . 'DEFAULT-CSS-URL'] = "CSS/jdm.css";
$_SESSION[ssig() . 'CSS-URL'] = "CSS/jdm.css";
$_SESSION[ssig() . 'FORUM-URL'] = "http://jeuxdemots.forummotions.com/";
$_SESSION[ssig() . 'JDM-LOGO-URL'] = "http://www.lirmm.fr/jeuxdemots/logo-jdm/logo-jdm-black-small.gif";
$_SESSION[ssig() . 'JDM-LAB-LOGO-URL'] = "http://www.lirmm.fr/jeuxdemots/pics/LogoLIRMM-JDM.gif";


$_SESSION[ssig() . 'PARAM-MAKE-BOT-GIFT'] = "Y";
$_SESSION[ssig() . 'PARAM-ALLOW-TRIALS'] = "Y";
$_SESSION[ssig() . 'PARAM-HELP-INSTRUCTIONS'] = "Y";

?>
