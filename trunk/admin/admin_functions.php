<?php

	function load_sql_file($file, $format ='') {
		$handle = '';
		if ($format == 'gzip') {
			$handle = gzopen ($file, "r"); 
		}
		else {
			$handle = @fopen($file,"r");
		}
		$query = '';
		if ($handle) {
			while (!feof($handle)) {
				$line = '';
				if ($format == 'gzip') {
					$line= gzgets($handle, 4096);
				}
				else {
					$line= fgets($handle, 4096);
				}
				$line = preg_replace('/\-\-.*$/','',$line);
				$query .= rtrim($line);
				if (substr($query, -1) == ';') {
					echo '<li>',$query;
					if (!mysql_query($query)) {
						echo '<b>', mysql_errno(), ": ", mysql_error(), '</b>';
					}
					echo '</li>';
					$query = '';
				}
			}
			if ($format == 'gzip') {
				gzclose($handle);
			}
			else {
				fclose($handle);
			}
		}
	}
	
	function create_loc_file($origfile,$lang) {
		$destfile = preg_replace('/\-EN.php$/','-'.$lang.'.php',$origfile);
		$orighandle = fopen($origfile,"r");
		$desthandle = fopen($destfile,"w");
		if ($orighandle) {
			while (!feof($orighandle)) {
				$line= fgets($orighandle, 4096);
				$line = preg_replace('/^\$GLOBALS\[\'EN\-/','$GLOBALS[\''.$lang.'-',$line);
				fwrite($desthandle,$line);
			}
			fclose($orighandle);
			fclose($desthandle);
		}
	}
	
	function dir_files($directory) {
		if ($handle = opendir($directory)) {
			while ($file = readdir($handle)) {
				if (substr($file,0,1) !== '.') {
					echo '<option value="',$file,'">',$file,'</option>';
				}
			}
			closedir($handle);
		}
	}
	
	function findext ($filename) 
	{ 
		$filename = strtolower($filename) ; 
		$exts = preg_split("/[/\\.]/", $filename) ; 
		$n = count($exts)-1; 
		$exts = $exts[$n]; 
		return $exts; 
	}
	
	function upload_file ($file, $fileName, $directory) {
		// Création d'un nom unique avec time()
		// Récupération du seul nom de fichier avec basename()
		$nouveauNom = time() . "-" . basename($fileName);
		$nouveauFichier = $directory . $nouveauNom;
		if (file_exists($nouveauFichier)) {
			die ('The file already exists!');
		}
		// déplacement du fichier dans le répertoire dépôt
		if (move_uploaded_file($file, $nouveauFichier)) {
			echo "$fileName uploaded";	
		}
		else {
			echo "Error while uploading the file $nomFichier!";	
		}
	}
	
	function update_param_file($fileToUpdate, $template) {
		
	}
?>