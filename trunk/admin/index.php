<?php
	session_start();
	include 'admin_functions.php';
	$step = 'EDIT_PARAM';
	if (file_exists('../PARAM.php')) {
		include '../misc_functions.php';
		open_session();
	 	if (!empty($_SESSION[ssig() . 'playerid'])) {
			openconnexion();
	 		if (get_player_admin($_SESSION[ssig() . 'playerid'])) {
		 		$_SESSION[ssig() . 'ADMIN'] = $_SESSION[ssig() . 'login'];
	 		}
	 	}
	}
	else {
		$step = 'ADMIN_LOGIN_FORM';
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>JeuxDeMots admin interface</title>
    <link rel="stylesheet" type="text/css" href="admin.css"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  </head>
<body>     
	<h1>JeuxDeMots admin interface</h1>
	<div id="header">
<ul>
	<li id="selected"><a href="index.php">Parameters</a></li>
	<li><a href="data.php">Data</a></li>
	<li><a href="announces.php">Announces</a></li>
	<li><a href="tips.php">Tips</a></li>
	<li><a href="users.php">Users</a></li>
	<li><a href="localize.php">Localization</a></li>
</ul>

</div>

	<div id="content">
 <?php

	/* ADMIN INTERFACE */
	if (!empty($_REQUEST['step'])) {
		$step = $_REQUEST['step'];	
	}

	if ($step == 'ADMIN_LOGIN_FORM' && !file_exists('../PARAM.php')) {
		echo '<p>You are installing the JeuxDeMots game. You will need a mysql database account and
		password. Please ask your administrator if you don\'t have them.</p>';
		echo '<p>Please choose an admin login and password and give your email address.
		The admin account will be used for modifying settings and also playing.</p>
		<p>When the install is finished, if you want to come back to the admin interface,
		please add <code>admin/</code> to the jeuxDeMots game URL: <a href="">',
		dirname($_SERVER['PHP_SELF']),'/',
		'</a></p>';
		
		$testFileName = "../testFile.php";
		$testFileHandle = @fopen($testFileName, 'w');
		if (!$testFileHandle) {
			echo '<p style="color:red">The permission to write in the jeuxdemots directory is denied to the installer.
			It is needed to install the game properly. Please ask your server administrator to
			give write permission to the web server in the ',
			 dirname(dirname($_SERVER['SCRIPT_FILENAME'])),' directory.</p>'; 
			 exit();
		}
		fclose($testFileHandle);
		unlink($testFileName);
		
		echo '<form action="#" method="post">
				<fieldset name="JeuxDeMots admin login">
				
				<p><label>Admin login <input name="ADMIN_LOGIN" type="text" value=""/></label></p>
				<p><label>Admin password <input name="ADMIN_PASSWORD" type="password" value=""/></label></p>
				<p><label>Admin email <input name="ADMIN_EMAIL" type="text" value=""/></label></p>
				<p><input type="submit" value="OK" /><input name="step" type="hidden" value="ADMIN_LOGIN"></p>
				</fieldset>
			</form>
		';
		exit();
	}

	if ($step == 'ADMIN_LOGIN' && !file_exists('../PARAM.php')) {
		if (!empty($_POST['ADMIN_LOGIN']) && !empty($_POST['ADMIN_PASSWORD'])) {
			copy('PARAM.sample.php','../PARAM.php');
			include '../misc_functions.php';
	 		$_SESSION[ssig() . 'ADMIN'] = $_POST['ADMIN_LOGIN'];
	 		$_SESSION[ssig() . 'ADMIN_PASSWORD'] = $_POST['ADMIN_PASSWORD'];
	 		$_SESSION[ssig() . 'ADMIN_EMAIL'] = $_POST['ADMIN_EMAIL'];
	 	}
	 	echo '<p>Now, <!-- update a previous installation
	 	<form action="#" method="post">
				<fieldset name="JeuxDeMots update interface">
				<p><label>Enter the relative path of the previous installation directory.
				<input name="PREVIOUS_INSTALLATION_PATH" type="text" value="../"/></label>
				Note: The path must be relative to this installation directory.</p>
				<p><input type="submit" value="Update" /><input name="step" type="hidden" 
				value="UPDATE_PREVIOUS_INSTALLATION"></p>
				</fieldset>
			</form>
	 	or--> <a href="?step=EDIT_PARAM">edit the parameters</a>.</p>';
	 	exit;
	 }
	
	if (empty($_SESSION[ssig() . 'ADMIN'])) {
		echo 'You are not admin. Try to log in from the <a href="../index.html">index</a> or 
		delete the PARAM.php file in the root directory';
		exit;
	}
	
	
	if ($step == 'UPDATE_PREVIOUS_INSTALLATION' && !empty($_REQUEST['PREVIOUS_INSTALLATION_PATH'])) {
		echo 'Update previous installation...';
		$instdir = $_REQUEST['PREVIOUS_INSTALLATION_PATH'];
		if ($instdir !== '' && substr($instdir, -1) !== '/') {
			$instdir .= '/';
		}
		$previousParamFile = '../' . $instdir . 'PARAM.php';
		copy($previousParamFile,'../PARAM.php');
		echo 'Previous installation updated';
		
	}
	
	// STEP EDIT_PARAM
	if ($step == 'EDIT_PARAM') {
		echo '<p>Enter or modify the parameter values. The mandatory values are in bold.
		If you don\'t know the significance of a parameter, leave the default value.</p>';
		include_once('../PARAM.php');
		$makebotgift = '';
		if ($_SESSION[ssig() . 'PARAM-MAKE-BOT-GIFT'] == 'Y') {
			$makebotgift = ' checked="checked" ';
		}
		$allowtrials = '';
		if ($_SESSION[ssig() . 'PARAM-ALLOW-TRIALS'] == 'Y') {
			$allowtrials = ' checked="checked" ';
		}
		$helpinstructions = '';
		if ($_SESSION[ssig() . 'PARAM-HELP-INSTRUCTIONS'] == 'Y') {
			$helpinstructions = ' checked="checked" ';
		}
		echo '
			<form action="#" method="post">
				<fieldset name="JeuxDeMots parameters">
				<p>Current version: ',$_SESSION[ssig() . 'version'], '</p>
				<p><label><strong>*Language of the game</strong> <input name="LANG" type="text" size="2" value="',$_SESSION['LANG'],'"/> 
				<span style="font-size:smaller;">(<a href="http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">ISO-639-1</a> two letter language code is preferred)</span></label></p>
				<p><label><strong>*mysql database host</strong> <input name="CONNEXION-URL" type="text" value="',$_SESSION[ssig() . 'CONNEXION-URL'],'"/></label></p>
				<p><label><strong>*mysql database user</strong> <input name="CONNEXION-USER" type="text" value="',$_SESSION[ssig() . 'CONNEXION-USER'],'"/></label></p>
				<p><label><strong>*mysql database password</strong> <input name="CONNEXION-PSWD" type="password" value="',$_SESSION[ssig() . 'CONNEXION-PSWD'],'"/></label></p>
				<p><label><strong>*mysql database name</strong> <input name="CONNEXION-DBNAME" type="text" value="',$_SESSION[ssig() . 'CONNEXION-DBNAME'],'"/></label></p>
				<p><label>Data directory <input name="DATADIR" type="text" value="',$_SESSION[ssig() . 'DATADIR'],'"/></label></p>
				<p><label>Backup directory <input name="BACKUPDIR" type="text" value="',$_SESSION[ssig() . 'BACKUPDIR'],'"/></label></p>
				<p><label>Export directory <input name="EXPORTDIR" type="text" value="',$_SESSION[ssig() . 'EXPORTDIR'],'"/></label></p>
				<p><label>Game contact email <input name="CONTACT-EMAIL" type="text" size="80" value="', $_SESSION[ssig() . 'CONTACT-EMAIL'],'"/></label></p>
				<p><label>Forum URL <input name="FORUM-URL" type="text" size="80" value="',htmlentities($_SESSION[ssig() . 'FORUM-URL']),'"/></label></p>
				<p><label>Logo URL <input name="JDM-LOGO-URL" type="text" size="80" value="',htmlentities($_SESSION[ssig() . 'JDM-LOGO-URL']),'"/></label></p>
				<p><label>Lab logo URL <input name="JDM-LAB-LOGO-URL" type="text" size="80" value="',htmlentities($_SESSION[ssig() . 'JDM-LAB-LOGO-URL']),'"/></label></p>
				<p><label>Make gifts with a bot <input name="PARAM-MAKE-BOT-GIFT" type="checkbox" value="Y" ',$makebotgift,'/></label></p>
				<p><label>Allow trials <input name="PARAM-ALLOW-TRIALS" type="checkbox" value="Y" ',$allowtrials,'/></label></p>
				<p><label>Help instructions <input name="PARAM-HELP-INSTRUCTIONS" type="checkbox" value="Y" ',$helpinstructions,'/></label></p>
				<p><input type="submit" value="OK" /><input name="step" type="hidden" value="DEFINE_PARAM"></p>
				</fieldset>
			</form>';
	}

	if ($step == 'DEFINE_PARAM') {
		if (file_exists('../PARAM.php')) {
			$sessionAdmin = '';
	 		if (!empty($_SESSION[ssig() . 'ADMIN'])) {
	 			$sessionAdmin = $_SESSION[ssig() . 'ADMIN'];
	 		}
	 		$sessionPassword = '';
	 		if (!empty($_SESSION[ssig() . 'ADMIN_PASSWORD'])) {
	 			$sessionPassword = $_SESSION[ssig() . 'ADMIN_PASSWORD'];
	 		}
	 		$sessionEmail = '';
	 		if (!empty($_SESSION[ssig() . 'ADMIN_EMAIL'])) {
	 			$sessionEmail = $_SESSION[ssig() . 'ADMIN_EMAIL'];
	 		}

			$paramFile = fopen('../PARAM.php','r');
			$paramTempFile = fopen('PARAM.php.tmp','w');
			while (!feof($paramFile)) 
			{ 
				$line = fgets($paramFile);
				if (preg_match('/\s*\$_SESSION\[[\'"]LANG[\'"]\] *= */',$line)) {
					$line = '$_SESSION["LANG"] = "'.$_REQUEST['LANG'].'";'."\n";
					$_SESSION["LANG"] = $_REQUEST['LANG'];
				}
				if (preg_match('/CONNEXION-URL[\'"]\] *= */',$line)) {
					$line = '$_SESSION[ssig() . \'CONNEXION-URL\'] = "'.$_REQUEST['CONNEXION-URL'].'";'."\n";
				}
				if (preg_match('/CONNEXION-USER[\'"]\] *= */',$line)) {
					$line = '$_SESSION[ssig() . \'CONNEXION-USER\'] = "'.$_REQUEST['CONNEXION-USER'].'";'."\n";
				}
				if (preg_match('/CONNEXION-PSWD[\'"]\] *= */',$line)) {
					$line = '$_SESSION[ssig() . \'CONNEXION-PSWD\'] = "'.$_REQUEST['CONNEXION-PSWD'].'";'."\n";
				}
				if (preg_match('/CONNEXION-DBNAME[\'"]\] *= */',$line)) {
					$line = '$_SESSION[ssig() . \'CONNEXION-DBNAME\'] = "'.$_REQUEST['CONNEXION-DBNAME'].'";'."\n";
				}
				if (preg_match('/DATADIR[\'"]\] *= */',$line)) {
					$line = '$_SESSION[ssig() . \'DATADIR\'] = "'.$_REQUEST['DATADIR'].'";'."\n";
				}
				if (preg_match('/BACKUPDIR[\'"]\] *= */',$line)) {
					$line = '$_SESSION[ssig() . \'BACKUPDIR\'] = "'.$_REQUEST['BACKUPDIR'].'";'."\n";
				}
				if (preg_match('/EXPORTDIR[\'"]\] *= */',$line)) {
					$line = '$_SESSION[ssig() . \'EXPORTDIR\'] = "'.$_REQUEST['EXPORTDIR'].'";'."\n";
				}
				if (preg_match('/CONTACT-EMAIL[\'"]\] *= */',$line)) {
					$line = '$_SESSION[ssig() . \'CONTACT-EMAIL\'] = "'.html_entity_decode($_REQUEST['CONTACT-EMAIL']).'";'."\n";
				}
				if (preg_match('/FORUM-URL[\'"]\] *= */',$line)) {
					$line = '$_SESSION[ssig() . \'FORUM-URL\'] = "'.html_entity_decode($_REQUEST['FORUM-URL']).'";'."\n";
				}
				if (preg_match('/JDM-LOGO-URL[\'"]\] *= */',$line)) {
					$line = '$_SESSION[ssig() . \'JDM-LOGO-URL\'] = "'.html_entity_decode($_REQUEST['JDM-LOGO-URL']).'";'."\n";
				}
				if (preg_match('/JDM-LAB-LOGO-URL[\'"]\] *= */',$line)) {
					$line = '$_SESSION[ssig() . \'JDM-LAB-LOGO-URL\'] = "'.html_entity_decode($_REQUEST['JDM-LAB-LOGO-URL']).'";'."\n";
				}
				if (preg_match('/JPARAM-MAKE-BOT-GIFT[\'"]\] *= */',$line)) {
					$line = '$_SESSION[ssig() . \'PARAM-MAKE-BOT-GIFT\'] = "'.$_REQUEST['PARAM-MAKE-BOT-GIFT'].'";'."\n";
				}
				if (preg_match('/PARAM-ALLOW-TRIALS[\'"]\] *= */',$line)) {
					$line = '$_SESSION[ssig() . \'PARAM-ALLOW-TRIALS\'] = "'.$_REQUEST['PARAM-ALLOW-TRIALS'].'";'."\n";
				}
				if (preg_match('/PARAM-HELP-INSTRUCTIONS[\'"]\] *= */',$line)) {
					$line = '$_SESSION[ssig() . \'PARAM-HELP-INSTRUCTIONS\'] = "'.$_REQUEST['PARAM-HELP-INSTRUCTIONS'].'";'."\n";
				}
				fwrite($paramTempFile,$line);
			} 
			fclose($paramFile); 
			fclose($paramTempFile);
			rename('PARAM.php.tmp','../PARAM.php');
			if (!file_exists('../LOC-'.$_SESSION['LANG'].'.php')) {
				create_loc_file('../LOC-EN.php',$_SESSION['LANG']);
			}
			if (!file_exists('../HELP-'.$_SESSION['LANG'].'.php')) {
				copy('../HELP-EN.php','../HELP-'.$_SESSION['LANG'].'.php');
			}
			if (!file_exists('../CHARTE-'.$_SESSION['LANG'].'.php')) {
				copy('../CHARTE-EN.php','../CHARTE-'.$_SESSION['LANG'].'.php');
			}
			$_SESSION[ssig() . 'ADMIN'] = $sessionAdmin;
			$_SESSION[ssig() . 'ADMIN_PASSWORD'] = $sessionPassword;
			$_SESSION[ssig() . 'ADMIN_EMAIL'] = $sessionEmail;
		}
		echo '<p>The interface is in English. When you have time, you can later download, edit and upload back
		the <code>LOC-',$_SESSION['LANG'],'.php</code>,
		<code>HELP-',$_SESSION['LANG'],'.php</code> and <code>CHARTE-',$_SESSION['LANG'],'.php</code> files
		by clicking on the <a href="localize.php">Localization</a> tab and translate the messages 
		in your language in order to localize the game interface.</p>
		<p>Then, the next step will create the database structure.
		Beware, the database table names are case sensitive. If you run JeuxDeMots on Windows, 
		you must add the following command in the <em>my.ini</em> file: <code>lower_case_table_names=0</code>.
		<a href="?step=CREATE_DB">Next step</a> </p>';
	}

	// STEP DB_PARAM
	if ($step == 'CREATE_DB') {
		echo 'Database access...<br/>';
        echo 'Testing the connexion...';
		$connexion = mysql_connect($_SESSION[ssig() . 'CONNEXION-URL'], $_SESSION[ssig() . 'CONNEXION-USER'], $_SESSION[ssig() . 'CONNEXION-PSWD']);
		if (!$connexion) {
               echo "Sorry, impossible to connect to the server " . $_SESSION[ssig() . 'CONNEXION-URL'] . "\n";
			   echo '<p> <a href="?step=EDIT_PARAM">Change the database settings (server, user or password)</a>';
               exit();
        }
        else {
        	echo 'OK<br/>';
           echo 'Testing if the database already exists...';
          	$result = mysql_query('USE ' . $_SESSION[ssig() . 'CONNEXION-DBNAME'] . ';',$connexion);
			if ($result) {
				echo '<p>The database already exists.
				Either <a href="?step=BUILD_DB">drop the contents and rebuild the structure</a>
				or <a href="?step=UPDATE_DB">update the existing database</a></p>';
				exit();
			}
           echo 'Creating the database...';
          	$result = mysql_query('CREATE DATABASE ' . $_SESSION[ssig() . 'CONNEXION-DBNAME'] . ' CHARACTER SET utf8;',$connexion);
			if (!$result) {
				echo "Sorry, impossible to create the database ", $_SESSION[ssig() . 'CONNEXION-DBNAME'], ': ', mysql_error($connexion);
				echo '<p> <a href="?step=EDIT_PARAM">Change the database settings (database name or user)</a></p>';
				exit();
			}
        	echo 'OK<br/>';
			echo '<p>Now, go to the <a href="?step=BUILD_DB">next step</a> in order to build the database structure.</p>';
        }
     }


	// STEP DB_PARAM
	if ($step == 'BUILD_DB') {
		$connexion = mysql_connect($_SESSION[ssig() . 'CONNEXION-URL'], $_SESSION[ssig() . 'CONNEXION-USER'], $_SESSION[ssig() . 'CONNEXION-PSWD']);
		if (!$connexion) {
               echo "Sorry, impossible to connect to the server " . $_SESSION[ssig() . 'CONNEXION-URL'] . "\n";
			   echo '<p> <a href="?step=EDIT_PARAM">Change the database settings (server, user or password)</a>';
               exit();
        }
		echo 'Accessing the database...';
        $result = mysql_query('USE ' . $_SESSION[ssig() . 'CONNEXION-DBNAME'] . ';',$connexion);
		if (!$result) {
			echo "Sorry, impossible to access to the database ", $_SESSION[ssig() . 'CONNEXION-DBNAME'] , ': ', mysql_error($connexion);
			exit();
		}
		 // Spécifie l'encodage UTF-8 pour dialoguer avec la BD
		mysql_query('SET NAMES utf8',$connexion);
		echo 'OK<br/>';
		echo 'Loading the database structure...';
		load_sql_file('database-structure.sql');
        echo 'OK<br/>';
		echo '<p>Now, go to the <a href="?step=FILL_DB">next step</a> in order to fill-in the database.</p>';
     }
     
     
	if ($step == 'FILL_DB') {
		openconnexion();
		echo 'Filling the database...<br/>';
		if (empty($_SESSION[ssig() . 'playerid']) && !empty($_SESSION[ssig() . 'ADMIN']) && !empty($_SESSION[ssig() . 'ADMIN_PASSWORD'])) {
			$request = 'INSERT INTO Players (name, pwd, email, admin) values (\''.$_SESSION[ssig() . 'ADMIN'].'\',\''.$_SESSION[ssig() . 'ADMIN_PASSWORD'].'\',\''.$_SESSION[ssig() . 'ADMIN_EMAIL']. '\',\'Y\');';
			$result = mysql_query($request);
			if (!$result) {
				echo "Sorry, impossible to modify the database ", $_SESSION[ssig() . 'CONNEXION-DBNAME'], ': ', mysql_error();
				echo '<p> <a href="?step=EDIT_PARAM">Change the database settings (name or user)</a></p>';
				exit();
			}
			$_SESSION[ssig() . 'login'] = $_SESSION[ssig() . 'ADMIN'];
			$_SESSION[ssig() . 'playerid'] = get_player_id($_SESSION[ssig() . 'ADMIN']);
			unset($_SESSION[ssig() . 'ADMIN']);
			unset($_SESSION[ssig() . 'ADMIN_PASSWORD']);
			load_sql_file('database-init.sql');
			echo '<p>The database has been filled with an announce, a tip, a guest player (name=guest) and a test player (name and password=test).</p>';
		}
		echo 'OK, now you can <a href="data.php">import data</a> in your language<br/>';
     }
     
     if ($step == 'UPDATE_DB') {
		openconnexion();
		echo 'Updating the database...<br/>';
		load_sql_file('database-update.sql');
		$request = 'INSERT INTO Players (name, pwd, email, admin) values (\''.$_SESSION[ssig() . 'ADMIN'].'\',\''.$_SESSION[ssig() . 'ADMIN_PASSWORD'].'\',\''.$_SESSION[ssig() . 'ADMIN_EMAIL']. '\',\'Y\');';
		$result = mysql_query($request);
		if (!$result) {
			echo "Sorry, impossible to modify the database ", $_SESSION[ssig() . 'CONNEXION-DBNAME'], ': ', mysql_error();
			echo '<p> <a href="?step=EDIT_PARAM">Change the database settings (database name or user)</a></p>';
			exit();
		}
		$_SESSION[ssig() . 'login'] = $_SESSION[ssig() . 'ADMIN'];
		$_SESSION[ssig() . 'playerid'] = get_player_id($_SESSION[ssig() . 'ADMIN']);
		unset($_SESSION[ssig() . 'ADMIN']);
		unset($_SESSION[ssig() . 'ADMIN_PASSWORD']);
		echo 'OK<br/>';
     }

?>
</div>
	</body>
	</html>
