<?php

function jdm_import_data($file,$format = 'text') {
	$filehandle = '';
	if ($format == 'gzip') {
		$filehandle = gzopen($file,'r');
	}
	else {
		$filehandle = fopen($file,'r');
	}
	$i=0;
	$line = '';
	if ($format == 'gzip') {
		$line= gzgets($filehandle);
	}
	else {
		$line= fgets($filehandle);
	}
	echo 'import data, first line :',$line,'<br/>';
	while ($line !== false) {
		$line=rtrim($line);
		$array = explode('#',$line);
		$node = mysql_real_escape_string($array[0]);
		$pos = '';
		if (!empty($array[1])) {
			$pos = $array[1];
		}
		$array = explode(';',$node);
		$name = $array[0];
		$annotation = '';
		$gloss = '';
		if (!empty($array[1])) {
			$annotation = mysql_real_escape_string($array[1]);
		}
		if (!empty($array[2])) {
			$gloss = mysql_real_escape_string($array[2]);
		}
		$nodeId = '';
		$posId='';
		$query = "SELECT id FROM Nodes WHERE type=1 and name='$name' and annotation='$annotation';";
		$result = mysql_query($query);
		if ($result) {
			if (mysql_num_rows($result)>0) {
				$node = mysql_fetch_object($result);
				$nodeId = $node->id;
			}
			else {
				$query = "INSERT INTO Nodes (name,annotation,gloss,type, w) VALUES ('$name','$annotation','$gloss',1,50);";
				$result2 = mysql_query($query);
				if ($result2) {
					$nodeId = mysql_insert_id();
				}
			}
			if ($pos!=='') {
			$query = "SELECT id FROM Nodes WHERE type=4 and name='$pos';";
			$result = mysql_query($query);
			if ($result) {
				if (mysql_num_rows($result)>0) {
					$node = mysql_fetch_object($result);
					$posId = $node->id;
				}
				else {
					$query = "INSERT INTO Nodes (name,type) VALUES ('$pos',4);";
					$result2 = mysql_query($query);
					if ($result2) {
						$posId = mysql_insert_id();
					}
				}
				$query = "INSERT INTO Relations (node1,node2,type) VALUES ('$nodeId','$posId',4);";
				$result3 = mysql_query($query);
				if (!$result3) {
					echo 'problem creating a pos relation';
				}
				else {
					
				}
			}
			}
			$i++;
			if ($i%100 == 0) {
				echo "$i nodes imported so far<br/>";					
			}
		}
		else {
			echo 'result1 empty: ', mysql_error();
		}
		if ($format == 'gzip') {
			$line= gzgets($filehandle);
		}
		else {
			$line= fgets($filehandle);
		}
	}
	if ($format == 'gzip') {
		gzclose($filehandle);
	}
	else {
		fclose($filehandle);
	}
}

?>
