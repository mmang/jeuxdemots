<?php session_start();?>
<?php include_once 'misc_functions.php'; ?>
<?php
    openconnexion();
	$_SESSION[ssig() . 'state']=0;
?>
<html>
 <head>
    <title>A propos de JeuxDeMots</title>
    <?php header_page_encoding(); ?>
  </head>
<?php include 'HTML-body.html' ; ?>
<?php topblock(); ?>
<div class="jdm-level1-block">
	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
    <?php echo "A propos de JeuxDeMots"; ?>
    </div>
	</div>

    <div class="jdm-login-block">
    <?php  loginblock(); ?>
    </div>
</div>

<div class="jdm-level2-block">
<TABLE	border="0"
	width="100%"
	cellspacing="3" cellpadding="10"
	summary="aide" bgcolor="white" style="opacity:0.90;"
	>
<TR><TH width="200">
    <TH>
    <TH width="150">

<TR valign= "top">
    <TH align="right"><P><h2>Auteurs</h2>
    <TH align="left" BGCOLOR="FAFAFA"> 
    <P>
    JeuxDeMots (c) Mathieu Lafourcade (&amp; Mathieu Mangeot pour la localisation), 2007-2009
    <?php echo '<br>version ' . $_SESSION[ssig() . 'version']?>
    <P>Mathieu.lafourcade@lirmm.fr
    <br>http://www.lirmm.fr/~lafourcade
    <P>
    (c) LIRMM, 2007-2009
    <P><a href="http://jeuxdemots.org/CHARTE-FR.php/">La charte de JeuxDeMots</a>
    <TH>

<TR valign= "top">
    <TH align="right"><P><h2>Remerciements</h2>
    <TH align="left" BGCOLOR="FAFAFA"> 
    <P>
    Tous les potes qui ont participé de près ou de loin : Gilles Krado, Didier Mok, Alain aj, Mehdi,...
    Stéphane sriou, Ninie, ...
    et évidement tous les joueurs, sans oublier les fondus du forum :)
    (à completer)
    <P>
    Merci Lyn pour le logo. Pour le design et les conseils photoshop une mention spéciale à Syl.
    <TH>
    
<TR valign= "top">
    <TH align="right"><P><h2>Contenu</h2>
    <TH align="left" BGCOLOR="FAFAFA"> 
    <P>
    <a href="mailto:<php echo $_SESSION[ssig().'CONTACT-EMAIL'];?>?subject=JeuxDeMots inappropriate contents" >Signaler</a> contenu inaproprié.
     <TH>
    
    <TR valign= "top">
    <TH align="right"><P><h2>Réseau lexical</h2>
    <TH align="left" BGCOLOR="FAFAFA">
    <P>
    Les données lexicales sont accessibles (<a href="<?php echo $_SESSION[ssig().'EXPORTDIR']?>"><?php echo $_SESSION[ssig().'EXPORTDIR']?></a>) 
	sous la licence Creative Commons Paternité-Partage des Conditions Initiales à l'Identique 2.0 France (<a href="http://creativecommons.org/licenses/by-sa/2.0/fr/">http://creativecommons.org/</a>).
		
    <P><a href="http://jeuxdemots.org/rezo.php">Accès interactif au réseau lexical<a> de JeuxDeMots (version bas niveau).
    <TH>
    
    <TR valign= "top">
    <TH align="right"><P><h2>Statistiques</h2>
    <TH align="left" BGCOLOR="FAFAFA"> 
    <P>
    <?php
	include('jdm-full-stats.txt');
	echo " (<a href=\"full_stat_maj.php\" target=\"blank\">Mettre à jour</a>)<P><BR>";
    ?>
    <TH>
</TABLE>
</div>

<?php playerinfoblock($_SESSION[ssig() . 'playerid']) ?>
<?php 
    bottomblock();
    closeconnexion();
?>

  </body>
</html>
