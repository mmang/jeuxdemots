<?php 
$GLOBALS['EN-'] = '[String not found]';

//------
$GLOBALS['EN-ENCODING-GENERAL'] = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
$GLOBALS['EN-ENCODING-MAIL'] = "UTF-8";
$GLOBALS['EN-ENCODING-DB'] = "utf8";
$GLOBALS['EN-ENCODING-MUSEUM'] = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
 
//------ relations
$GLOBALS['EN-r_associated'] = 'idea';
$GLOBALS['EN-r_associated-HELP'] = 'Tout terme lié d\'une façon ou d\'une autre au mot cible... Ce mot vous fait penser à quoi ?';
$GLOBALS['EN-r_acception'] = 'acception';
$GLOBALS['EN-r_acception-HELP'] = 'acception';
$GLOBALS['EN-r_definition'] = 'definition';
$GLOBALS['EN-r_definition-HELP'] = 'definition';
$GLOBALS['EN-r_domain'] = 'domain';
$GLOBALS['EN-r_domain-HELP'] = 'Il est demandé de fournir des domaines relatifs au mot cible. Par exemple, pour \'corner\', on pourra donner les domaines \'football\' ou \'sport\'.';
$GLOBALS['EN-r_pos'] = 'r_pos';
$GLOBALS['EN-r_pos-HELP'] = 'r_pos';
$GLOBALS['EN-r_syn'] = 'synonym';
$GLOBALS['EN-r_syn-HELP'] = 'À partir d\'un terme, il est demandé d\'énumérer les synonymes ou quasi-synonymes de ce terme.';
$GLOBALS['EN-r_isa'] = 'generic';
$GLOBALS['EN-r_isa-HELP'] = '\'animal\' est un générique de \'chat\', \'mammifère\', \'être vivant\' etc. en sont d\'autres...';
$GLOBALS['EN-r_anto'] = 'contrary';
$GLOBALS['FR-r_anto-HELP'] = '\'chaud\' est le contraire de \'froid\', vous vous rappelez ? :)';
$GLOBALS['EN-r_hypo'] = 'specific';
$GLOBALS['EN-r_hypo-HELP'] = '\'mouche\', \'abeille\', \'guêpe\' sont des spécifiques de \'insecte\'...';
$GLOBALS['EN-r_has_part'] = 'part-of';
$GLOBALS['EN-r_has_part-HELP'] = 'Il faut donner des parties/constituants/éléments du mot cible. Par exemple, \'voiture\' pourrait avoir comme parties : \'porte\', \'roue\', \'moteur\', ...';
$GLOBALS['EN-r_holo'] = 'all';
$GLOBALS['EN-r_holo-HELP'] = 'Le tout est ce qui contient l\'objet en question. Pour \'main\', on aura \'bras\', \'corps\', \'personne\', etc... On peut aussi voir le tout comme l\'ensemble auquel appartient un élément, comme \'classe\' pour \'élève\'.';
$GLOBALS['EN-r_locution'] = 'locution';
$GLOBALS['EN-r_locution-HELP'] = 'A partir d\'un terme, il est demandé d\'énumérer les locutions, expression ou mots composés en rapport avec ce terme. Par exemple, pour \'moulin\', ou pourra avoir \'moulin à vent\', \'moulin à eau\', \'moulin à café\'. Pour \'vendre\', on pourra avoir \'vendre la peau de l\'ours avant de l\'avoir tué\', \'vendre à perte\', etc...';
$GLOBALS['EN-r_agent'] = 'typical agent';
$GLOBALS['EN-r_agent-HELP'] = 'L\'agent (qu\'on appelle aussi le sujet) est l\'entité qui effectue l\'action. Par exemple dans - Le chat mange la souris -, l\'agent est le chat.';
$GLOBALS['EN-r_patient'] = 'typical patient';
$GLOBALS['EN-r_patient-HELP'] = 'Le patient (qu\'on appelle aussi l\'objet) est l\'entité qui subit l\'action. Par exemple dans - Le chat mange la souris -, le patient est la souris.';
$GLOBALS['EN-r_flpot'] = 'potentiel de FL';
$GLOBALS['EN-r_flpot-HELP'] = 'potentiel de FL';
$GLOBALS['EN-r_lieu'] = 'location';
$GLOBALS['EN-r_lieu-HELP'] = 'À partir d\'un nom d\'objet (ou autre), il est demandé d\'énumérer les lieux typiques ou peut se trouver l\'objet en question.';
$GLOBALS['EN-r_instr'] = 'typical instrument';
$GLOBALS['EN-r_instr-HELP'] = 'L\'instrument est l\'objet avec lequel on fait l\'action. Dans - Il mange sa salade avec une fourchette -, fourchette est l\'instrument.';
$GLOBALS['EN-r_carac'] = 'caracteristic';
$GLOBALS['EN-r_carac-HELP'] = 'Pour une terme donné, en général un objet, il est demandé d\'énumérer les caractéristiques possibles et/ou typiques de cet objet. Par exemple, pour \'eau\' on pourra avoir \'liquide\', \'froide\', \'chaude\', etc.';
$GLOBALS['EN-r_data'] = 'r_data';
$GLOBALS['EN-r_data-HELP'] = 'r_data';
$GLOBALS['EN-r_lemma'] = 'r_lemma';
$GLOBALS['EN-r_lemma-HELP'] = 'r_lemma';
$GLOBALS['EN-r_magn'] = 'magn';
$GLOBALS['EN-r_magn-HELP'] = 'La magnification ou amplification, par exemple - forte fièvre - ou - fièvre de cheval - pour fièvre. Ou encore - amour fou - pour amour, - peur bleue - pour peur.';
$GLOBALS['EN-r_antimagn'] = 'antimagn';
$GLOBALS['EN-r_antimagn-HELP'] = 'L\'inverse de la magnification, par exemple - bruine - pour pluie.';
$GLOBALS['EN-r_familly'] = 'family';
$GLOBALS['EN-r_familly-HELP'] = 'Des mots de la même famille sont demandés. Par exemple, pour \'lait\' on pourrait mettre \'laitier\', \'laitage\', \'laiterie\', etc.';
$GLOBALS['EN-r_carac-1'] = 'caractéristique-1';
$GLOBALS['EN-r_carac-1-HELP'] = 'caractéristique-1';
$GLOBALS['EN-r_agent-1'] = 'agent typique-1';
$GLOBALS['EN-r_agent-1-HELP'] = 'agent typique-1';
$GLOBALS['EN-r_instr-1'] = 'instrument-1';
$GLOBALS['EN-r_instr-1-HELP'] = 'instrument-1';
$GLOBALS['EN-r-patient-1'] = 'patient-1';
$GLOBALS['EN-r-patient-1-HELP'] = 'patient-1';
$GLOBALS['EN-r_domain-1'] = 'domaine-1';
$GLOBALS['EN-r_domain-1-HELP'] = 'domaine-1';
$GLOBALS['EN-r_lieu-1'] = 'lieu-1';
$GLOBALS['EN-r_lieu-1-HELP'] = 'A partir d\'un lieu, il est demandé d\'énumérer ce qu\'il peut typiquement s\'y trouver.';
$GLOBALS['EN-r_pred'] = 'predicate';
$GLOBALS['EN-r_pred-HELP'] = 'predicate';
$GLOBALS['EN-r_lieu_action'] = 'lieu_action';
$GLOBALS['EN-r_lieu_action-HELP'] = 'énumérer les action typiques possibles dans ce lieu.';
$GLOBALS['EN-r_action_lieu'] = 'action_lieu';
$GLOBALS['EN-r_action_lieu-HELP'] = 'A partir d\'une action (un verbe), énumérer les lieux typiques possibles où peut etre réalisée cette action.';
$GLOBALS['EN-r_maniere'] = 'manière';
$GLOBALS['EN-r_maniere-HELP'] = 'De quelles MANIÈRES peut être effectuée l\'action (le verbe) proposée. Il s\'agira d\'un adverbe ou d\'un équivalent comme une locution adverbiale, par exemple : \'rapidement\', \'sur le pouce\', \'goulûment\', \'salement\' ... pour \'manger\'.';
$GLOBALS['EN-r_sentiment'] = 'sentiment';
$GLOBALS['EN-r_sentiment-HELP'] = 'Pour un terme donné, évoquer des SENTIMENTS ou EMOTIONS que vous pourriez associer à ce terme. Par exemple, la joie, le plaisir, le dégoût, la peur, la haine, l\'amour, l\'indifférence, l\'envie, etc.';
$GLOBALS['EN-r_sentiment'] = 'sentiment';
$GLOBALS['EN-r_sentiment-HELP'] = 'Pour un terme donné, évoquer des SENTIMENTS ou EMOTIONS que vous pourriez associer à ce terme. Par exemple, la joie, le plaisir, le dégoût, la peur, la haine, l\'amour, l\'indifférence, l\'envie, etc.';
$GLOBALS['EN-r_sens'] = 'sens';
$GLOBALS['EN-r_sens-HELP'] = 'Quels SENS/SIGNIFICATIONS pouvez vous donner au terme proposé. Il s\'agira de termes évoquant chacun des sens possibles, par exemple : \'forces de l\'ordre\', \'contrat d\'assurance\', \'police typographique\', ... pour \'police\'.';
$GLOBALS['EN-r_cause'] = 'cause';
$GLOBALS['EN-r_cause-HELP'] = 'B (que vous devez donner) est une cause possible de A. A et B sont des verbes ou des noms.  Exemples : se blesser -> tomber ; vol -> pauvreté ; incendie -> négligence ; mort --> maladie ; etc.';
$GLOBALS['EN-r_consequence'] = 'conséquence';
$GLOBALS['EN-r_consequence-HELP'] = 'B (que vous devez donner) est une conséquence possible de A. A et B sont des verbes ou des noms.  Exemples : tomber -> se blesser ; faim -> voler ; allumer -> incendie ; négligence --> accident ; etc.';
$GLOBALS['EN-r_telique'] = 'rôle télique';
$GLOBALS['EN-r_telique-HELP'] = 'Le rôle télique indique le but ou la fonction du nom ou du verbe. Par exemple, couper pour couteau, scier pour scie, etc.';
$GLOBALS['EN-r_agentif'] = 'rôle agentif';
$GLOBALS['EN-r_agentif-HELP'] = 'Le rôle agentif indique le mode de création du nom. Par exemple, construire pour maison, rédiger ou imprimer pour livre, etc.';

//------

$GLOBALS['EN-GENGAME-TITLE'] = "JeuxDeMots: Association Game";
$GLOBALS['EN-GENGAME-GAMETYPE'] = "Association Game";
$GLOBALS['EN-GENGAME-WARN-NEWINSTR'] = "<blink>Beware new instruction</blink>";

$GLOBALS['EN-GENGAME-PASS'] = "Pass";
$GLOBALS['EN-GENGAME-BUYTIME'] = "Buy 30 s";
$GLOBALS['EN-GENGAME-SEND'] = "Send";
$GLOBALS['EN-GENGAME-TABOO'] = "Taboo words: ";
$GLOBALS['EN-GENGAME-NOPROPTERM'] = "No word proposed";
$GLOBALS['EN-GENGAME-LASTPROPTERM'] = "Last proposed word :";

$GLOBALS['EN-GENGAME-PROMPT-GENERATION'] = "Game generation";
$GLOBALS['EN-GENGAME-PROMPT-INSTRUCTION'] = "Reading instruction";
$GLOBALS['EN-GENGAME-PROMPT-INGAME'] = "In game";
$GLOBALS['EN-GENGAME-PROMPT-RES'] = "Results";

$GLOBALS['EN-GENGAME-REMAININGTIME'] = "Remaining time";
$GLOBALS['EN-GENGAME-PROMPT-WORDLEVEL'] = "Level: ";

$GLOBALS['EN-GENGAME-COUNTDOWN-SECONDS'] = " s";
$GLOBALS['EN-GENGAME-COUNTDOWN-END'] = "end";

$GLOBALS['EN-GENGAME-TRICK'] = "Trick";

$GLOBALS['EN-GENGAME-30SECONDS-BUTTON'] = "pics/30sB.gif";
$GLOBALS['EN-GENGAME-OK-BUTTON'] = "pics/J2M-OKB.gif";
$GLOBALS['EN-GENGAME-BONUS-IMAGE'] = "pics/JDM-bonus.gif";

//------
$GLOBALS['EN-SIGNIN-CONNECT-SUBMIT'] = "Login";
$GLOBALS['EN-SIGNIN-CONNECT-PROMPT'] = "Please log yourself";
$GLOBALS['EN-SIGNIN-YOURNAME'] = "Your name:";
$GLOBALS['EN-SIGNIN-YOURPWSD'] = "Your password (10 char max:";
$GLOBALS['EN-SIGNIN-YOUREMAIL'] = "Your email:";

$GLOBALS['EN-SIGNIN-REGISTER-SUBMIT'] = "Register";
$GLOBALS['EN-SIGNIN-REGISTER-PROMPT'] = "Not yet registered?";
$GLOBALS['EN-SIGNIN-REGISTER-WARNING'] = "<br/>Warning, if your email adress is not valid, we will not
be able to contact you.";

$GLOBALS['EN-SIGNIN-PWDFORGOT-SUBMIT'] = "Send it";
$GLOBALS['EN-SIGNIN-PWDFORGOT-PROMPT'] = "Password forgotten?";

// "bienvenue à '$Nom' qui vient de s'inscrire."
$GLOBALS['EN-SIGNIN-WELCOME'] = "welcome to '<0>' who just registered.";
$GLOBALS['EN-SIGNIN-THX-REGISTERING']  = "Thank you for registering, <0> . - you will be forwarded to <a href=\"jdm-accueil.php\">Home</a>";

$GLOBALS['EN-SIGNIN-INVALID-LOGIN'] = "invalid login - <a href=\"jdm-signin.php\">Back</a>";
$GLOBALS['EN-SIGNIN-OK-LOGIN'] = "ok - you will be forwarded to <a href=\"jdm-accueil.php\">Home</a>.";

$GLOBALS['EN-SIGNIN-NON-AVAILABLE'] = "This username already exists and is not available...";
$GLOBALS['EN-SIGNIN-USER-NOT-FOUND'] = "this username does not exist";
$GLOBALS['EN-SIGNIN-PASSWD-FORGET-TITLE'] = "Your password for JeuxDeMots";
$GLOBALS['EN-SIGNIN-PASSWD-FORGET-BODY'] = "Your password is <0>";
$GLOBALS['EN-SIGNIN-PASSWD-FORGET-SENT'] = "Password sent to <0> <BR>";
$GLOBALS['EN-SIGNIN-PAGE-TITLE'] = "Log in / Sign up";
$GLOBALS['EN-SIGNIN-VALID-EMPTY-EMAIL'] = "<font color='#FF0000'>The email cannot be empty</font>";
$GLOBALS['EN-SIGNIN-VALID-EMPTY-PASSWD'] = "<font color='#FF0000'>The password cannot be empty</font>";
$GLOBALS['EN-SIGNIN-VALID-INVALID-EMAIL'] = "<font color='#FF0000'>The email address contains invalid characters, please check.</font>";
$GLOBALS['EN-SIGNIN-VALID-EMPTY-USERNAME'] = "<font color='#FF0000'>The username cannot be empty</font>";


//------
// "Le $date, $what";
$GLOBALS['EN-EVENT-TMPL-DATE'] = "The <0>, <1>";

//------

$GLOBALS['EN-MAIL-FROM'] = "JeuxDeMots";

$GLOBALS['EN-MAIL-LEVEL'] = "\nYou have now <0> credits, <1> points of honnor and you are level <2>.";

$GLOBALS['EN-MAIL-BOTTOM'] = "
	See you soon,
    Regards,
    JeuxDeMots
    http://www.lirmm.fr/jeuxdemots-english
    
    Trick : <0>";

$GLOBALS['EN-MAIL-RESULT-TITLE'] = "Good news from JeuxDeMots";
$GLOBALS['EN-MAIL-RESULT'] = "Hello <0>,
    I am pleased to tell you that you just won <1> credits with the word '<2>' 
    helped by <3>.
    
    <0>, your answers for this game were: <4> .
    <3> answers for this game were: <5> .
    Thus, the words you have in common are: <6> .
    
    The rule was: <7> <2>
    
    Furthermore, you win some honnor points and your level increase slightly.
    ";

$GLOBALS['EN-MAIL-GIFT-TITLE'] = "A gift from JeuxDeMots is waiting for you...";
$GLOBALS['EN-MAIL-GIFT'] = "Hello <0>,
    I am pleased to tell you that you just received a present. 
    Go to the souk in order to open it. 
    ";

$GLOBALS['EN-MAIL-GIFT-DALTON'] = "Hey <0>, !
    Bart Simpson gave you a present. Go to the souk in order to open it. 
    ";

$GLOBALS['EN-MAIL-PARRAINAGE-TITLE'] = "Invitation for JeuxDeMots from <0>";
$GLOBALS['EN-MAIL-PARRAINAGE'] = "Hello, \n\n

(TO BE TRANSLATED)

Si vous savez que Jean-Pierre Foucault, Thierry Beccaro, Julien Lepers et Laurence Boccolini ne sont 
pas respectivement philosophe, sportif, chanteur et plante potagère du sud de l'Italie, vous venez 
de franchir la première étape pour participer à JeuxDeMots !

Vous rêviez de pouvoir enchaîner les « neuf points gagnants », le « quatre à la  suite » et le 
« face-à-face » tout en pouvant choisir un « 50 / 50 » ou « l'appel à l'ami Google » sans devoir 
assimiler les règles de Motus et sans vous faire sortir parce que « franchement, pas connaître le 
titre de la dernière chanson de Patrick Sébastien, ben, c'est carrément être trop nul » !!!
Et bien, JeuxDeMots est fait pour VOUS !

Vous voulez faire avancer la recherche sans devoir souscrire au 3637 
[mais ce n'est pas incompatible], devenir le Maître Capello du wikipedia ou le Richard Virenque 
de l'encyclopaedia universalis ou tout simplement aider le CNRS (sans les OGM) : 
JeuxdeMots est aussi fait pour VOUS !

World of Warcraft, Second Life ou Disneyland® Resort Paris vous font encore peur ou vous n'avez pas
pris le train du Web2.0 à destination des MySpace, Skyblog, FaceBook et autres réseaux sociaux : 
JeuxDeMot est tout de même fait pour VOUS !

Rejoignez nous et montrez ainsi à toute votre famille, vos amis et même à Madame Biglon
(votre ancienne maîtresse d'école) votre valeur ! Et si vous ne voulez rien montrer,
vous pouvez toujours prendre un pseudo !

Bref, si vous voulez faire partie d'une aventure avant que Google ne la rachète : venez participer 
à JeuxDeMots et inscrivez vous : http://www.lirmm.fr/jeuxdemots\n\n

\"<0>\" pense que vous êtes digne de nous rejoindre !\n";

$GLOBALS['EN-MAIL-OTHER-PLAYER-BEWARE'] = 'BEWARE';

$GLOBALS['EN-MAIL-OTHER-PLAYER-RULES'] = "
 En envoyant ce mail, vous acceptez :
	<br>(1) de communiquer votre adresse email au destinataire, 
	afin que ce dernier puisse vous répondre (auquel cas, vous aurez connaissance de son
	adresse) ;
	<BR>(2) de voir votre message gardé un certain temps pour vérifier les cas d'abus ;
	<BR>(3) de dépenser 500 crédits pour les frais attenants :)
	<BR> Si trop d'abus sont constatés, cette fonctionnalité sera désactivée.";

$GLOBALS['EN-MAIL-OTHER-PLAYER-FROM-TO'] = "From '<0>' to '<1>'";
$GLOBALS['EN-MAIL-OTHER-PLAYER-TITLE'] = "Title:";
$GLOBALS['EN-MAIL-OTHER-PLAYER-MESSAGE'] = "Message:";
$GLOBALS['EN-MAIL-OTHER-PLAYER-SEND-BUTTON'] = "Send";
$GLOBALS['EN-MAIL-OTHER-PLAYER-MESSAGE-SENT'] = "Message sent";
$GLOBALS['EN-MAIL-OTHER-PLAYER-NOT-ENOUGH-CREDITS'] = "Not enough credits";



//------

$GLOBALS['EN-MISC-CONTACT'] = "Contact";
$GLOBALS['EN-MISC-HOME'] = "Home";
$GLOBALS['EN-MISC-FORUM'] = "Forum";
$GLOBALS['EN-MISC-RANKING'] = "Ranking";
$GLOBALS['EN-MISC-EVENTS'] = "Events";
$GLOBALS['EN-MISC-SOUK'] = "Souk";
$GLOBALS['EN-MISC-OPTIONS'] = "Options";
$GLOBALS['EN-MISC-HELP'] = "Help";

$GLOBALS['EN-MISC-CREDIT'] = "Credits: ";
$GLOBALS['EN-MISC-HONNOR'] = "Honnor: ";
$GLOBALS['EN-MISC-LEVEL'] = "Level: ";

$GLOBALS['EN-MISC-LOGIN'] = "Logout";
$GLOBALS['EN-MISC-LOGOUT'] = "Login";

$GLOBALS['EN-MISC-PLAYAS-PLAY'] = "Play";
$GLOBALS['EN-MISC-PLAYAS-GUEST'] = " as guest";
$GLOBALS['EN-MISC-PLAYAS-GUESTWARNING'] = "You are playing as guest and you have not much inpact on the game<br>Please log in.";

//------

$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-0'] = "Give ASSOCIATED IDEAS for the following term:";

$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-3'] = "Give THEMES/DOMAINES (for example: 'Sports', 'Medecine', 'Cinema', 'Cuisine,' etc.) for the following word:";
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-5'] = "Give SYNONYMES for the following word:";
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-6'] = "Give GENERICS (for example: 'vehicle' pour 'car', 'mammal', 'animal' pour 'car') for the following word:";
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-7'] = "Give OPPOSITES (for example: 'cold' pour 'hot', 'high' pour 'low') for the following word:";
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-8'] = "Give SPECIFICS (for example: 'chat', 'chien', 'animal de compagnie',  etc. pour 'animal' - ou encore 'voiture', 'train', 'véhicule spatial', etc. pour 'véhicule') for the following word:";
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-9'] = "Give PARTS (for example: 'engine', 'wheel', etc. pour 'car' - ou encore 'civer', 'page', 'chapter' etc. pour 'book') for the following word:";
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-10'] = "Give WHOLE (for example: 'corps', 'bras', etc. pour 'coude' - ou encore 'banque' pour 'guichet') for the following word:";
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-11'] = "Give LOCUTIONS (for example: 'langue au chat', 'chat de gouttière', 'chat à neuf queues', ... pour 'chat') for the following word:";

$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-13'] = "Give typical SUJECT (for example: 'chat', 'animal', 'personne', ... pour 'manger') for the following verb:";
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-14'] = "Give typical OBJECTS (for example: 'viande', 'fruit', 'bonbon', ... pour 'manger') for the following verb:";
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-15'] = "Give typical PLACES (for example: 'pré', 'écurie', 'champs de courses', ... pour 'cheval') for the following word:";

$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-16'] = "Give typical INSTRUMENTS (for example: 'pelle', 'pioche', 'main', ... pour 'creuser') for the following verb:";
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-17'] = "Give typical CARACTERISTIQUES (for example: 'liquide', 'blanc', 'buvable', ... pour 'lait') for the following word:";

$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-20'] = "WHAT is more INTENSE (for example: 'strong fever', 'fièvre de cheval', ... for 'fever' - ou encore 'vivre intensément' pour 'vivre') for the following word:";
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-21'] = "WHAT is less INTENSE (for example: 'maisonette', ... pour 'maison' - ou encore 'marcher lentement', 'trainer' pour 'marcher') for the following word:";

$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-22'] = "Give words of the same FAMILY (for example: ) for the following word:";

$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-23'] = "WHAT could typically possess the following CHARACTERISTICS (for example: 'water', 'wine', 'milk' for 'liquid') :";
		
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-24'] = "WHAT can typicaly do the following SUBJECT (for example: 'eat', 'sleep', 'hunt' pour 'lion') :";
		
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-25'] = "WHAT can we DO with the following INSTRUMENT (for example: 'write', 'draw'  pour 'pen') :";
		
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-26'] = "Which ACTION can sustain the folowing OBJECT (for example: ...) :";
		
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-27'] = "Give TERMS belonging to the following DOMAIN (for example: 'goal', 'penalty' pour 'Football') :";

$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-28'] = "WHAT can we typically FIND in the following PLACE (for example: 'fish', 'shell', 'water', 'salt' pour 'sea') :";
		
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-30'] = "WHAT can we typically DO in the following PLACE (for example: 'eat', 'drink', 'order' pour 'restaurant' -- verbs are requested) :";
		
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-31'] = "In which PLACES can we do the following thing (for example: 'restaurant', 'kitchen', 'fast-food' for 'eat' -- places are requested) :";
		
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-32'] = "What kind of SENTIMENTS/EMOTIONS does the following term does to you?";

$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-34'] = "HOW can be done the following action:<br/><font size=\"-1\">It will be an adverb or an equivalent, eg: 'rapidement', 'sur le pouce', 'goulment', 'salement' ... for 'manger'</font>";

$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-35'] = "What MEANING can you give to the following term:<br/><font size=\"-1\">It will be a term evoking each of the possible meanings, eg: 'forces de l'ordre', 'contrat d'assurance', 'police typographique', ... for 'police'</font>";

$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-37'] = "Which GOAL/FUNCTION (telic role) can you give to the following term:<br/><font size=\"-1\">It will be a verb, eg: 'couper' for 'couteau', 'lire' for 'livre', ...</font>";
$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-38'] = "Which CREATION MODES (agentive role) can you give to the following term:<br/><font size=\"-1\">It will be a verb, eg: 'construire' for 'maison', 'rédiger'/'imprimer' for 'livre', ...</font>";

$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-41'] = "Which CONSEQUENCES (A causes B) can you give to the following term:<br/><font size=\"-1\">It will be a verb or a noun, eg:  'tomber' -> 'se blesser', 'faim' -> 'voler'/'dérober', ...</font>";

$GLOBALS['EN-GENGAME-FUNCTIONS-INSTR-42'] = "Which CAUSES (A has for cause B) can you give to the following term:<br/><font size=\"-1\">It will be a verb or a noun, eg: 'se blesser' -> 'tomber', 'voler'' -> 'faim', 'pauvreté', ...</font>";

$GLOBALS['FR-GENGAME-FUNCTIONS-ARRAY-3-INSTR'] = "Lista non-limitative de domaines (si aucun ne convient entrez le ou les votres, ou mettez *** s'il n'y a pas de domaine particulier) :";

$GLOBALS['FR-GENGAME-FUNCTIONS-ARRAY-3'] = array ("anatomie", "architecture", "astronomie", "audiovisuel", "automobile", "aviation", "beaux-arts", "biologie", "botanique",
"boucherie", "chemin de fer", "chimie", "chirurgie", "cinéma", "commerce", "couture", "cuisine", "droit", "économie", "électricité", "élevage", "équitation", "géographie", "géologie",
"grammaire", "histoire", "hydrologie", "imprimerie", "informatique", "jeux", "linguistique", "littérature", "marine", "mathématiques", "mécanique", "médecine", "militaire", 
"minéralogie", "musique", "mythologie", "optique", "peinture", "pharmacie", "philosophie", "photographie", "physiologie", "physique", "politique", "psychologie", "religion", 
"scolaire", "sexologie", "sociologie", "spectacle", "sports", "sylviculture", "technique", "télécommunications", "urbanisme", "versification", "vétérinaire", "viticulture", "zoologie");


$GLOBALS['EN-TERMSELECT-FUNCTIONS-NAME-TEMPLATE'] = 'Noun%';
$GLOBALS['EN-TERMSELECT-FUNCTIONS-ADJ-TEMPLATE'] = 'Adj%';
$GLOBALS['EN-TERMSELECT-FUNCTIONS-ADV-TEMPLATE'] = 'Adv%';
$GLOBALS['EN-TERMSELECT-FUNCTIONS-ADV-TEMPLATE'] = 'Verb:Inf';

$GLOBALS['EN-READ-INSTRUCTIONS'] = 'Please, read the instructions!';

//------

$GLOBALS['EN-OPTIONS-TITLE'] = "JeuxDeMots: options";
$GLOBALS['EN-OPTIONS-PROMPT'] = "Options";
$GLOBALS['EN-OPTIONS-CSS-FORM'] = 	"CSS :<BR>
	<input <0> id=\"go_css_submit\" type=\"submit\" name=\"go_css_submit\" value=\"Change\"> the url
	<input  id=\"go_css_url\" type=\"text\" size = \"120\" name=\"go_css_url\" value=\"<1>\">
	(by default put \"<2>\")";
$GLOBALS['EN-OPTIONS-CSS-EXAMPLE'] = "Example of css :";
$GLOBALS['EN-OPTIONS-CSS-MUSEUM-FORM'] = 	"CSS of your little museum :<BR>
	<input <0> id=\"go_css_museum_submit\" type=\"submit\" name=\"go_css_museum_submit\" value=\"Change\"> the url
	<input id=\"go_css_museum_url\" type=\"text\" size = \"120\" name=\"go_css_museum_url\" value=\"<1>\">";

$GLOBALS['EN-OPTIONS-PERSO-FORM'] = 	"Your URL:<br/>
	<input <0> id=\"go_perso_submit\" type=\"submit\" name=\"go_perso_submit\" value=\"Change\"> the url
	<input  id=\"go_perso_url\" type=\"text\" size = \"120\" name=\"go_perso_url\" value=\"<0>\">
	(eg: put \"http://myhomepage.com\")";


//------

$GLOBALS['EN-HOME-TITLE'] = "JeuxDeMots: Home";
$GLOBALS['EN-HOME-PROMPT'] = "Home";

$GLOBALS['EN-HOME-NEWS'] = "<P><blockquote>NEWS of the <0>";

$GLOBALS['EN-HOME-INFO'] = "info";

$GLOBALS['EN-HOME-HOT-LINKS'] = "Hot links";

$GLOBALS['EN-HOME-INSTRUCTIONS-TITLE'] = "How does this work? What should you we do?";

$GLOBALS['EN-HOME-INSTRUCTIONS'] = "A term (word) will be presented to you as well as an
instruction. During a timeframe of one minute, you have to enter as many proposals as you can
that follow the instruction. You will be often asked for freely associated ideas related to the given
term, but not always. Validate each proposal with the \"Send\" button or the \"Enter\" key.
Other players will share the same game. You will win credits when you have proposals in common
with other players. The more precise your proposal is, the more you win... if the other player has though of it.<BR><BR>
Please read the <a href=\"jdm-signin.php\">Charter of JeuxDeMots</a>.<P><P>";

$GLOBALS['EN-HOME-MOSTWANTED'] = 'Most Wanted';
$GLOBALS['EN-HOME-COLLECTIONS'] = 'Collections';
$GLOBALS['EN-HOME-GIFT-WAITING'] = "Gifts are waiting for you in the <A href=\"souk.php#giftlist\">souk</a>";
$GLOBALS['EN-HOME-RELATION-COUNT'] = "<0> relations have been produced so far, from which <1> taboo ones. Congrat !<P>
Weight sum of <2> ! <P>";
$GLOBALS['EN-HOME-TERM-COUNT'] = "... and <0> terms.";
$GLOBALS['EN-HOME-ACTIVE-PLAYER-COUNT'] = "<br /><br /><0> connected player.";
$GLOBALS['EN-HOME-ACTIVE-PLAYERS-COUNT'] = "<br /><br /><0> connected players.";
$GLOBALS['EN-HOME-OTHER-ANNOUNCES'] = "The other announces.";
$GLOBALS['EN-HOME-NO-PARTICULAR-THEME-ANCHOR'] =  "You do not have a particular <a href=\"souk.php\">theme</a>";
$GLOBALS['EN-HOME-PREFERRED-THEME-ANCHOR'] =   "Your favorite <a href=\"souk.php\">theme</a> is <0>";
$GLOBALS['EN-HOME-TRIALS-WAITING'] =  "You have <0> <a href=\"jdm-list-trial.php\">waiting trials</a>";
$GLOBALS['EN-HOME-LOTTERY'] = "Lottery";
$GLOBALS['EN-HOME-EASY-WORDS'] = "Easy words";
$GLOBALS['EN-HOME-GAME-THEMES'] = "Game themes";

$GLOBALS['EN-HOME-PLAY-BUTTON'] = "pics/jdm-logo-play.png";

$GLOBALS['EN-HOME-MEAN-GAIN'] = "Your average income is <0> creditos";
$GLOBALS['EN-HOME-GAMES-TOKENS'] = "You have <0> pending games and <1> tokens.";

//------

$GLOBALS['EN-SOUK-TITLE'] = "JeuxDeMots: the Souk";
$GLOBALS['EN-SOUK-PROMPT'] = "The Souk of thousand wonders: ";

$GLOBALS['EN-SOUK-YOUR-HUMOR'] = "Your humor:";
$GLOBALS['EN-SOUK-SHARE-HUMOR'] = "Share";
$GLOBALS['EN-SOUK-SHARED-HUMOR'] = "Shared humor!";

$GLOBALS['EN-SOUK-PREFERRED-THEME'] = "Your favorite theme:";
$GLOBALS['EN-SOUK-THEME-VALIDATE'] = "Validate";
$GLOBALS['EN-SOUK-VALIDATED-THEME'] = "Theme validated";
$GLOBALS['EN-SOUK-EMPTY-FIELD-THEME'] = "(empty field for deleting the theme)";
$GLOBALS['EN-SOUK-NO-PARTICULAR-THEME'] = "You do not have a particular theme.";
$GLOBALS['EN-SOUK-NO-EXISTING-THEME'] = "The theme does not exist, try something else !";

$GLOBALS['EN-SOUK-PRESENTS'] = "Presents";

$GLOBALS['EN-SOUK-SUBTITLE-COMP'] = "Buying competences";
$GLOBALS['EN-SOUK-SUBTITLE-TREAS'] = "Managing treasures";
$GLOBALS['EN-SOUK-SUBTITLE-TRIALS'] = "the trials";

$GLOBALS['EN-SOUK-SMALL-GAMES'] = 'Small games';
$GLOBALS['EN-SOUK-TRIALS'] = 'Trials';
$GLOBALS['EN-SOUK-DUELS'] = 'Duels';
$GLOBALS['EN-SOUK-VARIOUS'] = 'Various'; 

$GLOBALS['EN-SOUK-DEFIS-NOTERM'] = "The word '<0>' doesn't exist!";
$GLOBALS['EN-SOUK-DEFIS-NOTFREE'] = "The word <0> is still free !";
$GLOBALS['EN-SOUK-DEFIS-NOGUEST'] = "The guest player cannot capture a word";
$GLOBALS['EN-SOUK-DEFIS-NOTENOUGHMONEY'] = "Your investment should be at least 5 times the word value (<1>)";
$GLOBALS['EN-SOUK-DEFIS-NOMONEY'] = "You do not have enough credits";
$GLOBALS['EN-SOUK-DEFIS-NOTNOW'] = "You cannot capture this word now, please try again later";

$GLOBALS['EN-SOUK-COMP-ITEM'] = "Competence";
$GLOBALS['EN-SOUK-COMP-HONNOR-MIN'] = "Min Honnor";
$GLOBALS['EN-SOUK-COMP-CREDIT'] = "Credits";
$GLOBALS['EN-SOUK-COMP-STATUS'] = "Status";
$GLOBALS['EN-SOUK-COMP-QUOT'] = "Quotation";
$GLOBALS['EN-SOUK-COMP-SELECPC'] = "Selection %";
$GLOBALS['EN-SOUK-COMP-ADJUSTPC'] = "Adjust %";

$GLOBALS['EN-SOUK-COMP-EQUIPROBABILIZE'] = "Equiprobabilize";
$GLOBALS['EN-SOUK-COMP-ALL-COMPETENCES'] = "(5k Cr) all the competences.";


$GLOBALS['EN-SOUK-NEW-IMAGE'] = "pics/new.gif";

$GLOBALS['EN-SOUK-LIST-TRIAL-COMPLETED-FORM'] = "<li><form id=\"open_cr_trial<0>\" name=\"open_trial<0>\" method=\"post\" action=\"trialVote.php\" >
				<input <1> id=\"trial_vote_cr\" type=\"submit\" name=\"trial_vote_cr\" value=\"Show\">
				<input  id=\"proc_id\" type=\"hidden\" name=\"proc_id\" value=\"<2>\">
		 		trial <2> between '<3>' and '<4>' for term '<5>'.
				</form></li>";

$GLOBALS['EN-SOUK-LIST-TRIAL-TODO-FORM'] ="<li><form id=\"open_trial<0>\" name=\"open_trial<0>\" method=\"post\" action=\"trialVote.php\" >
		<input id=\"trial_vote\" type=\"submit\" name=\"trial_vote\" value=\"Vote\">
		<input  id=\"proc_id\" type=\"hidden\" name=\"proc_id\" value=\"<1>\">
		 - due in <2> hours.
		</form></li>";

$GLOBALS['EN-MAKE-TRIAL-FORM'] = "<form id=\"form-trial\" name=\"form-trial\" method=\"post\" action=\"generateResult_maketrial_process.php\" >
			Make a <input <0> id=\"submit-buy-trial\" type=\"submit\" name=\"submit-buy-trial\" value=\"trial\"> 
				 (500 Cr) to this player
			 <input id=\"trial_id\" type=\"hidden\" name=\"trial_id\" value=\"<1>\">
			 - indicate the reason :
			 <textarea name=\"trial_reason\" cols=40 rows=6></textarea>
			 </form>";
			 
$GLOBALS['EN-RESULT-MAKE-TRIAL'] = "Make a trial";
$GLOBALS['EN-RESULT-TRIAL'] = "trial";

			 
//------ Le souk by MM
// global warnings
$GLOBALS['EN-SOUK-WARNING-BADPLAYER'] = "This player does not exist";
$GLOBALS['EN-SOUK-WARNING-BADTERM'] = "This word does not exist";
$GLOBALS['EN-SOUK-WARNING-NOCASH'] = "You don't have enough credits";
$GLOBALS['EN-SOUK-WARNING-NOHONNOR'] = "You don't have enough honnor";
$GLOBALS['EN-SOUK-WARNING-NOGUEST'] = "The guest player cannot do that. Register !";
$GLOBALS['EN-SOUK-WARNING-NOCREDIT'] = "We do not offer credit facilities !";

//"le joueur '$player' a offert un cadeau au joueur '$dest'."
$GLOBALS['EN-SOUK-GIFT-EVENT'] = "player '<0>' made a gift to '<1>'.";
$GLOBALS['EN-SOUK-BUYCOMP-EVENT'] = "player '<0>' bought a competency.";
$GLOBALS['EN-SOUK-HYPEWORDS'] = "hype words";
$GLOBALS['EN-SOUK-INWORDS'] = "in words";
$GLOBALS['EN-SOUK-OUTWORDS'] = "out words";

// unused?
$GLOBALS['EN-SOUK-HYPEWORDS-VIEW-BUTTON'] = "View";
$GLOBALS['EN-SOUK-HYPEWORDS-VIEW-CREDITS'] = "Cr";
$GLOBALS['EN-SOUK-HYPEWORDS-VIEW-CONTENT'] = "hype words.";

$GLOBALS['EN-SOUK-HYPEWORDS-VIEW-FORM'] = "<input id=\"formhypewordsumit\" type=\"submit\" name=\"formhypewordsumit\" value=\"View\"> (10 Cr)
		hype words.";

$GLOBALS['EN-SOUK-CAGNOTTE-VIEW-FORM'] = "	<input id=\"cagnotesubmit\" type=\"submit\" name=\"cagnotesubmit\" value=\"View\"> (5 Cr)
		the amount of the lottery.";

$GLOBALS['EN-SOUK-CAGNOTTE-DISPLAY-WARNING'] = "The lottery contains <0> credits. 
		    Players <1> and <2> are ahead with a score of <3> credits. 
		    The lottery will be distributed in <4> jackpot(s)"; 

$GLOBALS['EN-SOUK-GAME-GIFT-FORM'] = "<input id=\"formmakegift\" type=\"submit\" name=\"formmakegift\" value=\"Offer\"> (100 Cr)
		a game with the term 
		<input id=\"giftterm\" type=\"text\" name=\"giftterm\" value=\"<0>\">
		to the player
		<input id=\"giftplayername\" type=\"text\" name=\"giftplayername\" value=\"<1>\"> 
		<input id=\"relation_type_gift\" type=\"hidden\" name=\"relation_type_gift\" value=\"0\">";


$GLOBALS['EN-SOUK-GAME-GIFT-WARNING-NOINPUT'] = "Enter a term and a player name";
$GLOBALS['EN-SOUK-GAME-GIFT-WARNING-SELFGIFT'] = "It would be nice but you can't offer gifts to yourself";
$GLOBALS['EN-SOUK-GAME-GIFT-WARNING-NOERROR'] = "Gift bought and sent";

$GLOBALS['EN-SOUK-GAME-GIFT-LIST'] = "Waiting gifts :";
$GLOBALS['EN-SOUK-GAME-GIFT-LIST-EMPTY'] = "Nobody offered you a gift";

$GLOBALS['EN-SOUK-GAME-GIFT-OPEN-FORM'] = "<input id=\"opengiftbut\" type=\"submit\" name=\"opengiftbut\" value=\"Open\">
		 a gift offered by <0> on <1>";

$GLOBALS['EN-SOUK-GRAPH-VIEW-FORM'] = " <input id=\"opengiftbut\" type=\"submit\" name=\"opengiftbut\" value=\"View\">
       (0 Cr) the graph.";
$GLOBALS['EN-SOUK-TRIAL-VIEW-FORM'] = " <input id=\"viewtrial\" type=\"submit\" name=\"viewtrial\" value=\"View\"> (0 Cr) the trials.";
$GLOBALS['EN-SOUK-GIFT-VIEW-FORM'] = " <input id=\"viewgift\" type=\"submit\" name=\"viewgift\" value=\"View\"> (0 Cr) my presents.";

$GLOBALS['EN-SOUK-WORD-LOOKUP-FORM'] = "  <input id=\"gotermlist\" type=\"submit\" name=\"gotermlist\" value=\"Lookup\">
       (0 Cr) words containing the string 
	    <input  id=\"goterm\" type=\"text\" name=\"goterm\" value=\"<0>\">";
	    
$GLOBALS['EN-SOUK-WORDLIST-DISPLAY-WARNING-NOINPUT'] = "Enter a non empty word";

$GLOBALS['EN-SOUK-PARRAINAGE-FORM'] = "	  <input id=\"parainage_submit\" type=\"submit\" name=\"parainage_submit\" value=\"Sponsor\">
      (0 Cr) a player with the following email address:
	  <input  size=\"50\" id=\"parainage_email\" type=\"text\" name=\"parainage_email\" value=\"john@doe.swh\">
	  ";

$GLOBALS['EN-SOUK-PARRAINAGE-WARNING-NOERROR'] = "Invitation sent (email address not checked)";
$GLOBALS['EN-SOUK-PARRAINAGE-WARNING-WRONGEMAIL'] = "Try with another email address...";

$GLOBALS['EN-SOUK-GENERATE-DEFIS-FORM'] = "<input id=\"parainage_submit\" type=\"submit\" name=\"defis_submit\" value=\"Capture\"> the word
	  <input  size=\"20\" id=\"parainage_email\" type=\"text\" name=\"defis_word\" value=\"word\">
	for an amount of 
	<input  size=\"20\" id=\"parainage_email\" type=\"text\" name=\"defis_amount\" value=\"1000\">
	credits.";

$GLOBALS['EN-SOUK-WORD-CAPTURE-FORM'] = "<font color=\"red\">ready for capture</font> 
<input id=\"chosengamesubmit\" type=\"submit\" name=\"chosengamesubmit\" value=\"Go !\">
		";

$GLOBALS['EN-SOUK-WORD-CAPTURE-EVENT'] = "The player &#39;<0>&#39; is trying to capture the word &#39;<1>&#39;.";

$GLOBALS['EN-SOUK-CHECK-TREASURE-FORM'] = 	"<input id=\"gochecktreasuressubmit\" type=\"submit\" name=\"gochecktreasuressubmit\" value=\"Display\">
	the treasures of the player
	<input  id=\"gochecktreasures\" type=\"text\" name=\"gochecktreasures\" value=\"\">";

$GLOBALS['EN-SOUK-BUYCOMP-SUBMIT'] = "Buy";
$GLOBALS['EN-SOUK-BUYCOMP-DONE'] = "bought";

$GLOBALS['EN-SOUK-WORD-RELATION-FORM'] = " <input id=\"gotermsubmit\" type=\"submit\" name=\"gotermsubmit\" value=\"Lookup\"> (DEBUG) the word
	    <input  id=\"gotermrel\" type=\"text\" name=\"gotermrel\" value=\"\">";

$GLOBALS['EN-SOUK-GAMELIST-FORM'] = "<input id=\"gamelist_submit\" type=\"submit\" name=\"gamelist_submit\" value=\"ongoing games\"> (DEBUG)";

$GLOBALS['EN-SOUK-CHOSENGAME-FORM'] = "<input id=\"chosengamesubmit\" type=\"submit\" name=\"chosengamesubmit\" value=\"Play\">";

$GLOBALS['EN-SOUK-STATISTICS-FORM'] = "<input id=\"bd_stats_submit\" type=\"submit\" name=\"bd_stats_submit\" value=\"Statistics\"> (DEBUG)";

$GLOBALS['EN-SOUK-CHECKOWNER-FORM'] = "	<input id=\"gotermownersubmit\" type=\"submit\" name=\"gotermownersubmit\" value=\"Check\"> the status of the word
	<input  id=\"gotermowner\" type=\"text\" name=\"gotermowner\" value=\"\">";


$GLOBALS['EN-SOUK-PROB-WARNING-NOERROR'] = "Probability adjusted";
$GLOBALS['EN-SOUK-COMP-WARNING-NOERROR'] = "Relation bought";

$GLOBALS['EN-SOUK-WARNING-CANT-GIFT'] = "Vous ne pouvez pas donner ce cadeau pour l'instant.";
$GLOBALS['EN-SOUK-BUY-TRIAL-TOKEN-NOCREDIT'] = "Désolé, vous n'avez pas cette somme de <0> crédits";
$GLOBALS['EN-SOUK-BUY-TRIAL-TOKEN-CONFIRM'] = "Jeton de procès acheté";

$GLOBALS['EN-SOUK-BUY-TRIAL-TOKEN-STATUS'] = "Vous avez déja <0> jetons de procès. Vous pouvez en <input <1> id=\"submit_buy_token\" type=\"submit\" name=\"submit_buy_token\" value=\"acheter\"> un de plus pour <2> crédits.";
$GLOBALS['EN-SOUK-RELATIONS-OCCURS'] = "<0> occurrences de relations <1> (<2> - <3>)";
$GLOBALS['EN-SOUK-NODES-OCCURS'] = "<0> occurrences de noeuds <1> (<2>)";

$GLOBALS['EN-SOUK-ARTEFACTS-TITLE'] = "Artefacts";
$GLOBALS['EN-SOUK-PLAY-LOTTERY'] = "\n<input  type=\"submit\" name=\"submit\" value=\"Jouer à la loterie\"> (<0> Cr)";
$GLOBALS['EN-SOUK-CHOSE-EASY'] = "\n<input  type=\"submit\" name=\"submit\" value=\"Mots à choisir\"> facile (gratuit !)";
$GLOBALS['EN-SOUK-CHOSE-HARD'] = "\n<input  type=\"submit\" name=\"submit\" value=\"Mots à choisir\"> dur (gratuit !)";
$GLOBALS['EN-SOUK-CHOSE-THEME'] = "\n<input  type=\"submit\" name=\"submit\" value=\"Mots thématique\"> variable (gratuit !)";
$GLOBALS['EN-SOUK-CRAZY-QUESTION'] = "\n<input  type=\"submit\" name=\"submit\" value=\"Questions farfelues\"> (gratuit !)";

//------

$GLOBALS['EN-EVENT-TITLE'] = "JeuxDeMots: events";
$GLOBALS['EN-EVENT-PROMPT'] = "Events";

// "les joueurs '$gamecreatorname' et '$playername' ont gagné <b>$points</b> crédits avec <b>'$fentry'</b>  pour la compétence '$relgpname'. Jackpot !"
$GLOBALS['EN-EVENT-WIN-JACKPOT'] = "players '<0>' and '<1>' have won <b><2></b> credits with <b>'<3>'</b> 
for the relation '<4>'. Jackpot !";

// "les joueurs '$gamecreatorname' et '$playername' ont gagné <b>$points</b> crédits avec <b>'$fentry'</b>."
$GLOBALS['EN-EVENT-WIN'] = "players '<0>' and '<1>' have won <b><2></b> credits with <b>'<3>'</b>.";

$GLOBALS['EN-EVENT-JACKPOT-TMPL'] = "Jackpot !";
$GLOBALS['EN-EVENT-TRIALWON-TMPL'] = "won a trial";
$GLOBALS['EN-EVENT-TRIALNO-TMPL'] = "non-lieu for the";
$GLOBALS['EN-EVENT-NEWPLAYER-TMPL'] = ", welcome to ";
$GLOBALS['EN-EVENTS-CAP'] = "<0> helped reaching a milestone of <1> relations. Bravo ! + <2> H";
$GLOBALS['EN-EVENT-CAP-TMPL'] = "helped reaching a milestone of";

//------

$GLOBALS['EN-TRIALVOTE-INSTRUCTIONS'] = "Seeing the answers, did the player follow the rule?";
$GLOBALS['EN-TRIALVOTE-TITLE'] = "vote for a trial";
$GLOBALS['EN-TRIALVOTE-THANKS'] = "thanks for your vote";
$GLOBALS['EN-TRIALVOTE-THANKSABS'] = "thanks for your abstention";
$GLOBALS['EN-TRIALVOTE-INSTRWERE'] = "The rule was :";

$GLOBALS['EN-TRIALVOTE-MAIL-TITLE'] = "Vous êtes juré d'un procès";
$GLOBALS['EN-TRIALVOTE-MAIL-BODY'] = "Bonjour <0>, 
				
Un procès a lieu et on demande votre expertise, allez sur votre page d'accueil...

";
$GLOBALS['EN-TRIALVOTE-PENDING-TRIAL'] = "Les procès qui vous attendent :";
$GLOBALS['EN-TRIALVOTE-NO-TRIAL'] = "Vous n'êtes juré d'aucun procès.";
$GLOBALS['EN-TRIALVOTE-50-LAST'] = "Les 50 derniers procès terminés :";
$GLOBALS['EN-TRIALVOTE-NO-GUEST'] = " non accessibles aux invités";
$GLOBALS['EN-TRIALVOTE-NO-TRIAL'] = "Aucun procès";
$GLOBALS['EN-TRIALVOTE-JUDGE'] = "le Juge";
$GLOBALS['EN-TRIALVOTE-JUDGE-GIFT'] = "le Juge a offert un cadeau à ";
$GLOBALS['EN-TRIALVOTE-EVENT'] = "le joueur '<0>' a gagné un procès face à '<1>' pour le mot '<2>'.";
$GLOBALS['EN-TRIALVOTE-WIN-TITLE'] = "Procès gagné";
$GLOBALS['EN-TRIALVOTE-WIN-BODY'] = "Bonjour <0>\nnous avons le plaisir de vous annoncer que vous avez gagné un procès (num <1>) face à '<2>' pour le mot '<3>'. \nVisitez le souk pour voir les votes liés à ce procès.\n";
$GLOBALS['EN-TRIALVOTE-LOSE-TITLE'] = "Procès perdu";
$GLOBALS['EN-TRIALVOTE-LOSE-BODY'] = "Bonjour <0>\nnous avons le regret de vous annoncer que vous avez perdu un procès (num <1>) face à '<2>' pour le mot '<3>'. \nVisitez le souk pour voir les votes liés à ce procès.\n";
$GLOBALS['EN-TRIALVOTE-REASON'] = "Motif du procès :";
$GLOBALS['EN-TRIALVOTE-ANSWERS'] = "Les réponses proposées par l'accusé ont été :";
$GLOBALS['EN-TRIALVOTE-DOUBT'] = "Relaxé au bénéfice du doute";
$GLOBALS['EN-TRIALVOTE-GUILTY'] = "Coupable";
$GLOBALS['EN-TRIALVOTE-INNOCENT'] = "Innocent";
$GLOBALS['EN-TRIALVOTE-ABSTAIN'] = "S'abstenir";
$GLOBALS['EN-TRIALVOTE-N-VOTES'] = "<0> voix";
$GLOBALS['EN-TRIALVOTE-INQUIRER'] = "Inquisiteur <b><0></b> versus accusé <b><1></b>";
$GLOBALS['EN-TRIALVOTE-REASON-SHORT'] = "Motif: <b><0></b><P>";
$GLOBALS['EN-TRIALVOTE-INSTR'] = "Instruction: <B><0></b><p>";
$GLOBALS['EN-TRIALVOTE-TERM'] = "Terme: <B><0></B><P>";



//------

$GLOBALS['EN-RANKING-TITLE'] = "JeuxDeMots: ranking";
$GLOBALS['EN-RANKING-PROMPT'] = "Ranking";
$GLOBALS['FR-RANKING-DISPLAY-SCORES'] = "scores";
$GLOBALS['FR-RANKING-DISPLAY-TRASURES'] = "treasures";

$GLOBALS['EN-RANKING-LABEL-NAME'] = "Name";
$GLOBALS['EN-RANKING-LABEL-HONNOR'] = "Honnor";
$GLOBALS['EN-RANKING-LABEL-HONNORMAX'] = "Honnor max";
$GLOBALS['EN-RANKING-LABEL-CREDITS'] = "Credits";
$GLOBALS['EN-RANKING-LABEL-NBPLAYED'] = "Played games";
$GLOBALS['EN-RANKING-LABEL-LEVEL'] = "Level";
$GLOBALS['EN-RANKING-LABEL-EFF'] = "Efficiency";
$GLOBALS['EN-RANKING-LABEL-NBTREASURE'] = "Treasures";

$GLOBALS['EN-RANKING-BEST-SCORES'] = "Best scores";
$GLOBALS['EN-RANKING-BEST-COUPLES'] = "Best couples";
$GLOBALS['EN-RANKING-BEST-FRIENDS'] = "Your best friends";

$GLOBALS['EN-RANKING-AND'] = " and ";
$GLOBALS['EN-RANKING-MINUTE'] = "mn";
$GLOBALS['EN-RANKING-15MINUTE'] = "15mn";
$GLOBALS['EN-RANKING-30MINUTE'] = "30mn";
$GLOBALS['EN-RANKING-HOUR'] = "h";
$GLOBALS['EN-RANKING-DAY'] = "d";
$GLOBALS['EN-RANKING-MONTH'] = "m";
$GLOBALS['EN-RANKING-YEAR'] = " year";
$GLOBALS['EN-RANKING-CREDITS'] = "Cr";

$GLOBALS['EN-RANKING-DISPLAY-DAY'] = "day";
$GLOBALS['EN-RANKING-DISPLAY-DAYS'] = "days";
$GLOBALS['EN-RANKING-DISPLAY-DAY-S'] = "day(s)";
$GLOBALS['EN-RANKING-DISPLAY-WEEK'] = "week";
$GLOBALS['EN-RANKING-DISPLAY-MONTH'] = "month";
$GLOBALS['EN-RANKING-DISPLAY-MONTHS'] = "months";
$GLOBALS['EN-RANKING-DISPLAY-YEAR'] = "year";

 
 $GLOBALS['EN-RANKING-DISPLAY-TITLE-ALL-PLAYERS'] = "All the players";
 $GLOBALS['EN-RANKING-DISPLAY-TITLE-ONLY-NEIGHBOURS'] = "Only the neighbours";
 $GLOBALS['EN-RANKING-DISPLAY-TITLE-PLAYERS'] = "The players";
 $GLOBALS['EN-RANKING-DISPLAY-TITLE-NEIGHBOURS'] = "The neighbours";
 $GLOBALS['EN-RANKING-DISPLAY-TITLE-FRIENDS'] = "The friends";
 $GLOBALS['EN-RANKING-DISPLAY-TITLE'] = "<0> active since <1> day(s) are displayed... ";

 $GLOBALS['EN-RANKING-DISPLAY-MENU'] = "Display all the active players since ";
 $GLOBALS['EN-RANKING-DISPLAY-ONLY-NEIGHBOURS'] = "Display only the neighbours";
 
  $GLOBALS['FR-RANKING-DISPLAY-START'] = "Display the ";

 $GLOBALS['FR-RANKING-DISPLAY-PLAYERS'] = "players";
 $GLOBALS['FR-RANKING-DISPLAY-NEIGHBOURS'] = "neighbours";
 $GLOBALS['FR-RANKING-DISPLAY-FRIENDS'] = "friends";

 $GLOBALS['FR-RANKING-DISPLAY-ACTIVE-SINCE'] = " active since ";

//------

$GLOBALS['EN-MUSEUM-TITLE'] = "JeuxDeMots: small museum";
$GLOBALS['EN-MUSEUM-PROMPT'] = "The small museum of <0>";
$GLOBALS['EN-MUSEUM-WARNING-NOTREASURE'] = "This player does not have any treasure.";

//------ relations informations

$GLOBALS['EN-SIGNIN-YOURINFO'] = "Your country";

$GLOBALS['EN-RELINFO-1'] = "Brasil";
$GLOBALS['EN-RELINFO-2'] = "Portugal";
$GLOBALS['EN-RELINFO-3'] = "Angola";
$GLOBALS['EN-RELINFO-4'] = "Cabo Verde";
$GLOBALS['EN-RELINFO-5'] = "Guiné-Bissau";
$GLOBALS['EN-RELINFO-6'] = "Moambique";
$GLOBALS['EN-RELINFO-7'] = "So Tomé e Prncipe";
$GLOBALS['EN-RELINFO-8'] = "Timor Leste";
$GLOBALS['EN-RELINFO-9'] = "Macau";
$GLOBALS['EN-RELINFO-10'] = "Outro";

//------

$GLOBALS['EN-RESULT-TITLE'] = "JeuxDeMots : result";
$GLOBALS['EN-RESULT-PG-PROMPT'] = "<P>Answars given by you (<0>) for this game (<1>) : ";
$GLOBALS['EN-RESULT-NOTHING'] =  " --empty-- ";
$GLOBALS['EN-RESULT-PROMPT'] = "Result";

$GLOBALS['EN-RESULT-NOPROP'] = "<h2>You have proposed nothing... I am sure you can do it:)</h2>";

$GLOBALS['EN-RESULT-NOGUEST'] = "Not available to guests";

$GLOBALS['EN-RESULT-PROMPT-BUYPLAY-HEADER'] = "You are frustrated by the stupid answers of your partner,
yours being certainly much more appropriate. You can:";
$GLOBALS['EN-RESULT-PROMPT-BUYPLAY-FORM'] = "<BR>Buy this game and propose it to other players";
$GLOBALS['EN-RESULT-BUY-TOKEN'] = "Buying tokens";
$GLOBALS['EN-RESULT-TOKEN'] = "tokens";
$GLOBALS['EN-RESULT-OR'] = "or";

$GLOBALS['EN-RESULT-PROMP-INVOKEWORD-FORM'] ="<P><font color=\"red\"> Jackpot !</font>
		<BR>You are performing well. As a reward, you can make a word to be proposed to other players more often.";
$GLOBALS['EN-RESULT-INVOKE'] = "Invoke";
$GLOBALS['EN-RESULT-THETERM'] = "the word";

$GLOBALS['EN-RESULT-PROMP-ADDFRIEND-FORM'] ="<P><font color=\"red\"> Jackpot!</font>
		<BR>You are gifted. To reward you, we invite you to add a friend.";
$GLOBALS['EN-RESULT-ADD'] = "Add";
$GLOBALS['EN-RESULT-ASFRIEND'] = "as a friend";

$GLOBALS['EN-RESULT-PROMPT-YOURANSWERS'] = "Your answers (<0>) for this game (<1>) : ";
$GLOBALS['EN-RESULT-PROMPT-HISANSWERS'] = "Partner's answers (<0>) for this game (<1>) : ";
$GLOBALS['EN-RESULT-PROMPT-INTERSECTION'] = "Intersection: ";

$GLOBALS['EN-RESULT-PROMPT-CREATED-PLAY'] = "<h2> Your game will be proposed to other players. You will be notified by email.</h2>";

$GLOBALS['EN-RESULT-BUYTOKEN-FORM'] ="If you are happy with your answers, you can propose them to more players.<P>Buy 
		<input id=\"submit5\" type=\"submit\" name=\"submit5\" value=\"5 tokens\"> (750 Cr) or 
		<input id=\"submit10\" type=\"submit\" name=\"submit10\" value=\"10 tokens\"> (1500 Cr)";

$GLOBALS['EN-RESULT-BUY-TRIAL-TOKEN-FORM'] = "<P>You can invest in a trial token.<br/>Buy 
		<input id=\"submit1\" type=\"submit\" name=\"submit1\" value=\"1 token\"> (200 Cr)";

$GLOBALS['EN-RESULT-PROMPT-GAIN'] = "<h2>You gain <0> credits and <1> honnor point(s) </h2>";

$GLOBALS['EN-RESULT-PROMPT-LEVEL-GAIN'] = "<br>You and your partner are winning <0> level points.";
$GLOBALS['EN-RESULT-PROMPT-LEVEL-LOOSE'] = "<br>You and your partner are loosing <0> level points.";

$GLOBALS['EN-RESULT-PROMPT-TERMLEVEL-GAIN'] = "<br>The word <0> gains <1> level points.";
$GLOBALS['EN-RESULT-PROMPT-TERMLEVEL-LOOSE'] = "<br>The word <0> loses <1> level points.";

$GLOBALS['EN-RESULT-MORE-HONNOR-POINTS'] = "<0> more honor points!";


//------
$GLOBALS['EN-BUYPLAY-CONGRAT-INVEST'] ="<h1>Congratulation for this investment of <0> tokens
 for an amount of <1> credits.</h1>";
$GLOBALS['EN-BUYPLAY-CHEAT'] = "<h1>Sorry a guest player cannot rebuy a game. 
    	If you read this, it means that the session exprired</h1>";
$GLOBALS['EN-BUYPLAY-EVENT'] = "player '<0>' rebought a game for <b>'<1>'</b>.";
//------

$GLOBALS['EN-BUYTOKEN-CONGRAT-INVEST'] = "<h1>Congratulation for this investment in <0> tokens
 for an amount of <1> credits.</h1>";
$GLOBALS['EN-BUYTOKEN-CHEAT'] = "<h1>Sorry a guest player cannot invest in tokens. 
    	If you read this, it means that the session expired</h1>";
$GLOBALS['EN-BUYTOKEN-SORRY'] = "<h1>Sorry, you do not have <0> credits, you should have checked before :)</h1>";

// "le joueur '$login' a investi dans le mot <b>'$fentry'</b>."
$GLOBALS['EN-BUYTOKEN-EVENT'] = "player '<0>' invested in <b>'<1>'</b>.";

//------
$GLOBALS['EN-BUY-TRIAL-TOKEN-CONGRAT-INVEST'] = "Congratulations for this investment in <0> trial tokens for <1> credits.";
$GLOBALS['EN-BUY-TRIAL-TOKEN-CHEAT'] = "Sorry, the guest player cannot buy tokens. 
    	If you read this, it means that the session might have expired.";
$GLOBALS['EN-BUY-TRIAL-TOKEN-SORRY'] = "Sorry, you do not have this amount of <0> credits, you should have checked before :)";
$GLOBALS['EN-BUY-TRIAL-TOKEN-EVENT'] = "the player &#39;<0>&#39;bought a trial token.";


//------
$GLOBALS['EN-BUYWISH-CONGRAT-INVEST'] = "<h1>Congratulation for invoking '<0>' for an amount of <1> credits. It may appear one day...</h1>";
$GLOBALS['EN-BUYWISH-CHEAT'] = "<h1>Sorry a guest player cannot invoke a word. 
    	If you read this, it means that the session might have expired.</h1>";
$GLOBALS['EN-BUYWISH-SORRY'] = "<h1>Sorry, you do not have <0> credits, you should have checked before :)</h1>";

// "le joueur '$login' a investi dans le mot <b>'$fentry'</b>."
$GLOBALS['EN-BUYWISH-EVENT'] = "player '<0>' invoked a word...";
$GLOBALS['EN-BUYWISH-EMPTY-WORD'] = "empty word, too bad !";

$GLOBALS['EN-ADDFRIEND-CONGRAT-INVEST'] = "Congratulations, you have '<0>' as a new friend.";
$GLOBALS['EN-ADDFRIEND-ERROR'] = "This player does not exist.";
$GLOBALS['EN-ADDFRIEND-CHEAT'] = "<h1>Sorry, the guest player cannot add friends. 
    	If you read this, it means that the session might have expired.</h1>";
$GLOBALS['EN-ADDFRIEND-SORRY'] = "<h1>Sorry, you do not have <0> credits, you should have checked before :)</h1>";
$GLOBALS['EN-ADDFRIEND-EVENT'] = "the player &#39;<0>&#39; has a new friend...";
$GLOBALS['EN-ADDFRIEND-EMPTY-WORD'] = "friend empty, sorry!";


//------
$GLOBALS['EN-TRIAL-CONGRAT-INVEST'] = "<h1>Congratulation for this investment in <0> 
for an amount of <1> credits.";
$GLOBALS['EN-TRIAL-CHEAT'] = "<h1>Sorry a guest player cannot make a trial. 
    	If you read this, it means that the session might have expired</h1>";
$GLOBALS['EN-BUYTRIAL-SORRY'] = "<h1>Sorry, you do not have <0> credits, you should have checked before :)</h1>";

//------
$GLOBALS['EN-PLAYER-PERM-CAGNOTTE-BINGO'] = "Bingo! you drop the lottery plot of <0> credits.";
$GLOBALS['EN-PLAYER-PERM-CAGNOTTE-EVENT'] = "the players '<0>' and '<1>' won the lottery plot with an amount of <2> and share it... and make benefit others.";
$GLOBALS['EN-PLAYER-PERM-CAGNOTTE-MAIL'] = "Hello,\nthe lottery plot dropped and you win credits...\n";
$GLOBALS['EN-PLAYER-PERM-CAGNOTTE-MAIL-TITLE'] = "Credits...";

//------

$GLOBALS['EN-REQUEST-VERB-TRANSTIVE'] = "SELECT id FROM `Nodes` WHERE w > 50 
	AND id IN (SELECT node1 FROM Relations WHERE node2 
	IN (SELECT id FROM Nodes WHERE type=4 and name LIKE 'POS:verb')
	) order by w DESC LIMIT <0>";

$GLOBALS['EN-REQUEST-VERB-EASY'] = "SELECT id FROM `Nodes` WHERE w > 50 
	AND id IN (SELECT node1 FROM Relations WHERE node2 
	IN (SELECT id FROM Nodes WHERE type=4 and name LIKE 'POS:verb')
	) order by w DESC LIMIT <0>";

$GLOBALS['EN-REQUEST-NOUN-EASY'] = "SELECT id FROM `Nodes` WHERE w > 50 
	AND id IN (SELECT node1 FROM Relations WHERE node2 
	IN (SELECT id FROM Nodes WHERE type=4 and name LIKE 'POS:noun')
	) order by w DESC LIMIT <0>";

$GLOBALS['EN-REQUEST-NOUN-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'POS:noun'); ";

$GLOBALS['EN-REQUEST-VERB-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'POS:verb' and type=4); ";

$GLOBALS['EN-REQUEST-NOUN-ADJ-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'POS:adj' OR name LIKE 'POS:adv');";

$GLOBALS['EN-REQUEST-NOUN-ADJ-VER-ADV-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'POS:noun' OR name LIKE 'POS:verb' OR name LIKE 'POS:adj' OR name LIKE 'POS:adv');";

$GLOBALS['EN-REQUEST-NOUN-VER-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE  type=4 and name LIKE 'POS:noun' OR name LIKE 'POS:verb');";


//------
// "le joueur '$player_name' fait de la magie noire avec le mot '$fentry'."
$GLOBALS['EN-BLACK-MAGIC-EVENT'] = "player '<0>' is making black magic with '<1>'.";
//"le joueur '$player_name' fait de la magie blanche avec le mot '$fentry'."
$GLOBALS['EN-WHITE-MAGIC-EVENT'] = "player '<0>' is making white magic with '<1>'.";

//------
$GLOBALS['EN-TGEN-CHOOSE-TERM'] = "Choose a term that you like:";

$GLOBALS['EN-RESULT-CHANGE'] = "Change";
$GLOBALS['EN-RESULT-YOUR-NICKNAME'] = " your nickname ";
$GLOBALS['EN-RESULT-CHANGE-YOUR-NICKNAME'] = "You are good! As a reward, you can change your nickname.";

$GLOBALS['EN-TERMCAPT-SHORT'] = "'<0>' vient d'être capturé.";
$GLOBALS['EN-TERMCAPT-LONG'] = "'<0>' (recherché pour <1> Cr) vient d'être capturé.";
$GLOBALS['EN-TERMOWN-SHORT'] = "'<0>' vient de changer de propriétaire.";
$GLOBALS['EN-TERMOWN-LONG'] = "'<0>' (recherché pour <1> Cr) vient ce changer de propriétaire.";
$GLOBALS['EN-TERMOWN-FREE'] = "le terme est libre";
$GLOBALS['EN-TERMOWN-NONFREE'] = "le terme est possédé par les joueurs '<0>' et '<1>'";

$GLOBALS['EN-NETWORK-TITLE'] = "The network";
$GLOBALS['EN-NETWORK-SEARCH-TERM'] = "<form id=\"gotermrel\" name=\"gotermrel\" method=\"post\" action=\"rezo.php\" >
	    <input id=\"gotermsubmit\" type=\"submit\" name=\"gotermsubmit\" value=\"Look up\"> the term
	    <input  id=\"gotermrel\" type=\"text\" name=\"gotermrel\" value=\"<0>\" size=100>
	    </form>";
$GLOBALS['EN-NETWORK-TERM-NON-EXIST'] = "<br>The term <0> is not in the network!";
$GLOBALS['EN-NETWORK-TERM-HEADER'] = "'<0>' (id=<1> => <2> ; level = <3> ; luminosity = <4>)";
$GLOBALS['EN-NETWORK-NUMBER-RELATIONS'] = "<0> relations ";


?>
