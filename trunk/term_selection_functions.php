<?php include_once 'misc_functions.php' ; ?>

<?php 
// debug
//
$_SESSION[ssig() . 'offlimits_thresh'] = 300;

// encours
function get_offlimits($termid, $relid) {
	start_time_record("get_offlimits");
    $_SESSION[ssig() . 'offlimits'] = array ();
    $_SESSION[ssig() . 'offlimits_size'] = array ();
    $th = $_SESSION[ssig() . 'offlimits_thresh'];
	$query = "SELECT node2, w FROM Relations WHERE type=$relid AND node1 = $termid and w >= $th ORDER BY 'w' DESC; " ;
	$r =  mysql_query($query) or die("pb in get_offlimits : $query");
	$nb = mysql_num_rows($r);
	
	for ($i=0 ; $i<$nb ; $i++) {
	    $termid= mysql_result($r , $i , 0);
	    $w= mysql_result($r , $i , 1);
	    array_push($_SESSION[ssig() . 'offlimits'], get_term_from_id($termid));
	    array_push($_SESSION[ssig() . 'offlimits_size'], min(5,max(1,round($w/300))));
	}
	
	debugecho("get_offlimits=" . end_time_record("get_offlimits"));
}


function formats_offlimits() {
    $nb = count($_SESSION[ssig() . 'offlimits']);
    for ($i=0 ; $i<$nb ; $i++) {
    	if ($i == 0) {
    		$tag2 = "";
    	} else {
    		$tag2 = " &bull; ";
    	}
    	$size = $_SESSION[ssig() . 'offlimits_size'][$i];
    	if ($size == 1) {$tagsize = "";}
    	if ($size == 2) {$tagsize = "size=\"+1\"";}
    	if ($size == 3) {$tagsize = "size=\"+2\"";}
  	 	if ($size == 4) {$tagsize = "size=\"+3\"";}
    	if ($size >= 4) {$tagsize = "size=\"+4\"";}
		if ($_SESSION[ssig() . 'candidate'] == $_SESSION[ssig() . 'offlimits'][$i]) {
	  	  echo "$tag2 
	  	  <font color=\"red\" $tagsize><div id=\"taboo-done\">" . $_SESSION[ssig() . 'offlimits'][$i] . "</div></font> ";
		} else {
		    echo "$tag2 
		    <font $tagsize>" . $_SESSION[ssig() . 'offlimits'][$i] . "</font> ";
		}
    }
    echo "<SCRIPT TYPE=\"text/javascript\">";
    echo "$('taboo-done').shake(5);";
    echo "</SCRIPT>";
}


$_SESSION[ssig() . 'opp_rel'][0]=0;$_SESSION[ssig() . 'opp_rel'][3]=3;$_SESSION[ssig() . 'opp_rel'][5]=5;$_SESSION[ssig() . 'opp_rel'][7]=7;
$_SESSION[ssig() . 'opp_rel'][22]=22;

$_SESSION[ssig() . 'opp_rel'][6]=8;$_SESSION[ssig() . 'opp_rel'][8]=6;
$_SESSION[ssig() . 'opp_rel'][9]=10;$_SESSION[ssig() . 'opp_rel'][10]=9;
$_SESSION[ssig() . 'opp_rel'][15]=28;$_SESSION[ssig() . 'opp_rel'][28]=15;
$_SESSION[ssig() . 'opp_rel'][17]=23;$_SESSION[ssig() . 'opp_rel'][23]=17;

function get_suggestions_old($termid, $relid) {
	
	start_time_record("get_suggestions");
    $_SESSION[ssig() . 'suggestions'] = array ();
    return;
    
    
    $th = $_SESSION[ssig() . 'offlimits_thresh'];
    $oprelid=$_SESSION[ssig() . 'opp_rel'][$relid]+0;
    
	$query = "SELECT name FROM Nodes WHERE 
		type = 1 and
		name NOT LIKE \"::%\"
		and id IN (SELECT node1 FROM Relations WHERE type=$oprelid AND node2 = $termid and w >= 60) 
		and id NOT IN (SELECT node2 FROM Relations WHERE type=$relid AND node1 = $termid) 
		ORDER BY Rand() LIMIT 7; " ;
	
	debugecho("get_suggestions query=" . $query);
	
	$r =  mysql_query($query) or die("pb in get_suggestions : $query");
	$nb = mysql_num_rows($r);
	
	for ($i=0 ; $i<$nb ; $i++) {
	    $term = mysql_result($r , $i , 0);
	    array_push($_SESSION[ssig() . 'suggestions'], $term);
	}
	debugecho("get_suggestions=" . end_time_record("get_suggestions"));
}

function get_suggestions($termid, $relid) {
	
	start_time_record("get_suggestions");
	$_SESSION[ssig() . 'suggestions_Q1'] = array ();
	$_SESSION[ssig() . 'suggestions_Q2'] = array ();
    $_SESSION[ssig() . 'suggestions'] = array ();

    $th = $_SESSION[ssig() . 'offlimits_thresh'];
    $oprelid=$_SESSION[ssig() . 'opp_rel'][$relid]+0;
    
    $query1 = "SELECT node1 FROM Relations WHERE type=$oprelid AND node2 = $termid and w >= 60";
    $r1 =  mysql_query($query1) or die("pb in get_suggestions 1 : $query");
    $query2 = "SELECT node2 FROM Relations WHERE type=$relid AND node1 = $termid";
    $r2 =  mysql_query($query2) or die("pb in get_suggestions 2 : $query");
    
	for ($i=0 ; $i<mysql_num_rows($r1) ; $i++) {
	    $node1 = mysql_result($r1 , $i , 0);
	    array_push($_SESSION[ssig() . 'suggestions_Q1'], $node1);
	}
	for ($i=0 ; $i<mysql_num_rows($r2) ; $i++) {
	    $node1 = mysql_result($r2 , $i , 0);
	    array_push($_SESSION[ssig() . 'suggestions_Q2'], $node1);
	}
	
	$array_dif = array_diff(
				$_SESSION[ssig() . 'suggestions_Q1'], 
				$_SESSION[ssig() . 'suggestions_Q2']);
	
	for ($i=0 ; $i< count($array_dif) ; $i++) {
	    $id = $array_dif[$i];
	    if ($id != 0) {
	    	$term = get_term_from_id($id);
	    	if (stripos($term, "::") === 0) {
	    	
	    	} else {
	    		array_push($_SESSION[ssig() . 'suggestions'], $term);
	    	}
	    }   
	}
	
	debugecho("get_suggestions=" . end_time_record("get_suggestions"));
}


function formats_suggestions() {
    $nb = count($_SESSION[ssig() . 'suggestions']);
    if ($nb > 20) {
    	$mar1 = "<marquee BEHAVIOR=\"Alternate\" >";
    	$mar2 = "</marquee>";
    }
    for ($i=0 ; $i<$nb ; $i++) {
    if ($i == 0) {
    		$tag2 = "$mar1";
    	} else {
    		$tag2 = " &bull; ";
    	}
	if ($_SESSION[ssig() . 'candidate'] == $_SESSION[ssig() . 'suggestions'][$i]) {
	    echo " $tag2 
	    <font color=\"green\"><div id=\"suggestion-done\">" . $_SESSION[ssig() . 'suggestions'][$i] . "</div></font> ";
	} else {
	    echo " $tag2 
	    " . $_SESSION[ssig() . 'suggestions'][$i] . " ";
	}
    }
    echo $mar2;
    echo "<SCRIPT TYPE=\"text/javascript\">";
    echo "$('suggestion-done').beat(2,30);";
    echo "</SCRIPT>";
}


function selectFrequentWord() {
    start_time_record("selectFrequentWord");

    $playerlevel = get_player_level($_SESSION[ssig() . 'playerid']);
    $range = round($playerlevel * 30);

    
    $query = "SELECT id, name, level, annotation, gloss FROM (SELECT * FROM Nodes WHERE type=1 ORDER BY w DESC LIMIT $range) AS TAB; " ;
    //echo "<br>q=$query";
    //echo "<br>pl=$playerlevel";

    $r =  mysql_query($query) or die("pb in selectFrequentWord : $query");

    $id = select_rand_in_tab($r);
    
    $_SESSION[ssig() . 'gameentryid'] = mysql_result($r , $id , 0);
    $_SESSION[ssig() . 'gameentry'] = mysql_result($r , $id , 1);
    $_SESSION[ssig() . 'gameentrylevel'] = mysql_result($r , $id , 2);
    $_SESSION[ssig() . 'gameannotation'] = mysql_result($r , $id , 3);
    $_SESSION[ssig() . 'gamegloss'] = mysql_result($r , $id , 4);

    $_SESSION[ssig() . 'termcolor'] = "blue";

    $_SESSION[ssig() . 'termselectionmethod']="selectFrequentWord";
    end_time_record("selectFrequentWord");
}




function selectHypeWord() {
    //echo  "selectHypeWord";
    if (test_playerp($_SESSION[ssig() . 'login']) == "Y") {$_SESSION[ssig() . 'deb-message'] =  "<br>selectHypeWord" ; };

    $query = "SELECT name, token, relationType FROM `HypeWords` WHERE token > 0" ;
    //echo $query;
    $r =  @mysql_query($query) or die("Pb in selectHypeWord 1 : $query");
    $id = select_rand_in_tab($r);

    $hypename = mysql_result($r , $id , 0);
    $hypetoken = mysql_result($r , $id , 1);
	$rel = mysql_result($r , $id , 2);

    debugecho($hypename);
    debugecho($hypetoken);

    // on insert le mot
    // ca echoue s'il existe deja
    // on s'en fout, l'essentiel est qu'il soit dans la base
    //
    $query = "INSERT INTO Nodes (name, type, w, creationdate, level) VALUES (\"$hypename\" , 1 , 50, CURRENT_TIMESTAMP, 50)";
    //echo $query;
    $r =  @mysql_query($query);

    // on fetch le mot, il devrait y etre
    //
    $query = "SELECT id, name, level, annotation, gloss FROM `Nodes` WHERE name = \"$hypename\" ; " ;
    //echo $query;
    $r =  @mysql_query($query) or die ("Pb in selectHypeWord 2 : $query");

    if (mysql_num_rows($r) == 0) {
	echo "<P>BUG selectHypeWord : send a mail to ",$_SESSION[ssig().'CONTACT-EMAIL']," with the following data:
		    <BR>id: $id hypename: \"$hypename\" hypetoken: $hypetoken query: $query " ;
	break;
    }


    $_SESSION[ssig() . 'gameentryid'] = mysql_result($r , 0 , 0);
    $_SESSION[ssig() . 'gameentry'] = mysql_result($r , 0 , 1);
    $_SESSION[ssig() . 'gameentrylevel'] = mysql_result($r , 0 , 2);
    $_SESSION[ssig() . 'gameannotation'] = mysql_result($r , 0 , 3);
    $_SESSION[ssig() . 'gamegloss'] = mysql_result($r , 0 , 4);
    

    $query = "UPDATE `HypeWords` SET token = token-1 WHERE name= \"$hypename\";" ;
    $r =  @mysql_query($query) or die("Pb in selectHypeWord 3 : $query");

    $_SESSION[ssig() . 'termcolor'] = "lightgreen";
    $_SESSION[ssig() . 'termselectionmethod']="selectHypeWord";
}

function select_term_general() {
	if ($_SESSION[ssig() . 'playerid'] == 0) {
		selectRandomWord_common();
		return;
	}
	
    $query = "SELECT COUNT(*) FROM `HypeWords` WHERE token > 0" ;
    $r =  @mysql_query($query) or die("pb in select_term_general : $query");
    $nb = mysql_result($r , 0 , 0);
    
  
    $probHype = min(20, $nb);
    $probFrequent = max(20,min(60, $probHype + (95 - get_player_level($_SESSION[ssig() . 'playerid']))));

    $rand = rand(0,100);
    
    // for debug
    if ($_SESSION[ssig() . 'login']=="test") {$rand=25;}

    if ($rand <= $probHype) {
	selectHypeWord();
	} else { 
	if ($rand <= $probFrequent) {
	    selectFrequentWord();
	    } else {
	    selectRandomWord();
	    }
	}
}

function selectRandomWord() {
    start_time_record("selectRandomWord");
    $query = "SELECT id, name, level, annotation, gloss FROM `Nodes` WHERE type=1 ; " ;
    $r =  @mysql_query($query) or die("pb in selectRandomWord : $query");

    $id = select_rand_in_tab($r);

    $_SESSION[ssig() . 'gameentryid'] = mysql_result($r , $id , 0);
    $_SESSION[ssig() . 'gameentry'] = mysql_result($r , $id , 1);
    $_SESSION[ssig() . 'gameentrylevel'] = mysql_result($r , $id , 2);
    $_SESSION[ssig() . 'gameannotation'] = mysql_result($r ,$id , 3);
    $_SESSION[ssig() . 'gamegloss'] = mysql_result($r , $id , 4);
    $_SESSION[ssig() . 'termcolor'] = "orange";

    $_SESSION[ssig() . 'termselectionmethod']="selectRandomWord";
    end_time_record("selectRandomWord");

}

function selectRandomWord_common() {
    start_time_record("selectRandomWord_common");
    $query = "SELECT node1 FROM `Relations` WHERE type=36 and node2=191741;" ;
    $r =  @mysql_query($query) or die("pb in selectRandomWord_common : $query");
	$r_id = select_rand_in_tab($r);
	$node_id = mysql_result($r , $r_id , 0);
	
    $query = "SELECT id, name, level, annotation, gloss FROM `Nodes` WHERE id=$node_id; " ;
    $r =  @mysql_query($query) or die("pb in selectRandomWord_common : $query");
    
    $_SESSION[ssig() . 'gameentryid'] = mysql_result($r , 0 , 0);
    $_SESSION[ssig() . 'gameentry'] = mysql_result($r , 0 , 1);
    $_SESSION[ssig() . 'gameentrylevel'] = mysql_result($r , 0 , 2);
    $_SESSION[ssig() . 'gameannotation'] = mysql_result($r , 0 , 3);
    $_SESSION[ssig() . 'gamegloss'] = mysql_result($r , 0 , 4);
    $_SESSION[ssig() . 'termcolor'] = "orange";

    $_SESSION[ssig() . 'termselectionmethod']="selectRandomWord";
    end_time_record("selectRandomWord_common");

}

function select_random_word_N_V() {
    start_time_record("select_random_word_N_V");

 //   $query = "SELECT node1 FROM `Relations` where type=4 and node2 
//		IN (SELECT id FROM Nodes WHERE name LIKE 'Nom%' OR name LIKE 'Ver:Inf' OR name LIKE 'Adj%' OR name LIKE 'Adv%'); " ;
    
    $query = get_msg('REQUEST-NOUN-VER-RANDOM');
    $r =  @mysql_query($query) or die("pb in select_random_word_N_V : $query");

    $id = select_rand_in_tab($r);
    $nodeid = mysql_result($r , $id , 0);

    $query = "SELECT id, name, level, annotation, gloss FROM `Nodes` WHERE id =$nodeid ; " ;
    $r =  @mysql_query($query) or die("pb in select_random_word_N_V 2 : $query");

    $_SESSION[ssig() . 'gameentryid'] = mysql_result($r , 0 , 0);
    $_SESSION[ssig() . 'gameentry'] = mysql_result($r ,  0 , 1);
    $_SESSION[ssig() . 'gameentrylevel'] = mysql_result($r , 0 , 2);
    $_SESSION[ssig() . 'gameannotation'] = mysql_result($r , 0 , 3);
    $_SESSION[ssig() . 'gamegloss'] = mysql_result($r , 0 , 4);
    $_SESSION[ssig() . 'termcolor'] = "orange";

    
    $_SESSION[ssig() . 'termselectionmethod']="select_random_word_N_V";
    end_time_record("select_random_word_N_V");

}

function select_random_word_N_V_ADJ_ADV() {
    start_time_record("select_random_word_N_V_ADJ_ADV");

 //   $query = "SELECT node1 FROM `Relations` where type=4 and node2 
//		IN (SELECT id FROM Nodes WHERE name LIKE 'Nom%' OR name LIKE 'Ver:Inf' OR name LIKE 'Adj%' OR name LIKE 'Adv%'); " ;
    
    $query = get_msg('REQUEST-NOUN-ADJ-VER-ADV-RANDOM');
    $r =  @mysql_query($query) or die("pb in select_random_word_N_V_ADJ_ADV : $query");

    $id = select_rand_in_tab($r);
    $nodeid = mysql_result($r , $id , 0);

    $query = "SELECT id, name, level, annotation, gloss FROM `Nodes` WHERE id =$nodeid ; " ;
    $r =  @mysql_query($query) or die("pb in select_random_word_N_V_ADJ_ADV 2 : $query");

    $_SESSION[ssig() . 'gameentryid'] = mysql_result($r , 0 , 0);
    $_SESSION[ssig() . 'gameentry'] = mysql_result($r ,  0 , 1);
    $_SESSION[ssig() . 'gameentrylevel'] = mysql_result($r , 0 , 2);
    $_SESSION[ssig() . 'gameannotation'] = mysql_result($r , 0 , 3);
    $_SESSION[ssig() . 'gamegloss'] = mysql_result($r , 0 , 4);
    $_SESSION[ssig() . 'termcolor'] = "orange";

    
    $_SESSION[ssig() . 'termselectionmethod']="select_random_word_N_V_ADJ_ADV";
    end_time_record("select_random_word_N_V_ADJ_ADV");

}

function select_random_word_ADJ_ADV() {
    start_time_record("select_random_word_ADJ_ADV");

   // $query = "SELECT node1 FROM `Relations` where type=4 and node2 
	//	IN (SELECT id FROM Nodes WHERE name LIKE 'Adj%' OR name LIKE 'Adv%'); " ;
    
    $query = get_msg('REQUEST-NOUN-ADJ-RANDOM');
         
    $r =  @mysql_query($query) or die("pb in select_random_word_ADJ_ADV : $query");

    $id = select_rand_in_tab($r);
    $nodeid = mysql_result($r , $id , 0);

    $query = "SELECT id, name, level, annotation, gloss FROM `Nodes` WHERE id =$nodeid ; " ;
    $r =  @mysql_query($query) or die("pb in select_random_word_ADJ_ADV 2 : $query");

    $_SESSION[ssig() . 'gameentryid'] = mysql_result($r , 0 , 0);
    $_SESSION[ssig() . 'gameentry'] = mysql_result($r ,  0 , 1);
    $_SESSION[ssig() . 'gameentrylevel'] = mysql_result($r , 0 , 2);
    $_SESSION[ssig() . 'gameannotation'] = mysql_result($r , 0 , 3);
    $_SESSION[ssig() . 'gamegloss'] = mysql_result($r , 0 , 4);
    $_SESSION[ssig() . 'termcolor'] = "orange";

    
    $_SESSION[ssig() . 'termselectionmethod']="select_random_word_ADJ_ADV";
    end_time_record("select_random_word_ADJ_ADV");

}

function select_random_word_N() {
    start_time_record("select_random_word_N");

  //  $query = "SELECT node1 FROM `Relations` where type=4 and node2 
 //		IN (SELECT id FROM Nodes WHERE name LIKE 'N%'); " ;
    
    $query = get_msg('REQUEST-NOUN-RANDOM');
    $r =  @mysql_query($query) or die("pb in select_random_word_N : $query");

    $id = select_rand_in_tab($r);
    $nodeid = mysql_result($r , $id , 0);

    $query = "SELECT id, name, level, annotation, gloss FROM `Nodes` WHERE id =$nodeid ; " ;
    $r =  @mysql_query($query) or die("pb in select_random_word_N 2 : $query");

    $_SESSION[ssig() . 'gameentryid'] = mysql_result($r , 0 , 0);
    $_SESSION[ssig() . 'gameentry'] = mysql_result($r ,  0 , 1);
    $_SESSION[ssig() . 'gameentrylevel'] = mysql_result($r , 0 , 2);
    $_SESSION[ssig() . 'gameannotation'] = mysql_result($r , 0 , 3);
    $_SESSION[ssig() . 'gamegloss'] = mysql_result($r , 0 , 4);
    $_SESSION[ssig() . 'termcolor'] = "orange";

    
    $_SESSION[ssig() . 'termselectionmethod']="select_random_word_N";
    end_time_record("select_random_word_N");

}

function select_random_word_V() {
    start_time_record("select_random_word_V");

  //  $query = "SELECT node1 FROM `Relations` where type=4 and node2 
 //		IN (SELECT id FROM Nodes WHERE name LIKE 'N%'); " ;
    
    $query = get_msg('REQUEST-VERB-RANDOM');
    $r =  @mysql_query($query) or die("pb in select_random_word_V : $query");

    $id = select_rand_in_tab($r);
    $nodeid = mysql_result($r , $id , 0);

    $query = "SELECT id, name, level, annotation, gloss FROM `Nodes` WHERE id =$nodeid ; " ;
    $r =  @mysql_query($query) or die("pb in select_random_word_N 2 : $query");

    $_SESSION[ssig() . 'gameentryid'] = mysql_result($r , 0 , 0);
    $_SESSION[ssig() . 'gameentry'] = mysql_result($r ,  0 , 1);
    $_SESSION[ssig() . 'gameentrylevel'] = mysql_result($r , 0 , 2);
    $_SESSION[ssig() . 'gameannotation'] = mysql_result($r , 0 , 3);
    $_SESSION[ssig() . 'gamegloss'] = mysql_result($r , 0 , 4);
    $_SESSION[ssig() . 'termcolor'] = "orange";

    
    $_SESSION[ssig() . 'termselectionmethod']="select_random_word_V";
    end_time_record("select_random_word_V");

}


function select_easy_word_N() {
    start_time_record("select_easy_word_N");
    $range = 1000;

 //   $query = "SELECT id FROM `Nodes` WHERE w > 50 
//	AND id IN (SELECT node1 FROM Relations WHERE node2 
//	IN (SELECT id FROM Nodes WHERE type=4 and name LIKE 'N%')
//	) order by w DESC LIMIT $range" ;
    
    $GLOBALS[0]=$range;
    $query = get_msg('REQUEST-NOUN-EASY');
    // $query = "select distinct(node1) from Relations where type = 4 and node2 in (select id FROM Nodes where type=4 and name like 'Nom:%')";
    
    //$query = "select R.node1 from Relations as R, Nodes as N where R.type = 4
//and R.node2 in (146881, 146884, 146885, 146886, 146887, 146893, 146895, 147279, 148212, 148283, 149152, 150563, 161743, 161769, 171870) 
//and R.node1 = N.id and N.w > 50"; 
       
       
    $r =  @mysql_query($query) or die("pb in select_easy_word_N : $query");

    $id = select_rand_in_tab($r);
    $nodeid = mysql_result($r , $id , 0);

    $query = "SELECT id, name, level, annotation, gloss FROM `Nodes` WHERE id =$nodeid ; " ;
    $r =  @mysql_query($query) or die("pb in select_easy_word_N 2 : $query");

    $_SESSION[ssig() . 'gameentryid'] = mysql_result($r , 0 , 0);
    $_SESSION[ssig() . 'gameentry'] = mysql_result($r ,  0 , 1);
    $_SESSION[ssig() . 'gameentrylevel'] = mysql_result($r , 0 , 2);
    $_SESSION[ssig() . 'gameannotation'] = mysql_result($r , 0 , 3);
    $_SESSION[ssig() . 'gamegloss'] = mysql_result($r , 0 , 4);
    $_SESSION[ssig() . 'termcolor'] = "blue";

    
    $_SESSION[ssig() . 'termselectionmethod']="select_easy_word_N";
    end_time_record("select_easy_word_N");

}

function select_easy_word_V() {
    start_time_record("select_easy_word_V");
    $range = 1000;

   // $query = "SELECT id FROM `Nodes` WHERE w > 50 
	//AND id IN (SELECT node1 FROM Relations WHERE node2 
	//IN (SELECT id FROM Nodes WHERE type=4 and name LIKE 'Ver:Inf')
	//) order by w DESC LIMIT $range" ;
    
    $GLOBALS[0]=$range;
    $query = get_msg('REQUEST-VERB-EASY');
    
    $r =  @mysql_query($query) or die("pb in select_easy_word_V : $query");

    $id = select_rand_in_tab($r);
    $nodeid = mysql_result($r , $id , 0);

    $query = "SELECT id, name, level, annotation, gloss FROM `Nodes` WHERE id =$nodeid ; " ;
    $r =  @mysql_query($query) or die("pb in select_easy_word_V 2 : $query");

    $_SESSION[ssig() . 'gameentryid'] = mysql_result($r , 0 , 0);
    $_SESSION[ssig() . 'gameentry'] = mysql_result($r ,  0 , 1);
    $_SESSION[ssig() . 'gameentrylevel'] = mysql_result($r , 0 , 2);
    $_SESSION[ssig() . 'gameannotation'] = mysql_result($r , 0 , 3);
    $_SESSION[ssig() . 'gamegloss'] = mysql_result($r , 0 , 4);
    $_SESSION[ssig() . 'termcolor'] = "blue";

    
    $_SESSION[ssig() . 'termselectionmethod']="select_easy_word_V";
    end_time_record("select_easy_word_V");

}

function select_V_transitive() {
    start_time_record("select_V_transitive");
    //echo "select_V_transitive()";
    
    $range = 1000;

    // $query = "SELECT id FROM `Nodes` WHERE w > 50 
	// AND id IN (SELECT node1 FROM Relations WHERE node2 
	// IN (SELECT id FROM Nodes WHERE type=18 and name LIKE 'Ver:Transitif')
	// ) order by w DESC LIMIT $range" ;
    
    $GLOBALS[0]=$range;
    $query = get_msg('REQUEST-VERB-TRANSTIVE');
       
    $r =  @mysql_query($query) or die("pb in select_V_transitive : $query");

    $id = select_rand_in_tab($r);
    $nodeid = mysql_result($r , $id , 0);

    $query = "SELECT id, name, level, annotation, gloss FROM `Nodes` WHERE id =$nodeid ; " ;
    $r =  @mysql_query($query) or die("pb in select_V_transitive 2 : $query");

    $_SESSION[ssig() . 'gameentryid'] = mysql_result($r , 0 , 0);
    $_SESSION[ssig() . 'gameentry'] = mysql_result($r ,  0 , 1);
    $_SESSION[ssig() . 'gameentrylevel'] = mysql_result($r , 0 , 2);
    $_SESSION[ssig() . 'gameannotation'] = mysql_result($r , 0 , 3);
    $_SESSION[ssig() . 'gamegloss'] = mysql_result($r , 0 , 4);
    $_SESSION[ssig() . 'termcolor'] = "blue";

    
    $_SESSION[ssig() . 'termselectionmethod']="select_V_transitive";
    end_time_record("select_V_transitive");

}

function select_easy_word_NV() {
    start_time_record("select_easy_word_NV");
    $range = 1000;

    $query = "SELECT id FROM `Nodes` WHERE w > 50 
	AND id IN (SELECT node1 FROM Relations WHERE node2 
	IN (SELECT id FROM Nodes WHERE type=4 and (name LIKE 'Ver:Inf' OR name LIKE 'N%'))
	) order by w DESC LIMIT $range" ;
    $r =  @mysql_query($query) or die("pb in select_easy_word_NV : $query");

    $id = select_rand_in_tab($r);
    $nodeid = mysql_result($r , $id , 0);

    $query = "SELECT id, name, level, annotation, gloss FROM `Nodes` WHERE id =$nodeid ; " ;
    $r =  @mysql_query($query) or die("pb in select_easy_word_NV 2 : $query");

    $_SESSION[ssig() . 'gameentryid'] = mysql_result($r , 0 , 0);
    $_SESSION[ssig() . 'gameentry'] = mysql_result($r ,  0 , 1);
    $_SESSION[ssig() . 'gameentrylevel'] = mysql_result($r , 0 , 2);
    $_SESSION[ssig() . 'gameannotation'] = mysql_result($r , 0 , 3);
    $_SESSION[ssig() . 'gamegloss'] = mysql_result($r , 0 , 4);
    $_SESSION[ssig() . 'termcolor'] = "blue";

    
    $_SESSION[ssig() . 'termselectionmethod']="select_easy_word_NV";
    end_time_record("select_easy_word_NV");

}

// provisoire
// selection equiprobable pour le moment
//
function select_potential_word() {
    start_time_record("select_potential_word");
    $relid = $_SESSION[ssig() . 'gamerelationtype'];

    debugecho("in select_potential_word : $relid");
    
    $range = 1000;

    
    $query = "SELECT node1, w FROM `Relations` where type=12 and node1 != 0 and node2 in 
    (SELECT id from Nodes where type = 6 and name like \"%:$relid\")" ;
    $r =  @mysql_query($query) or die("pb in select_potential_word : $query");

    //$id = select_rand_in_tab($r);
    $id = select_rand_in_tab_with_weight($r);

    // y en a pas
    //
    if ($id == -1) {selectRandomWord(); return;}

    $nodeid = mysql_result($r , $id , 0);

    $query = "SELECT id, name, level, annotation, gloss FROM `Nodes` WHERE id =$nodeid ; " ;
    $r =  @mysql_query($query) or die("pb in select_potential_word 2 : $query");

    $_SESSION[ssig() . 'gameentryid'] = mysql_result($r , 0 , 0);
    $_SESSION[ssig() . 'gameentry'] = mysql_result($r ,  0 , 1);
    $_SESSION[ssig() . 'gameentrylevel'] = mysql_result($r , 0 , 2);
    $_SESSION[ssig() . 'gameannotation'] = mysql_result($r , 0 , 3);
    $_SESSION[ssig() . 'gamegloss'] = mysql_result($r , 0 , 4);
    $_SESSION[ssig() . 'termcolor'] = "blue";


    $_SESSION[ssig() . 'termselectionmethod']="select_potential_word + relation=$relid";
    end_time_record("select_potential_word");

}


function select_gift_term() {
    start_time_record("select_gift_term");
	
	//debugecho("in select_gift_term ");
	//debugecho("_SESSION['termgift'] = " . $_SESSION[ssig() . 'termgift']);
	
    $termid = $_SESSION[ssig() . 'termgift'];
    $query = "SELECT id, name, level, annotation, gloss FROM `Nodes` WHERE name =\"$termid\" ; " ;
    $r =  @mysql_query($query) or die("pb in select_gift_term : $query");

    $_SESSION[ssig() . 'gameentryid'] = mysql_result($r , 0 , 0);
    $_SESSION[ssig() . 'gameentry'] = mysql_result($r ,  0 , 1);
    $_SESSION[ssig() . 'gameentrylevel'] = mysql_result($r , 0 , 2);
    $_SESSION[ssig() . 'gameannotation'] = mysql_result($r , 0 , 3);
    $_SESSION[ssig() . 'gamegloss'] = mysql_result($r , 0 , 4);
    $_SESSION[ssig() . 'termcolor'] = "#FF00FF";

    //debugecho("_SESSION['gameentryid'] = " . $_SESSION[ssig() . 'gameentryid']);
    //debugecho("_SESSION['gameentry'] = " . $_SESSION[ssig() . 'gameentry']);
    
    $_SESSION[ssig() . 'termselectionmethod']="select_gift_term";
    end_time_record("select_gift_term");
    //end_debugecho();
}


?>
