<?php session_start();?>
<?php include_once 'misc_functions.php'; ?>
<?php
    openconnexion();
	$_SESSION[ssig() . 'redirect_if_session_finished'] = 'N';
?>
<html>
 <head>
    <title><?php echo "Le R�zo mutuel"; ?></title>
    <?php header_page_encoding(); ?>
  </head>
<?php include 'HTML-body.html' ; ?>
<?php topblock(); ?>

<?php



function rezomut_make_wordrelation_form() {

	//echo "'". $_POST['gotermrel'] . "'";
	//echo "'". $_GET['gotermrel'] . "'";
	
	$term = trim($_POST['gotermrel']);
	if ($term == "") {
		$term = trim($_GET['gotermrel']);	
	}
	$_SESSION[ssig() . 'clique_target'] = $term;
	echo "<form id=\"gotermrel\" name=\"gotermrel\" method=\"post\" action=\"rezomut.php\" >
	    <input id=\"gotermsubmit\" type=\"submit\" name=\"gotermsubmit\" value=\"Chercher\"> le mot
	    <input  id=\"gotermrel\" type=\"text\" name=\"gotermrel\" value=\"$term\" size=70>
	    </form>";
}

function old_rezomut_make_wordrelation_form() {

	//echo "'". $_POST['gotermrel'] . "'";
	//echo "'". $_GET['gotermrel'] . "'";
	
	$term = trim($_POST['gotermrel']);
	if ($term == "") {
		$term = trim($_GET['gotermrel']);	
	}
	$_SESSION[ssig() . 'clique_target'] = $term;
	echo "<form id=\"gotermrel\" name=\"gotermrel\" method=\"post\" action=\"rezomut.php\" >
	    <input id=\"gotermsubmit\" type=\"submit\" name=\"gotermsubmit\" value=\"Chercher\"> le mot
	    <input  id=\"gotermrel\" type=\"text\" name=\"gotermrel\" value=\"$term\" size=70>
	    	<select name=\"constraint\">
			<option value=\"strict\">Strict</option>
			<option value=\"loose\">Loose</option>
			</select>
			
			<select name=\"relation\">
			<option value=\"0\">Associated ideas</option>
			<option value=\"5\">Syn</option>
			<option value=\"6\">Is-a</option>
			<option value=\"-1\">All</option>
			</select>
	    </form>";
}

function rezomut_make_fusion_form() {
	echo "<form id=\"fusion_form\" name=\"gotermrel\" method=\"post\" action=\"rezomut.php\" >
	    <input id=\"clique_fusion\" type=\"submit\" name=\"clique_fusion\" value=\"Fusionner\"> la clique
	    <input  id=\"clique1\" type=\"text\" name=\"clique1\" value=\"\" size=5> et
	    <input  id=\"clique2\" type=\"text\" name=\"clique2\" value=\"\" size=5>
	    </form>";
}


function process_forms() {
	if (($_POST['gotermsubmit']!= "") || ($_GET['gotermrel']!= "")){
		$term = trim($_POST['gotermrel']);
		if ($term == "") {
			$term = trim($_GET['gotermrel']);	
		}
		rezomut_display_wordrelation_list($term);
		flush();
		display_cliques_pages();
		flush();
	}
	
	if ($_POST['clique_fusion']!= ""){
		$i = trim($_POST['clique1']);
		$j = trim($_POST['clique2']);
		//echo "<br>i=$i j=$j";
		$t = array_keys($_SESSION[ssig() . 'mat_clique']);
		$c_1 = $t[$i];
		$c_2 = $t[$j];
		fusion_cliques($c_1, $c_2);
		display_cliques_pages();
		flush();
	}
}


function compute_term_list_id($node1_id) {
	$tab = array();
	array_push($tab,$node1_id);
	
	 $query= "SELECT distinct(node2) from Relations WHERE
			node1=$node1_id
		    AND Relations.type in (0, 5, 6, 3, 8)";
	 $r =  @mysql_query($query) or die("pb in compute_term_list($node1_id)  : $query");
	 $nb = mysql_num_rows($r);
	 for ($i=0 ; $i<$nb ; $i++) {
		$node2 = mysql_result($r , $i , 0);
		//echo "<br>node2 = $node2";
		$query2= "SELECT count(id) from Relations WHERE
			node1=$node2 and node2=$node1_id
		    AND Relations.type in (0, 5, 6, 3, 8)";	
		$r2 =  @mysql_query($query2) or die("pb in compute_term_list($node1_id)  : $query");
	 	$count = mysql_result($r2 , 0 , 0);
		if (($count > 0) && ($node2 > 0) && ($node2 != '')) {
			//echo "<br>node2 = $node2 KEEP = " . get_term_from_id($node2);
			if ((array_search($node2, $tab) == false) && ($node2 != '')) {
		 		array_push($tab,$node2);
			}
		 }
	}
	
	$tab = array_merge(array_unique($tab));
//	print_r($tab);
//	flush();
	return($tab);
}


function compute_cliques1() {
	start_time_record('compute_cliques1');
	$init_clique = clique_make($_SESSION[ssig() . 'mat_size']);
	insert_initial_clique($init_clique, $_SESSION[ssig() . 'mat_total_weight']);
    delete_graph_links();
    $duree= end_time_record('compute_cliques1');
	echo "<br>computing cliques with compute_cliques ONE() : $duree ";
	flush();
}

function compute_cliques2() {
	start_time_record('compute_cliques2');
	 for ($i=0 ; $i< $_SESSION[ssig() . 'mat_size'] ; $i++) {
	      	 make_ligne_potclique($i);
	      	 make_col_potclique($i);	    	
	 }
 	for ($i=1 ; $i< $_SESSION[ssig() . 'mat_size'] ; $i++) {
	      	 $newclique = consolidate_clique_ligne($i) ;
	       	 $w = compute_clique_weight($newclique);
			 insert_clique($newclique, $w);
			 
			// $newclique = consolidate_clique_col($i) ;
		   //  $w = compute_clique_weight($newclique);
		    // insert_clique($newclique, $w);		    
	     }		 
    delete_graph_links();
    $duree= end_time_record('compute_cliques2');
    echo "<br>computing cliques with compute_cliques TWO() : $duree ";
    flush();
}

function rezomut_display_wordrelation_list($term) {
    //echo "<P>in display_wordrelation_list value :" . $_POST['gotermsubmit'] . "--" . $_POST['gotermrel'];
  	
	$rel=$_POST['relation'];
	if ($rel == '') {$rel = 0;}
	$type = $rel; 
  		
  	$constraint=$_POST['constraint'];
  	$strictp = (($constraint == "strict")|| ($constraint == ''));
	
  	if (($_POST['gotermsubmit']!= "") || ($_GET['gotermrel']!= "")){
	$id = term_exist_in_BD_p($term);
	if ($id == 0) {display_warning("<br>Le terme $term n'existe pas !");}
	    else {
	    $_SESSION[ssig() . 'mat_term'] = '';
	    $_SESSION[ssig() . 'mat_id'] = '';	
	    $_SESSION[ssig() . 'mat_weight'] = '';
	    $_SESSION[ssig() . 'mat_size'] = 0;
	    
	  	$term_list_id = compute_term_list_id($id);
	   
	    
	    for ($i=0 ; $i<count($term_list_id) ; $i++) {
		//	echo "<br>term = " . get_term_from_id($term_list_id[$i]) . " " . $term_list_id[$i];
	   	//	flush();
	    	$_SESSION[ssig() . 'mat_term'][$_SESSION[ssig() . 'mat_size']] = get_term_from_id($term_list_id[$i]);
	     	$_SESSION[ssig() . 'mat_id'][$_SESSION[ssig() . 'mat_size']] = $term_list_id[$i];
	     	$_SESSION[ssig() . 'mat_size'] = $_SESSION[ssig() . 'mat_size']+1;	     		
		}
	  
	    $_SESSION[ssig() . 'mat_total_weight'] = 0;
	    $_SESSION[ssig() . 'mat_missing_links'] = 0;
	    
	    // on remplie la matrice de poids
	   	compte_weight_matrix($type);
	     
	    // echo"<P>MATRIX of weights :";
	    // Print_r($_SESSION[ssig() . 'mat_weight']);
	     
	    // echo"<P>Total weight = " . $_SESSION[ssig() . 'mat_total_weight'] ;
	    // echo"<P># missing links = " . $_SESSION[ssig() . 'mat_missing_links'] ;
	     
	    $_SESSION[ssig() . 'mat_clique'] = '';
	    $_SESSION[ssig() . 'mat_col'] = '';
	    $_SESSION[ssig() . 'mat_ligne'] = '';
	  
	    compute_cliques1();	   
    }
  }
}

function make_ligne_potclique ($line) {
	//echo "<br>make_ligne_potclique ($line)";
	$clique = '';
	 for ($j=0 ; $j< $_SESSION[ssig() . 'mat_size'] ; $j++) {
	 	$w1 = $_SESSION[ssig() . 'mat_weight'][$line][$j];
	 	$w2 = $_SESSION[ssig() . 'mat_weight'][$j][$line];
	 	//echo "$w ";
	 	if (($w1 > 0) && ($w2 >0)) {
	 		$clique = $clique . '1'	;
	 	} else {
	 		$clique = $clique . '0'	;
	 	}	 	
	 }
	 //echo "== $clique";
	 $_SESSION[ssig() . 'mat_ligne'][$line]=$clique;
}

function make_col_potclique ($line) {
	//echo "<br>make_col_potclique ($line)";
	$clique = '';
	 for ($j=0 ; $j< $_SESSION[ssig() . 'mat_size'] ; $j++) {
	 	$w1 = $_SESSION[ssig() . 'mat_weight'][$j][$line];
	 	$w2 = $_SESSION[ssig() . 'mat_weight'][$line][$j];
	 	//echo "$w ";
	 	if (($w1 > 0) && ($w2 >0)) {
	 		$clique = $clique . '1'	;
	 	} else {
	 		$clique = $clique . '0'	;
	 	}	 	
	 }
	 //echo "== $clique";
	 $_SESSION[ssig() . 'mat_col'][$line]=$clique;
}


function intersect_cliques($c_1, $c_2) {
	//echo "<br>fusion_cliques($c_1, $c_2)" ;
	$l1 = strlen($c_1);
	$l2 = strlen($c_2);
	$lmax = max($l1, $l2); /// en fait c'est pareil
	
	$clique_inter = "";
	for ($i=0 ; $i< $lmax ; $i++) {
		if (($c_1[$i] == '1') && ($c_2[$i] == '1')) {
			$clique_inter = $clique_inter . '1';
		} else {
			$clique_inter = $clique_inter . '0';
		}
	}
	return $clique_inter;
}

function consolidate_clique_ligne($line) {
	//echo "<br>validate_clique($line)";
	$clique1 = $_SESSION[ssig() . 'mat_ligne'][$line];
	$clique2 = $_SESSION[ssig() . 'mat_col'][$line];
	$newclique = intersect_cliques($clique1, $clique2);
	return $newclique;
	
	$newclique = $_SESSION[ssig() . 'mat_ligne'][$line];
	$l = strlen($clique);
	for ($i=0 ; $i<$l; $i++) {
		$char = $clique[$i];
		if ($char == 1) {
			//sub_clique($cli_a, $cli_b) 
			$clique2 = $_SESSION[ssig() . 'mat_col'][$i];
			$newclique = intersect_cliques($newclique, $clique2);
		}
	}
	//echo "<br>$line consolidate_clique_lig $clique ==> $newclique";
	return $newclique;
}

function consolidate_clique_col($col) {
	//echo "<br>validate_clique($line)";
	$clique = $_SESSION[ssig() . 'mat_col'][$col];
	$newclique = $_SESSION[ssig() . 'mat_col'][$col];
	$l = strlen($clique);
	for ($i=0 ; $i<$l; $i++) {
		$char = $clique[$i];
		if ($char == 1) {
			//sub_clique($cli_a, $cli_b) 
			$clique2 = $_SESSION[ssig() . 'mat_ligne'][$i];
			$newclique = intersect_cliques($newclique, $clique2);
		}
	}
	echo "<br>$col consolidate_clique_col $clique ==> $newclique";
	return $newclique;
}

function validate_clique_ligne($line) {
	//echo "<br>validate_clique($line)";
	$clique = $_SESSION[ssig() . 'mat_ligne'][$line];
	$l = strlen($clique);
	for ($i=0 ; $i<$l; $i++) {
		$char = $clique[$i];
		if ($char == 1) {
			//sub_clique($cli_a, $cli_b) 
			$clique2 = $_SESSION[ssig() . 'mat_col'][$i];
			if (sub_clique($clique, $clique2) == false) {
				//echo "<br> $clique is not included in $clique2";
				return false;
			}
		}
	}
	return true;
}


function validate_clique_col($col) {
	//echo "<br>validate_clique($line)";
	$clique = $_SESSION[ssig() . 'mat_col'][$col];
	$l = strlen($clique);
	for ($i=0 ; $i<$l; $i++) {
		$char = $clique[$i];
		if ($char == 1) {
			//sub_clique($cli_a, $cli_b) 
			$clique2 = $_SESSION[ssig() . 'mat_ligne'][$i];
			if (sub_clique($clique, $clique2) == false) {
				//echo "<br> $clique is not included in $clique2";
				return false;
			}
		}
	}
	return true;
}


function display_cliques_pages() {
	
	echo"<P>MATRICE DE POIDS";
	display_weight_table();
	flush();
	echo"<P>CLIQUES";
	display_clique_table();
	flush();
	echo"<P>SIMILARITE";
	display_sim_table($_SESSION[ssig() . 'mat_clique']);
	flush();
}

function compte_weight_matrix ($type) {
	//$chunk = "AND type = $type";
	$chunk = "AND type in (0, 5, 6, 3, 8)";
  	if ($type == -1) {
		$chunk = "";
	}
	//print_r($_SESSION[ssig() . 'mat_id']);
	//print_r($_SESSION[ssig() . 'mat_term']);
	//flush();
	
	// on remplie la matrice de poids
	    for ($i=0 ; $i< $_SESSION[ssig() . 'mat_size'] ; $i++) {
	     	 for ($j=0 ; $j< $_SESSION[ssig() . 'mat_size'] ; $j++) {
	     	 	if ($i != $j) {
	     	 	$n1id = $_SESSION[ssig() . 'mat_id'][$i];
	     	 	$n2id = $_SESSION[ssig() . 'mat_id'][$j];
	     	 	$query= "SELECT sum(w) FROM Relations Where 
					node1 = $n1id  AND node2 = $n2id $chunk";
				$r =  @mysql_query($query) or die("pb in compte_weight_matrix : $query");
				$val = mysql_result($r , 0 , 0);
				if ($val > 0){
					//$t1 = $_SESSION[ssig() . 'mat_term'][$i];
					//$t2 = $_SESSION[ssig() . 'mat_term'][$j];
					//echo "<br>lien entre $t1 $val $t2",
					$_SESSION[ssig() . 'mat_total_weight'] = $_SESSION[ssig() . 'mat_total_weight'] + $val;
				} else {
					$_SESSION[ssig() . 'mat_missing_links'] = $_SESSION[ssig() . 'mat_missing_links'] + 1;
				}
				$_SESSION[ssig() . 'mat_weight'][$i][$j] = $val;
			}
	     	if ($i == $j) {
					$_SESSION[ssig() . 'mat_weight'][$i][$j] = 1;
				}
	     	 }
	     }
}


function is_clique_already_thereBIS($clique) {
	$t = array_keys($_SESSION[ssig() . 'mat_clique']);
	echo "<br>==> testing of sub_clique $clique ";
	//Print_r($t);
	
	$nb = count($t);
	//echo "<br>$nb";
	for ($i=0 ; $i< $nb ; $i++) {
		echo "<br>$i comparing $clique and ". $t[$i];
		if ($t[$i]."" === $clique) {
			echo " FOUND !";
		} else {
			// LE "" . est important !!!!
			if (sub_clique($clique, "". $t[$i])) 
			{
				echo " ==> True";
				return true;
			} else {
				echo " ==> False";
			}
		}
	}
	//echo "<br>is_clique_already_there($clique) ==> False";
	return false;
}


function get_term_weight($term) {
	$query = "SELECT w FROM `Nodes` WHERE `name` = \"$term\"";
	$r =  @mysql_query($query) or die("bug in get_term_weight : $query");
	return mysql_result($r , 0 , 0);
}


function display_clique_table() {
	// print_r(array_keys($table));
/*	$t = array_keys($_SESSION[ssig() . 'mat_clique']);
	$nb = count($t);
	for ($i=0 ; $i< $nb ; $i++) {
		$elt = $t[$i];
		if (is_clique_already_there("".$elt)) {
			//echo "<br>testing: $elt ==> to be deleted";
			//unset($_SESSION[ssig() . 'mat_clique'][$elt]);
		} else {
			//echo "<br>testing: $elt ==> to be kept";
		}
	}
*/

	$t = array_keys($_SESSION[ssig() . 'mat_clique']);
	$nb = count($t);
	for ($i=0 ; $i< $nb ; $i++) {
		$elt = $t[$i];
		
		echo "<br> $i: ";
		display_clique($elt);
		
		$tw = $_SESSION[ssig() . 'mat_clique'][$elt]['totw'];
		$nbl = $_SESSION[ssig() . 'mat_clique'][$elt]['nblinks'];
		$moy = round($_SESSION[ssig() . 'mat_clique'][$elt]['moy']);
		$conf = round($_SESSION[ssig() . 'mat_clique'][$elt]['conf']);
		echo " <font size=1>(P = $tw / nl = $nbl / moy = $moy / REL = $conf)</font> ";
	}
}

function display_weight_table() {
	//$table = $_SESSION[ssig() . 'mat_weight'];
	//print_r($_SESSION[ssig() . 'mat_weight']);
	$nb = $_SESSION[ssig() . 'mat_size'];
	if ($nb <= 1) {
		echo "<P>Aucune clique";
		return;
	}
	
	for ($i=0 ; $i< $nb ; $i++) {
		//echo "<TD><center>" . $_SESSION[ssig() . 'mat_term'][$i] . "</center>";
		echo " <SMALL>$i:" .  $_SESSION[ssig() . 'mat_term'][$i]. " - </SMALL>";
	
	}
	
	echo "<span style=\"font-size:10\"><TABLE border=1><TR><TD>";
	for ($i=0 ; $i< $nb ; $i++) {
		//echo "<TD><center>" . $_SESSION[ssig() . 'mat_term'][$i] . "</center>";
		echo "<TD><SMALL><center>" . $i . "</center></SMALL>";
	
	}
	for ($i=0 ; $i<$nb ; $i++) {
		$poids = get_term_weight($_SESSION[ssig() . 'mat_term'][$i]);
		echo "<TR><TD><SMALL>" . $i . ": " . $_SESSION[ssig() . 'mat_term'][$i] . " ($poids)</SMALL>";
		for ($j=0 ; $j< $nb ; $j++) {
			
			$w = $_SESSION[ssig() . 'mat_weight'][$i][$j];
			//$w = round($sim,2);
			if ($w > 0) {
				if ($_SESSION[ssig() . 'mat_weight'][$j][$i]> 0) {
					echo "<TD BGCOLOR=\"99FF99\"><SMALL><center>$w</center></SMALL>";
				} else {
					echo "<TD BGCOLOR=\"FFFF66\"><SMALL><center>$w</center></SMALL>";
				}
			} else {
				if ($i==$j) {
					echo "<TD BGCOLOR=\"99FF99\"><SMALL><center>1</center>";
				} else {
					echo "<TD>";
				}
			}
		}
	}
	echo "</TABLE></span>";
	flush();
}


function display_sim_table($table) {
	$t = array_keys($table);
	$nb = count($t);
	echo "<P><TABLE border=1><TR><TD>";
	for ($i=0 ; $i< $nb ; $i++) {
		echo "<TD><center>$i</center>";
	}
	for ($i=0 ; $i< $nb ; $i++) {
		echo "<TR><TD>$i";
		for ($j=0 ; $j< $nb ; $j++) {
			
		$c_1 = $t[$i];
		$c_2 = $t[$j];
		$sim = compare_cliques($c_1, $c_2);
		$sim = round($sim,2);
		echo "<TD><tt><center>$sim</center></tt>";
		}
	}
	echo "</TABLE>";
	
	//echo'<P>T==';
	//print_r($t);
	$tbis = array_keys($_SESSION[ssig() . 'mat_clique']);
	$nb_bis = count($tbis);
	
	//echo'<P> TBIS ==';
	//print_r($tbis);
	
	
	for ($i=0 ; $i < $nb_bis ; $i++) {
		$c = $tbis[$i];
		//echo "<br>unseting $c";
		//$val = array_search($c,$t);
		if ($i >= $nb) {
			//echo "<br>$i: $c  ==> deleted";
			unset($_SESSION[ssig() . 'mat_clique'][$c]);
		} else {
			//echo "<br>$i: $c ==> kept";
		}
	}
}

function display_clique($clique) {
	$nb = strlen($clique);
	$code = bindec($clique);
	//echo " $code ";
	for ($i=0 ; $i< $nb ; $i++) {
		$val = substr($clique, $i, 1);
		if ($val == "1"){
			$term = $_SESSION[ssig() . 'mat_term'][$i];
			echo "'$term' ";
		}
	}
	
}

function compare_cliques($c_1, $c_2) {
	$l1 = strlen($c_1);
	$l2 = strlen($c_2);
	$lmax = max($l1, $l2); /// en fait c'est pareil
	
	$sum_inter = 0;
	$sum_c1 = 0;
	$sum_c2 = 0;

	//echo "lmax $lmax";
	$clique_inter = "";
	for ($i=0 ; $i< $lmax ; $i++) {
		$val1 = substr($c_1, $i, 1);
		$val2 = substr($c_2, $i, 1);
		if (($val1 == "1") && ($val2 == "1")) {
			$clique_inter = $clique_inter . "1";
		} else {
			$clique_inter = $clique_inter . "0";
		}
	}
	//echo $clique_inter . "<br>";
	compute_clique_weight($clique_inter);
	$inter_w = $_SESSION[ssig() . 'mat_clique'][$clique_inter]['totw'];
	$w1 = $_SESSION[ssig() . 'mat_clique'][$c_1]['totw'];
	$w2 = $_SESSION[ssig() . 'mat_clique'][$c_2]['totw'];
	
	$sim = $inter_w / sqrt($w1 * $w2);
	//echo "unseting:" . $_SESSION[ssig() . 'mat_clique'][$clique_inter];
	//unset($_SESSION[ssig() . 'mat_clique'][$clique_inter]);
	return $sim;
}

function fusion_cliques($c_1, $c_2) {
	//echo "<br>fusion_cliques($c_1, $c_2)" ;
	$l1 = strlen($c_1);
	$l2 = strlen($c_2);
	$lmax = max($l1, $l2); /// en fait c'est pareil
	
	$sum_inter = 0;
	$sum_c1 = 0;
	$sum_c2 = 0;

	//echo "lmax $lmax";
	$clique_union = "";
	for ($i=0 ; $i< $lmax ; $i++) {
		$val1 = substr($c_1, $i, 1);
		$val2 = substr($c_2, $i, 1);
		if (($val1 == "1") || ($val2 == "1")) {
			$clique_union = $clique_union . "1";
		} else {
			$clique_union = $clique_union . "0";
		}
	}
	//echo "fusion cliques  ==> " . $clique_union . "<br>";
	unset($_SESSION[ssig() . 'mat_clique'][$c_1]);
	unset($_SESSION[ssig() . 'mat_clique'][$c_2]);
	compute_clique_weight($clique_union);
}

function delete_graph_links() {
	// echo "<p>delete_graph_links";
	for ($i=0 ; $i< $_SESSION[ssig() . 'mat_size'] ; $i++) {
		for ($j=$i ; $j< $_SESSION[ssig() . 'mat_size'] ; $j++) {
	    	if (($i != $j) && (($_SESSION[ssig() . 'mat_weight'][$i][$j] == 0) || ($_SESSION[ssig() . 'mat_weight'][$j][$i] == 0))) {
	     	 	delete_clique_link_all ($_SESSION[ssig() . 'mat_clique'], $i, $j);
	    	}
		}
	}
}

function delete_clique_link_all ($table, $a, $b) {
	// echo "<p>delete_clique_link for $a , $b";
	// echo "<br>table before";
	// print_r(array_keys($table));
	$t = array_keys($table);
	$nb = count($t);
	for ($i=0 ; $i< $nb ; $i++) {
		$elt = $t[$i];
		//  echo "<br>--> delete_clique_link for $elt";
		delete_clique_link($elt, $a, $b);
	}
}

function delete_clique_link($clique, $a, $b) {
	$del = false;
	if ((clique_true_p_bit($clique, $a))
		and
		(clique_true_p_bit($clique, $b))
		) {
		 $cli1 = clique_set_0_nth_bit ($clique, $a);
		 
		 insert_clique($cli1, 0);
		 compute_clique_weight($cli1);
		//  echo "<br>----> creation of $cli1";
		 
		 $cli2 = clique_set_0_nth_bit ($clique, $b);
		 insert_clique($cli2, 0);
		 compute_clique_weight($cli2);
		//  echo "<br>----> creation of $cli2";
		 
		 $del = true;
	} else {
		// echo "<br>----> nothing new";
	}
	
	if ($del == true) {
		unset($_SESSION[ssig() . 'mat_clique'][$clique]);
		// echo "<br>----> destruction of $clique";
		
		if (is_clique_already_there($cli1)) {
			unset($_SESSION[ssig() . 'mat_clique'][$cli1]);
			// echo "<br>------> destruction of $cli1 because already included";
		}
		if (is_clique_already_there($cli2)) {
			unset($_SESSION[ssig() . 'mat_clique'][$cli2]);
			// echo "<br>------> destruction of $cli2 because already included";
		}
		
	}
}


function is_clique_already_there($clique) {
	$t = array_keys($_SESSION[ssig() . 'mat_clique']);
	//echo "<br>==> testing of sub_clique $clique ";
	//Print_r($t);
	
	$nb = count($t);
	//echo "<br>$nb";
	for ($i=0 ; $i< $nb ; $i++) {
		//echo "<br>$i comparing $clique and ". $t[$i];
		if (strcmp($t[$i], $clique) == 0) {
		//	echo " FOUND !";
		} else {
			// LE "" . est important !!!!
			if (sub_clique("" . $clique, "". $t[$i])) 
			{
			//	echo " ==> True";
				return true;
			} else {
			//	echo " ==> False";
			}
		}
	}
	
	//echo "<br>is_clique_already_there($clique) ==> False";
	return false;
}

// return true if a is included in b
//
function sub_cliqueBIS($cli_a, $cli_b) {
	echo "<br>====> testing of sub_clique: is $cli_a in $cli_b ?";
	
	$val1 = round(bindec($cli_a));
	$val2 = round(bindec($cli_b));
	$test = ($val1 < $val2);
	echo "<br>====>  is $val1 < $val2 ==> $test?";
	
	return $test;
}
//$code = bindec($clique);
function sub_clique($cli_a, $cli_b) {
	//echo "<br>====> testing of sub_clique: is $cli_a in $cli_b ?";
	$nb = strlen($cli_a);
	for ($i=0 ; $i< $nb ; $i++) {
		//$val = substr($cli_a, $i, 1);
		$val1 = $cli_a[$i];
		$val2 = $cli_b[$i];
		//echo "val1=$val1/val2=$val2";
		if (($val1 == "1") and ($val2 != "1")) {
			//echo " ==> false";
			return false;
		}
	}
	//echo " ==> true";
	return true;
}

function insert_initial_clique($clique, $maxweight) {
	$_SESSION[ssig() . 'mat_clique'][$clique]['totw'] = $maxweight;
	$size = substr_count($clique, '1');
	$_SESSION[ssig() . 'mat_clique'][$clique]['nblinks'] = ($size * ($size -1));
	$_SESSION[ssig() . 'mat_clique'][$clique]['moy'] = $maxweight / $_SESSION[ssig() . 'mat_clique'][$clique]['nblinks'];
	$_SESSION[ssig() . 'mat_clique'][$clique]['conf'] = $_SESSION[ssig() . 'mat_clique'][$clique]['moy'] * log($size);   
	
}

function insert_clique($clique, $maxweight) {
	$_SESSION[ssig() . 'mat_clique'][$clique]['totw'] = $maxweight;
	$size = substr_count($clique, '1');
	$_SESSION[ssig() . 'mat_clique'][$clique]['nblinks'] = ($size * ($size -1));
	$_SESSION[ssig() . 'mat_clique'][$clique]['moy'] = $maxweight / $_SESSION[ssig() . 'mat_clique'][$clique]['nblinks'];
	$_SESSION[ssig() . 'mat_clique'][$clique]['conf'] = $_SESSION[ssig() . 'mat_clique'][$clique]['moy'] * log($size);   
}

function clique_make($size) {
	return str_repeat ("1", $size);
}

function clique_return_nth_bit ($clique, $n) {
	return substr($clique, $n, 1);
}

function clique_true_p_bit ($clique, $n) {
	return (substr($clique, $n, 1) == '1');
}

// substr_replace  ( mixed $string  , string $replacement  , int $start  [, int $length  ] )
function clique_set_1_nth_bit ($clique, $n) {
	substr_replace($clique, "1", $n, 1);
	return $clique;
}

function clique_set_0_nth_bit ($clique, $n) {
	return substr_replace($clique, "0", $n, 1);
	//return $clique;
}

function compute_clique_weight($clique){
	$w = 0;
	    for ($i=0 ; $i< $_SESSION[ssig() . 'mat_size'] ; $i++) {
	     	 for ($j=0 ; $j< $_SESSION[ssig() . 'mat_size'] ; $j++) {
	     	 	if ($i != $j) {
	     	 	$n1id = $_SESSION[ssig() . 'mat_id'][$i];
	     	 	$n2id = $_SESSION[ssig() . 'mat_id'][$j];
				$val = $_SESSION[ssig() . 'mat_weight'][$i][$j];	
				
				if (clique_true_p_bit($clique, $i)
					and
					clique_true_p_bit($clique, $j)
					) {
						$w = $w+ $val;
					}
	     	 }
	     	 }
	     }
	$_SESSION[ssig() . 'mat_clique'][$clique]['totw'] = $w;
	$size = substr_count($clique, '1');
	$_SESSION[ssig() . 'mat_clique'][$clique]['nblinks'] = ($size * ($size -1));
	$_SESSION[ssig() . 'mat_clique'][$clique]['moy'] = $w / $_SESSION[ssig() . 'mat_clique'][$clique]['nblinks'];  
	$_SESSION[ssig() . 'mat_clique'][$clique]['conf'] = $_SESSION[ssig() . 'mat_clique'][$clique]['moy'] * log($size);    
	return $w; 
}
?>

<div class="jdm-level1-block">
	
	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
    <?php echo "Le R�zo mutuel"; 
     //	rezomut_make_wordrelation_form();
     ?>
    </div>
	</div>

</div>

<div class="jdm-level2-block">
<TABLE border="0" width="100%" cellspacing="0" cellpadding="0%"
	summary="jeuxdemots">

<TR><TH  valign="top" width="200pts">
    <TH  align="left">
    <?php
	   rezomut_make_wordrelation_form();
	    process_forms();
	    rezomut_make_fusion_form();
    ?>
    <TH>
    
</TABLE>
</div>

<?php //playerinfoblock($_SESSION[ssig() . 'playerid']) ?>
<?php 
    bottomblock();
    closeconnexion();
    
    
    
    
?>




  </body>
</html>
