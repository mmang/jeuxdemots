<?php 
$GLOBALS['PT-'] = '[String not found]';

//------
$GLOBALS['PT-ENCODING-GENERAL'] = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
$GLOBALS['PT-ENCODING-MAIL'] = "UTF-8";
$GLOBALS['PT-ENCODING-DB'] = "utf8";
$GLOBALS['PT-ENCODING-MUSEUM'] = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
 
//------ relations
$GLOBALS['PT-r_associated'] = 'idéia associada'; 
$GLOBALS['PT-r_associated-HELP'] = 'Qualquer termo ligado de alguma forma ao termo proposto. Essa palavra faz você pensar em quê?'; 
$GLOBALS['PT-r_acception'] = 'sentido';
$GLOBALS['PT-r_acception-HELP'] = 'Sentidos da palavra';
$GLOBALS['PT-r_definition'] = 'definição';
$GLOBALS['PT-r_definition-HELP'] = 'Definição lógica, em termos de características específicas que distinguem esta palavra das demais palavras';
$GLOBALS['PT-r_domain'] = 'domínio';
$GLOBALS['PT-r_domain-HELP'] = 'Domínios relacionados ao termo proposto. Por exemplo, para &quot;escanteio&quot;, pode-se dar os domínios &quot;futebol&quot; ou &quot;esporte&quot;.';
$GLOBALS['PT-r_pos'] = 'r_pos';
$GLOBALS['PT-r_pos-HELP'] = 'r_pos';
$GLOBALS['PT-r_syn'] = 'sinonímia';
$GLOBALS['PT-r_syn-HELP'] = 'A partir de um termo, deve-se enumerar os sinônimos ou quase-sinônimos deste termo.';
$GLOBALS['PT-r_isa'] = 'genérico';
$GLOBALS['PT-r_isa-HELP'] = '&quot;mamífero&quot;, &quot;animal&quot;, &quot;ser vivo&quot; são genéricos para o termo &quot;gato&quot;';
$GLOBALS['PT-r_anto'] = 'contrário';
$GLOBALS['PT-r_anto-HELP'] = '&quot;quente&quot; é o contrário de &quot;frio&quot;';
$GLOBALS['PT-r_hypo'] = 'específico';
$GLOBALS['PT-r_hypo-HELP'] = '&quot;mosca&quot;, &quot;abelha&quot;, &quot;vespa&quot; são específicos de &quot;inseto&quot;';
$GLOBALS['PT-r_has_part'] = 'parte-de';
$GLOBALS['PT-r_has_part-HELP'] = 'Partes, constituintes ou elementos do termo proposto. Por exemplo, um &quot;carro&quot; possui &quot;porta&quot;, &quot;roda&quot;, &quot;motor&quot;, etc.';
$GLOBALS['PT-r_holo'] = 'todo';
$GLOBALS['PT-r_holo-HELP'] = 'O todo é aquilo que contém o termo proposto. Para &quot;mão&quot; pode-se sugerir &quot;braço&quot;, &quot;corpo&quot;, &quot;pessoa&quot;, etc. O todo também pode indicar o conjunto ao qual pertence um elemento, como &quot;turma&quot; para &quot;aluno&quot;.';
$GLOBALS['PT-r_locution'] = 'locução';
$GLOBALS['PT-r_locution-HELP'] = 'A partir de um termo proposto, enumerar as expressões, locuções ou termos compostos relacionados a esse termo. Por exemplo, para &quot;pé&quot;, pode-se sugerir &quot;pé de moleque&quot;, &quot;sem pé nem cabeça&quot;, &quot;levantar com o pé esquerdo&quot;. Para &quot;vender&quot;, pode-se sugerir &quot;vender a mãe e entregar a sogra&quot;, &quot;vender fiado&quot;, etc...';
$GLOBALS['PT-r_agent'] = 'agente típico';
$GLOBALS['PT-r_agent-HELP'] = 'O agente (também chamado de sujeito) é a entidade que efetua uma ação. Por exemplo, na frase &quot;O gato comeu o rato&quot;, o agente é o gato.';
$GLOBALS['PT-r_patient'] = 'paciente típico';
$GLOBALS['PT-r_patient-HELP'] = 'O paciente (também chamado de objeto) é a entidade que sofre uma ação. Por exemplo, na frase &quot;O gato comeu o rato&quot;, o paciente é o rato.';
$GLOBALS['PT-r_flpot'] = 'potentiel de FL';
$GLOBALS['PT-r_flpot-HELP'] = 'potentiel de FL';
$GLOBALS['PT-r_lieu'] = 'lugar';
$GLOBALS['PT-r_lieu-HELP'] = 'A partir de um nome de objeto, enumerar os lugares onde se encontra tipicamente este objeto. Por exemplo &quot;hospital&quot;, &quot;sala de aula&quot;, &quot;cozinha&quot; para &quot;tesoura&quot;';
$GLOBALS['PT-r_instr'] = 'instrumento típico';
$GLOBALS['PT-r_instr-HELP'] = 'O instrumento é o objeto com o qual realizamos uma ação. Em &quot;Ele comeu a sopa com a colher&quot;, colher é o instrumento.';
$GLOBALS['PT-r_carac'] = 'característica';
$GLOBALS['PT-r_carac-HELP'] = 'Para um termo proposto, em geral um objeto, pede-se para enumerar as características possíveis e/ou típicas deste objeto. Por exemplo, para &quot;água&quot; pode-se sugerir &quot;líquida&quot;, &quot;fria&quot;, &quot;quente&quot;, etc.';
$GLOBALS['PT-r_data'] = 'r_data';
$GLOBALS['PT-r_data-HELP'] = 'r_data';
$GLOBALS['PT-r_lemma'] = 'r_lemma';
$GLOBALS['PT-r_lemma-HELP'] = 'r_lemma';
$GLOBALS['PT-r_magn'] = 'magn';
$GLOBALS['PT-r_magn-HELP'] = 'Aumento de intensidade ou magnificação, por exemplo: &quot;chuva forte&quot;, &quot;tromba dʼágua&quot;, etc. para &quot;chuva&quot; - ou ainda &quot;viver intensamente&quot; para &quot;viver&quot;';
$GLOBALS['PT-r_antimagn'] = 'antimagn';
$GLOBALS['PT-r_antimagn-HELP'] = 'Diminuição de intensidade, inverso da magnificação, por exemplo: &quot;casinha&quot;, &quot;casebre&quot;, etc. para &quot;casa&quot; - ou ainda &quot;caminhar lentamente&quot;, &quot;vagar&quot; para &quot;caminhar&quot;.';
$GLOBALS['PT-r_familly'] = 'família';
$GLOBALS['PT-r_familly-HELP'] = 'Palavras da mesma família. Por exemplo &quot;desligar&quot;, &quot;ligação&quot;, &quot;desligamento&quot; para &quot;ligar&quot;, etc.';
$GLOBALS['PT-r_carac-1'] = 'caractéristique-1';
$GLOBALS['PT-r_carac-1-HELP'] = 'caractéristique-1';
$GLOBALS['PT-r_agent-1'] = 'agent typique-1';
$GLOBALS['PT-r_agent-1-HELP'] = 'agent typique-1';
$GLOBALS['PT-r_instr-1'] = 'instrument-1';
$GLOBALS['PT-r_instr-1-HELP'] = 'instrument-1';
$GLOBALS['PT-r-patient-1'] = 'patient-1';
$GLOBALS['PT-r-patient-1-HELP'] = 'patient-1';
$GLOBALS['PT-r_domain-1'] = 'domaine-1';
$GLOBALS['PT-r_domain-1-HELP'] = 'domaine-1';
$GLOBALS['PT-r_lieu-1'] = 'lieu-1';
$GLOBALS['PT-r_lieu-1-HELP'] = 'lieu-1';
$GLOBALS['PT-r_lieu-1-HELP'] = 'A partir d\'un lieu, il est demandé d\'énumérer ce qu\'il peut typiquement s\'y trouver.';
$GLOBALS['PT-r_pred'] = 'predicate';
$GLOBALS['PT-r_pred-HELP'] = 'predicate';
$GLOBALS['PT-r_lieu_action'] = 'lieu_action';
$GLOBALS['PT-r_lieu_action-HELP'] = 'énumérer les action typiques possibles dans ce lieu.';
$GLOBALS['PT-r_action_lieu'] = 'action_lieu';
$GLOBALS['PT-r_action_lieu-HELP'] = 'A partir d\'une action (un verbe), énumérer les lieux typiques possibles où peut etre réalisée cette action.';

$GLOBALS['PT-r_maniere'] = 'maneira';
$GLOBALS['PT-r_maniere-HELP'] = 'De quais MANEIRAS pode ser efetuada a ação (verbo) proposta. Deve-se sugerir um advérbio ou locução adverbial, por exemplo : &quot;rapidamente&quot;, &quot;como um condenado&quot;, &quot;desesperadamente&quot; etc. para &quot;comer&quot;.';
$GLOBALS['PT-r_sentiment'] = 'sentimento';
$GLOBALS['PT-r_sentiment-HELP'] = 'Para um termo dado, evocar SENTIMENTOS ou EMOÇÕES associados. Por exemplo, &quot;alegria&quot;, &quot;prazer&quot;, &quot;medo&quot;, &quot;ódio&quot;, &quot;amor&quot;, &quot;inveja&quot;, etc.';
$GLOBALS['PT-r_sens'] = 'sentido';
$GLOBALS['PT-r_sens-HELP'] = 'Quais SENTIDOS/SIGNIFICADOS você pode dar ao termo proposto. Deve-se sugerir termos relacionados que indicam o sentido do termo : &quot;água potável&quot;, &quot;recursos financeiros&quot;, &quot;caracteres&quot;, etc. para &quot;fonte&quot;.';
$GLOBALS['PT-r_cause'] = 'causa';
$GLOBALS['PT-r_cause-HELP'] = 'B (que você deve indicar) é uma causa possível de A. A e B são verbos ou substantivos. Exemplos: &quot;machucar-se&quot; -> &quot;cair&quot;, &quot;assalto&quot; -> &quot;pobreza&quot;, &quot;incêndio&quot; -> &quot;negligência&quot; etc.';
$GLOBALS['PT-r_consequence'] = 'consequência';
$GLOBALS['PT-r_consequence-HELP'] = 'B (que você deve indicar) é uma consequência possível de A. A e B são verbos ou substantivos. Exemplos:  &quot;cair&quot; -> &quot;machucar-se&quot;, &quot;fome&quot; -> &quot;roubo&quot;, &quot;acender&quot; -> &quot;incêndio&quot; etc.';
$GLOBALS['PT-r_telique'] = 'papel télico';
$GLOBALS['PT-r_telique-HELP'] = 'O papel télico indica o objetivo ou a função do nome ou do verbo. Por exemplo, &quot;cortar&quot; para faca, &quot;serrar&quot; para &quot;serrote&quot;, etc.';
$GLOBALS['PT-r_agentif'] = 'papel agentivo';
$GLOBALS['PT-r_agentif-HELP'] = 'O papel agentivo indica o modo de criação de um objeto. Por exemplo, &quot;construir&quot; para &quot;casa&quot;, &quot;redigir&quot; ou &quot;imprimir&quot; para &quot;livro&quot;, etc.';


//------

$GLOBALS['PT-GENGAME-TITLE'] = "JeuxDeMots: Jogo de Associação";
$GLOBALS['PT-GENGAME-GAMETYPE'] = "Jogo de Associação";
$GLOBALS['PT-GENGAME-WARN-NEWINSTR'] = "<blink>Atenção, novas instruções</blink>";

$GLOBALS['PT-GENGAME-PASS'] = "Pular";
$GLOBALS['PT-GENGAME-BUYTIME'] = "Comprar 30 segundos";
$GLOBALS['PT-GENGAME-SEND'] = "Enviar";
$GLOBALS['PT-GENGAME-TABOO'] = "Palavras proibidas: ";
$GLOBALS['PT-GENGAME-NOPROPTERM'] = "Nenhuma palavra sugerida";
$GLOBALS['PT-GENGAME-LASTPROPTERM'] = "Última palavra sugerida :";

$GLOBALS['PT-GENGAME-PROMPT-GENERATION'] = "Geração de partida";
$GLOBALS['PT-GENGAME-PROMPT-INSTRUCTION'] = "Lendo instruções";
$GLOBALS['PT-GENGAME-PROMPT-INGAME'] = "Jogando";
$GLOBALS['PT-GENGAME-PROMPT-RES'] = "Resultados";

$GLOBALS['PT-GENGAME-REMAININGTIME'] = "Tempo restante";
$GLOBALS['PT-GENGAME-PROMPT-WORDLEVEL'] = "Nível: ";

$GLOBALS['PT-GENGAME-COUNTDOWN-SECONDS'] = "s";
$GLOBALS['PT-GENGAME-COUNTDOWN-END'] = "fim";

$GLOBALS['PT-GENGAME-TRICK'] = "Dica";

$GLOBALS['PT-GENGAME-30SECONDS-BUTTON'] = "pics/30sB.gif";
$GLOBALS['PT-GENGAME-OK-BUTTON'] = "pics/J2M-OKB.gif";
$GLOBALS['PT-GENGAME-BONUS-IMAGE'] = "pics/JDM-bonus.gif";

//------
$GLOBALS['PT-SIGNIN-CONNECT-SUBMIT'] = "Identificação";
$GLOBALS['PT-SIGNIN-CONNECT-PROMPT'] = "Por favor, identifique-se";
$GLOBALS['PT-SIGNIN-YOURNAME'] = "Usuário:";
$GLOBALS['PT-SIGNIN-YOURPWSD'] = "Senha (máx. 10 caract.):";
$GLOBALS['PT-SIGNIN-YOUREMAIL'] = "Email:";

$GLOBALS['PT-SIGNIN-REGISTER-SUBMIT'] = "Registrar-se";
$GLOBALS['PT-SIGNIN-REGISTER-PROMPT'] = "Não possui conta?";
$GLOBALS['PT-SIGNIN-REGISTER-WARNING'] = "<br/>Atenção, se seu email não for válido, não poderemos entrar em contato";

$GLOBALS['PT-SIGNIN-PWDFORGOT-SUBMIT'] = "Enviar";
$GLOBALS['PT-SIGNIN-PWDFORGOT-PROMPT'] = "Esqueceu a senha?";

// "bienvenue ‡ '$Nom' qui vient de s'inscrire."
$GLOBALS['PT-SIGNIN-WELCOME'] = "boas vindas a '<0>' que acabou de se registrar.";
$GLOBALS['PT-SIGNIN-THX-REGISTERING']  = "Obrigado por registrar-se, <0> . - você será redirigido para a <a href=\"jdm-accueil.php\">página inicial</a>";

$GLOBALS['PT-SIGNIN-INVALID-LOGIN'] = "login inválido - <a href=\"jdm-signin.php\">Voltar</a>";
$GLOBALS['PT-SIGNIN-OK-LOGIN'] = "OK - você será redirigido para a <a href=\"jdm-accueil.php\">página inicial</a>.";

$GLOBALS['PT-SIGNIN-NON-AVAILABLE'] = "Este nome de usuário já existe e não está disponível...";
$GLOBALS['PT-SIGNIN-USER-NOT-FOUND'] = "usuário inexistente";
$GLOBALS['PT-SIGNIN-PASSWD-FORGET-TITLE'] = "Sua senha para JeuxDeMots";
$GLOBALS['PT-SIGNIN-PASSWD-FORGET-BODY'] = "A sua senha é <0>";
$GLOBALS['PT-SIGNIN-PASSWD-FORGET-SENT'] = "Senha enviada para <0> <BR>";
$GLOBALS['PT-SIGNIN-PAGE-TITLE'] = "Conexão / Inscrição";
$GLOBALS['PT-SIGNIN-VALID-EMPTY-EMAIL'] = "<font color='#FF0000'>O e-mail não pode ser vazio</font>";
$GLOBALS['PT-SIGNIN-VALID-EMPTY-PASSWD'] = "<font color='#FF0000'>A senha não pode ser vazia</font>";
$GLOBALS['PT-SIGNIN-VALID-INVALID-EMAIL'] = "<font color='#FF0000'>O e-mail contém caracteres não autorizados. Verifique!</font>";
$GLOBALS['PT-SIGNIN-VALID-EMPTY-USERNAME'] = "<font color='#FF0000'>O nome de usuário não pode ser vazio</font>";


//------
// "Le $date, $what";
$GLOBALS['PT-EVENT-TMPL-DATE'] = "<0>, <1>";

//------

$GLOBALS['PT-MAIL-FROM'] = "JeuxDeMots";

$GLOBALS['PT-MAIL-LEVEL'] = "\nVocê possui <0> créditos, <1> pontos de honra e está no nível <2>.";

$GLOBALS['PT-MAIL-BOTTOM'] = "
	Até mais,    
    JeuxDeMots
    http://jeuxdemots.imag.fr/por
    
    Dica : <0>";

$GLOBALS['PT-MAIL-RESULT-TITLE'] = "Boas notícias de JeuxDeMots";
$GLOBALS['PT-MAIL-RESULT'] = "Olá <0>,
    É com grande alegria que comunicamos que você ganhou <1> crédito(s) com a palavra '<2>' 
    com a ajuda do seu colega <3>.
    
    <0>, suas respostas para essa partida foram: <4> .
    as respostas de <3> para essa partida foram: <5> .
    Portanto, as palavras que vocês têm em comum foram: <6> .
    
    A regra era: <7> <2>
    
    Além disso, você ganhou alguns pontos de honra e seu nível aumentou um pouco.
    ";

$GLOBALS['PT-MAIL-GIFT-TITLE'] = "Um presente de JeuxDeMots está esperando por você...";
$GLOBALS['PT-MAIL-GIFT'] = "Olá <0>,
    É com grande alegria que comunicamos que você acaba de ganhar um presente. 
    Vá para a feira para abri-lo. 
    ";

$GLOBALS['PT-MAIL-GIFT-DALTON'] = "Hey <0>, !
    Bart Simpson lhe deu um presente. Vá para a feira para abri-lo. 
    ";

$GLOBALS['PT-MAIL-PARRAINAGE-TITLE'] = "Convite para JeuxDeMots de <0>";
$GLOBALS['PT-MAIL-PARRAINAGE'] = "Olá, \n\n

(TO BE TRANSLATED)
Si vous savez que Jean-Pierre Foucault, Thierry Beccaro, Julien Lepers et Laurence Boccolini ne sont 
pas respectivement philosophe, sportif, chanteur et plante potagère du sud de l'Italie, vous venez 
de franchir la première étape pour participer à JeuxDeMots !

Vous rêviez de pouvoir enchaîner les « neuf points gagnants », le « quatre à la  suite » et le 
« face-à-face » tout en pouvant choisir un « 50 / 50 » ou « l'appel à l'ami Google » sans devoir 
assimiler les règles de Motus et sans vous faire sortir parce que « franchement, pas connaître le 
titre de la dernière chanson de Patrick Sébastien, ben, c'est carrément être trop nul » !!!
Et bien, JeuxDeMots est fait pour VOUS !

Vous voulez faire avancer la recherche sans devoir souscrire au 3637 
[mais ce n'est pas incompatible], devenir le Maître Capello du wikipedia ou le Richard Virenque 
de l'encyclopaedia universalis ou tout simplement aider le CNRS (sans les OGM) : 
JeuxdeMots est aussi fait pour VOUS !

World of Warcraft, Second Life ou Disneyland® Resort Paris vous font encore peur ou vous n'avez pas
pris le train du Web2.0 à destination des MySpace, Skyblog, FaceBook et autres réseaux sociaux : 
JeuxDeMot est tout de même fait pour VOUS !

Rejoignez nous et montrez ainsi à toute votre famille, vos amis et même à Madame Biglon
(votre ancienne maîtresse d'école) votre valeur ! Et si vous ne voulez rien montrer,
vous pouvez toujours prendre un pseudo !

Bref, si vous voulez faire partie d'une aventure avant que Google ne la rachète : venez participer 
à JeuxDeMots et inscrivez vous : http://www.lirmm.fr/jeuxdemots\n\n

\"<0>\" pense que vous êtes digne de nous rejoindre !\n";

$GLOBALS['PT-MAIL-OTHER-PLAYER-BEWARE'] = 'ATENÇÃO';

$GLOBALS['PT-MAIL-OTHER-PLAYER-RULES'] = "
Ao enviar este email, você está concordando em:
       <br>(1) comunicar o seu endereço de email ao destinatário, 
       a fim de que ele possa responder (nesse caso, você conhecerá o endereço
       de email dele) ;
       <BR>(2) salvar esta mensagem no servidor durante um certo tempo;
       <BR>(3) gastar 500 créditos para os custos do correio 
       <BR> Caso sejam constatados abusos, essa funcionalidade será desativada.";

$GLOBALS['PT-MAIL-OTHER-PLAYER-FROM-TO'] = "De '<0>' para '<1>'";
$GLOBALS['PT-MAIL-OTHER-PLAYER-TITLE'] = "Assunto:";
$GLOBALS['PT-MAIL-OTHER-PLAYER-MESSAGE'] = "Mensagem:";
$GLOBALS['PT-MAIL-OTHER-PLAYER-SEND-BUTTON'] = "Enviar";
$GLOBALS['PT-MAIL-OTHER-PLAYER-MESSAGE-SENT'] = "Menssagem enviada";
$GLOBALS['PT-MAIL-OTHER-PLAYER-NOT-ENOUGH-CREDITS'] = "Crédito insuficiente";

//------

$GLOBALS['PT-MISC-CONTACT'] = "Contato";
$GLOBALS['PT-MISC-CONTACT-EMAIL'] = "mailto:jeux.de.meaux@gmail.com?subject=JeuxDeMots feedback";
$GLOBALS['PT-MISC-HOME'] = "Home";
$GLOBALS['PT-MISC-FORUM'] = "Fórum";
$GLOBALS['PT-MISC-RANKING'] = "Ranking";
$GLOBALS['PT-MISC-EVENTS'] = "Eventos";
$GLOBALS['PT-MISC-SOUK'] = "Feira";
$GLOBALS['PT-MISC-OPTIONS'] = "Opções";
$GLOBALS['PT-MISC-HELP'] = "Ajuda";

$GLOBALS['PT-MISC-CREDIT'] = "Créditos: ";
$GLOBALS['PT-MISC-HONNOR'] = "Honra: ";
$GLOBALS['PT-MISC-LEVEL'] = "Nível: ";

$GLOBALS['PT-MISC-LOGIN'] = "Sair";
$GLOBALS['PT-MISC-LOGOUT'] = "Identificar-se";

$GLOBALS['PT-MISC-PLAYAS-PLAY'] = "Jogar";
$GLOBALS['PT-MISC-PLAYAS-GUEST'] = " como convidado";
$GLOBALS['PT-MISC-PLAYAS-GUESTWARNING'] = "Você está jogando como convidado e não está contribuindo para o jogo. Por favor identifique-se.";

//------

$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-0'] = "Escreva IDEIAS ASSOCIADAS ao termo abaixo:";

$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-3'] = "Escreva TEMAS/DOMÍNIOS (por exemplo: 'Esportes', 'Medicina', 'Cinema', 'Gastronomia,' etc.) para a palavra abaixo:";
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-5'] = "Escreva SINÔNIMOS para a palavra abaixo (por exemplo: 'retrato', 'foto' para 'fotografia'):";
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-6'] = "Escreva GENÉRICOS (por examplo: 'veículo' para 'carro', 'mamífero', 'animal' para 'cachorro') para a palavra abaixo:";
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-7'] = "Escreva CONTRÁRIOS (por examplo: 'frio' para 'quente', 'alto' para 'baixo') para a palavra abaixo:";
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-8'] = "Escreva ESPECÍFICOS (por examplo: 'gato', 'cachorro', 'animal doméstico',  etc. para 'animal' - ou então 'carro', 'trem', 'nave espacial', etc. para 'veículo') para a palavra abaixo:";
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-9'] = "Escreva PARTES (por exemplo: 'motor', 'roda', etc. para 'carro' - ou ainda 'capa', 'página', 'capítulo' etc. para 'livro') para a palavra abaixo:";
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-10'] = "Escreva TODOS (por exemplo: 'corpo', 'braço', etc. para 'cotovelo' - ou ainda 'banco' para 'caixa') para a palavra abaixo:";
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-11'] = "Escreva LOCUÇÕES (por exemplo: 'pulo do gato', 'comprar gato por lebre', etc. para 'gato') para a palavra abaixo:";
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-13'] = "Escreva SUJEITOS típicos (por exemplo: 'gato', 'animal', 'pessoa', etc. para 'comer') para o verbo abaixo:";
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-14'] = "Escreva OBJETOS típicos (por exemplo: 'carne', 'frutas', 'bombom', etc. para 'comer') para o verbo abaixo:";
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-15'] = "Escreva LUGARES típicos (por exemplo: 'campo', 'estrebaria', 'jockey clube', etc. para 'cavalo') para a palavra abaixo:";

$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-16'] = "Escreva INSTRUMENTOS típicos (por exemplo: 'pá', 'retroescavadeira', 'mão', para 'cavar') para o verbo abaixo:";
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-17'] = "Escreva CARACTERÍSTICAS típicas (por exemplo: 'líquido', 'branco', 'bebível', etc. para 'leite') para a palavra abaixo:";
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-20'] = "O que é MAIS INTENSO (por exemplo: 'chuva forte', 'tromba d'água', etc. para 'chuva' - ou ainda 'viver intensamente' para 'viver') para a palavra abaixo:";
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-21'] = "O que é MENOS INTENSO (por exemplo: 'casinha', 'casebre', etc. para 'casa' - ou ainda 'caminhar lentamente', 'vagar' para 'caminhar') para a palavra abaixo:";

$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-22'] = "Escreva palavras da mesma FAMÍLIA (por exemplo: 'desligar', 'ligação', 'desligamento' para 'ligar') para a palavra abaixo:"; #???? CARLOS RAMISCH

$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-23'] = "WHAT could typically possess the following CHARACTERISTICS (for example: 'water', 'wine', 'milk' for 'liquid') :";
		
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-24'] = "WHAT can typicaly do the following SUBJECT (for example: 'eat', 'sleep', 'hunt' pour 'lion') :";
		
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-25'] = "WHAT can we DO with the following INSTRUMENT (for example: 'write', 'draw'  pour 'pen') :";
		
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-26'] = "Which ACTION can sustain the folowing OBJECT (for example: ...) :";
		
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-27'] = "Give TERMS belonging to the following DOMAIN (for example: 'goal', 'penalty' pour 'Football') :";

$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-28'] = "WHAT can we typically FIND in the following PLACE (for example: 'fish', 'shell', 'water', 'salt' pour 'sea') :";
		
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-30'] = "WHAT can we typically DO in the following PLACE (for example: 'eat', 'drink', 'order' pour 'restaurant' -- verbs are requested) :";
		
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-31'] = "In which PLACES can we do the following thing (for example: 'restaurant', 'kitchen', 'fast-food' for 'eat' -- places are requested) :";
		
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-32'] = "What kind of SENTIMENTS/EMOTIONS does the following term does to you?";

$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-34'] = "HOW can be done the following action:<br/><font size=\"-1\">It will be an adverb or an equivalent, eg: 'rapidement', 'sur le pouce', 'goulûment', 'salement' ... for 'manger'</font>";

$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-35'] = "What MEANING can you give to the following term:<br/><font size=\"-1\">It will be a term evoking each of the possible meanings, eg: 'forces de l'ordre', 'contrat d'assurance', 'police typographique', ... for 'police'</font>";

$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-37'] = "Which GOAL/FUNCTION (telic role) can you give to the following term:<br/><font size=\"-1\">It will be a verb, eg: 'couper' for 'couteau', 'lire' for 'livre', ...</font>";
$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-38'] = "Which CREATION MODES (agentive role) can you give to the following term:<br/><font size=\"-1\">It will be a verb, eg: 'construire' for 'maison', 'rédiger'/'imprimer' for 'livre', ...</font>";

$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-41'] = "Which CONSEQUENCES (A causes B) can you give to the following term:<br/><font size=\"-1\">It will be a verb or a noun, eg:  'tomber' -> 'se blesser', 'faim' -> 'voler'/'dérober', ...</font>";

$GLOBALS['PT-GENGAME-FUNCTIONS-INSTR-42'] = "Which CAUSES (A has for cause B) can you give to the following term:<br/><font size=\"-1\">It will be a verb or a noun, eg: 'se blesser' -> 'tomber', 'voler'' -> 'faim', 'pauvreté', ...</font>";

$GLOBALS['PT-GENGAME-FUNCTIONS-ARRAY-3-INSTR'] = "Lista não exaustiva de domínios (se nenhum domínio é adequado, sugira outro ou escreva *** caso não haja um domínio em particular) :";

$GLOBALS['PT-GENGAME-FUNCTIONS-ARRAY-3'] =   array ("anatomia", "arquitetura", "astronomia", "audiovisual", "automovél", "aviação", "belas artes", "biologia", 
"botânica", "carnes", "ferrovias", "química", "cirurgia", "cinema", "comércio", "costura", "cozinha", "direito", "economia", "eletricidade", "pecuária", "equitação", 
"geografia", "geologia", "gramática", "história", "hidrologia", "imprensa", "informática", "jogos", "linguística", "literatura", "marinha", "matemática", "mecânica", 
"medicina", "militar", "mineralogia", "música", "mitologia", "ótica", "pintura", "farmácia", "filosofia", "fotografia", "fisiologia", "física", "política", 
"psicologia", "religião", "escolar", "sexologia", "sociologia", "espetáculo", "esportes", "agronomia", "técnico", "telecomunicações", "urbanismo", "poesia", "veterinária",
"viticultura", "zoologia");


$GLOBALS['PT-TERMSELECT-FUNCTIONS-NAME-TEMPLATE'] = 'noun%';
$GLOBALS['PT-TERMSELECT-FUNCTIONS-ADJ-TEMPLATE'] = 'adj%';
$GLOBALS['PT-TERMSELECT-FUNCTIONS-ADV-TEMPLATE'] = 'adv%';
$GLOBALS['PT-TERMSELECT-FUNCTIONS-ADV-TEMPLATE'] = 'verb%';

$GLOBALS['PT-READ-INSTRUCTIONS'] = 'Por favor, leia as instruções!';

//------

$GLOBALS['PT-OPTIONS-TITLE'] = "JeuxDeMots: opções";
$GLOBALS['PT-OPTIONS-PROMPT'] = "Opções";
$GLOBALS['PT-OPTIONS-CSS-FORM'] = 	"CSS :<BR>
	<input <0> id=\"go_css_submit\" type=\"submit\" name=\"go_css_submit\" value=\"Mudar\"> a URL
	<input  id=\"go_css_url\" type=\"text\" size = \"120\" name=\"go_css_url\" value=\"<1>\">
	(por default use \"<2>\")";
$GLOBALS['PT-OPTIONS-CSS-EXAMPLE'] = "Exemplo de css :";
$GLOBALS['PT-OPTIONS-CSS-MUSEUM-FORM'] = 	"CSS do seu pequeno museu :<BR>
	<input <0> id=\"go_css_museum_submit\" type=\"submit\" name=\"go_css_museum_submit\" value=\"Mudar\"> a URL
	<input id=\"go_css_museum_url\" type=\"text\" size = \"120\" name=\"go_css_museum_url\" value=\"<1>\">";

$GLOBALS['PT-OPTIONS-PERSO-FORM'] = 	"Sua URL:<br/>
	<input <0> id=\"go_perso_submit\" type=\"submit\" name=\"go_perso_submit\" value=\"Mudar\"> a URL
	<input  id=\"go_perso_url\" type=\"text\" size = \"120\" name=\"go_perso_url\" value=\"<0>\">
	(por exemplo \"http://myhomepage.com\")";


//------

$GLOBALS['PT-HOME-TITLE'] = "JeuxDeMots: Home";
$GLOBALS['PT-HOME-PROMPT'] = "Home";

$GLOBALS['PT-HOME-NEWS'] = "<P><blockquote>NOVIDADES <0>";

$GLOBALS['PT-HOME-INFO'] = "informação";

$GLOBALS['PT-HOME-HOT-LINKS'] = "Links";

$GLOBALS['PT-HOME-INSTRUCTIONS-TITLE'] = "Como funciona? O que devo fazer? ";

$GLOBALS['PT-HOME-INSTRUCTIONS'] = "Um termo (palavra) será apresentada juntamente
com as instruções. Durante um minuto, você deve escrever tantas sugestões quanto
você conseguir, seguindo as instruções. Na maioria das vezes (mas não sempre), o
jogo vai pedir para você escrever livremente ideias associadas ao termo dado.
Confirme cada sugestão clicando em \"Enviar\" ou pressionando a tecla \"Enter\".
Outros jogadores irão sugerir palavras para esse mesmo termo. Você ganha créditos
quando suas sugestões e as sugestões de outro jogador para uma mesma partida possuem
palavras em comum. Quanto mais específica e apropriada for a sugestão, mais chances
você tem de ganhar... se o outro jogador também tiver pensado nela!<BR><BR>
Por favor leia os <a href=\"jdm-signin.php\">termos de JeuxDeMots</a>.<P><P>";

$GLOBALS['PT-HOME-MOSTWANTED'] = 'Mais procuradas';
$GLOBALS['PT-HOME-COLLECTIONS'] = 'Coleções';
$GLOBALS['PT-HOME-GIFT-WAITING'] = "Presentes estão à sua espera na <A href=\"souk.php#giftlist\">feira</a>";
$GLOBALS['PT-HOME-RELATION-COUNT'] = "<0> relações foram produzidas até o momento, das quais <1> são palavras proibidas. Parabéns!<P>";
$GLOBALS['PT-HOME-TERM-COUNT'] = "... e <0> termos.";
$GLOBALS['PT-HOME-ACTIVE-PLAYER-COUNT'] = "<br /><br /><0> jogador conectado.";
$GLOBALS['PT-HOME-ACTIVE-PLAYERS-COUNT'] = "<br /><br /><0> jogadores conectados.";
$GLOBALS['PT-HOME-OTHER-ANNOUNCES'] = "Outros anúncios.";
$GLOBALS['PT-HOME-NO-PARTICULAR-THEME-ANCHOR'] =  "Você não escolheu um <a href=\"souk.php\">tema</a>";
$GLOBALS['PT-HOME-PREFERRED-THEME-ANCHOR'] =   "Seu <a href=\"souk.php\">tema</a> favorito é <0>";
$GLOBALS['PT-HOME-WAITING-TRIALS'] =  "Você possui <0> <a href=\"jdm-list-trial.php\">processos em espera</a>";
$GLOBALS['PT-HOME-LOTTERY'] = "Loteria";
$GLOBALS['PT-HOME-EASY-WORDS'] = "Palavras fáceis";
$GLOBALS['PT-HOME-GAME-THEMES'] = "Temas de jogo";

$GLOBALS['PT-HOME-PLAY-BUTTON'] = "pics/jdm-logo-jogar.png";

$GLOBALS['PT-HOME-MEAN-GAIN'] = "Você ganha em média <0> créditos";
$GLOBALS['PT-HOME-GAMES-TOKENS'] = "Você tem <0> partidas e <1> tokens em espera";

//------

$GLOBALS['PT-SOUK-TITLE'] = "JeuxDeMots: feira";
$GLOBALS['PT-SOUK-PROMPT'] = "Feira das mil maravilhas: ";

$GLOBALS['PT-SOUK-YOUR-HUMOR'] = "Seu humor:";
$GLOBALS['PT-SOUK-SHARE-HUMOR'] = "Compartilhar";
$GLOBALS['PT-SOUK-SHARED-HUMOR'] = "Humor compartilhado!";

$GLOBALS['PT-SOUK-PREFERRED-THEME'] = "Seu tema favorito:";
$GLOBALS['PT-SOUK-THEME-VALIDATE'] = "Validar";
$GLOBALS['PT-SOUK-VALIDATED-THEME'] = "Tema validado";
$GLOBALS['PT-SOUK-EMPTY-FIELD-THEME'] = "(deixar vazio para excluir o tema)";
$GLOBALS['PT-SOUK-NO-PARTICULAR-THEME'] = "Você não escolheu um tema.";
$GLOBALS['PT-SOUK-NO-EXISTING-THEME'] = "Este tema não existe, por favor escolha outro!";

$GLOBALS['PT-SOUK-PRESENTS'] = "Presentes";

$GLOBALS['PT-SOUK-SUBTITLE-COMP'] = "Comprando competências";
$GLOBALS['PT-SOUK-SUBTITLE-TREAS'] = "Gerenciando tesouros";
$GLOBALS['PT-SOUK-SUBTITLE-TRIALS'] = "tribunal";

$GLOBALS['PT-SOUK-SMALL-GAMES'] = 'Partidas pequenas';
$GLOBALS['PT-SOUK-TRIALS'] = 'Processos';
$GLOBALS['PT-SOUK-DUELS'] = 'Duelos';
$GLOBALS['PT-SOUK-VARIOUS'] = 'Diversos'; 

$GLOBALS['PT-SOUK-DEFIS-NOTERM'] = "A palavra '<0>' não existe!";
$GLOBALS['PT-SOUK-DEFIS-NOTFREE'] = "A palavra <0> está disponível!";
$GLOBALS['PT-SOUK-DEFIS-NOGUEST'] = "O jogador convidado não pode capturar uma palavra";
$GLOBALS['PT-SOUK-DEFIS-NOTENOUGHMONEY'] = "Seu investimento deve ser de ao menos 5 vezes o valor da palavra (<1>)";
$GLOBALS['PT-SOUK-DEFIS-NOMONEY'] = "Você não possui créditos suficientes";
$GLOBALS['PT-SOUK-DEFIS-NOTNOW'] = "Você não pode capturar essa palavra agora, por favor tente novamente mais tarde";

$GLOBALS['PT-SOUK-COMP-ITEM'] = "Competência";
$GLOBALS['PT-SOUK-COMP-HONNOR-MIN'] = "Honra mín.";
$GLOBALS['PT-SOUK-COMP-CREDIT'] = "Créditos";
$GLOBALS['PT-SOUK-COMP-STATUS'] = "Status";
$GLOBALS['PT-SOUK-COMP-QUOT'] = "Citação";
$GLOBALS['PT-SOUK-COMP-SELECPC'] = "Seleção %";
$GLOBALS['PT-SOUK-COMP-ADJUSTPC'] = "Ajustar %";

$GLOBALS['PT-SOUK-COMP-EQUIPROBABILIZE'] = "Probabilidades iguais";
$GLOBALS['PT-SOUK-COMP-ALL-COMPETENCES'] = "(5k Cr) todas as competências.";


$GLOBALS['PT-SOUK-NEW-IMAGE'] = "pics/new.gif";




$GLOBALS['PT-SOUK-LIST-TRIAL-COMPLETED-FORM'] = "<li><form id=\"open_cr_trial<0>\" name=\"open_trial<0>\" method=\"post\" action=\"trialVote.php\" >
				<input <1> id=\"trial_vote_cr\" type=\"submit\" name=\"trial_vote_cr\" value=\"Mostrar\">
				<input  id=\"proc_id\" type=\"hidden\" name=\"proc_id\" value=\"<2>\">
		 		processo <2> entre '<3>' e '<4>' para o termo '<5>'.
				</form></li>";

$GLOBALS['PT-SOUK-LIST-TRIAL-TODO-FORM'] ="<li><form id=\"open_trial<0>\" name=\"open_trial<0>\" method=\"post\" action=\"trialVote.php\" >
		<input id=\"trial_vote\" type=\"submit\" name=\"trial_vote\" value=\"Votar\">
		<input  id=\"proc_id\" type=\"hidden\" name=\"proc_id\" value=\"<1>\">
		 - o prazo expira em <2> horas.
		</form></li>";

$GLOBALS['PT-MAKE-TRIAL-FORM'] = "<form id=\"form-trial\" name=\"form-trial\" method=\"post\" action=\"generateResult_maketrial_process.php\" >
			Mover um <input <0> id=\"submit-buy-trial\" type=\"submit\" name=\"submit-buy-trial\" value=\"processo\"> 
				 (500 Cr) contra este jogador
			 <input id=\"trial_id\" type=\"hidden\" name=\"trial_id\" value=\"<1>\">
			 - indicar o motivo:
			 <textarea name=\"trial_reason\" cols=40 rows=6></textarea>
			 </form>";
$GLOBALS['PT-RESULT-MAKE-TRIAL'] = "Processar";
	$GLOBALS['PT-RESULT-TRIAL'] = "processo";


	//------ Le souk by MM
	// global warnings
	$GLOBALS['PT-SOUK-WARNING-BADPLAYER'] = "Este jogador não existe";
	$GLOBALS['PT-SOUK-WARNING-BADTERM'] = "Esta palavra não existe";
	$GLOBALS['PT-SOUK-WARNING-NOCASH'] = "Você não possui créditos suficientes";
	$GLOBALS['PT-SOUK-WARNING-NOHONNOR'] = "Você não possui honra suficiente";
	$GLOBALS['PT-SOUK-WARNING-NOGUEST'] = "O jogador convidado não pode realizar esta ação. Registre-se!";
	$GLOBALS['PT-SOUK-WARNING-NOCREDIT'] = "Nós não vendemos fiado!";

	//"le joueur '$player' a offert un cadeau au joueur '$dest'."
	$GLOBALS['PT-SOUK-GIFT-EVENT'] = "jogador '<0>' deu um presente para '<1>'.";
	$GLOBALS['PT-SOUK-BUYCOMP-EVENT'] = "jogador '<0>' comprou uma competência.";
	$GLOBALS['PT-SOUK-HYPEWORDS'] = "palavras \"hype\"";
	$GLOBALS['PT-SOUK-INWORDS'] = "palavras \"in\"";
	$GLOBALS['PT-SOUK-OUTWORDS'] = "palavras \"out\"";

	// unused?
	$GLOBALS['PT-SOUK-HYPEWORDS-VIEW-BUTTON'] = "Ver";
	$GLOBALS['PT-SOUK-HYPEWORDS-VIEW-CREDITS'] = "Cr";
	$GLOBALS['PT-SOUK-HYPEWORDS-VIEW-CONTENT'] = "Palavras hype.";

	$GLOBALS['PT-SOUK-HYPEWORDS-VIEW-FORM'] = "<input id=\"formhypewordsumit\" type=\"submit\" name=\"formhypewordsumit\" value=\"Ver\"> (10 Cr)
			palavras hype.";

	$GLOBALS['PT-SOUK-CAGNOTTE-VIEW-FORM'] = "	<input id=\"cagnotesubmit\" type=\"submit\" name=\"cagnotesubmit\" value=\"Ver\"> (5 Cr)
			o valor do prêmio da loteria.";

	$GLOBALS['PT-SOUK-CAGNOTTE-DISPLAY-WARNING'] = "A loteria contém <0> créditos. 
			    Os jogadores <1> e <2> estão na frente com um score de <3> créditos. 
			    A loteria será sorteada daqui a <4> jackpot(s)"; 

	$GLOBALS['PT-SOUK-GAME-GIFT-FORM'] = "<input id=\"formmakegift\" type=\"submit\" name=\"formmakegift\" value=\"Presentear\"> (100 Cr)
			uma partida com o termo
			<input id=\"giftterm\" type=\"text\" name=\"giftterm\" value=\"<0>\">
			para o jogador
			<input id=\"giftplayername\" type=\"text\" name=\"giftplayername\" value=\"<1>\"> 
			<input id=\"relation_type_gift\" type=\"hidden\" name=\"relation_type_gift\" value=\"0\">";


	$GLOBALS['PT-SOUK-GAME-GIFT-WARNING-NOINPUT'] = "Escreva um termo e um nome de um jogador";
	$GLOBALS['PT-SOUK-GAME-GIFT-WARNING-SELFGIFT'] = "Seria bom se fosse possível, mas você não pode dar um presente a si mesmo";
	$GLOBALS['PT-SOUK-GAME-GIFT-WARNING-NOERROR'] = "Presente comprado e enviado";

	$GLOBALS['PT-SOUK-GAME-GIFT-LIST'] = "Presentes à espera :";
	$GLOBALS['PT-SOUK-GAME-GIFT-LIST-EMPTY'] = "Você não ganhou nehum presente";

	$GLOBALS['PT-SOUK-GAME-GIFT-OPEN-FORM'] = "<input id=\"opengiftbut\" type=\"submit\" name=\"opengiftbut\" value=\"Abrir\">
			 um presente oferecido por <0> em <1>";

	$GLOBALS['PT-SOUK-GRAPH-VIEW-FORM'] = " <input id=\"opengiftbut\" type=\"submit\" name=\"opengiftbut\" value=\"Ver\">
	       (0 Cr) o grafo.";
	$GLOBALS['PT-SOUK-TRIAL-VIEW-FORM'] = " <input id=\"viewtrial\" type=\"submit\" name=\"viewtrial\" value=\"Ver\"> (0 Cr) os processos.";
	$GLOBALS['PT-SOUK-GIFT-VIEW-FORM'] = " <input id=\"viewgift\" type=\"submit\" name=\"viewgift\" value=\"Ver\"> (0 Cr) meus presentes.";

	$GLOBALS['PT-SOUK-WORD-LOOKUP-FORM'] = "  <input id=\"gotermlist\" type=\"submit\" name=\"gotermlist\" value=\"Procurar\">
	       (0 Cr) palavras contendo os caracteres 
		    <input  id=\"goterm\" type=\"text\" name=\"goterm\" value=\"<0>\">";
		    
	$GLOBALS['PT-SOUK-WORDLIST-DISPLAY-WARNING-NOINPUT'] = "Entre uma palavra não vazia";

	$GLOBALS['PT-SOUK-PARRAINAGE-FORM'] = "	  <input id=\"parainage_submit\" type=\"submit\" name=\"parainage_submit\" value=\"Convidar\">
	      (0 Cr) um jogador com o seguinte endereço de email:
		  <input  size=\"50\" id=\"parainage_email\" type=\"text\" name=\"parainage_email\" value=\"john@doe.swh\">
		  ";

	$GLOBALS['PT-SOUK-PARRAINAGE-WARNING-NOERROR'] = "Convite enviado (email não verificado)";
	$GLOBALS['PT-SOUK-PARRAINAGE-WARNING-WRONGEMAIL'] = "Tente outro endereço de email...";

	$GLOBALS['PT-SOUK-GENERATE-DEFIS-FORM'] = "<input id=\"parainage_submit\" type=\"submit\" name=\"defis_submit\" value=\"Capturar\"> a palavra
		  <input  size=\"20\" id=\"parainage_email\" type=\"text\" name=\"defis_word\" value=\"word\">
		por um total de  
		<input  size=\"20\" id=\"parainage_email\" type=\"text\" name=\"defis_amount\" value=\"1000\">
		créditos.";

	$GLOBALS['PT-SOUK-WORD-CAPTURE-FORM'] = "<font color=\"red\">pronto para capturar</font> 
	<input id=\"chosengamesubmit\" type=\"submit\" name=\"chosengamesubmit\" value=\"Ir!\">
			";

	$GLOBALS['PT-SOUK-WORD-CAPTURE-EVENT'] = "jogador &#39;<0>&#39; está tentando capturar a palavra &#39;<1>&#39;.";

	$GLOBALS['PT-SOUK-CHECK-TREASURE-FORM'] = 	"<input id=\"gochecktreasuressubmit\" type=\"submit\" name=\"gochecktreasuressubmit\" value=\"Mostrar\">
		os tesouros do jogador
		<input  id=\"gochecktreasures\" type=\"text\" name=\"gochecktreasures\" value=\"\">";

	$GLOBALS['PT-SOUK-BUYCOMP-SUBMIT'] = "Comprar";
	$GLOBALS['PT-SOUK-BUYCOMP-DONE'] = "comprado";

	$GLOBALS['PT-SOUK-WORD-RELATION-FORM'] = " <input id=\"gotermsubmit\" type=\"submit\" name=\"gotermsubmit\" value=\"Procurar\"> (DEBUG) a palavra
		    <input  id=\"gotermrel\" type=\"text\" name=\"gotermrel\" value=\"\">";

	$GLOBALS['PT-SOUK-GAMELIST-FORM'] = "<input id=\"gamelist_submit\" type=\"submit\" name=\"gamelist_submit\" value=\"partidas em andamento\"> (DEBUG)";

	$GLOBALS['PT-SOUK-CHOSENGAME-FORM'] = "<input id=\"chosengamesubmit\" type=\"submit\" name=\"chosengamesubmit\" value=\"Jogar\">";

	$GLOBALS['PT-SOUK-STATISTICS-FORM'] = "<input id=\"bd_stats_submit\" type=\"submit\" name=\"bd_stats_submit\" value=\"Estatísticas\"> (DEBUG)";

	$GLOBALS['PT-SOUK-CHECKOWNER-FORM'] = "	<input id=\"gotermownersubmit\" type=\"submit\" name=\"gotermownersubmit\" value=\"Verificar\"> o status da palavra
		<input  id=\"gotermowner\" type=\"text\" name=\"gotermowner\" value=\"\">";


	$GLOBALS['PT-SOUK-PROB-WARNING-NOERROR'] = "Probabilidade ajustada";
	$GLOBALS['PT-SOUK-COMP-WARNING-NOERROR'] = "Relação comprada";
	
	$GLOBALS['PT-SOUK-WARNING-CANT-GIFT'] = "Você não pode dar presentes por enquanto.";
	$GLOBALS['PT-SOUK-BUY-TRIAL-TOKEN-NOCREDIT'] = "Sinto muito, você não possui <0> créditos";
	$GLOBALS['PT-SOUK-BUY-TRIAL-TOKEN-CONFIRM'] = "Token de processo comprado";

	$GLOBALS['PT-SOUK-BUY-TRIAL-TOKEN-STATUS'] = "Você já tem <0> tokens de processos. Você pode <input <1> id=\"submit_buy_token\" type=\"submit\" name=\"submit_buy_token\" value=\"comprar\"> mais um por <2> créditos.";
	$GLOBALS['PT-SOUK-RELATIONS-OCCURS'] = "<0> occorrências de relações <1> (<2> - <3>)";
	$GLOBALS['PT-SOUK-NODES-OCCURS'] = "<0> ocorrências de nodos <1> (<2>)";

	$GLOBALS['PT-SOUK-ARTEFACTS-TITLE'] = "Artefatos";
	$GLOBALS['PT-SOUK-PLAY-LOTTERY'] = "\n<input  type=\"submit\" name=\"submit\" value=\"Jogar na loteria\"> (<0> Cr)";
	$GLOBALS['PT-SOUK-CHOSE-EASY'] = "\n<input  type=\"submit\" name=\"submit\" value=\"Escolher palavras\"> fáceis (grátis!)";
	$GLOBALS['PT-SOUK-CHOSE-HARD'] = "\n<input  type=\"submit\" name=\"submit\" value=\"Escolher palavras\"> difíceis (grátis!)";
	$GLOBALS['PT-SOUK-CHOSE-THEME'] = "\n<input  type=\"submit\" name=\"submit\" value=\"Palavras temáticas\"> diversas (grátis!)";
	$GLOBALS['PT-SOUK-CRAZY-QUESTION'] = "\n<input  type=\"submit\" name=\"submit\" value=\"Perguntas maluquinhas\"> (grátis!)";

	//------

	$GLOBALS['PT-EVENT-TITLE'] = "JeuxDeMots: eventos";
	$GLOBALS['PT-EVENT-PROMPT'] = "Eventos";

	// "les joueurs '$gamecreatorname' et '$playername' ont gagnÈ <b>$points</b> crÈdits avec <b>'$fentry'</b>  pour la compÈtence '$relgpname'. Jackpot !"
	$GLOBALS['PT-EVENT-WIN-JACKPOT'] = "jogadores '<0>' e '<1>' ganharam <b><2></b> créditos com <b>'<3>'</b> 
	para a relação '<4>'. Jackpot !";

	// "les joueurs '$gamecreatorname' et '$playername' ont gagnÈ <b>$points</b> crÈdits avec <b>'$fentry'</b>."
	$GLOBALS['PT-EVENT-WIN'] = "jogadores '<0>' e '<1>' ganharam <b><2></b> créditos com <b>'<3>'</b>.";

	$GLOBALS['PT-EVENT-JACKPOT-TMPL'] = "Um prêmio!";
	$GLOBALS['PT-EVENT-TRIALWON-TMPL'] = "ganhou um processo";
	$GLOBALS['PT-EVENT-TRIALNO-TMPL'] = "perdeu um processo";
	$GLOBALS['PT-EVENT-NEWPLAYER-TMPL'] = ", benvindo a ";
	$GLOBALS['PT-EVENTS-CAP'] = "<0> ajudou a alcançar a marca de <1> relações. Bravo ! + <2> Honra";	
	$GLOBALS['PT-EVENT-CAP-TMPL'] = "ajudou a alcançar a marca de";	

	//------
	
	$GLOBALS['PT-TRIALVOTE-INSTRUCTIONS'] = "Dadas estas respostas, você considera que o jogador respeitou as regras?";
	$GLOBALS['PT-TRIALVOTE-TITLE'] = "julgar um processo";
	$GLOBALS['PT-TRIALVOTE-THANKS'] = "obrigado pelo seu voto";
	$GLOBALS['PT-TRIALVOTE-THANKSABS'] = "obrigado por se abster";
	$GLOBALS['PT-TRIALVOTE-INSTRWERE'] = "As instruções eram:";	
		
	$GLOBALS['PT-TRIALVOTE-MAIL-TITLE'] = "Você é jurado em um processo";
	$GLOBALS['PT-TRIALVOTE-MAIL-BODY'] = "Olá <0>, 
				
	Você foi escolhido para atuar como jurado de um processo. Para votar, acesse a página home...

	";
	$GLOBALS['PT-TRIALVOTE-PENDING-TRIAL'] = "Processos em espera:";
	$GLOBALS['PT-TRIALVOTE-NO-TRIAL'] = "Não há nenhum processo em espera.";
	$GLOBALS['PT-TRIALVOTE-50-LAST'] = "Os últimos 50 processos encerrados:";
	$GLOBALS['PT-TRIALVOTE-NO-GUEST'] = " não acessível ao jogador convidado";
	$GLOBALS['PT-TRIALVOTE-NO-TRIAL'] = "Nenhum processo";
	$GLOBALS['PT-TRIALVOTE-JUDGE'] = "o juiz";
	$GLOBALS['PT-TRIALVOTE-JUDGE-GIFT'] = "o juiz deu um presente para ";
	$GLOBALS['PT-TRIALVOTE-EVENT'] = "o jogador '<0>' ganhou um processo contra o jogador '<1>' com a palavra '<2>'.";
	$GLOBALS['PT-TRIALVOTE-WIN-TITLE'] = "Processo ganho";
	$GLOBALS['PT-TRIALVOTE-WIN-BODY'] = "Olá <0>\nÉ com alegria que informamos que você ganhou um processo (núm. <1>) contra o jogador '<2>' pela palavra '<3>'. \nAcesse a feira para verificar os votos do seu processo.\n";
	$GLOBALS['PT-TRIALVOTE-LOSE-TITLE'] = "Processo perdido";
	$GLOBALS['PT-TRIALVOTE-LOSE-BODY'] = "Olá <0>\nÉ com pesar que informamos que você perdeu um processo (núm. <1>) contra o jogador '<2>' pela palavra '<3>'. \nAcesse a feira para verificar os votos do seu processo.\n";
	$GLOBALS['PT-TRIALVOTE-REASON'] = "Motivo do processo:";
	$GLOBALS['PT-TRIALVOTE-ANSWERS'] = "As respostas propostas pelo acusado foram:";
	$GLOBALS['PT-TRIALVOTE-DOUBT'] = "Liberado por falta de provas";
	$GLOBALS['PT-TRIALVOTE-GUILTY'] = "Culpado";
	$GLOBALS['PT-TRIALVOTE-INNOCENT'] = "Inocente";
	$GLOBALS['PT-TRIALVOTE-ABSTAIN'] = "Abster-se";
	$GLOBALS['PT-TRIALVOTE-N-VOTES'] = "<0> votos";
	$GLOBALS['PT-TRIALVOTE-INQUIRER'] = "Acusador <b><0></b> contra acusado <b><1></b>";
	$GLOBALS['PT-TRIALVOTE-REASON-SHORT'] = "Motivo: <b><0></b><P>";
	$GLOBALS['PT-TRIALVOTE-INSTR'] = "Instruções: <B><0></b><p>";
	$GLOBALS['PT-TRIALVOTE-TERM'] = "Termo: <B><0></B><P>";	
	
	//------

	$GLOBALS['PT-RANKING-TITLE'] = "JeuxDeMots: ranking";
	$GLOBALS['PT-RANKING-PROMPT'] = "Ranking";
	$GLOBALS['PT-RANKING-DISPLAY-SCORES'] = "scores";
	$GLOBALS['PT-RANKING-DISPLAY-TREASURES'] = "tesouros";



	$GLOBALS['PT-RANKING-LABEL-NAME'] = "Nome";
	$GLOBALS['PT-RANKING-LABEL-HONNOR'] = "Honra";
	$GLOBALS['PT-RANKING-LABEL-HONNORMAX'] = "Honra máx";
	$GLOBALS['PT-RANKING-LABEL-CREDITS'] = "Créditos";
	$GLOBALS['PT-RANKING-LABEL-NBPLAYED'] = "Partidas jogadas";
	$GLOBALS['PT-RANKING-LABEL-LEVEL'] = "Nível";
	$GLOBALS['PT-RANKING-LABEL-EFF'] = "Eficiência";
	$GLOBALS['PT-RANKING-LABEL-NBTREASURE'] = "Tesouros";

	$GLOBALS['PT-RANKING-BEST-SCORES'] = "Melhores scores";
	$GLOBALS['PT-RANKING-BEST-COUPLES'] = "Melhores duplas";
	$GLOBALS['PT-RANKING-BEST-FRIENDS'] = "Seus melhores amigos";

	$GLOBALS['PT-RANKING-AND'] = " e ";
	$GLOBALS['PT-RANKING-MINUTE'] = "min";
	$GLOBALS['PT-RANKING-15MINUTE'] = "15min";
	$GLOBALS['PT-RANKING-30MINUTE'] = "30min";
	$GLOBALS['PT-RANKING-HOUR'] = "h";
	$GLOBALS['PT-RANKING-DAY'] = "d";
	$GLOBALS['PT-RANKING-MONTH'] = "m";
	$GLOBALS['PT-RANKING-YEAR'] = " ano";
	$GLOBALS['PT-RANKING-CREDITS'] = "Cr";

	$GLOBALS['PT-RANKING-DISPLAY-DAY'] = "dia";
	$GLOBALS['PT-RANKING-DISPLAY-DAYS'] = "dias";
	$GLOBALS['PT-RANKING-DISPLAY-DAY-S'] = "dia(s)";
	$GLOBALS['PT-RANKING-DISPLAY-WEEK'] = "semana";
	$GLOBALS['PT-RANKING-DISPLAY-MONTH'] = "mês";
	$GLOBALS['PT-RANKING-DISPLAY-MONTHS'] = "mês";
	$GLOBALS['PT-RANKING-DISPLAY-YEAR'] = "ano";

	 
	 $GLOBALS['PT-RANKING-DISPLAY-TITLE-ALL-PLAYERS'] = "Todos os jogadores";
	 $GLOBALS['PT-RANKING-DISPLAY-TITLE-ONLY-NEIGHBOURS'] = "Somente os vizinhos";
	 $GLOBALS['PT-RANKING-DISPLAY-TITLE-PLAYERS'] = "Os jogadores";
	 $GLOBALS['PT-RANKING-DISPLAY-TITLE-NEIGHBOURS'] = "Os vizinhos";
	 $GLOBALS['PT-RANKING-DISPLAY-TITLE-FRIENDS'] = "Os amigos";
	 $GLOBALS['PT-RANKING-DISPLAY-TITLE'] = "<0> ativos há <1> dias estão sendo mostrados... ";

	 $GLOBALS['PT-RANKING-DISPLAY-MENU'] = "Mostrar todos os jogadors ativos desde ";
	 $GLOBALS['PT-RANKING-DISPLAY-ONLY-NEIGHBOURS'] = "Mostrar somente os vizinhos";

	 $GLOBALS['PT-RANKING-DISPLAY-START'] = "Mostrar os ";

	 $GLOBALS['PT-RANKING-DISPLAY-PLAYERS'] = "jogadores";
	 $GLOBALS['PT-RANKING-DISPLAY-NEIGHBOURS'] = "vizinhos";
	 $GLOBALS['PT-RANKING-DISPLAY-FRIENDS'] = "amigos";

	 $GLOBALS['PT-RANKING-DISPLAY-ACTIVE-SINCE'] = " ativos há ";


	//------

	$GLOBALS['PT-MUSEUM-TITLE'] = "JeuxDeMots: pequeno museu";
	$GLOBALS['PT-MUSEUM-PROMPT'] = "Pequeno museu de <0>";
	$GLOBALS['PT-MUSEUM-WARNING-NOTREASURE'] = "Este jogador não possui tesouros.";

	//------ relations informations

	$GLOBALS['PT-SIGNIN-YOURINFO'] = "País";

	$GLOBALS['PT-RELINFO-1'] = "Brasil";
	$GLOBALS['PT-RELINFO-2'] = "Portugal";
	$GLOBALS['PT-RELINFO-3'] = "Angola";
	$GLOBALS['PT-RELINFO-4'] = "Cabo Verde";
	$GLOBALS['PT-RELINFO-5'] = "Guiné-Bissau";
	$GLOBALS['PT-RELINFO-6'] = "Moçambique";
	$GLOBALS['PT-RELINFO-7'] = "São Tomé e Príncipe";
	$GLOBALS['PT-RELINFO-8'] = "Timor Leste";
	$GLOBALS['PT-RELINFO-9'] = "Macau";
	$GLOBALS['PT-RELINFO-10'] = "Outro";

	//------

	$GLOBALS['PT-RESULT-TITLE'] = "JeuxDeMots : resultado";
	$GLOBALS['PT-RESULT-PG-PROMPT'] = "<P>Suas respostas (<0>) para esta partida (<1>) : ";
	$GLOBALS['PT-RESULT-NOTHING'] =  " --nada-- ";
	$GLOBALS['PT-RESULT-PROMPT'] = "Resultado";

	$GLOBALS['PT-RESULT-NOPROP'] = "<h2>Você não respondeu nada... Tenho certeza de que vai conseguir :-)</h2>";

	$GLOBALS['PT-RESULT-NOGUEST'] = "Não disponível para convidado";

	$GLOBALS['PT-RESULT-PROMPT-BUYPLAY-HEADER'] = "Você está decepcionado com as respostas inadequadas do seu colega,
	as suas são muito mais apropriadas. Você pode:";
	$GLOBALS['PT-RESULT-PROMPT-BUYPLAY-FORM'] = "<BR>Comprar este jogo e sugeri-lo a outros jogadores";
	$GLOBALS['PT-RESULT-BUY-TOKEN'] = "Comprar tokens";
	$GLOBALS['PT-RESULT-TOKEN'] = "tokens";
	$GLOBALS['PT-RESULT-OR'] = "ou";

	$GLOBALS['PT-RESULT-PROMP-INVOKEWORD-FORM'] ="<P><font color=\"red\"> Um prêmio!</font>
			<BR>Você está se saindo muito bem. Como recompensa, você pode fazer com que uma palavra seja sugerida a outros jogadores com maior frequência.";
	$GLOBALS['PT-RESULT-INVOKE'] = "Selecionar";
	$GLOBALS['PT-RESULT-THETERM'] = "a palavra";

	$GLOBALS['PT-RESULT-PROMP-ADDFRIEND-FORM'] ="<P><font color=\"red\"> Um prêmio!</font>
			<BR>Você tem talento. Como recompensa, convidamos você a adicionar um amigo.";
	$GLOBALS['PT-RESULT-ADD'] = "Adicionar";
	$GLOBALS['PT-RESULT-ASFRIEND'] = "como amigo";

	$GLOBALS['PT-RESULT-PROMPT-YOURANSWERS'] = "Suas respostas (<0>) para esta partida (<1>) : ";
	$GLOBALS['PT-RESULT-PROMPT-HISANSWERS'] = "Respostas do seu colega (<0>) para esta partida (<1>) : ";
	$GLOBALS['PT-RESULT-PROMPT-INTERSECTION'] = "Intersecção: ";

	$GLOBALS['PT-RESULT-PROMPT-CREATED-PLAY'] = "<h2> Sua partida será sugerida para outros jogadores. Você será notificado por email.</h2>";

	$GLOBALS['PT-RESULT-BUYTOKEN-FORM'] ="Se você está satisfeito com as suas respostas, você pode sugerir esta partida para outros jogadores.<P>Comprar 
			<input id=\"submit5\" type=\"submit\" name=\"submit5\" value=\"5 tokens\"> (750 Cr) or 
			<input id=\"submit10\" type=\"submit\" name=\"submit10\" value=\"10 tokens\"> (1500 Cr)";

	$GLOBALS['PT-RESULT-BUY-TRIAL-TOKEN-FORM'] = "<P>Você pode investir em um token de processo.<br/>Comprar 
			<input id=\"submit1\" type=\"submit\" name=\"submit1\" value=\"1 token\"> (200 Cr)";

	$GLOBALS['PT-RESULT-PROMPT-GAIN'] = "<h2>Você ganha <0> crédito(s) e <1> ponto(s) de honra</h2>";

	$GLOBALS['PT-RESULT-PROMPT-LEVEL-GAIN'] = "<br>Você e seu colega ganharam <0> pontos de nível.";
	$GLOBALS['PT-RESULT-PROMPT-LEVEL-LOOSE'] = "<br>Você e seu colega perderam <0> pontos de nível.";

	$GLOBALS['PT-RESULT-PROMPT-TERMLEVEL-GAIN'] = "<br>A palavra <0> ganha <1> pontos de nível.";
	$GLOBALS['PT-RESULT-PROMPT-TERMLEVEL-LOOSE'] = "<br>A palavra <0> perde <1> pontos de nível.";

	$GLOBALS['PT-RESULT-MORE-HONNOR-POINTS'] = "<0> pontos de honra extra!";

	// unused?
	$GLOBALS['PT-RESULT-NOTHING'] =  " --nada-- ";

	//------
	$GLOBALS['PT-BUYPLAY-CONGRAT-INVEST'] ="<h1>Parabéns por este investimento em <0> tokens
	 que custaram <1> créditos.</h1>";
	$GLOBALS['PT-BUYPLAY-CHEAT'] = "<h1>Um convidado não pode comprar uma partida novamente. 
		Se você está lendo essa mensagem, significa que sua sessão expirou</h1>";
	$GLOBALS['PT-BUYPLAY-EVENT'] = "jogador '<0>' comprou novamente uma partida <b>'<1>'</b>.";
	//------

	$GLOBALS['PT-BUYTOKEN-CONGRAT-INVEST'] = "<h1>Parabéns por este investimento em <0> tokens
	 que custaram <1> créditos.</h1>";
	$GLOBALS['PT-BUYTOKEN-CHEAT'] = "<h1>Um convidado não pode comprar tokens. 
		Se você está lendo essa mensagem, significa que sua sessão expirou</h1>";
	$GLOBALS['PT-BUYTOKEN-SORRY'] = "<h1>Desculpa, você não possui <0> créditos, você deveria ter verificado antes :)</h1>";

	// "le joueur '$login' a investi dans le mot <b>'$fentry'</b>."
	$GLOBALS['PT-BUYTOKEN-EVENT'] = "jogador '<0>' investiu em <b>'<1>'</b>.";

	//------
	$GLOBALS['PT-BUY-TRIAL-TOKEN-CONGRAT-INVEST'] = "Parabéns por este investimento em <0> tokens de processo que custaram <1> créditos.";
	$GLOBALS['PT-BUY-TRIAL-TOKEN-CHEAT'] = "Um convidado não pode comprar tokens. 
		Se você está lendo essa mensagem, significa que sua sessão expirou.";
	$GLOBALS['PT-BUY-TRIAL-TOKEN-SORRY'] = "Desculpa, você não possui <0> créditos, você deveria ter verificado antes :)";
	$GLOBALS['PT-BUY-TRIAL-TOKEN-EVENT'] = "jogador &#39;<0>&#39; comprou um token de processo.";


	//------
	$GLOBALS['PT-BUYWISH-CONGRAT-INVEST'] = "<h1>Parabéns por invocar a palavra '<0>' que custou <1> créditos. Ela poderá aparecer um dia...</h1>";
	$GLOBALS['PT-BUYWISH-CHEAT'] = "<h1>Um convidado não pode invocar palavras. 
		Se você está lendo essa mensagem, significa que sua sessão expirou</h1>";
	$GLOBALS['PT-BUYWISH-SORRY'] = "<h1>Desculpa, você não possui <0> créditos, você deveria ter verificado antes :)</h1>";

	// "le joueur '$login' a investi dans le mot <b>'$fentry'</b>."
	$GLOBALS['PT-BUYWISH-EVENT'] = "jogador '<0>' invocou uma palavra...";
	$GLOBALS['PT-BUYWISH-EMPTY-WORD'] = "palavra vazia, que azar!";

	$GLOBALS['PT-ADDFRIEND-CONGRAT-INVEST'] = "Parabéns, '<0>' é seu novo amigo.";
	$GLOBALS['PT-ADDFRIEND-ERROR'] = "Este jogador não existe.";
	$GLOBALS['PT-ADDFRIEND-CHEAT'] = "<h1>Um convidado não pode adicionar amigos. 
		Se você está lendo essa mensagem, significa que sua sessão expirou</h1>";
	$GLOBALS['PT-ADDFRIEND-SORRY'] = "<h1>Desculpa, você não possui <0> créditos, você deveria ter verificado antes :)</h1>";
	$GLOBALS['PT-ADDFRIEND-EVENT'] = "jogador &#39;<0>&#39; tem um novo amigo...";
	$GLOBALS['PT-ADDFRIEND-EMPTY-WORD'] = "desculpa, amigo vazio!";


	//------
	$GLOBALS['PT-TRIAL-CONGRAT-INVEST'] = "Parabéns por este investimento em um processo.";
	$GLOBALS['PT-TRIAL-CHEAT'] = "<h1>Um convidado não pode mover um processo. 
		Se você está lendo essa mensagem, significa que sua sessão expirou</h1>";
	$GLOBALS['PT-BUYTRIAL-SORRY'] = "<h1>Desculpa, você não possui <0> créditos, você deveria ter verificado antes :)</h1>";

	//------
	$GLOBALS['PT-PLAYER-PERM-CAGNOTTE-BINGO'] = "Bingo! Você ganhou <0> créditos na loteria.";
	$GLOBALS['PT-PLAYER-PERM-CAGNOTTE-EVENT'] = "jogadores '<0>' e '<1>' ganharam <2> na loteria e compartilharam o prêmio... e ajudaram os demais jogadores.";
	$GLOBALS['PT-PLAYER-PERM-CAGNOTTE-MAIL'] = "Olá,\na loteria foi sorteada e você ganhou créditos...\n";
	$GLOBALS['PT-PLAYER-PERM-CAGNOTTE-MAIL-TITLE'] = "Créditos...";

	//------

	$GLOBALS['PT-REQUEST-VERB-TRANSTIVE'] = "SELECT id FROM `Nodes` WHERE w > 50 
		AND id IN (SELECT node1 FROM Relations WHERE node2 
		IN (SELECT id FROM Nodes WHERE type=4 and name LIKE 'verb%')
		) order by w DESC LIMIT <0>";

	$GLOBALS['PT-REQUEST-VERB-EASY'] = "SELECT id FROM `Nodes` WHERE w > 50 
		AND id IN (SELECT node1 FROM Relations WHERE node2 
		IN (SELECT id FROM Nodes WHERE type=4 and name LIKE 'verb%')
		) order by w DESC LIMIT <0>";

	//$GLOBALS['PT-REQUEST-NOUN-EASY'] = "SELECT id FROM `Nodes` WHERE w >= 50 
	$GLOBALS['PT-REQUEST-NOUN-EASY'] = "SELECT id FROM `Nodes` WHERE w > 50 
	AND id IN (SELECT distinct node1 FROM Relations WHERE node2 
	IN (SELECT id FROM Nodes WHERE type=4 and name LIKE 'noun%')
	) order by w DESC LIMIT <0>";

$GLOBALS['PT-REQUEST-NOUN-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'noun%'); ";

$GLOBALS['PT-REQUEST-VERB-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'verb%' and type=4); ";

$GLOBALS['PT-REQUEST-NOUN-ADJ-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'noun%' OR name LIKE 'adj%' OR name LIKE 'adv%');";

$GLOBALS['PT-REQUEST-NOUN-ADJ-VER-ADV-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE name LIKE 'noun%' OR name LIKE 'verb%' OR name LIKE 'adj%' OR name LIKE 'adv%');";

$GLOBALS['PT-REQUEST-NOUN-VER-RANDOM'] = "SELECT node1 FROM `Relations` where type=4 and node2 
		IN (SELECT id FROM Nodes WHERE  type=4 and name LIKE 'noun%' OR name LIKE 'verb%');";


//------
// "le joueur '$player_name' fait de la magie noire avec le mot '$fentry'."
$GLOBALS['PT-BLACK-MAGIC-EVENT'] = "jogador '<0>' está fazendo magia negra com '<1>'.";
//"le joueur '$player_name' fait de la magie blanche avec le mot '$fentry'."
$GLOBALS['PT-WHITE-MAGIC-EVENT'] = "jogador '<0>' está fazendo magia negra com '<1>'.";

//------
$GLOBALS['PT-TGEN-CHOOSE-TERM'] = "Escolha um termo de sua preferência:";

$GLOBALS['PT-RESULT-CHANGE'] = "Mudar";
$GLOBALS['PT-RESULT-YOUR-NICKNAME'] = " seu nome de usuário ";
$GLOBALS['PT-RESULT-CHANGE-YOUR-NICKNAME'] = "Você é bom nisso! Como recompensa, você pode mudar seu nome de usuário.";

$GLOBALS['PT-TERMCAPT-SHORT'] = "O termo '<0>' acaba de ser capturado.";
$GLOBALS['PT-TERMCAPT-LONG'] = "O termo '<0>' (procurado por <1> Cr) acaba de ser capturado.";
$GLOBALS['PT-TERMOWN-SHORT'] = "O termo '<0>'  acaba de mudar de proprietário.";
$GLOBALS['PT-TERMOWN-LONG'] = "O termo '<0>' (procurado por <1> Cr) acaba de mudar de proprietário.";
$GLOBALS['PT-TERMOWN-FREE'] = "o termo está disponível";
$GLOBALS['PT-TERMOWN-NONFREE'] = "o termo está em propriedade dos jogadores '<0>' e '<1>'";

$GLOBALS['PT-NETWORK-TITLE'] = "A Rede";
$GLOBALS['PT-NETWORK-SEARCH-TERM'] = "<form id=\"gotermrel\" name=\"gotermrel\" method=\"post\" action=\"rezo.php\" >
	    <input id=\"gotermsubmit\" type=\"submit\" name=\"gotermsubmit\" value=\"Procurar\"> o termo
	    <input  id=\"gotermrel\" type=\"text\" name=\"gotermrel\" value=\"<0>\" size=100>
	    </form>";
$GLOBALS['PT-NETWORK-TERM-NON-EXIST'] = "<br>O termo <0> não está na rede!";
$GLOBALS['PT-NETWORK-TERM-HEADER'] = "'<0>' (id=<1> => <2> ; nível = <3> ; luminosidade = <4>)";
$GLOBALS['PT-NETWORK-NUMBER-RELATIONS'] = "<0> relações ";


?>
