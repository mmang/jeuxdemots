<?php session_start();?>
<?php include_once 'misc_functions.php'; ?>

<?php 
function display_proposal_done() {
	//$url = "generateGames.php";
	$url = $_SESSION[ssig() . 'raffinement-manger-url'];

	if ($_GET['delete_prop'] != "") {
			$delete_prop = $_GET['delete_prop'];
    		//echo "<BR>deleting";
    		$tag = 'gameC'. $delete_prop;
    		$last_tag = 'gameC'. $_SESSION[ssig() . 'gamenbcandidate'];
   			$_SESSION[$tag] = $_SESSION[$last_tag];
    		$_SESSION[$last_tag] = "";
    		$_SESSION[ssig() . 'gamenbcandidate'] = $_SESSION[ssig() . 'gamenbcandidate']-1;
	}
	if ($_GET['expand_prop'] != "") {
		$expand_prop = $_GET['expand_prop']+0;
		//echo $expand_prop;
	}
	if ($_GET['select_prop_name'] != "") {
		$tag = 'gameC'. $_GET['select_prop_rank'];
		$_SESSION[$tag] = $_GET['select_prop_name'];
	}
	if ($_GET['up_prop'] != "") {
		$tag = 'gameC'. $_GET['up_prop'];
		$term = $_SESSION[$tag];
		//echo "old term = $term";
		$newterm = get_raffinement_up($term);
		//echo "news term = $newterm";
		
		$_SESSION[$tag] = $newterm;
	}
	if ($_GET['up_max'] != "") {
		if (get_player_credit($_SESSION[ssig() . 'playerid']) >= 100) {	
	    	inc_player_credit($_SESSION[ssig() . 'playerid'], -100);
			$_SESSION[ssig() . 'maxpropnumber'] = $_SESSION[ssig() . 'maxpropnumber'] +1; 
		}
	}
	
	if ($_SESSION[ssig() . 'gamenbcandidate'] > 0) {
		if (artefact_owner(4) == $_SESSION[ssig() . 'playerid']) {$artefact=true;}
		echo "<div class=\"jdm-proposal-block\">";
    	for ($i = 1; $i <= $_SESSION[ssig() . 'gamenbcandidate']; $i++) {
			// echo "<br>" . $i;
			$tag = 'gameC'. $i;
			//$term = $_SESSION[$tag];
			$term= normalize_candidate($_SESSION[$tag]);
			if	($term != "") {
				//echo ($_SESSION[ssig() . 'maxpropnumber'] - $i + 1);
				//echo " : ";
				$status = term_wrong_in_BD_p($term);
								
				echo INTERN_make_raffinement_link_up($term,$i);
				echo "\n<a href=\"$url.php?delete_prop=$i\">";
				echo  produce_candidate_class(format_entry($term), false, $status);
				echo "\n</a>";
				if ($artefact) 
				{
					$val = array_search($term, $_SESSION[ssig() . 'answer_list']);
					if ($val === false)
					{
						echo "";
					} else {
						echo " <font color=\"red\">&hearts;</font> ";
					}
				}
				echo INTERN_make_raffinement_link_down($term,$i);
				if ($expand_prop == $i) 
				{
					display_all_raffinements($term,$i);
				} else {
					echo "<br>";
				}
			}
   		}
    	echo "<br>" . $_SESSION[ssig() . 'gamenbcandidate'] . "/" . $_SESSION[ssig() . 'maxpropnumber'];
    	make_raffinement_link_add_one();
   		echo "</div>";
	}
}

function make_raffinement_link_add_one () {
	if (get_player_credit($_SESSION[ssig() . 'playerid']) >= 100) {	
		$url = $_SESSION[ssig() . 'raffinement-manger-url'];
		echo " <a href=\"$url.php?up_max=1\">";
    	echo "+";
    	echo " </a>";
	}
}


function display_all_raffinements($node1_name,$rank) {
	//echo " ---------- ";
	$url = $_SESSION[ssig() . 'raffinement-manger-url'];
	$id1 = term_exist_in_BD_p($node1_name);
	if ($id1 == 0) {return 0;}
	$query = "SELECT N.name FROM Relations as R, Nodes as N WHERE R.type=1 
		AND R.node1 = $id1 and N.id = R.node2";
	//echo $query;
	$r =  @mysql_query($query) or die("pb display_all_raffinements 1: $query");
	$nb= mysql_num_rows($r);
	if ($nb > 0) {
		echo "<div class=\"jdm-proposal-select-block\">";
		for ($i = 0; $i <= $nb; $i++) {
			$name= mysql_result($r , $i , 0);
			echo "\n<a href=\"$url.php?select_prop_name=$name&select_prop_rank=$rank\">";
			echo "\n". format_entry($name);
			echo "\n</a>";
			if ($i == $nb-1) {} else {echo "<br>";}
			//echo "</div>";
		}
		echo "</div>";
	}
}

function get_raffinement_up($term) {
	$id1 = term_exist_in_BD_p($term);
	if ($id1 == 0) {return;}
	$query = "SELECT N.name FROM Relations as R, Nodes as N WHERE R.type=1 
		AND R.node2 = $id1 and N.id = R.node1";
	//echo $query;
	$r =  @mysql_query($query) or die("pb get_raffinement_up 1: $query");
	$name= mysql_result($r , 0 , 0);
	return $name;
}

function INTERN_make_raffinement_link_down($term,$i=0) {
	$url = $_SESSION[ssig() . 'raffinement-manger-url'];
	if (has_some_raffinement_down($term) > 0) {
		echo "<a href=\"$url.php?expand_prop=$i\">";
    	echo "&gt;";
    	echo "</a>";
    }
}

function INTERN_make_raffinement_link_up($term,$i=0) {
	$url = $_SESSION[ssig() . 'raffinement-manger-url'];
	if (has_some_raffinement_up($term) > 0) {
    	echo "<a href=\"$url.php?up_prop=$i\">";
    	echo "&lt;";
    	echo "</a>";
    }
}

function precompute_answer_list() {
	start_time_record("precompute_answer_list");
	if (($_SESSION[ssig() . 'gamestatus'] != "ANSWER") || (artefact_owner(4) != $_SESSION[ssig() . 'playerid']))
	{
		$_SESSION[ssig() . 'answer_list'] = array();
		return;
	}
	$gamecreatorid = $_SESSION[ssig() . 'gamecreatorid'];
	$gameid = $_SESSION[ssig() . 'gameid'];
	$query = "SELECT answer FROM `PendingAnswers` AS PA 
		WHERE PA.playerId = '$gamecreatorid' AND PA.gameId = '$gameid'";
	$r =  @mysql_query($query) or die("precompute_answer_list : $query");
	$nb = mysql_num_rows($r);
	for ($i=0 ; $i<$nb ; $i++) {
		$w = mysql_result($r , $i , 0);
		$_SESSION[ssig() . 'answer_list'][$i] = $w;
	}
	debugecho("precompute_answer_list=" . end_time_record("precompute_answer_list"));
}



?>
