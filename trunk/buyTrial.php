<?php session_start();?>
<?php include_once 'misc_functions.php' ; ?>
<?php include_once 'trial_functions.php' ; ?>
<?php openconnexion(); ?>
<html>
 <head>
    <title>JeuxDeMots : proc�s</title>
    <?php header_page_encoding(); ?>
  </head>
  <body>
<?php include 'HTML-body.html' ; ?>
<?php topblock(); ?>
<?php check_referer(); ?>
<?php
function produce_trial() {
     //debugecho("post trial_id  " . $_POST["trial_id"]) ;
     //echo "post trial_id  " . $_POST["trial_id"];
    if ($_SESSION[ssig() . 'PARAM-ALLOW-TRIALS'] == "N") {return;}
     
    $login = $_SESSION[ssig() . 'login'];
    if ($login == "") {$login = "guest";}

    $playerid = $_SESSION[ssig() . 'playerid'];
    $gameid = $_SESSION[ssig() . 'gameid'];
    $entry = $_SESSION[ssig() . 'gameentry'];
    
    if ($playerid == 0) {
    	//echo "<h1>D�sol� le joueur invit� ne peut pas faire de proc�s. Si vous lisez ceci, c'est que la session a du expirer</h1>";
    	
    	echo "<div class=\"jdm-notice1-block\"><div class=\"jdm-notice1\">";
		echo get_msg('MAKE-TRIAL-CHEAT');
		echo "</div></div>";
    	return;
    }
    
   // echo "<br> nb token " . $nbtoken;
    $cost = 500;

    if (get_player_credit($playerid) >= $cost) {
    
		inc_player_credit($playerid, -$cost);
		inc_player_trial_tokens($playerid, -1);
		finalize_trial($_POST["trial_id"],$_POST["trial_reason"]);
		
		//echo "<h1>Bravo pour cet investissement dans un proc�s pour un montant de $cost cr�dits.</h1>";
		$GLOBALS[0]=$cost;
		echo "<div class=\"jdm-notice1-block\"><div class=\"jdm-notice1\">";
		echo get_msg('TRIAL-CONGRAT-INVEST');
		echo "</div></div>";
    
    } else {
		//echo "<h1>D�sol�, vous n'avez pas cette somme de $cost cr�dits, il fallait v�rifier avant :)</h1>";
		$GLOBALS[0]=$cost;
		echo "<div class=\"jdm-notice1-block\"><div class=\"jdm-notice1\">";
		echo get_msg('BUYTRIAL-SORRY');
		echo "</div></div>";
    }
}

?>

<div class="jdm-level1-block">
	
	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
    </div>
    <?php if ($_SESSION[ssig() . 'FRAUD'] == true) {
    	echo " :: acc�s frauduleux...";
    }
    ?>
	</div>

    <div class="jdm-login-block">
    <?php  loginblock(); ?>
    </div>
</div>

<div class="jdm-level2-block">
<TABLE border="0" width="100%" cellspacing="0" cellpadding="0%"
	summary="jeuxdemots" bgcolor="white">

<TR><TH bgcolor="#EEF6F8" align="right" width="200">
    <TH bgcolor="#EEF6F8" colspan="2" align="left">
	<div class="jdm-instruction">
	</div>
	
<TR><TH>
    <TH align="left">
    <spacer type="block" width="1" height="30"> 
    <div class="jdm-prompt-2">
    <?php produce_trial() ?>		
    </div>
    <TH>

</TABLE>
</div>

<div class="jdm-playas-block-3">
<?php produceplayasform(); ?>
</div>

<?php playerinfoblock($_SESSION[ssig() . 'playerid']) ?>
<?php
    bottomblock();
    closeconnexion();
?>

  </body>
</html>
