<?php session_start();?>
<?php include 'misc_functions.php' ; ?>
<?php openconnexion();?>

<html>
 <head>
    <title><?php echo get_msg('RANKING-TITLE'); ?></title>
    <?php header_page_encoding(); ?>
    <meta http-equiv="expires" content="0">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache, must-revalidate"> 
  </head>
<?php include 'HTML-body.html' ; ?>
<?php topblock(); ?>

<?php
if ($_SESSION[ssig() . 'toggle_friend']) {
	$_SESSION[ssig() . 'mut-friend'] = get_mutal_friend(365*24*3600);
}

if ($_SESSION[ssig() . 'playerid'] == "") {$_SESSION[ssig() . 'playerid'] = 0;}

$_SESSION[ssig() . 'time_span'] = round($_GET['time_span']);
if ($_SESSION[ssig() . 'time_span'] < 1) {$_SESSION[ssig() . 'time_span'] = 30;}

if ($_GET['rank_display'] != 0) {
	$_SESSION[ssig() . 'rank_display'] = round($_GET['rank_display']);
}
if ($_SESSION[ssig() . 'rank_display'] < 1) {$_SESSION[ssig() . 'rank_display'] = 10;}

if ($_SESSION[ssig() . 'rank_display'] >= 10000) {
	$_SESSION[ssig() . 'rank_display_tag'] = get_msg('RANKING-DISPLAY-TITLE-ALL-PLAYERS');
} else {
	$_SESSION[ssig() . 'rank_display_tag'] = get_msg('RANKING-DISPLAY-TITLE-ONLY-NEIGHBOURS');
}

if ($_SESSION[ssig() . 'type_display'] == 'all') {	
	$_SESSION[ssig() . 'rank_display_tag'] = get_msg('RANKING-DISPLAY-TITLE-PLAYERS');
	$_SESSION[ssig() . 'rank_display'] = 10000;
};
if ($_SESSION[ssig() . 'type_display'] == 'neighboors') {
	$_SESSION[ssig() . 'rank_display_tag'] = get_msg('RANKING-DISPLAY-TITLE-NEIGHBOURS');
	$_SESSION[ssig() . 'rank_display'] = 10;
};
if ($_SESSION[ssig() . 'type_display'] == 'friends') {
	$_SESSION[ssig() . 'rank_display_tag'] = get_msg('RANKING-DISPLAY-TITLE-FRIENDS');
	$_SESSION[ssig() . 'rank_display'] = 10000;
};


// debug
/*
 echo "<br>_SESSION['rank_display']=".$_SESSION[ssig() . 'rank_display'];
echo "<br>_SESSION['time_span']=".$_SESSION[ssig() . 'time_span'];
echo "<br>_SESSION['playerid']=".$_SESSION[ssig() . 'playerid'];
*/

function generateRanking() {
    $_SESSION[ssig() . 'state']=0;

    $last_player = get_last_player();
    if ($last_player == 0) {$last_player = -1;}
    
    $query =  "SELECT id FROM `Players`";
    $r =  @mysql_query($query) or die("Bug in generateRanking 1: $query");
    // echo $query;
    for ($i=0 ; $i<mysql_num_rows($r) ; $i++) {
		$id = mysql_result($r , $i , 0);
		//consolidate_stats($id);
    }
    
    //echo "<br>crit posted" . $_GET['crit'];
    $crit = "honnor";
    if ($_GET['crit'] != "") {
	$crit = $_GET['crit'];
    }
    
    if ($crit == "name") {$order = "ASC" ;} else  {$order = "DESC";}
    if (($crit != "name") AND
    	($crit != "honnor") AND
    	($crit != "credit") AND
    	($crit != "maxhonnor") AND
    	($crit != "nbplayed") AND
    	($crit != "level") AND
    	($crit != "eff") AND
    	($crit != "nb_tres")
    	) 
    	{return;}
    	
    //$query =  "SELECT name , honnor , credit , maxhonnor , nbplayed, level, id FROM `Players`  ORDER BY `$crit` " . $order;

    $friendtag = " OR id=0 ";
    if ($_SESSION[ssig() . 'type_display'] == 'friends') {
    	$friendtag = "";
    	$flist = make_friend_list($_SESSION[ssig() . 'playerid']);
    	if ($flist == '') {
    			$friendtag = " OR id=0 ";
   	 } else{
    	//echo "coucou ";
    	$friendtag = " and id in $flist or id=" . $_SESSION[ssig() . 'playerid'] . ' ';
    	//echo $friendtag;
    	}
    }
    
    $timetag = " WHERE TO_DAYS(NOW()) - TO_DAYS(lastlogin) <= " . $_SESSION[ssig() . 'time_span'];
    $query =  "SELECT name , honnor , credit , maxhonnor , nbplayed, level, id , FORMAT(honnor/(nbplayed+1),2) AS eff,
    	 nb_tres, css_mus_url, perso_url, lastlogin
		FROM `Players`  
		$timetag $friendtag
		ORDER BY `$crit` " . $order . ", eff DESC;
		";
		
		// ORDER BY `$crit` " . $order . ", eff DESC
    $r =  @mysql_query($query) or die("Bug in generateRanking 2: $query");
    
    // on cherche la pos du joueur courant
    for ($i=0 ; $i<mysql_num_rows($r) ; $i++) {
		$id = mysql_result($r , $i , 6);
		if ($_SESSION[ssig() . 'playerid']==$id) {
			$playerpos = $i;
			//echo "<br>find player =".$_SESSION[ssig() . 'playerid']." at pos=$playerpos";
			
			break;
		}
    }
    
    for ($i=0 ; $i<mysql_num_rows($r) ; $i++) {
		$name = mysql_result($r , $i , 0);
		$honnor = mysql_result($r , $i , 1);
		$credit = mysql_result($r , $i , 2);
		//$credit = rand(0, 100000);
		$maxhonnor = mysql_result($r , $i , 3);
		$nbplayed = mysql_result($r , $i , 4);
		$level = round(mysql_result($r , $i , 5), 2);
	
		$id = mysql_result($r , $i , 6);
		$eff = mysql_result($r , $i , 7);
		$nb_tres = mysql_result($r , $i , 8);
		$css_mus_url = mysql_result($r , $i , 9);
		$perso_url = mysql_result($r , $i , 10);
	
		$j = $i+1;

		if (abs($playerpos - $i) < $_SESSION[ssig() . 'rank_display']) {
			
 	  	if ($perso_url != "") {
			$fname = "<a  target='_BLANK' href=\"$perso_url\">" . $name . "</a>" ;
		} else {
			$fname = $name;
		}
	
	
		if ($css_mus_url != "") {
			$tres_str = "<a href=\"jdm-mylittlemuseum.php?owner=$id\">" . $nb_tres . "</a>" ;
		} else {
			$tres_str = $nb_tres;
		}
	
   		if ($_SESSION[ssig() . 'mut-friend'] == $id) {
			$heart = "<img src=\"pics/coeur-gif-031.gif\"  BORDER=0 ALIGN=ABSMIDDLE></A>";
		} else {
			$heart = "";
		}
	
	
		if ($last_player == $id) {
			$mail_tag = "<A href=\"jdm-mail.php\"><img src=\"pics/email-1.jpg\"  BORDER=0 alt=\"email\" ALIGN=ABSMIDDLE></A>";
		} else {
			$mail_tag = "";
		}
		if ($id == $_SESSION[ssig() . 'playerid']) {echo "<TR bgcolor=\"lightgreen\"  VALIGN=MIDDLE>";} else {echo "<TR  VALIGN=MIDDLE>";}
		if (test_any_playerp($id)=="Y") {
	  	  echo "<TH>$j<TH><s>$heart$fname$mail_tag</s><TH>$honnor<TH>$maxhonnor<TH>$credit<TH>$nbplayed<TH>$level<TH>$eff\n";
	   	} 
			else {
	   		echo "<TH>$j<TH>$heart$fname$mail_tag<TH>$honnor<TH>$maxhonnor<TH>$credit<TH>$nbplayed<TH>$level<TH>$eff\n";
	    }
    }
    }
}

?>
<?php clean_scores(); ?>

<div class="jdm-level1-block">
	
	<div class="jdm-prompt-block">
    <div class="jdm-prompt">
        <?php echo get_msg('RANKING-PROMPT') . ": " .  get_msg('RANKING-DISPLAY-PLAYERS') . " 
     <font size=-1>&gt; 
     <a href=\"generateRanking.php\">" .  get_msg('RANKING-DISPLAY-SCORES') . " </a></font>
     
     <font size=-1>&gt; 
     <a href=\"generateRanking-3.php\">" .   get_msg('RANKING-DISPLAY-TREASURES') . " </a></font>
     "; 
     
     ?>
   </div>
	</div>

    <div class="jdm-login-block">
    <?php  loginblock(); ?>
    </div>
</div>

<div class="jdm-level2-block">

	<div class="jdm-ranking-block-shadowxxx">
    <div class="jdm-ranking-block">
   <?php //echo $_SESSION[ssig() . 'rank_display'] ?> 
   
   <?php 
   $GLOBALS[0] = $_SESSION[ssig() . 'rank_display_tag'];
   $GLOBALS[1] = $_SESSION[ssig() . 'time_span'];
   echo get_msg('RANKING-DISPLAY-TITLE'),' <br>';
   	echo get_msg('RANKING-DISPLAY-MENU'); ?>
   	<a href="generateRanking.php?time_span=365&rank_display=10000">1 <?php echo get_msg('RANKING-DISPLAY-YEAR');?></a> -
   	<a href="generateRanking.php?time_span=182&rank_display=10000">6 <?php echo get_msg('RANKING-DISPLAY-MONTHS');?></a> -
   	<a href="generateRanking.php?time_span=30&rank_display=10000">1 <?php echo get_msg('RANKING-DISPLAY-MONTH');?></a> - 
    <a href="generateRanking.php?time_span=15&rank_display=10000">15 <?php echo get_msg('RANKING-DISPLAY-DAYS');?></a> - 
    <a href="generateRanking.php?time_span=7&rank_display=10000">1 <?php echo get_msg('RANKING-DISPLAY-WEEK');?></a> -
    <a href="generateRanking.php?time_span=1&rank_display=10000">1 <?php echo get_msg('RANKING-DISPLAY-DAY');?></a>
    / <a href="generateRanking.php?time_span=<?php echo $_SESSION[ssig() . 'time_span']; ?>&rank_display=10"><?php echo get_msg('RANKING-DISPLAY-ONLY-NEIGHBOURS');?></a>
	<TABLE	 border="1px"
		    width="100%"
		    cellspacing="0" cellpadding="0%"
		    summary="Ranking">
	<TR bgcolor="#EEF6F8"><TH>
	    <TH <?php if ($_GET['crit']== "name") echo "bgcolor=\"lightgrey\"" ?>><a  href="generateRanking.php?crit=name"><?php echo get_msg('RANKING-LABEL-NAME'); ?></a>
	    <TH <?php if (($_GET['crit']== "honnor")|($_GET['crit']== "")) echo "bgcolor=\"lightgrey\"" ?>><a   href="generateRanking.php?crit=honnor"><?php echo get_msg('RANKING-LABEL-HONNOR'); ?></a>
	    <TH <?php if ($_GET['crit']== "maxhonnor") echo "bgcolor=\"lightgrey\"" ?>><a  href="generateRanking.php?crit=maxhonnor"><?php echo get_msg('RANKING-LABEL-HONNORMAX'); ?></a>
	    <TH <?php if ($_GET['crit']== "credit") echo "bgcolor=\"lightgrey\"" ?>><a  href="generateRanking.php?crit=credit"><?php echo get_msg('RANKING-LABEL-CREDITS'); ?></a>
	    <TH <?php if ($_GET['crit']== "nbplayed") echo "bgcolor=\"lightgrey\"" ?>><a  href="generateRanking.php?crit=nbplayed"><?php echo get_msg('RANKING-LABEL-NBPLAYED'); ?></a>
	    <TH <?php if ($_GET['crit']== "level") echo "bgcolor=\"lightgrey\"" ?>><a  href="generateRanking.php?crit=level"><?php echo get_msg('RANKING-LABEL-LEVEL'); ?></a>
	    <TH <?php if ($_GET['crit']== "eff") echo "bgcolor=\"lightgrey\"" ?>><a  href="generateRanking.php?crit=eff"><?php echo get_msg('RANKING-LABEL-EFF'); ?></a>
		<?php generateRanking() ?>
	</TABLE>	
    </div>
    </div>
	
	<!-- 
    <div class="jdm-best-score-block-shadow">
    <div class="jdm-best-score-block">
  	<div class="jdm-best-score-title">
  	<?php	
  	echo get_msg('RANKING-BEST-SCORES');
  	handle_toggle('toggle_score', 'generateRanking.php');
  	echo "</div>";
  	if ($_SESSION[ssig() . 'toggle_score']) {
  		get_best_score(300,"5".get_msg('RANKING-MINUTE')); 
		get_best_score(900,get_msg('RANKING-15MINUTE')); 
		get_best_score(1800,get_msg('RANKING-30MINUTE')); 
		get_best_score(3600,"1".get_msg('RANKING-HOUR')); 
		get_best_score(6*3600,"6".get_msg('RANKING-HOUR')); 
		get_best_score(12*3600,"12".get_msg('RANKING-HOUR')); 
		get_best_score(24*3600,"1".get_msg('RANKING-DAY')); 
		get_best_score(7*24*3600,"7".get_msg('RANKING-DAY')); 
		get_best_score(30*24*3600,"30".get_msg('RANKING-DAY')); 
		get_best_score(6*30*24*3600,"6".get_msg('RANKING-MONTH')); 
		get_best_score(365*24*3600,"1" .get_msg('RANKING-YEAR')); 
  	}
     ?>
    </div>
    </div>
	
    <div class="jdm-best-couple-block-shadow">
    <div class="jdm-best-couple-block">
  	<div class="jdm-best-couple-title">
  	<?php 
		echo get_msg('RANKING-BEST-COUPLES');
  		handle_toggle('toggle_couple', 'generateRanking.php');
  		echo "</div>";
		if ($_SESSION[ssig() . 'toggle_couple']) {
			get_best_couple(300,"5".get_msg('RANKING-MINUTE')); 
     		get_best_couple(900,get_msg('RANKING-15MINUTE')); 
     		get_best_couple(1800,get_msg('RANKING-30MINUTE')); 
			get_best_couple(3600,"1".get_msg('RANKING-HOUR')); 
			get_best_couple(6*3600,"6".get_msg('RANKING-HOUR'));
			get_best_couple(12*3600,"12".get_msg('RANKING-HOUR'));
			get_best_couple(24*3600,"1".get_msg('RANKING-DAY')); 
			get_best_couple(7*24*3600,"7".get_msg('RANKING-DAY'));
			get_best_couple(30*24*3600,"30".get_msg('RANKING-DAY'));
			get_best_couple(6*30*24*3600,"6".get_msg('RANKING-MONTH')); 
			get_best_couple(365*24*3600,"1" .get_msg('RANKING-YEAR'));
		}
	?>
	</div>
    </div>
	
    <div class="jdm-best-friend-block-shadow">
    <div class="jdm-best-friend-block">
  	<div class="jdm-best-friend-title">
  	<?php 
  		echo get_msg('RANKING-BEST-FRIENDS');
		handle_toggle('toggle_friend', 'generateRanking.php');
		echo "</div>";
  		if ($_SESSION[ssig() . 'toggle_friend']) {
			get_best_bestfriend(300,"5".get_msg('RANKING-MINUTE'));
			get_best_bestfriend(900,get_msg('RANKING-15MINUTE')); 
			get_best_bestfriend(1800,get_msg('RANKING-30MINUTE')); 
			get_best_bestfriend(3600,"1".get_msg('RANKING-HOUR')); 
			get_best_bestfriend(6*3600,"6".get_msg('RANKING-HOUR')); 
			get_best_bestfriend(12*3600,"12".get_msg('RANKING-HOUR'));
			get_best_bestfriend(24*3600,"1".get_msg('RANKING-DAY')); 
			get_best_bestfriend(7*24*3600,"7".get_msg('RANKING-DAY')); 
			get_best_bestfriend(30*24*3600,"30".get_msg('RANKING-DAY')); 
			get_best_bestfriend(6*30*24*3600,"6".get_msg('RANKING-MONTH')); 
			get_best_bestfriend(365*24*3600,"1" .get_msg('RANKING-YEAR')); 
		}
    ?>
	</div>
    </div>
	 -->
</div>

<?php

function clean_scores() {
	//  on fait le menage
	$query = "DELETE FROM `LastScores` WHERE Score < 40;";
	//echo $query;
	$r =  @mysql_query($query) or die("bug in clean_scores : $query");
	
	// on vire tous les scores d'un id < au 20000e
	$query = "SELECT id FROM `LastScores` ORDER BY `id` DESC LIMIT 20000,1";
	$r =  @mysql_query($query) or die("bug in clean_scores : $query");
	$nb = mysql_num_rows($r);
	if ($nb > 0) {
		$id = mysql_result($r , 0 , 0);
		$query = "DELETE FROM `LastScores` WHERE id < $id;";
		//echo $query;
		$r =  @mysql_query($query) or die("bug in clean_scores : $query");
	}
}



function get_best_score($since, $tag, $n=1) {
	// $since est la durée en milisecondes
	// 24 h = 24 * 3600 * 1000 par exemple
	$now = microtime_float();
	
   $query = "SELECT Score, p1_id, p2_id, date FROM `LastScores` 
    WHERE (($now - date) < $since) ORDER BY Score DESC LIMIT $n;";
   
   $r =  @mysql_query($query) or die("get_best_score : $query");
   $nb = mysql_num_rows($r);
   if ($nb > 0) {	
   		$score = mysql_result($r , 0 , 0);
    	$p1 = mysql_result($r , 0 , 1);
    	$p2 = mysql_result($r , 0 , 2);
    	$date = mysql_result($r , 0 , 3);
    	$p1name = get_player_name($p1);
    	$p2name = get_player_name($p2);
    	$and = get_msg('RANKING-AND');
    	echo "<div class=\"jdm-best-score-item\">";
    	echo "<div class=\"jdm-best-score-item-tag\">$tag</div>";
    	echo "<div class=\"jdm-best-score-item-names\">'$p1name'$and'$p2name'</div>";
    	echo "<div class=\"jdm-best-score-item-cr\">$score ",get_msg('RANKING-CREDITS'),"</div>";
    	echo "</div>";
    }
}

function get_best_couple($since, $tag, $n=1) {
	// $since est la durée en milisecondes
	// 24 h = 24 * 3600 * 1000 par exemple
	$now = microtime_float();
	
	// SELECT Marque, AVG(Compteur) AS Moyenne FROM VOITURE GROUP BY Marque
	 $query ="SELECT AVG(Score) AS S, p1_id, p2_id FROM `LastScores` WHERE 
id IN (SELECT id FROM LastScores WHERE (($now - date) < $since)) GROUP BY p1_id,p2_id ORDER BY S DESC LIMIT $n;";

  // $query = "SELECT SUM(Score) AS S, p1_id, p2_id, date FROM `LastScores` 
   // WHERE (($now - date) < $since) GROUP BY p1_id, p2_id ORDER BY Score DESC LIMIT 1;";
   //echo $query;
   $r =  @mysql_query($query) or die("bug in get_best_couple : $query");
   $nb = mysql_num_rows($r);

   if ($nb > 0) {
   		$score = round(mysql_result($r , 0 , 0));
    	$p1 = mysql_result($r , 0 , 1);
    	$p2 = mysql_result($r , 0 , 2);
    	$p1name = get_player_name($p1);
    	$p2name = get_player_name($p2);
    	$and = get_msg('RANKING-AND');

       	echo "<div class=\"jdm-best-couple-item\">";
    	echo "<div class=\"jdm-best-couple-item-tag\">$tag</div>";
    	echo "<div class=\"jdm-best-couple-item-names\">'$p1name'$and'$p2name'</div>";
    	echo "<div class=\"jdm-best-couple-item-cr\">$score ",get_msg('RANKING-CREDITS'),"</div>";
    	echo "</div>";
   }
}

function get_best_bestfriend($since, $tag, $n=1) {
	// $since est la durée en milisecondes
	// 24 h = 24 * 3600 * 1000 par exemple
	$now = microtime_float();
	$playerid = $_SESSION[ssig() . 'playerid'];
	
	// SELECT Marque, AVG(Compteur) AS Moyenne FROM VOITURE GROUP BY Marque
	 $query ="SELECT AVG(Score) AS S, p1_id, p2_id FROM `LastScores` WHERE 
id IN (SELECT id FROM LastScores WHERE (($now - date) < $since)
AND (p1_id = $playerid OR p2_id = $playerid)
) GROUP BY p1_id,p2_id ORDER BY S DESC LIMIT $n;";

  // $query = "SELECT SUM(Score) AS S, p1_id, p2_id, date FROM `LastScores` 
   // WHERE (($now - date) < $since) GROUP BY p1_id, p2_id ORDER BY Score DESC LIMIT 1;";
   //echo $query;
   $r =  @mysql_query($query) or die("bug in get_best_bestfriend : $query");
   $nb = mysql_num_rows($r);

   if ($nb > 0) {
   		$score = round(mysql_result($r , 0 , 0));
    	$p1 = mysql_result($r , 0 , 1);
    	$p2 = mysql_result($r , 0 , 2);
    	
    	if ($p1 == $playerid) {$p1 = $p2;}
    	$p1name = get_player_name($p1);
    	//$p2name = get_player_name($p2);
        echo "<div class=\"jdm-best-friend-item\">";
    	echo "<div class=\"jdm-best-friend-item-tag\">$tag</div>";
    	echo "<div class=\"jdm-best-friend-item-names\">'$p1name'</div>";
    	echo "<div class=\"jdm-best-friend-item-cr\">$score ",get_msg('RANKING-CREDITS'),"</div>";
    	echo "</div>";
   }
}

function get_mutal_friend($since) {
	$now = microtime_float();
	$playerid = $_SESSION[ssig() . 'playerid'];
	
	 $query ="SELECT AVG(Score) AS S, p1_id, p2_id FROM `LastScores` WHERE 
			id IN (SELECT id FROM LastScores WHERE (($now - date) < $since)
			AND (p1_id = $playerid OR p2_id = $playerid)
			) GROUP BY p1_id,p2_id ORDER BY S DESC LIMIT 1;";
	$r =  @mysql_query($query) or die("bug in get_mutal_friend 1 : $query");
   	$nb = mysql_num_rows($r);
	if ($nb == 0) {return -1;}
	
    $p1 = mysql_result($r , 0 , 1);
    $p2 = mysql_result($r , 0 , 2);
    if ($p1 == $playerid) {$p1 = $p2;}
    
    $query ="SELECT AVG(Score) AS S, p1_id, p2_id FROM `LastScores` WHERE 
			id IN (SELECT id FROM LastScores WHERE (($now - date) < $since)
			AND (p1_id = $p1 OR p2_id = $p1)
			) GROUP BY p1_id,p2_id ORDER BY S DESC LIMIT 1;";
	$r =  @mysql_query($query) or die("bug in get_mutal_friend 2 : $query");
	$nb = mysql_num_rows($r);
	if ($nb == 0) {return -1;}
	
	
    $p1bis = mysql_result($r , 0 , 1);
    $p2bis = mysql_result($r , 0 , 2);
    if ($p1bis == $p1) {$p1bis = $p2bis;}
    
    
    if ($p1bis == $playerid) {
    	//echo "MUTUAL FRIEND = $p1";
    	return $p1;
    } else {
    	//echo "MUTUAL FRIEND = -1";
    	return -1;}
}
?>

<div class="jdm-playas-block-1">
<?php produceplayasform(); ?>
</div>

<?php playerinfoblock($_SESSION[ssig() . 'playerid']) ?>
<?php 
    bottomblock();
    closeconnexion();
?>

  </body>
</html>
