BIBLIST="const const-abbr acl books cl csl linguamatica lre lrec nle propor ws confs"
PDFLIST="acl books cl csl linguamatica lre lrec nle propor ws confs"
today=`date +%Y%m%d`

rm -rf latex

mkdir latex
mkdir latex/bib
mkdir latex/bib/scripts
mkdir latex/bib/pdf

for file in $BIBLIST; do
  cp bib/${file}.bib latex/bib
done
for file in $PDFLIST; do
  cp bib/${file}.pdf latex/bib/pdf
done

cp bib/clean.sh latex/bib
cp bib/README latex/bib
cp bib/scripts/* latex/bib/scripts

cp *.def *.cls *.sty latex

zip -r latex_${today}.zip latex/

rm -rf latex
