% Beamer personal style
% Carlos Ramisch
% June 21st, 2009

\ProvidesClass{latex/mybeamer}

%\newcommand{\mybeamerOption}[1]{\DeclareOption{#1}{\input{mybeamer#1.def}}}
%\LoadClass[xcolor=dvipsnames,14pt,mathserif,hyperref={pdfpagelabels=false]{beamer}
%\mybeamerOption{Gray}

\RequirePackage{type1cm}
\RequirePackage{fix-cm}
\RequirePackage{color}

\newcommand{\mybeamerOption}[1]{\DeclareOption{mybeamer#1}{\input{latex/mybeamer#1.def}}}
\mybeamerOption{Gray}
\mybeamerOption{Tourquoise}
\mybeamerOption{Red}
\mybeamerOption{Brick}
\mybeamerOption{Lemon}
\mybeamerOption{Purple}
\mybeamerOption{Girly}
\DeclareOption{draft}{\PassOptionsToClass{draft}{beamer}}
\protect\ProcessOptions

\LoadClass[xcolor=dvipsnames,14pt,mathserif,hyperref={pdfpagelabels=false}]{beamer}

% DEFAULT LAYOUT COLORS
\ifx \titleforeground \@undefined
	\def\titleforeground{black}
\fi
\ifx \titlebackground \@undefined
	\def\titlebackground{gray}
\fi
\ifx \slidebackgroundbottom \@undefined
	\def\slidebackgroundbottom{white}
\fi
\ifx \slidebackgroundtop \@undefined
	\def\slidebackgroundtop{gray}
\fi
\ifx \alertforeground \@undefined
	\def\alertforeground{black}
\fi

\usetheme{Singapore}
\usecolortheme[named=\titleforeground]{structure}
\useinnertheme{circles}
\useoutertheme[subsection=false]{smoothbars}
\setbeamercovered{still covered={\opaqueness<1->{10}},again covered={\opaqueness<1->{45}}}
\setbeamertemplate{background canvas}[vertical shading][bottom=\slidebackgroundbottom,top=\slidebackgroundtop]
\setbeamercolor{title}{bg=\titlebackground}
\setbeamertemplate{blocks}[rounded][shadow=true]
\setbeamertemplate{footline}{
	\mbox{
		\insertshortauthor[center,width=0.25\paperwidth]
		\insertshorttitle[center,width=0.65\paperwidth]
		\insertframenumber/\inserttotalframenumber
	}
}

% MINI (simplified) COMMAND VERSIONS TO FIT IN THE SLIDE AREA
\ifx \minicite \@undefined
	\newcommand{\minicite}[1]{{\scriptsize\cite{#1}}}
\fi
\ifx \miniex \@undefined
	\newcommand{\miniex}[1]{{\scriptsize\ex{#1}}}
\fi
\ifx \minialert \@undefined
	\newcommand{\minialert}[1]{\textbf{#1}}
\fi
\ifx \minicmd \@undefined
	\newcommand{\minicmd}[1]{{\small{\tt #1}}}
\fi

\ifx \thead \@undefined
	\newcommand{\thead}[1]{ \hline \\ [-0.8em] #1 \\ [.5em]  }
\else
	\renewcommand{\thead}[1]{ \hline \\ [-0.8em] #1 \\ [.5em]  }
\fi
\renewcommand{\alert}[1]{{\color{\alertforeground}\textbf{\emph{#1}}}}
