A computational linguistics bibliography

This folder contains a set of bibliographical references in BibTeX format. The 
main research area concerned by these references is computational linguistics 
(natural language processing). It is largely incomplete, but I hope it is useful 
for those who work in the field and that often spend too much time retrieving 
and formatting references.

Conferences and workshops:

ACL annual meeting: 2006 to 2009 including some student research workshops
PROPOR Processing of Portuguese Language: 2006, 2008 and 2010
Language Resources and Evaluation Conference LREC: 2008 and 2010
CoNLL Natural Language Learning: 2004 to 2009 including some shared tasks
MWE workshops: 2003, 2004, 2006, 2007, 2008 and 2009.

Journals:

Computational Linguistics: All from 2000 to 2010 and some old articles
Computer Speech and Language: All from 2005 to 2010
Language Resources and Evaluation: All from 2005 to 2010
Natural Language Engineering: All from 1995 to 2010
Linguamatica: All from 2009 to 2010

Miscellaneous books, thesis and workshops

Some statistics:

> acl.bib cleaned, 900 entries and 900 unique entries
> books.bib cleaned, 36 entries and 36 unique entries
> cl.bib cleaned, 397 entries and 397 unique entries
> confs.bib cleaned, 253 entries and 253 unique entries
> const.bib cleaned, 81 entries and 81 unique entries
> const-abbr.bib cleaned, 81 entries and 81 unique entries
> csl.bib cleaned, 183 entries and 183 unique entries
> linguamatica.bib cleaned, 20 entries and 20 unique entries
> lre.bib cleaned, 120 entries and 120 unique entries
> lrec.bib cleaned, 1284 entries and 1284 unique entries
> nle.bib cleaned, 287 entries and 287 unique entries
> propor.bib cleaned, 135 entries and 135 unique entries
> ws.bib cleaned, 393 entries and 393 unique entries
TOTAL: 4011 entries and 4011 unique entries

Most of the references contain at least title, authors and page numbers, as well
as a cross-reference to the journal/conference in wich it appeared. Special 
characters were modified to corresponding LaTeX in order to allow simple ASCII
encoding and avoid cross-platform incompatibilities. Actually, the files are in
UTF-8 Linux \n format. I removed information about abstracts and keywords. The
sources of these references are many, but include: ACL anthology, ELSEVIER's 
Science Direct, Springer Link, MIT press website, MWE sourceforge website, 
Online edition of Linguamatica, Cambridge Press website.

In order to use these files, please make a link to the folder where you unpacked
this package and call it "latex". Then, in your .tex source file, use the 
following line in order to include all the references:

\bibliography{latex/bib/const,latex/bib/acl,latex/bib/books,latex/bib/cl,latex/bib/csl,latex/bib/linguamatica,latex/bib/lre,latex/bib/lrec,latex/bib/nle,latex/bib/propor,latex/bib/ws,latex/bib/confs}

Please notice that you should ALWAYS start with the constants file (const or 
const-abbr) and finish with the conferences definitions (confs). Since the
files use BibTeX's "crossref", you should pass an option to BibTeX:

bibtex --min-crossref 9999 <myfile.aux>

Using const-abbr instead of const changes the strings to the abbreviated version
that is shorter and thus saves space on the page. You can delete unused .bib
files from the list above, but remember to keep the first and last files in the
same position.

In future versions, I would like to include in this bibliography:
COLING
EMNLP
NAACL
EACL
MT Conferences (EAMT, SMT, MT marathon, MT summit)
Other workshops (NLL, SIGLEX, SIGSEM)
Older volumes and editions of LRE, CSL, ACL, LREC, CL, etc.
Up-to-date volumes and editions of all above (after June 2010).

If you would like to contribute (add/correct), plese don't hesitate to send me 
an email :-)

last modified: June 1, 2010
author: Carlos Ramisch
