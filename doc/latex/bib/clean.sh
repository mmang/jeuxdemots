#!/bin/bash
# This script cleans my bibliography
# Great! It has no comment. Congrats to myself...

BIBCLEAN="bibclean -max-width 0"
BIBLIST=" acl.bib books.bib cl.bib coling.bib confs.bib const.bib const-abbr.bib csl.bib eacl.bib ijcai.bib interspeech.bib linguamatica.bib lre.bib lrec.bib naacl.bib nle.bib propor.bib ws.bib misc.bib"
TESTLIST="acl.bib books.bib cl.bib coling.bib confs.bib                          csl.bib eacl.bib ijcai.bib interspeech.bib linguamatica.bib lre.bib lrec.bib nle.bib naacl.bib  propor.bib ws.bib misc.bib"

for bibfile in $BIBLIST; do
	$BIBCLEAN $bibfile 2> errors.txt > /dev/null
	if [ -s errors.txt ]; then
		echo "> $bibfile not cleaned because of the following errors:"
		cat errors.txt
	else
		cat $bibfile | $BIBCLEAN | 
		sed -e 's/ã/{\\~{a}}/g' -e 's/õ/{\\~{o}}/g' -e 's/ñ/{\\~{n}}/g' \
		-e 's/Ã/{\\~{A}}/g' -e 's/Õ/{\\~{O}}/g' -e 's/Ñ/{\\~{N}}/g' \
		-e "s/á/{\\\'{a}}/g" -e "s/é/{\\\'{e}}/g" -e "s/í/{\\\'{i}}/g" -e "s/ó/{\\\'{o}}/g" -e "s/ú/{\\\'{u}}/g" -e "s/ń/{\\\'{n}}/g" -e "s/ý/{\\\'{y}}/g" -e "s/ś/{\\\'{s}}/g" -e "s/ć/{\\\'{c}}/g" -e "s/ł/{\\\l{}}/g" \
		-e "s/Á/{\\\'{A}}/g" -e "s/É/{\\\'{E}}/g" -e "s/Í/{\\\'{I}}/g" -e "s/Ó/{\\\'{O}}/g" -e "s/Ú/{\\\'{U}}/g" -e "s/Ń/{\\\'{N}}/g" -e "s/Ý/{\\\'{Y}}/g" -e "s/Ś/{\\\'{S}}/g" -e "s/Ć/{\\\'{C}}/g" -e "s/Ł/{\\\L{}}/g" \
		-e "s/â/{\\\^{a}}/g" -e "s/ê/{\\\^{e}}/g" -e "s/î/{\\\^{i}}/g" -e "s/ô/{\\\^{o}}/g" -e "s/û/{\\\^{u}}/g" \
		-e "s/Â/{\\\^{A}}/g" -e "s/Ê/{\\\^{E}}/g" -e "s/Î/{\\\^{I}}/g" -e "s/Ô/{\\\^{O}}/g" -e "s/Û/{\\\^{U}}/g" \
		-e "s/à/{\\\`{a}}/g" -e "s/è/{\\\`{e}}/g" -e "s/ì/{\\\`{i}}/g" -e "s/ò/{\\\`{o}}/g" -e "s/ù/{\\\`{u}}/g" \
		-e "s/À/{\\\`{A}}/g" -e "s/È/{\\\`{E}}/g" -e "s/Ì/{\\\`{I}}/g" -e "s/Ò/{\\\`{O}}/g" -e "s/Ù/{\\\`{U}}/g" \
		-e 's/ä/{\\\"{a}}/g' -e 's/ë/{\\\"{e}}/g' -e 's/ï/{\\\"{i}}/g' -e 's/ö/{\\\"{o}}/g' -e 's/ü/{\\\"{u}}/g' \
		-e 's/Ä/{\\\"{A}}/g' -e 's/Ë/{\\\"{E}}/g' -e 's/Ï/{\\\"{I}}/g' -e 's/Ö/{\\\"{O}}/g' -e 's/Ü/{\\\"{U}}/g' \
		-e "s/ç/{\\\c{c}}/g" -e "s/ş/{\\\c{s}}/g" -e "s/ȩ/{\\\c{e}}/g" \
		-e "s/Ç/{\\\c{C}}/g" -e "s/Ş/{\\\c{S}}/g" -e "s/Ȩ/{\\\c{E}}/g" \
		-e "s/ğ/{\\\u{g}}/g" -e "s/ž/{\\\u{z}}/g" -e "s/š/{\\\u{s}}/g"  -e "s/ă/{\\\u{a}}/g" \
		-e "s/Š/{\\\u{S}}/g" -e "s/Ž/{\\\u{Z}}/g" -e "s/Š/{\\\u{S}}/g"  -e "s/Ă/{\\\u{A}}/g" \
		-e "s/č/{\\\v{c}}/g" -e "s/ě/{\\\v{e}}/g" -e "s/ř/{\\\v{r}}/g" -e "s/ň/{\\\v{n}}/g" \
        -e "s/Č/{\\\v{C}}/g" -e "s/Ě/{\\\v{E}}/g" -e "s/Ř/{\\\v{R}}/g" -e "s/Ň/{\\\v{N}}/g" \
		-e "s/“/``/g" -e "s/”/''/g" -e "s/[—–―]/---/g" -e "s/…/\ldots{}/g" -e "s/[’‘]/'/g" \
		-e "s/ø/\\\o{}/g" -e "s/å/{\\\aa{}}/g" \
		-e "s/Ø/\\\O{}/g" -e "s/Å/{\\\AA{}}/g" \
		-e 's/ß/{\\ss{}}/g' -e "s/æ/{\\\ae{}}/g" -e "s/œ/{\\\oe{}}/g" -e "s/ð/dh/g" \
		-e "s/£/\\\pounds/g" -e "s/€/EUR/g" -e "s/©/{\textcopyright{}}/g" -e 's/ę/e/g' > temp
		mv temp $bibfile
		iconv -f utf8 -t ascii $bibfile > /dev/null 2> tmp_error
		if [ -s tmp_error ]; then
		    echo "Attention: $bibfile has illegal characters."
		fi
		rm tmp_error		
		entries=`grep "@[a-zA-Z]*{" $bibfile | sed -e 's/,$/}/g' -e 's/@[a-zA-Z]*{/\\cite{/g' | wc -l`
		uniqueentries=`grep "@[a-zA-Z]*{" $bibfile | sed -e 's/,$/}/g' -e 's/@[a-zA-Z]*{/\\cite{/g' | awk '{print tolower($0);}' | sort | uniq | wc -l`		
		echo "> $bibfile cleaned, $entries entries and $uniqueentries unique entries"
	fi
done

# Use TESTLIST to avoid comparing const.bib and const-abbr.bib which obviously 
# contain duplicate entries
total=`cat $TESTLIST | grep "@" | sed 's/@[a-zA-Z]*//g' | wc -l`
totaluniq=`cat $TESTLIST | grep "@" | sed 's/@[a-zA-Z]*//g' | awk '{print tolower($0);}' | sort | uniq | wc -l`
echo "TOTAL: $total entries and $totaluniq unique entries" 

echo "Testing..."

for bibfile in $TESTLIST; do
    bibfilenoext=`echo "$bibfile" | sed "s/\.bib//g"`		
	echo "\\documentclass{article}" > ${bibfilenoext}.tex
	echo "\\\\begin{document}" >> ${bibfilenoext}.tex
	grep "@[a-zA-Z]*{" $bibfile | sed -e 's/,$/}/g' -e 's/@[a-zA-Z]*{/\\cite{/g' >> ${bibfilenoext}.tex
	echo "\\\\bibliographystyle{plain}" >> ${bibfilenoext}.tex
	echo "\\\\bibliography{const,$bibfilenoext,confs}" >> ${bibfilenoext}.tex
	echo "\\end{document}" >> ${bibfilenoext}.tex
	pdflatex -interaction batchmode ${bibfilenoext}.tex > /dev/null
	bibtex --min-crossref 9999 ${bibfilenoext}.aux  > /dev/null
	pdflatex -interaction batchmode ${bibfilenoext}.tex  > /dev/null
	pdflatex -interaction batchmode ${bibfilenoext}.tex  > /dev/null
	rm ${bibfilenoext}.tex
	echo "Test result in ${bibfilenoext}.pdf"
	rm -rf *.aux *.bbl *.blg *.log
done

rm errors.txt

